<!DOCTYPE html>
<html lang="en">
<head>
    <title>Log in</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="./VAADIN/themes/service99crm/favicon.ico">
    <link rel="icon" type="image/vnd.microsoft.icon" href="./VAADIN/themes/service99crm/favicon.ico">
    <style type="text/css">
        /* Override some defaults */
        html, body {
            background-color: #eee;
        }

        body {
            padding-top: 40px;
        }

        .container {
            width: 400px;
        }

        /* The white background content wrapper */
        .container > .content {
            background-color: #fff;
            padding: 20px;
            margin: 0 -20px;
            -webkit-border-radius: 10px 10px 10px 10px;
            -moz-border-radius: 10px 10px 10px 10px;
            border-radius: 10px 10px 10px 10px;
            -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .15);
            -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .15);
            box-shadow: 0 1px 2px rgba(0, 0, 0, .15);
        }

        .login-form {
            margin-left: 50px;
            margin-right: 50px;
        }

        legend {
            margin-right: -50px;
            font-weight: bold;
            color: #404040;
        }

    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="row">
            <div class="login-form">
                <h2>Service99</h2>
                <p>Введите логин/пароль</p>
                <form role="form" action="/login" method="post">
                    <div class="form-group">
                        <label for="login">Логин</label><input class="form-control" id="login" name="login"
                                                               autofocus="autofocus">
                    </div>
                    <div class="form-group">
                        <label for="password">Пароль</label><input class="form-control" type="password" id="password"
                                                                   name="password">
                    </div>
                    <div class="form-group pull-right">
                        <button class="btn btn-primary" type="submit">Войти</button>
                    </div>
                </form>
            <#if error.isPresent()>
                <p>Логин или пароль введены неверно, попробуйте ещё</p>
            </#if>
            </div>
        </div>
    </div>
</div>
</body>
</html>