package ru.service99.web.ui;

import ru.service99.domain.RepairOrder;

public interface OnRepairChoiceListener {
    void onChoice(RepairOrder order);
}
