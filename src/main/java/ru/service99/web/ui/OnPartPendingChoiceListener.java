package ru.service99.web.ui;

import ru.service99.domain.PartPending;

public interface OnPartPendingChoiceListener {
    void onChoice(PartPending partPending);
}
