package ru.service99.web.ui;

import ru.service99.domain.PartInstance;

import java.util.Set;

public interface OnPriceChangeListener {
    void onChange(Set<PartInstance> instances);
}
