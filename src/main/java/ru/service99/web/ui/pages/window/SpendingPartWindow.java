package ru.service99.web.ui.pages.window;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.*;
import ru.service99.SecurityUtils;
import ru.service99.domain.Authority;
import ru.service99.domain.DocumentOut;
import ru.service99.domain.PartInstance;
import ru.service99.domain.StatusDocument;
import ru.service99.service.DocumentService;
import ru.service99.service.ModelService;
import ru.service99.service.PartService;
import ru.service99.web.model.FreePart;
import ru.service99.web.ui.OnPartChoiceListener;
import ru.service99.web.ui.ViewUtils;

import java.text.SimpleDateFormat;
import java.util.*;

public class SpendingPartWindow extends Window {

    private static final String PART_NUMBER_COLUMN = "part.partNumber";
    private static final String NAME_COLUMN = "part.name";
    private static final String COUNT_COLUMN = "count";
    private static final String STOCK_COLUMN = "stock.name";
    private static final String IN_PRICE_COLUMN = "inPrice";
    private static final String OPT_PRICE_COLUMN = "optPrice";
    private static final String OUT_PRICE_COLUMN = "outPrice";
    private static final String COMMENT_COLUMN = "part.comment";

    private static final String PART_NUMBER_VISIBLE_COLUMN = ViewUtils.getMessage("table.partNumber");
    private static final String NAME_VISIBLE_COLUMN = ViewUtils.getMessage("table.name");
    private static final String COUNT_VISIBLE_COLUMN = ViewUtils.getMessage("table.count");
    private static final String STOCK_VISIBLE_COLUMN = ViewUtils.getMessage("table.stock");
    private static final String IN_PRICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.inPrice");
    private static final String OPT_PRICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.optPrice");
    private static final String OUT_PRICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.outPrice");
    private static final String COMMENT_VISIBLE_COLUMN = ViewUtils.getMessage("table.comment");

    private static final String[] fieldNamesWithInPrice = new String[]{PART_NUMBER_COLUMN, NAME_COLUMN, COUNT_COLUMN,
            STOCK_COLUMN, IN_PRICE_COLUMN, OPT_PRICE_COLUMN, OUT_PRICE_COLUMN, COMMENT_COLUMN};
    private static final String[] fieldNamesWithoutInPrice = new String[]{PART_NUMBER_COLUMN, NAME_COLUMN, COUNT_COLUMN,
            STOCK_COLUMN, OPT_PRICE_COLUMN, OUT_PRICE_COLUMN, COMMENT_COLUMN};

    private SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yy", Locale.getDefault());

    private DocumentService documentService;
    private ModelService modelService;
    private PartService partService;

    private DocumentOut documentOut;
    private Set<PartInstance> partInstances;

    private Table table = new Table();

    private TextField commentTextField;
    private VerticalLayout vertical;

    public SpendingPartWindow(DocumentService documentService, ModelService modelService, PartService partService) {
        setCaption(ViewUtils.getMessage("spendingPartsPage.window.title.new"));
        this.documentService = documentService;
        this.modelService = modelService;
        this.partService = partService;
        this.partInstances = new HashSet<>();
        init();
    }

    public SpendingPartWindow(DocumentService documentService, ModelService modelService, PartService partService,
                              DocumentOut documentOut) {
        this(documentService, modelService, partService);
        setCaption(ViewUtils.getMessage("spendingPartsPage.window.title.edit") + " " + documentOut.getId());
        this.documentOut = documentOut;
        this.partInstances.addAll(documentOut.getPartInstances());
        init();
    }

    public void init() {
        initLayout();
        if (documentOut != null) initTable(createContainer(new ArrayList<>(partInstances)));
        else initTable(createContainer(new ArrayList<>()));
    }

    private void initLayout() {
        center();
        setModal(true);
        setHeight("90%");
        setWidth("90%");

        vertical = new VerticalLayout();
        vertical.setSpacing(true);
        vertical.setMargin(true);
        vertical.setSizeFull();

        HorizontalLayout fieldLayout = new HorizontalLayout();
        fieldLayout.setSpacing(true);
        fieldLayout.setDefaultComponentAlignment(Alignment.BOTTOM_CENTER);

        Label authorDocumentLabel;
        Label dateDocumentLabel;
        commentTextField = new TextField(ViewUtils.getMessage("field.comment"));
        commentTextField.setSizeFull();

        Button addButton = new Button("+");
        addButton.addClickListener(clickEvent -> clickAddButton());

        Button removeButton = new Button("-");
        removeButton.addClickListener(clickEvent -> clickRemoveButton());

        Button saveButton = new Button(ViewUtils.getMessage("button.save"));
        saveButton.addClickListener(clickEvent -> saveDocument());

        if (documentOut != null) {
            dateDocumentLabel = new Label(ViewUtils.getMessage("field.date") + " " +
                    sdf.format(documentOut.getDate()));
            authorDocumentLabel = new Label(ViewUtils.getMessage("field.author") + " "
                    + documentOut.getAuthor().getName());
            setFields();
        } else {
            dateDocumentLabel = new Label(ViewUtils.getMessage("field.date") + " " +
                    sdf.format(new Date()));
            authorDocumentLabel = new Label(ViewUtils.getMessage("field.author") + " "
                    + SecurityUtils.getCurrentUser().getName());
        }

        fieldLayout.addComponent(dateDocumentLabel);
        fieldLayout.addComponent(authorDocumentLabel);
        fieldLayout.addComponent(addButton);
        fieldLayout.addComponent(removeButton);

        vertical.addComponent(fieldLayout);
        vertical.addComponent(commentTextField);
        vertical.addComponent(table);
        vertical.addComponent(saveButton);

        vertical.setExpandRatio(fieldLayout, 1);
        vertical.setExpandRatio(commentTextField, 1);
        vertical.setExpandRatio(table, 6);
        vertical.setExpandRatio(saveButton, 1);

        setContent(vertical);
    }

    private void setFields() {
        if (documentOut.getStatus() == StatusDocument.Завершено) {
            vertical.setEnabled(false);
        } else {
            vertical.setEnabled(true);
        }
        commentTextField.setValue(documentOut.getComment());
    }

    private void clickAddButton() {
        for (Window window : getUI().getWindows()) if (window instanceof SelectPartInstanceWindow) return;
        if (SecurityUtils.getCurrentUser().getAllowedStocks().isEmpty()) {
            Notification.show(ViewUtils.getMessage("notifications.accessDenied"), Notification.Type.ERROR_MESSAGE);
            return;
        }
        SelectPartInstanceWindow window = new SelectPartInstanceWindow(modelService, partService, null, partInstances);
        window.setPartChoiceListener(new MyOnPartChoiceListener());
        getUI().addWindow(window);
    }

    private void clickRemoveButton() {
        FreePart selected = (FreePart) table.getValue();
        if (selected == null) return;
        for (PartInstance instance : partInstances) {
            if (Objects.equals(instance.getPart().getId(), selected.getPart().getId())) {
                partInstances.remove(instance);
                break;
            }
        }
        initTable(createContainer(new ArrayList<>(partInstances)));
    }

    private void saveDocument() {
        if (!SecurityUtils.hasAuthority(Authority.списание_деталей)) {
            Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                    Notification.Type.ERROR_MESSAGE);
            return;
        }
        if (documentOut == null) documentOut = new DocumentOut();
        documentOut.setComment(commentTextField.getValue());
        documentOut.setPartInstances(partInstances);
        try {
            DocumentOut savedDocument = documentService.saveDocumentOut(documentOut);
            documentOut = documentService.findDocumentOutById(savedDocument.getId());
            partInstances = new HashSet<>(documentOut.getPartInstances());
            //Notification.show(ViewUtils.getMessage("notifications.saved"), Notification.Type.TRAY_NOTIFICATION);
            setFields();
        } catch (Exception e) {
            StringBuilder builder = new StringBuilder(ViewUtils.getMessage("notifications.notSaved"));
            if (e.getLocalizedMessage() != null) {
                builder.append("\n");
                builder.append(e.getLocalizedMessage());
            } else {
                for (StackTraceElement element : e.getStackTrace()) {
                    builder.append("\n");
                    builder.append(element);
                }
            }
            Notification.show(builder.toString(), Notification.Type.ERROR_MESSAGE);
        }
    }

    private void initTable(BeanItemContainer<FreePart> container) {
        table.setContainerDataSource(container);
        table.setSizeFull();
        table.setColumnCollapsingAllowed(true);
        table.setColumnCollapsed(COMMENT_COLUMN, true);
        if (SecurityUtils.hasAuthority(Authority.видеть_входную_цену)) {
            table.setVisibleColumns(fieldNamesWithInPrice);
            table.setColumnHeader(IN_PRICE_COLUMN, IN_PRICE_VISIBLE_COLUMN);
        } else {
            table.setVisibleColumns(fieldNamesWithoutInPrice);
        }
        table.setColumnHeader(PART_NUMBER_COLUMN, PART_NUMBER_VISIBLE_COLUMN);
        table.setColumnHeader(NAME_COLUMN, NAME_VISIBLE_COLUMN);
        table.setColumnHeader(COUNT_COLUMN, COUNT_VISIBLE_COLUMN);
        table.setColumnHeader(STOCK_COLUMN, STOCK_VISIBLE_COLUMN);
        table.setColumnHeader(IN_PRICE_COLUMN, IN_PRICE_VISIBLE_COLUMN);
        table.setColumnHeader(OPT_PRICE_COLUMN, OPT_PRICE_VISIBLE_COLUMN);
        table.setColumnHeader(OUT_PRICE_COLUMN, OUT_PRICE_VISIBLE_COLUMN);
        table.setColumnHeader(COMMENT_COLUMN, COMMENT_VISIBLE_COLUMN);
        table.setSelectable(true);
        table.setImmediate(true);
    }

    private BeanItemContainer<FreePart> createContainer(List<PartInstance> instances) {
        BeanItemContainer<FreePart> container = new BeanItemContainer<>(FreePart.class);
        container.addNestedContainerProperty(PART_NUMBER_COLUMN);
        container.addNestedContainerProperty(NAME_COLUMN);
        container.addNestedContainerProperty(STOCK_COLUMN);
        container.addNestedContainerProperty(IN_PRICE_COLUMN);
        container.addNestedContainerProperty(OPT_PRICE_COLUMN);
        container.addNestedContainerProperty(OUT_PRICE_COLUMN);
        container.addNestedContainerProperty(COMMENT_COLUMN);
        List<FreePart> freeParts = FreePart.instancesToFreeParts(instances);
        container.addAll(freeParts);
        container.sort(new Object[]{NAME_COLUMN, COUNT_COLUMN}, new boolean[]{true, true});
        return container;
    }

    private class MyOnPartChoiceListener implements OnPartChoiceListener {
        @Override
        public void onChoice(Set<PartInstance> instances) {
            partInstances.addAll(instances);
            initTable(createContainer(new ArrayList<>(partInstances)));
        }
    }
}
