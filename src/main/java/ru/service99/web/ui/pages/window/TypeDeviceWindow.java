package ru.service99.web.ui.pages.window;

import com.vaadin.ui.*;
import ru.service99.SecurityUtils;
import ru.service99.domain.Authority;
import ru.service99.domain.TypeDevice;
import ru.service99.service.ModelService;
import ru.service99.web.ui.ViewUtils;

public class TypeDeviceWindow extends Window {

    private ModelService modelService;

    private TypeDevice typeDevice;

    public TypeDeviceWindow(ModelService modelService) {
        setCaption(ViewUtils.getMessage("typeDevicesPage.window.title.new"));
        this.modelService = modelService;
        init();
    }

    public TypeDeviceWindow(ModelService modelService, TypeDevice typeDevice) {
        this(modelService);
        setCaption(ViewUtils.getMessage("typeDevicesPage.window.title.edit") + " " + typeDevice.getId());
        this.typeDevice = typeDevice;
        init();
    }

    private void init() {
        center();
        setModal(true);

        VerticalLayout vertical = new VerticalLayout();
        vertical.setSpacing(true);
        vertical.setMargin(true);
        vertical.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

        HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.setSpacing(true);

        TextField nameTextField = new TextField(ViewUtils.getMessage("field.name"));
        nameTextField.setWidth("330px");

        if (typeDevice != null) nameTextField.setValue(typeDevice.getName());

        Button saveButton = new Button(ViewUtils.getMessage("button.save"));
        saveButton.addClickListener(clickEvent -> {
            if (!SecurityUtils.hasAuthority(Authority.редактирование_типа_устройства)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
            if (typeDevice == null) typeDevice = new TypeDevice();
            typeDevice.setName(nameTextField.getValue());
            try {
                modelService.saveTypeDevice(typeDevice);
                Notification.show(ViewUtils.getMessage("notifications.saved"), Notification.Type.TRAY_NOTIFICATION);
                close();
            } catch (Exception e) {
                StringBuilder builder = new StringBuilder(ViewUtils.getMessage("notifications.notSaved"));
                if (e.getLocalizedMessage() != null) {
                    builder.append("\n");
                    builder.append(e.getLocalizedMessage());
                } else {
                    for (StackTraceElement element : e.getStackTrace()) {
                        builder.append("\n");
                        builder.append(element);
                    }
                }
                Notification.show(builder.toString(), Notification.Type.ERROR_MESSAGE);
            }
        });

        Button removeButton = new Button(ViewUtils.getMessage("button.remove"));
        if (typeDevice == null) removeButton.setEnabled(false);
        removeButton.addClickListener(event -> {
            if (!SecurityUtils.hasAuthority(Authority.редактирование_фирмы)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
            ConfirmWindow window = new ConfirmWindow(ViewUtils.getMessage("confirm.message.remove"));
            window.setOkClickListener(() -> {
                typeDevice.setIsRemoved(true);
                try {
                    modelService.saveTypeDevice(typeDevice);
                    close();
                } catch (Exception e) {
                    typeDevice.setIsRemoved(false);
                    StringBuilder builder = new StringBuilder(ViewUtils.getMessage("notifications.notSaved"));
                    if (e.getLocalizedMessage() != null) {
                        builder.append("\n");
                        builder.append(e.getLocalizedMessage());
                    } else {
                        for (StackTraceElement element : e.getStackTrace()) {
                            builder.append("\n");
                            builder.append(element);
                        }
                    }
                    Notification.show(builder.toString(), Notification.Type.ERROR_MESSAGE);
                }
            });
            getUI().addWindow(window);
        });

        Button cancelButton = new Button(ViewUtils.getMessage("button.cancel"));
        cancelButton.addClickListener(event -> close());

        buttonsLayout.addComponent(cancelButton);
        buttonsLayout.addComponent(removeButton);
        buttonsLayout.addComponent(saveButton);

        vertical.addComponent(nameTextField);
        vertical.addComponent(buttonsLayout);

        setContent(vertical);
    }
}
