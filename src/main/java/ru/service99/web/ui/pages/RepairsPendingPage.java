package ru.service99.web.ui.pages;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ShortcutAction;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.service99.SecurityUtils;
import ru.service99.domain.PartPending;
import ru.service99.domain.RepairOrder;
import ru.service99.domain.StatusRepairOrder;
import ru.service99.domain.User;
import ru.service99.service.*;
import ru.service99.service.criteria.PartSearchCriteria;
import ru.service99.service.criteria.RepairOrderSearchCriteria;
import ru.service99.web.model.RepairPending;
import ru.service99.web.ui.ViewUtils;
import ru.service99.web.ui.pages.window.RepairWindow;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

@Component
@Scope("prototype")
@SpringView(name = RepairsPendingPage.NAME)
public class RepairsPendingPage extends BasePage {

    public static final String NAME = "repairsPending";

    @Autowired
    private RepairOrderService repairOrderService;
    @Autowired
    private PartService partService;
    @Autowired
    private UserService userService;
    @Autowired
    private ModelService modelService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private FirmService firmService;

    private Table table = new Table();

    private ComboBox masterComboBox;

    private HashMap<Long, User> masterMap = new HashMap<>();

    private static final String ID_COLUMN = "order.id";
    private static final String MODEL_COLUMN = "order.model.name";
    private static final String MASTER_COLUMN = "order.master.name";
    private static final String PARTNUMBER_COLUMN = "partPending.part.partNumber";
    private static final String PART_NAME_COLUMN = "partPending.part.name";
    private static final String NEEDED_COUNT_COLUMN = "partPending.neededCount";
    private static final String FREE_COUNT_COLUMN = "freeCount";

    private static final String ID_VISIBLE_COLUMN = ViewUtils.getMessage("table.id");
    private static final String MODEL_VISIBLE_COLUMN = ViewUtils.getMessage("table.model");
    private static final String MASTER_VISIBLE_COLUMN = ViewUtils.getMessage("table.master");
    private static final String PARTNUMBER_VISIBLE_COLUMN = ViewUtils.getMessage("table.partNumber");
    private static final String PART_NAME_VISIBLE_COLUMN = ViewUtils.getMessage("table.partName");
    private static final String NEEDED_COUNT_VISIBLE_COLUMN = ViewUtils.getMessage("table.neededCount");
    private static final String FREE_COUNT_VISIBLE_COLUMN = ViewUtils.getMessage("table.freeCount");

    private static final String[] fieldNames = new String[]{ID_COLUMN, MODEL_COLUMN, MASTER_COLUMN,
            PARTNUMBER_COLUMN, PART_NAME_COLUMN, NEEDED_COUNT_COLUMN, FREE_COUNT_COLUMN};

    @PostConstruct
    public void init() {
        initLayout();
        applyFilter();
    }

    private void initLayout() {
        HorizontalLayout filterContainer = new HorizontalLayout();
        filterContainer.setSpacing(true);
        filterContainer.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);

        masterComboBox = createMasterComboBox();
        masterComboBox.setWidth("150px");
        masterComboBox.select(masterMap.get(SecurityUtils.getCurrentUser().getId()));

        Button applyButton = new Button(ViewUtils.getMessage("button.select"));
        applyButton.addClickListener(clickEvent -> applyFilter());
        applyButton.setSizeFull();
        applyButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);

        filterContainer.addComponent(masterComboBox);
        filterContainer.addComponent(applyButton);

        baseLayout.addComponent(filterContainer);
        baseLayout.addComponent(table);

        baseLayout.setExpandRatio(filterContainer, 1);
        baseLayout.setExpandRatio(table, 10);
    }

    private ComboBox createMasterComboBox() {
        BeanItemContainer<User> container = new BeanItemContainer<>(User.class);
        List<User> users = userService.findAll();
        users.forEach(user -> masterMap.put(user.getId(), user));
        container.addAll(users);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.master"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        return select;
    }

    private void applyFilter() {
        TreeSet<RepairPending> repairsPending = new TreeSet<>();
        RepairOrderSearchCriteria repairsCriteria = new RepairOrderSearchCriteria();
        repairsCriteria.setStatus(StatusRepairOrder.Ож_детали);
        repairsCriteria.setMaster((User) masterComboBox.getValue());
        List<RepairOrder> repairOrders = repairOrderService.findByCriteria(repairsCriteria, null).getContent();
        for (RepairOrder ro : repairOrders) {
            if (ro.getPartsPending().isEmpty()) continue;
            for (PartPending pp : ro.getPartsPending()) {
                PartSearchCriteria partCriteria = new PartSearchCriteria();
                partCriteria.setPartId(pp.getPart().getId());
                long freeCount = partService.getCountFreeInstancesByCriteria(partCriteria);
                if (freeCount > 0) {
                    RepairPending repairPending = new RepairPending();
                    repairPending.setOrder(ro);
                    repairPending.setPartPending(pp);
                    repairPending.setFreeCount(freeCount);
                    repairsPending.add(repairPending);
                }
            }
        }
        initTable(createContainer(new ArrayList<>(repairsPending)));
    }

    private void initTable(BeanItemContainer<RepairPending> container) {
        table.setContainerDataSource(container);
        table.setSizeFull();
        table.setColumnCollapsingAllowed(true);
        table.setVisibleColumns(fieldNames);
        table.setColumnHeader(ID_COLUMN, ID_VISIBLE_COLUMN);
        table.setColumnHeader(MODEL_COLUMN, MODEL_VISIBLE_COLUMN);
        table.setColumnHeader(MASTER_COLUMN, MASTER_VISIBLE_COLUMN);
        table.setColumnHeader(PARTNUMBER_COLUMN, PARTNUMBER_VISIBLE_COLUMN);
        table.setColumnHeader(PART_NAME_COLUMN, PART_NAME_VISIBLE_COLUMN);
        table.setColumnHeader(NEEDED_COUNT_COLUMN, NEEDED_COUNT_VISIBLE_COLUMN);
        table.setColumnHeader(FREE_COUNT_COLUMN, FREE_COUNT_VISIBLE_COLUMN);
        table.setSelectable(true);
        table.setImmediate(true);
        table.addItemClickListener(event -> {
            if (!event.isDoubleClick()) return;
            for (Window window : getUI().getWindows()) if (window instanceof RepairWindow) return;
            RepairPending order = (RepairPending) event.getItemId();
            RepairWindow window = new RepairWindow(
                    repairOrderService,
                    modelService,
                    customerService,
                    firmService,
                    userService,
                    partService,
                    order.getOrder());
            window.addCloseListener(new WindowCloseListener());
            getUI().addWindow(window);
        });
    }

    private BeanItemContainer<RepairPending> createContainer(List<RepairPending> repairOrders) {
        BeanItemContainer<RepairPending> container = new BeanItemContainer<>(RepairPending.class);
        container.addAll(repairOrders);
        container.addNestedContainerProperty(ID_COLUMN);
        container.addNestedContainerProperty(MODEL_COLUMN);
        container.addNestedContainerProperty(MASTER_COLUMN);
        container.addNestedContainerProperty(PARTNUMBER_COLUMN);
        container.addNestedContainerProperty(PART_NAME_COLUMN);
        container.addNestedContainerProperty(NEEDED_COUNT_COLUMN);
        return container;
    }

    private class WindowCloseListener implements Window.CloseListener {
        @Override
        public void windowClose(Window.CloseEvent closeEvent) {
            //applyFilter();
        }
    }
}
