package ru.service99.web.ui.pages.window;

import com.vaadin.ui.*;
import ru.service99.SecurityUtils;
import ru.service99.domain.Authority;
import ru.service99.domain.Customer;
import ru.service99.service.CustomerService;
import ru.service99.web.ui.ViewUtils;

public class CustomerWindow extends Window {

    private CustomerService customerService;

    private Customer customer;

    public CustomerWindow(CustomerService customerService) {
        setCaption(ViewUtils.getMessage("customersPage.window.title.new"));
        this.customerService = customerService;
        init();
    }

    public CustomerWindow(CustomerService customerService, Customer customer) {
        this(customerService);
        setCaption(ViewUtils.getMessage("customersPage.window.title.edit") + " " + customer.getId());
        this.customer = customer;
        init();
    }

    private void init() {
        center();
        setModal(true);
        setWidth("500px");

        VerticalLayout vertical = new VerticalLayout();
        vertical.setSpacing(true);
        vertical.setMargin(true);
        vertical.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

        HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.setSpacing(true);

        TextField nameTextField = new TextField(ViewUtils.getMessage("field.userName"));
        TextField phoneTextField = new TextField(ViewUtils.getMessage("field.phone"));
        TextField addressTextField = new TextField(ViewUtils.getMessage("field.address"));
        TextField commentTextField = new TextField(ViewUtils.getMessage("field.comment"));

        nameTextField.setSizeFull();
        phoneTextField.setSizeFull();
        addressTextField.setSizeFull();
        commentTextField.setSizeFull();

        if (customer != null) {
            nameTextField.setValue(customer.getName());
            phoneTextField.setValue(customer.getPhoneNumber() != null ? customer.getPhoneNumber() : "");
            addressTextField.setValue(customer.getAddress() != null ? customer.getAddress() : "");
            commentTextField.setValue(customer.getComment() != null ? customer.getComment() : "");
        }

        Button saveButton = new Button(ViewUtils.getMessage("button.save"));
        saveButton.addClickListener(clickEvent -> {
            if (!SecurityUtils.hasAuthority(Authority.редактирование_клиента)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
            if (customer == null) customer = new Customer();
            customer.setName(nameTextField.getValue());
            customer.setPhoneNumber(phoneTextField.getValue());
            customer.setAddress(addressTextField.getValue());
            customer.setComment(commentTextField.getValue());
            try {
                customerService.save(customer);
                Notification.show(ViewUtils.getMessage("notifications.saved"), Notification.Type.TRAY_NOTIFICATION);
                close();
            } catch (Exception e) {
                StringBuilder builder = new StringBuilder(ViewUtils.getMessage("notifications.notSaved"));
                if (e.getLocalizedMessage() != null) {
                    builder.append("\n");
                    builder.append(e.getLocalizedMessage());
                } else {
                    for (StackTraceElement element : e.getStackTrace()) {
                        builder.append("\n");
                        builder.append(element);
                    }
                }
                Notification.show(builder.toString(), Notification.Type.ERROR_MESSAGE);
            }
        });

        Button cancelButton = new Button(ViewUtils.getMessage("button.cancel"));
        cancelButton.addClickListener(event -> close());

        buttonsLayout.addComponent(cancelButton);
        buttonsLayout.addComponent(saveButton);

        vertical.addComponent(nameTextField);
        vertical.addComponent(phoneTextField);
        vertical.addComponent(addressTextField);
        vertical.addComponent(commentTextField);
        vertical.addComponent(buttonsLayout);

        setContent(vertical);
    }
}
