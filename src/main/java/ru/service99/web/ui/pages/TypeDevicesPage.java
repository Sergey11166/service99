package ru.service99.web.ui.pages;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.Window;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.service99.SecurityUtils;
import ru.service99.domain.Authority;
import ru.service99.domain.TypeDevice;
import ru.service99.service.ModelService;
import ru.service99.web.ui.ViewUtils;
import ru.service99.web.ui.pages.window.TypeDeviceWindow;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
@Scope("prototype")
@SpringView(name = TypeDevicesPage.NAME)
public class TypeDevicesPage extends BasePage {

    public static final String NAME = "type-devices";

    @Autowired
    private ModelService modelService;

    private Table table = new Table();

    private static final String ID_COLUMN = "id";
    private static final String NAME_COLUMN = "name";

    private static final String ID_VISIBLE_COLUMN = ViewUtils.getMessage("table.id");
    private static final String NAME_VISIBLE_COLUMN = ViewUtils.getMessage("table.name");

    private static final String[] fieldNames = new String[]{ID_COLUMN, NAME_COLUMN};

    @PostConstruct
    public void init() {
        initLayout();
        initTable(createContainer(modelService.findAllTypeDevices()));
    }

    private void initLayout() {
        Button addButton = new Button(ViewUtils.getMessage("button.new"));
        addButton.addClickListener(event -> {
            if (!SecurityUtils.hasAuthority(Authority.редактирование_типа_устройства)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
            for (Window window : getUI().getWindows()) if (window instanceof TypeDeviceWindow) return;
            TypeDeviceWindow window = new TypeDeviceWindow(modelService);
            window.addCloseListener(new MyCloseListener());
            getUI().addWindow(window);
        });

        baseLayout.addComponent(table);
        baseLayout.addComponent(addButton);

        baseLayout.setExpandRatio(table, 8);
        baseLayout.setExpandRatio(addButton, 1);
    }

    private void initTable(BeanItemContainer<TypeDevice> container) {
        table.setContainerDataSource(container);
        table.setHeight("100%");
        table.setVisibleColumns(fieldNames);
        table.setColumnHeader(ID_COLUMN, ID_VISIBLE_COLUMN);
        table.setColumnHeader(NAME_COLUMN, NAME_VISIBLE_COLUMN);
        table.setSelectable(true);
        table.setImmediate(true);
        table.addItemClickListener(event -> {
            if (!event.isDoubleClick()) return;
            for (Window window : getUI().getWindows()) if (window instanceof TypeDeviceWindow) return;
            TypeDevice typeDevice = (TypeDevice) event.getItemId();
            TypeDeviceWindow window = new TypeDeviceWindow(modelService, typeDevice);
            window.addCloseListener(new MyCloseListener());
            getUI().addWindow(window);
        });
    }

    private BeanItemContainer<TypeDevice> createContainer(List<TypeDevice> typeDevices) {
        BeanItemContainer<TypeDevice> container = new BeanItemContainer<>(TypeDevice.class);
        container.addAll(typeDevices);
        return container;
    }

    private class MyCloseListener implements Window.CloseListener {
        @Override
        public void windowClose(Window.CloseEvent closeEvent) {
            //initTable(createContainer(modelService.findAllTypeDevices()));
        }
    }
}
