package ru.service99.web.ui.pages;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import ru.service99.SecurityUtils;
import ru.service99.domain.Authority;
import ru.service99.domain.DocumentOut;
import ru.service99.domain.User;
import ru.service99.service.DocumentService;
import ru.service99.service.ModelService;
import ru.service99.service.PartService;
import ru.service99.service.UserService;
import ru.service99.service.criteria.DocumentSearchCriteria;
import ru.service99.web.ui.ViewUtils;
import ru.service99.web.ui.pages.window.SpendingPartWindow;

import javax.annotation.PostConstruct;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@Component
@Scope("prototype")
@SpringView(name = SpendingPartsPage.NAME)
public class SpendingPartsPage extends BasePage {

    public static final String NAME = "spending-parts";

    private static final String ID_COLUMN = "id";
    private static final String DATE_COLUMN = "date";
    private static final String AUTHOR_COLUMN = "author.name";
    private static final String STATUS_COLUMN = "status";
    private static final String COMMENT_COLUMN = "comment";

    private static final String ID_VISIBLE_COLUMN = ViewUtils.getMessage("table.id");
    private static final String DATE_VISIBLE_COLUMN = ViewUtils.getMessage("table.date");
    private static final String AUTHOR_VISIBLE_COLUMN = ViewUtils.getMessage("table.author");
    private static final String STATUS_VISIBLE_COLUMN = ViewUtils.getMessage("table.status");
    private static final String COMMENT_VISIBLE_COLUMN = ViewUtils.getMessage("table.comment");

    private static final String[] fieldNames = new String[]{ID_COLUMN, DATE_COLUMN, AUTHOR_COLUMN,
            STATUS_COLUMN, COMMENT_COLUMN};

    @Autowired
    DocumentService documentService;
    @Autowired
    UserService userService;
    @Autowired
    ModelService modelService;
    @Autowired
    PartService partService;

    private Table table = new Table();

    private DateField dateField1;
    private DateField dateField2;

    private ComboBox authorComboBox;

    @PostConstruct
    public void init() {
        initLayout();
        applyFilter();
    }

    private void initLayout() {
        HorizontalLayout horizontal = new HorizontalLayout();
        horizontal.setSpacing(true);
        horizontal.setSizeFull();

        VerticalLayout filterLayout = new VerticalLayout();
        filterLayout.setSpacing(true);

        VerticalLayout tableLayout = new VerticalLayout();
        tableLayout.setSpacing(true);
        tableLayout.setSizeFull();

        dateField1 = new DateField(ViewUtils.getMessage("field.date1"));
        dateField2 = new DateField(ViewUtils.getMessage("field.date2"));

        authorComboBox = createAuthorComboBox();

        Button applyButton = new Button(ViewUtils.getMessage("button.search"));
        applyButton.addClickListener(clickEvent -> applyFilter());

        Button addButton = new Button(ViewUtils.getMessage("button.new"));
        addButton.addClickListener(event -> {
            if (!SecurityUtils.hasAuthority(Authority.списание_деталей)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
            for (Window window : getUI().getWindows()) if (window instanceof SpendingPartWindow) return;
            SpendingPartWindow window = new SpendingPartWindow(documentService, modelService, partService);
            window.addCloseListener(new MyCloseListener());
            getUI().addWindow(window);
        });

        filterLayout.addComponent(dateField1);
        filterLayout.addComponent(dateField2);
        filterLayout.addComponent(authorComboBox);
        filterLayout.addComponent(applyButton);

        tableLayout.addComponent(table);
        tableLayout.addComponent(addButton);
        tableLayout.setExpandRatio(table, 8);
        tableLayout.setExpandRatio(addButton, 1);

        horizontal.addComponent(filterLayout);
        horizontal.addComponent(tableLayout);
        horizontal.setExpandRatio(filterLayout, 1);
        horizontal.setExpandRatio(tableLayout, 5);

        baseLayout.addComponent(horizontal);
        baseLayout.setExpandRatio(horizontal, 8);
    }

    private ComboBox createAuthorComboBox() {
        BeanItemContainer<User> container = new BeanItemContainer<>(User.class);
        List<User> users = userService.findAll();
        container.addAll(users);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.author"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        return select;
    }

    private void initTable(BeanItemContainer<DocumentOut> container) {
        table.setContainerDataSource(container);
        table.setSizeFull();
        table.setColumnCollapsingAllowed(true);
        table.setVisibleColumns(fieldNames);
        table.setColumnHeader(ID_COLUMN, ID_VISIBLE_COLUMN);
        table.setColumnHeader(DATE_COLUMN, DATE_VISIBLE_COLUMN);
        table.setColumnHeader(AUTHOR_COLUMN, AUTHOR_VISIBLE_COLUMN);
        table.setColumnHeader(STATUS_COLUMN, STATUS_VISIBLE_COLUMN);
        table.setColumnHeader(COMMENT_COLUMN, COMMENT_VISIBLE_COLUMN);
        table.setSelectable(true);
        table.setImmediate(true);
        table.addItemClickListener(event -> {
            if (!event.isDoubleClick()) return;
            for (Window window : getUI().getWindows()) if (window instanceof SpendingPartWindow) return;
            DocumentOut documentOut = (DocumentOut) event.getItemId();
            SpendingPartWindow window = new SpendingPartWindow(documentService, modelService, partService, documentOut);
            window.addCloseListener(new MyCloseListener());
            getUI().addWindow(window);
        });
    }

    private void applyFilter() {
        DocumentSearchCriteria criteria = new DocumentSearchCriteria();

        criteria.setDate1(dateField1.getValue());
        criteria.setAuthor((User) authorComboBox.getValue());

        Date date2 = dateField2.getValue();
        if (date2 != null) {
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date2);
            calendar.add(Calendar.DATE, 1);
            criteria.setDate2(calendar.getTime());
        }

        initTable(createContainer(documentService.findDocumentOutByCriteria(criteria,
                new PageRequest(0, 100, new Sort(Sort.Direction.DESC, "id")))
                .getContent()));
    }

    private BeanItemContainer<DocumentOut> createContainer(List<DocumentOut> documents) {
        BeanItemContainer<DocumentOut> container = new BeanItemContainer<>(DocumentOut.class);
        container.addNestedContainerProperty(AUTHOR_COLUMN);
        container.addAll(documents);
        return container;
    }

    private class MyCloseListener implements Window.CloseListener {
        @Override
        public void windowClose(Window.CloseEvent closeEvent) {
            //applyFilter();
        }
    }
}
