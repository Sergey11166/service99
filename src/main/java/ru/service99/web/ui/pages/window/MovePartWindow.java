package ru.service99.web.ui.pages.window;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.*;
import ru.service99.SecurityUtils;
import ru.service99.domain.*;
import ru.service99.service.DocumentService;
import ru.service99.service.ModelService;
import ru.service99.service.PartService;
import ru.service99.web.model.FreePart;
import ru.service99.web.ui.OnPartChoiceListener;
import ru.service99.web.ui.ViewUtils;

import java.text.SimpleDateFormat;
import java.util.*;

public class MovePartWindow extends Window {

    private static final String PART_NUMBER_COLUMN = "part.partNumber";
    private static final String NAME_COLUMN = "part.name";
    private static final String COUNT_COLUMN = "count";
    private static final String IN_PRICE_COLUMN = "inPrice";
    private static final String OPT_PRICE_COLUMN = "optPrice";
    private static final String OUT_PRICE_COLUMN = "outPrice";
    private static final String COMMENT_COLUMN = "part.comment";

    private static final String PART_NUMBER_VISIBLE_COLUMN = ViewUtils.getMessage("table.partNumber");
    private static final String NAME_VISIBLE_COLUMN = ViewUtils.getMessage("table.name");
    private static final String COUNT_VISIBLE_COLUMN = ViewUtils.getMessage("table.count");
    private static final String IN_PRICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.inPrice");
    private static final String OPT_PRICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.optPrice");
    private static final String OUT_PRICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.outPrice");
    private static final String COMMENT_VISIBLE_COLUMN = ViewUtils.getMessage("table.comment");

    private static final String[] fieldNamesWithInPrice = new String[]{PART_NUMBER_COLUMN, NAME_COLUMN, COUNT_COLUMN,
            IN_PRICE_COLUMN, OPT_PRICE_COLUMN, OUT_PRICE_COLUMN, COMMENT_COLUMN};
    private static final String[] fieldNamesWithoutInPrice = new String[]{PART_NUMBER_COLUMN, NAME_COLUMN, COUNT_COLUMN,
            OPT_PRICE_COLUMN, OUT_PRICE_COLUMN, COMMENT_COLUMN};

    private SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yy", Locale.getDefault());

    private DocumentService documentService;
    private ModelService modelService;
    private PartService partService;

    private DocumentMove documentMove;
    private Set<PartInstance> partInstances;

    private Table table = new Table();

    private ComboBox stockFromComboBox;
    private ComboBox stockToComboBox;
    private ComboBox statusComboBox;
    private TextField commentTextField;
    private Button saveButton;
    private Button addButton;
    private Button removeButton;
    private Button deleteButton;

    private Date lastChangeDate;

    private HashMap<Long, Stock> stockFromMap = new HashMap<>();
    private HashMap<Long, Stock> stockToMap = new HashMap<>();

    private Long currentStockId;

    public MovePartWindow(DocumentService documentService, ModelService modelService, PartService partService) {
        setCaption(ViewUtils.getMessage("movesPartsPage.window.title.new"));
        this.documentService = documentService;
        this.modelService = modelService;
        this.partService = partService;
        this.partInstances = new HashSet<>();
        init();
    }

    public MovePartWindow(DocumentService documentService, ModelService modelService, PartService partService,
                          DocumentMove documentMove) {
        this(documentService, modelService, partService);
        setCaption(ViewUtils.getMessage("movesPartsPage.window.title.edit") + " " + documentMove.getId());
        this.documentMove = documentMove;
        this.partInstances.addAll(documentMove.getPartInstances());
        this.lastChangeDate = documentService.findLastRevisionDocumentMoveById(documentMove.getId())
                .getRevisionDate().toDate();
        init();
    }

    public void init() {
        initLayout();
        initTable(createContainer(new ArrayList<>(partInstances)));
    }

    private void initLayout() {
        center();
        setModal(true);
        setHeight("90%");
        setWidth("90%");

        VerticalLayout vertical = new VerticalLayout();
        vertical.setSpacing(true);
        vertical.setMargin(true);
        vertical.setSizeFull();

        HorizontalLayout fieldLayout = new HorizontalLayout();
        fieldLayout.setSpacing(true);
        fieldLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);

        statusComboBox = createStatusComboBox();
        stockFromComboBox = createStockFromComboBox();
        stockToComboBox = createStockToComboBox();

        Label authorDocumentLabel = new Label();
        Label dateDocumentLabel = new Label();

        commentTextField = new TextField(ViewUtils.getMessage("field.comment"));
        commentTextField.setSizeFull();

        addButton = new Button("+");
        addButton.addClickListener(clickEvent -> clickAddButton());

        removeButton = new Button("-");
        removeButton.addClickListener(clickEvent -> clickRemoveButton());

        saveButton = new Button(ViewUtils.getMessage("button.save"));
        saveButton.addClickListener(clickEvent -> saveButtonClick());

        deleteButton = new Button(ViewUtils.getMessage("button.remove"));
        deleteButton.addClickListener(clickEvent -> clickDeleteButton());
        deleteButton.setEnabled(false);

        fieldLayout.addComponent(dateDocumentLabel);
        fieldLayout.addComponent(authorDocumentLabel);
        fieldLayout.addComponent(stockFromComboBox);
        fieldLayout.addComponent(stockToComboBox);
        fieldLayout.addComponent(statusComboBox);
        fieldLayout.addComponent(addButton);
        fieldLayout.addComponent(removeButton);
        fieldLayout.addComponent(deleteButton);

        vertical.addComponent(fieldLayout);
        vertical.addComponent(commentTextField);
        vertical.addComponent(table);
        vertical.addComponent(saveButton);

        vertical.setExpandRatio(fieldLayout, 1);
        vertical.setExpandRatio(commentTextField, 1);
        vertical.setExpandRatio(table, 6);
        vertical.setExpandRatio(saveButton, 1);

        setContent(vertical);

        if (documentMove != null) {
            dateDocumentLabel.setValue(ViewUtils.getMessage("field.date") + " " +
                    sdf.format(documentMove.getDate()));
            authorDocumentLabel.setValue(ViewUtils.getMessage("field.author") + " "
                    + documentMove.getAuthor().getName());
            setFields();
        } else {
            dateDocumentLabel.setValue(ViewUtils.getMessage("field.date") + " " +
                    sdf.format(new Date()));
            authorDocumentLabel.setValue(ViewUtils.getMessage("field.author") + " "
                    + SecurityUtils.getCurrentUser().getName());
            statusComboBox.setEnabled(false);
        }
    }

    private ComboBox createStatusComboBox() {
        BeanItemContainer<StatusDocument> container = new BeanItemContainer<>(StatusDocument.class);
        container.addAll(Arrays.asList(StatusDocument.values()));
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.status"), container);
        select.setNullSelectionAllowed(false);
        return select;
    }

    private ComboBox createStockFromComboBox() {
        BeanItemContainer<Stock> container = new BeanItemContainer<>(Stock.class);
        List<Stock> stocks = partService.findAllStocks();
        stocks.forEach(stock -> stockFromMap.put(stock.getId(), stock));
        container.addAll(stocks);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.stockFrom"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.setNullSelectionAllowed(false);
        select.addValueChangeListener(event -> {
            Stock selectedStock = (Stock) event.getProperty().getValue();
            if (currentStockId != null && selectedStock != null
                    && !Objects.equals(currentStockId, selectedStock.getId())) {
                partInstances.clear();
                initTable(createContainer(new ArrayList<>(partInstances)));
            }
            currentStockId = selectedStock != null ? selectedStock.getId() : null;
        });
        return select;
    }

    private ComboBox createStockToComboBox() {
        BeanItemContainer<Stock> container = new BeanItemContainer<>(Stock.class);
        List<Stock> stocks = partService.findAllStocks();
        stocks.forEach(stock -> stockToMap.put(stock.getId(), stock));
        container.addAll(stocks);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.stockTo"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.setNullSelectionAllowed(false);
        return select;
    }

    private void setFields() {
        statusComboBox.select(documentMove.getStatus());
        if (documentMove.getStatus() == StatusDocument.Завершено) {
            statusComboBox.setEnabled(false);
            commentTextField.setEnabled(false);
            saveButton.setEnabled(false);
            addButton.setEnabled(false);
            removeButton.setEnabled(false);
            deleteButton.setEnabled(false);
            table.setSelectable(false);
        } else {
            statusComboBox.setEnabled(true);
            commentTextField.setEnabled(true);
            saveButton.setEnabled(true);
            addButton.setEnabled(true);
            removeButton.setEnabled(true);
            deleteButton.setEnabled(true);
            table.setSelectable(true);
        }
        stockToComboBox.setEnabled(false);
        stockFromComboBox.setEnabled(false);
        stockFromComboBox.select(stockFromMap.get(documentMove.getStockFrom().getId()));
        stockToComboBox.select(stockToMap.get(documentMove.getStockTo().getId()));
        commentTextField.setValue(documentMove.getComment());
    }

    private void clickAddButton() {
        for (Window window : getUI().getWindows()) if (window instanceof SelectPartInstanceWindow) return;
        Stock stock = (Stock) stockFromComboBox.getValue();
        if (stock == null) {
            Notification.show(ViewUtils.getMessage("notifications.selectStockFrom"), Notification.Type.ERROR_MESSAGE);
            return;
        }
        SelectPartInstanceWindow window = new SelectPartInstanceWindow(modelService, partService, stock, partInstances);
        window.setPartChoiceListener(new MyOnPartChoiceListener());
        getUI().addWindow(window);
    }

    private void clickRemoveButton() {
        FreePart selected = (FreePart) table.getValue();
        if (selected == null) return;
        for (PartInstance instance : partInstances) {
            if (Objects.equals(instance.getPart().getId(), selected.getPart().getId())) {
                partInstances.remove(instance);
                break;
            }
        }
        initTable(createContainer(new ArrayList<>(partInstances)));
    }

    private void saveButtonClick() {
        if (isEditedAnotherUser()) {
            Notification.show(ViewUtils.getMessage("notifications.editedAnotherUser"),
                    Notification.Type.ERROR_MESSAGE);
            return;
        }
        if (!SecurityUtils.hasAuthority(Authority.перемещение_деталей)) {
            Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                    Notification.Type.ERROR_MESSAGE);
            return;
        }
        if (documentMove == null) documentMove = new DocumentMove();
        Stock stockFrom = (Stock) stockFromComboBox.getValue();
        Stock stockTo = (Stock) stockToComboBox.getValue();
        StatusDocument status = (StatusDocument) statusComboBox.getValue();
        documentMove.setStockFrom(stockFrom);
        documentMove.setStockTo(stockTo);
        documentMove.setStatus(status);
        documentMove.setComment(commentTextField.getValue());
        documentMove.setPartInstances(partInstances);

        try {
            DocumentMove savedDocument = documentService.saveDocumentMove(documentMove);
            documentMove = documentService.findDocumentMoveById(savedDocument.getId());
            partInstances = new HashSet<>(documentMove.getPartInstances());
            Notification.show(ViewUtils.getMessage("notifications.saved"), Notification.Type.TRAY_NOTIFICATION);
            setFields();
            lastChangeDate = documentService.findLastRevisionDocumentMoveById(documentMove.getId())
                    .getRevisionDate().toDate();
        } catch (Exception e) {
            StringBuilder builder = new StringBuilder(ViewUtils.getMessage("notifications.notSaved"));
            if (e.getLocalizedMessage() != null) {
                builder.append("\n");
                builder.append(e.getLocalizedMessage());
            } else {
                for (StackTraceElement element : e.getStackTrace()) {
                    builder.append("\n");
                    builder.append(element);
                }
            }
            Notification.show(builder.toString(), Notification.Type.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }

    private void clickDeleteButton() {
        ConfirmWindow window = new ConfirmWindow(ViewUtils.getMessage("confirm.message.remove"));
        window.setOkClickListener(() -> {
            try {
                documentService.deleteDocumentMove(documentMove);
                close();
            } catch (Exception e) {
                StringBuilder builder = new StringBuilder(ViewUtils.getMessage("notifications.notSaved"));
                if (e.getLocalizedMessage() != null) {
                    builder.append("\n");
                    builder.append(e.getLocalizedMessage());
                } else {
                    for (StackTraceElement element : e.getStackTrace()) {
                        builder.append("\n");
                        builder.append(element);
                    }
                }
                Notification.show(builder.toString(), Notification.Type.ERROR_MESSAGE);
                e.printStackTrace();
            }
        });
        getUI().addWindow(window);
    }

    private void initTable(BeanItemContainer<FreePart> container) {
        table.setContainerDataSource(container);
        table.setSizeFull();
        table.setColumnCollapsingAllowed(true);
        if (SecurityUtils.hasAuthority(Authority.видеть_входную_цену)) {
            table.setVisibleColumns(fieldNamesWithInPrice);
            table.setColumnHeader(IN_PRICE_COLUMN, IN_PRICE_VISIBLE_COLUMN);
        } else {
            table.setVisibleColumns(fieldNamesWithoutInPrice);
        }
        table.setColumnHeader(PART_NUMBER_COLUMN, PART_NUMBER_VISIBLE_COLUMN);
        table.setColumnHeader(NAME_COLUMN, NAME_VISIBLE_COLUMN);
        table.setColumnHeader(COUNT_COLUMN, COUNT_VISIBLE_COLUMN);
        table.setColumnHeader(IN_PRICE_COLUMN, IN_PRICE_VISIBLE_COLUMN);
        table.setColumnHeader(OPT_PRICE_COLUMN, OPT_PRICE_VISIBLE_COLUMN);
        table.setColumnHeader(OUT_PRICE_COLUMN, OUT_PRICE_VISIBLE_COLUMN);
        table.setColumnHeader(COMMENT_COLUMN, COMMENT_VISIBLE_COLUMN);
        table.setSelectable(true);
        table.setImmediate(true);
    }

    private BeanItemContainer<FreePart> createContainer(List<PartInstance> instances) {
        BeanItemContainer<FreePart> container = new BeanItemContainer<>(FreePart.class);
        container.addNestedContainerProperty(PART_NUMBER_COLUMN);
        container.addNestedContainerProperty(NAME_COLUMN);
        container.addNestedContainerProperty(IN_PRICE_COLUMN);
        container.addNestedContainerProperty(OPT_PRICE_COLUMN);
        container.addNestedContainerProperty(OUT_PRICE_COLUMN);
        container.addNestedContainerProperty(COMMENT_COLUMN);
        List<FreePart> freeParts = FreePart.instancesToFreeParts(instances);
        container.addAll(freeParts);
        container.sort(new Object[]{NAME_COLUMN, COUNT_COLUMN}, new boolean[]{true, true});
        return container;
    }

    private boolean isEditedAnotherUser() {
        if (documentMove != null && documentMove.getId() != null) {
            Date date = documentService.findLastRevisionDocumentMoveById(documentMove.getId())
                    .getRevisionDate().toDate();
            return lastChangeDate.getTime() != date.getTime();
        } else return false;
    }

    private class MyOnPartChoiceListener implements OnPartChoiceListener {
        @Override
        public void onChoice(Set<PartInstance> instances) {
            partInstances.addAll(instances);
            initTable(createContainer(new ArrayList<>(partInstances)));
        }
    }
}
