package ru.service99.web.ui.pages;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import ru.service99.SecurityUtils;
import ru.service99.domain.*;
import ru.service99.service.DocumentService;
import ru.service99.service.PlaceService;
import ru.service99.service.RepairOrderService;
import ru.service99.service.UserService;
import ru.service99.service.criteria.DocumentSearchCriteria;
import ru.service99.web.ui.ViewUtils;
import ru.service99.web.ui.pages.window.MoveDevicesWindow;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.Calendar;

@Component
@Scope("prototype")
@SpringView(name = MovesDevicesPage.NAME)
public class MovesDevicesPage extends BasePage {

    public static final String NAME = "moves-devices";

    private static final String ID_COLUMN = "id";
    private static final String DATE_COLUMN = "date";
    private static final String AUTHOR_COLUMN = "author.name";
    private static final String PLACE_FROM_COLUMN = "placeFrom.name";
    private static final String PLACE_TO_COLUMN = "placeTo.name";
    private static final String STATUS_COLUMN = "status";
    private static final String COMMENT_COLUMN = "comment";

    private static final String ID_VISIBLE_COLUMN = ViewUtils.getMessage("table.id");
    private static final String DATE_VISIBLE_COLUMN = ViewUtils.getMessage("table.date");
    private static final String AUTHOR_VISIBLE_COLUMN = ViewUtils.getMessage("table.author");
    private static final String PLACE_FROM_VISIBLE_COLUMN = ViewUtils.getMessage("table.placeFrom");
    private static final String PLACE_TO_VISIBLE_COLUMN = ViewUtils.getMessage("table.placeTo");
    private static final String STATUS_VISIBLE_COLUMN = ViewUtils.getMessage("table.status");
    private static final String COMMENT_VISIBLE_COLUMN = ViewUtils.getMessage("table.comment");

    private static final String[] fieldNames = new String[]{ID_COLUMN, DATE_COLUMN, AUTHOR_COLUMN,
            PLACE_FROM_COLUMN, PLACE_TO_COLUMN, STATUS_COLUMN, COMMENT_COLUMN};

    @Autowired
    DocumentService documentService;
    @Autowired
    UserService userService;
    @Autowired
    PlaceService placeService;
    @Autowired
    RepairOrderService repairOrderService;

    private Table table = new Table();

    private DateField dateField1;
    private DateField dateField2;

    private ComboBox authorComboBox;
    private ComboBox statusComboBox;
    private ComboBox placeFromComboBox;
    private ComboBox placeToComboBox;

    @PostConstruct
    public void init() {
        initLayout();
        applyFilter();
    }

    private void initLayout() {
        HorizontalLayout horizontal = new HorizontalLayout();
        horizontal.setSpacing(true);
        horizontal.setSizeFull();

        VerticalLayout filterLayout = new VerticalLayout();
        filterLayout.setSpacing(true);

        VerticalLayout tableLayout = new VerticalLayout();
        tableLayout.setSpacing(true);
        tableLayout.setSizeFull();

        dateField1 = new DateField(ViewUtils.getMessage("field.date1"));
        dateField2 = new DateField(ViewUtils.getMessage("field.date2"));

        authorComboBox = createAuthorComboBox();
        statusComboBox = createStatusComboBox();
        placeFromComboBox = createPlaceComboBox(ViewUtils.getMessage("field.placeFrom"));
        placeToComboBox = createPlaceComboBox(ViewUtils.getMessage("field.placeTo"));

        Button applyButton = new Button(ViewUtils.getMessage("button.search"));
        applyButton.addClickListener(clickEvent -> applyFilter());

        Button addButton = new Button(ViewUtils.getMessage("button.new"));
        addButton.addClickListener(event -> {
            if (!SecurityUtils.hasAuthority(Authority.перемещение_устройств)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
            for (Window window : getUI().getWindows()) if (window instanceof MoveDevicesWindow) return;
            MoveDevicesWindow window = new MoveDevicesWindow(documentService, repairOrderService, placeService);
            window.addCloseListener(new MyCloseListener());
            getUI().addWindow(window);
        });

        filterLayout.addComponent(dateField1);
        filterLayout.addComponent(dateField2);
        filterLayout.addComponent(authorComboBox);
        filterLayout.addComponent(statusComboBox);
        filterLayout.addComponent(placeFromComboBox);
        filterLayout.addComponent(placeToComboBox);
        filterLayout.addComponent(applyButton);

        tableLayout.addComponent(table);
        tableLayout.addComponent(addButton);
        tableLayout.setExpandRatio(table, 8);
        tableLayout.setExpandRatio(addButton, 1);

        horizontal.addComponent(filterLayout);
        horizontal.addComponent(tableLayout);
        horizontal.setExpandRatio(filterLayout, 1);
        horizontal.setExpandRatio(tableLayout, 5);

        baseLayout.addComponent(horizontal);
        baseLayout.setExpandRatio(horizontal, 8);
    }

    private ComboBox createAuthorComboBox() {
        BeanItemContainer<User> container = new BeanItemContainer<>(User.class);
        List<User> users = userService.findAll();
        container.addAll(users);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.author"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        return select;
    }

    private ComboBox createStatusComboBox() {
        BeanItemContainer<StatusDocument> container = new BeanItemContainer<>(StatusDocument.class);
        container.addAll(Arrays.asList(StatusDocument.values()));
        return new ComboBox(ViewUtils.getMessage("field.status"), container);
    }

    private ComboBox createPlaceComboBox(String label) {
        BeanItemContainer<Place> container = new BeanItemContainer<>(Place.class);
        List<Place> places = placeService.findAllPlaces();
        container.addAll(places);
        ComboBox select = new ComboBox(label, container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        return select;
    }

    private void initTable(BeanItemContainer<DocumentMoveDevices> container) {
        table.setContainerDataSource(container);
        table.setSizeFull();
        table.setColumnCollapsingAllowed(true);
        table.setVisibleColumns(fieldNames);
        table.setColumnHeader(ID_COLUMN, ID_VISIBLE_COLUMN);
        table.setColumnHeader(DATE_COLUMN, DATE_VISIBLE_COLUMN);
        table.setColumnHeader(AUTHOR_COLUMN, AUTHOR_VISIBLE_COLUMN);
        table.setColumnHeader(PLACE_FROM_COLUMN, PLACE_FROM_VISIBLE_COLUMN);
        table.setColumnHeader(PLACE_TO_COLUMN, PLACE_TO_VISIBLE_COLUMN);
        table.setColumnHeader(STATUS_COLUMN, STATUS_VISIBLE_COLUMN);
        table.setColumnHeader(COMMENT_COLUMN, COMMENT_VISIBLE_COLUMN);
        table.setSelectable(true);
        table.setImmediate(true);
        table.addItemClickListener(event -> {
            if (!event.isDoubleClick()) return;
            for (Window window : getUI().getWindows()) if (window instanceof MoveDevicesWindow) return;
            DocumentMoveDevices documentMoveDevices = (DocumentMoveDevices) event.getItemId();
            MoveDevicesWindow window = new MoveDevicesWindow(documentService, repairOrderService, placeService,
                    documentMoveDevices);
            window.addCloseListener(new MyCloseListener());
            getUI().addWindow(window);
        });
    }

    private void applyFilter() {
        DocumentSearchCriteria criteria = new DocumentSearchCriteria();

        criteria.setDate1(dateField1.getValue());
        criteria.setAuthor((User) authorComboBox.getValue());
        criteria.setStatus((StatusDocument) statusComboBox.getValue());
        criteria.setPlaceFrom((Place) placeFromComboBox.getValue());
        criteria.setPlaceTo((Place) placeToComboBox.getValue());

        Date date2 = dateField2.getValue();
        if (date2 != null) {
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date2);
            calendar.add(Calendar.DATE, 1);
            criteria.setDate2(calendar.getTime());
        }

        initTable(createContainer(documentService.findDocumentMoveDevicesByCriteria(criteria,
                new PageRequest(0, 100, new Sort(Sort.Direction.DESC, "id")))
                .getContent()));
    }

    private BeanItemContainer<DocumentMoveDevices> createContainer(List<DocumentMoveDevices> documents) {
        BeanItemContainer<DocumentMoveDevices> container = new BeanItemContainer<>(DocumentMoveDevices.class);
        container.addNestedContainerProperty(AUTHOR_COLUMN);
        container.addNestedContainerProperty(PLACE_FROM_COLUMN);
        container.addNestedContainerProperty(PLACE_TO_COLUMN);
        container.addAll(documents);
        return container;
    }

    private class MyCloseListener implements Window.CloseListener {
        @Override
        public void windowClose(Window.CloseEvent closeEvent) {
            //applyFilter();
        }
    }
}
