package ru.service99.web.ui.pages.window;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.*;
import ru.service99.SecurityUtils;
import ru.service99.domain.*;
import ru.service99.service.ModelService;
import ru.service99.service.PartService;
import ru.service99.service.criteria.PartSearchCriteria;
import ru.service99.web.ui.OnPartChoiceListener;
import ru.service99.web.ui.OnPartPendingChoiceListener;
import ru.service99.web.ui.ViewUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SelectPartWindow extends Window {

    private static final String ID_COLUMN = "id";
    private static final String PART_NUMBER_COLUMN = "partNumber";
    private static final String NAME_COLUMN = "name";
    private static final String LAST_IN_PRICE_COLUMN = "lastInPrice";
    private static final String LAST_OPT_PRICE_COLUMN = "lastOptPrice";
    private static final String LAST_OUT_PRICE_COLUMN = "lastOutPrice";
    private static final String COMMENT_COLUMN = "comment";

    private static final String ID_VISIBLE_COLUMN = ViewUtils.getMessage("table.id");
    private static final String PART_NUMBER_VISIBLE_COLUMN = ViewUtils.getMessage("table.partNumber");
    private static final String NAME_VISIBLE_COLUMN = ViewUtils.getMessage("table.name");
    private static final String LAST_IN_PRICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.inPrice");
    private static final String LAST_OPT_PRICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.optPrice");
    private static final String LAST_OUT_PRICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.outPrice");
    private static final String COMMENT_VISIBLE_COLUMN = ViewUtils.getMessage("table.comment");

    private static final String[] fieldNamesWithInPrice = new String[]{ID_COLUMN, PART_NUMBER_COLUMN, NAME_COLUMN,
            LAST_IN_PRICE_COLUMN, LAST_OPT_PRICE_COLUMN, LAST_OUT_PRICE_COLUMN, COMMENT_COLUMN};
    private static final String[] fieldNamesWithoutInPrice = new String[]{ID_COLUMN, PART_NUMBER_COLUMN, NAME_COLUMN,
            LAST_OPT_PRICE_COLUMN, LAST_OUT_PRICE_COLUMN, COMMENT_COLUMN};

    private PartService partService;
    private ModelService modelService;

    private OnPartChoiceListener partChoiceListener;
    private OnPartPendingChoiceListener partPendingChoiceListener;

    private Table table = new Table();

    private TextField nameTextField;
    private TextField numberTextField;
    private TextField countTextField;

    private ComboBox typeDeviceComboBox;
    private ComboBox producerComboBox;
    private ComboBox modelComboBox;

    public SelectPartWindow(ModelService modelService, PartService partService) {
        super(ViewUtils.getMessage("window.title.select"));
        this.partService = partService;
        this.modelService = modelService;
        init();
    }

    public void init() {
        initLayout();
        applyFilter();
    }

    private void initLayout() {
        center();
        setModal(true);
        setHeight(90, Unit.PERCENTAGE);

        HorizontalLayout horizontal = new HorizontalLayout();
        horizontal.setSpacing(true);
        horizontal.setMargin(true);

        VerticalLayout filterLayout = new VerticalLayout();
        filterLayout.setSpacing(true);
        Panel filterPanel = new Panel();
        filterPanel.setContent(filterLayout);

        HorizontalLayout fieldLayout = new HorizontalLayout();
        fieldLayout.setSpacing(true);
        fieldLayout.setDefaultComponentAlignment(Alignment.BOTTOM_CENTER);

        VerticalLayout tableLayout = new VerticalLayout();
        tableLayout.setSpacing(true);
        Panel tablePanel = new Panel();
        tablePanel.setContent(tableLayout);

        nameTextField = new TextField(ViewUtils.getMessage("field.name"));
        numberTextField = new TextField(ViewUtils.getMessage("field.partnumber"));
        countTextField = new TextField(ViewUtils.getMessage("field.count"));
        countTextField.setValue("1");

        typeDeviceComboBox = createTypeDeviceComboBox();
        producerComboBox = createProducerPlaceComboBox();
        modelComboBox = createModelComboBox();

        Button applyButton = new Button(ViewUtils.getMessage("button.search"));
        applyButton.addClickListener(clickEvent -> applyFilter());

        Button selectButton = new Button(ViewUtils.getMessage("button.add"));
        selectButton.addClickListener(event -> selectClick());

        Button newPartButton = new Button(ViewUtils.getMessage("button.newPart"));
        newPartButton.addClickListener(event -> {
            if (!SecurityUtils.hasAuthority(Authority.редактирование_детали)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
            for (Window window : getUI().getWindows()) if (window instanceof PartWindow) return;
            PartWindow window = new PartWindow(partService, modelService);
            window.addCloseListener(new MyCloseListener());
            getUI().addWindow(window);
        });

        filterLayout.addComponent(nameTextField);
        filterLayout.addComponent(numberTextField);

        filterLayout.addComponent(typeDeviceComboBox);
        filterLayout.addComponent(producerComboBox);
        filterLayout.addComponent(modelComboBox);
        filterLayout.addComponent(applyButton);

        fieldLayout.addComponent(countTextField);
        fieldLayout.addComponent(selectButton);
        fieldLayout.addComponent(newPartButton);

        tableLayout.addComponent(fieldLayout);
        tableLayout.addComponent(table);
        tableLayout.setExpandRatio(table, 1);

        horizontal.addComponent(filterPanel);
        horizontal.addComponent(tablePanel);

        setContent(horizontal);
    }

    private void selectClick() {
        Set<PartInstance> newInstances = new HashSet<>();
        Part part = (Part) table.getValue();
        if (part == null) {
            Notification.show(ViewUtils.getMessage("notifications.selectPart"), Notification.Type.ERROR_MESSAGE);
            return;
        }
        int count;
        try {
            count = Integer.parseInt(countTextField.getValue());
        } catch (NumberFormatException e) {
            Notification.show(ViewUtils.getMessage("notifications.wrongCount"), Notification.Type.ERROR_MESSAGE);
            return;
        }
        if (count == 0) {
            Notification.show(ViewUtils.getMessage("notifications.selectCount"), Notification.Type.ERROR_MESSAGE);
            return;
        }
        if (partChoiceListener != null) {
            for (int i = 0; i < count; i++) {
                PartInstance newInstance = new PartInstance();
                newInstance.setPart(part);
                newInstances.add(newInstance);
            }
            partChoiceListener.onChoice(newInstances);
        } else if (partPendingChoiceListener != null) {
            PartPending partPending = new PartPending();
            partPending.setPart(part);
            partPending.setNeededCount(count);
            partPendingChoiceListener.onChoice(partPending);
        }
        close();
    }

    private ComboBox createTypeDeviceComboBox() {
        BeanItemContainer<TypeDevice> container = new BeanItemContainer<>(TypeDevice.class);
        List<TypeDevice> typeDevices = modelService.findAllTypeDevices();
        container.addAll(typeDevices);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.typeDevice"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.addValueChangeListener(event -> {
            Producer producer = (Producer) producerComboBox.getValue();
            if (producer == null) return;
            TypeDevice typeDevice = (TypeDevice) event.getProperty().getValue();
            List<Model> models;
            if (typeDevice != null) {
                models = modelService.findModelByProducerIdAndTypeDeviceId(
                        producer.getId(), typeDevice.getId());
            } else {
                models = modelService.findModelByProducerId(producer.getId());
            }
            BeanItemContainer<Model> modelContainer = new BeanItemContainer<>(Model.class);
            modelContainer.addAll(models);
            modelComboBox.setContainerDataSource(modelContainer);
        });
        return select;
    }

    private ComboBox createProducerPlaceComboBox() {
        BeanItemContainer<Producer> container = new BeanItemContainer<>(Producer.class);
        List<Producer> producers = modelService.findAllProducers();
        container.addAll(producers);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.producer"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.addValueChangeListener(event -> {
            TypeDevice typeDevice = (TypeDevice) typeDeviceComboBox.getValue();
            Producer producer = (Producer) event.getProperty().getValue();
            List<Model> models = new ArrayList<>();
            if (producer != null) {
                modelComboBox.setEnabled(true);
                if (typeDevice != null) {
                    models = modelService.findModelByProducerIdAndTypeDeviceId(
                            producer.getId(), typeDevice.getId());
                } else {
                    models = modelService.findModelByProducerId(producer.getId());
                }
            } else modelComboBox.setEnabled(false);
            BeanItemContainer<Model> modelContainer = new BeanItemContainer<>(Model.class);
            modelContainer.addAll(models);
            modelComboBox.setContainerDataSource(modelContainer);
        });
        return select;
    }

    private ComboBox createModelComboBox() {
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.model"));
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.setEnabled(false);
        return select;
    }

    private void initTable(BeanItemContainer<Part> container) {
        table.setContainerDataSource(container);
        table.setColumnCollapsingAllowed(true);
        if (SecurityUtils.hasAuthority(Authority.видеть_входную_цену)) {
            table.setVisibleColumns(fieldNamesWithInPrice);
            table.setColumnHeader(LAST_IN_PRICE_COLUMN, LAST_IN_PRICE_VISIBLE_COLUMN);
        } else {
            table.setVisibleColumns(fieldNamesWithoutInPrice);
        }
        table.setColumnHeader(ID_COLUMN, ID_VISIBLE_COLUMN);
        table.setColumnHeader(PART_NUMBER_COLUMN, PART_NUMBER_VISIBLE_COLUMN);
        table.setColumnHeader(NAME_COLUMN, NAME_VISIBLE_COLUMN);
        table.setColumnHeader(LAST_IN_PRICE_COLUMN, LAST_IN_PRICE_VISIBLE_COLUMN);
        table.setColumnHeader(LAST_OPT_PRICE_COLUMN, LAST_OPT_PRICE_VISIBLE_COLUMN);
        table.setColumnHeader(LAST_OUT_PRICE_COLUMN, LAST_OUT_PRICE_VISIBLE_COLUMN);
        table.setColumnHeader(COMMENT_COLUMN, COMMENT_VISIBLE_COLUMN);
        table.setSelectable(true);
        table.setImmediate(true);
    }

    private void applyFilter() {
        TypeDevice typeDevice = (TypeDevice) typeDeviceComboBox.getValue();
        Producer producer = (Producer) producerComboBox.getValue();
        Model model = (Model) modelComboBox.getValue();
        String name = nameTextField.getValue();
        String number = numberTextField.getValue();

        PartSearchCriteria criteria = new PartSearchCriteria();
        criteria.setName(name);
        criteria.setPartNumber(number);
        criteria.setModel(model);
        criteria.setProducer(producer);
        criteria.setTypeDevice(typeDevice);

        initTable(createContainer(partService.findPartsByCriteria(criteria, null).getContent()));
    }

    private BeanItemContainer<Part> createContainer(List<Part> parts) {
        BeanItemContainer<Part> container = new BeanItemContainer<>(Part.class);
        container.addAll(parts);
        return container;
    }

    public void setPartChoiceListener(OnPartChoiceListener partChoiceListener) {
        this.partChoiceListener = partChoiceListener;
    }

    public void setPartPendingChoiceListener(OnPartPendingChoiceListener partPendingChoiceListener) {
        this.partPendingChoiceListener = partPendingChoiceListener;
    }

    private class MyCloseListener implements Window.CloseListener {
        @Override
        public void windowClose(Window.CloseEvent closeEvent) {
            applyFilter();
        }
    }
}
