package ru.service99.web.ui.pages;

import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.service99.SecurityUtils;
import ru.service99.domain.Authority;
import ru.service99.web.AdvancedFileDownloader;
import ru.service99.web.FileReceiver;
import ru.service99.web.ui.ViewUtils;

import javax.annotation.PostConstruct;

@Component
@Scope("prototype")
@SpringView(name = TemplatesPage.NAME)
public class TemplatesPage extends BasePage {

    public static final String NAME = "templates";

    @PostConstruct
    public void init() {
        if (!SecurityUtils.hasAuthority(Authority.управление_шаблонами)) {
            Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                    Notification.Type.ERROR_MESSAGE);
            return;
        }
        initLayout();
    }

    private void initLayout() {
        VerticalLayout vertical = new VerticalLayout();
        vertical.setSpacing(true);
        vertical.setMargin(true);

        HorizontalLayout repairOrderTicketLayout = new HorizontalLayout();
        repairOrderTicketLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);
        repairOrderTicketLayout.setSpacing(true);

        HorizontalLayout repairOrderReportLayout = new HorizontalLayout();
        repairOrderReportLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);
        repairOrderReportLayout.setSpacing(true);

        HorizontalLayout saleOrderTicketLayout = new HorizontalLayout();
        saleOrderTicketLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);
        saleOrderTicketLayout.setSpacing(true);

        HorizontalLayout saleOrderReceiptLayout = new HorizontalLayout();
        saleOrderReceiptLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);
        saleOrderReceiptLayout.setSpacing(true);

        HorizontalLayout saleOrderWaybillLayout = new HorizontalLayout();
        saleOrderWaybillLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);
        saleOrderWaybillLayout.setSpacing(true);

        HorizontalLayout importPartsLayout = new HorizontalLayout();
        importPartsLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);
        importPartsLayout.setSpacing(true);

        Label repairOrderTicketLabel = new Label(ViewUtils.getMessage("field.template.repairOrderTicket"));
        Label repairOrderReportLabel = new Label(ViewUtils.getMessage("field.template.repairOrderReport"));
        Label saleOrderTicketLabel = new Label(ViewUtils.getMessage("field.template.saleOrderTicket"));
        Label saleOrderReceiptLabel = new Label(ViewUtils.getMessage("field.template.saleOrderReceipt"));
        Label saleOrderWaybillLabel = new Label(ViewUtils.getMessage("field.template.saleOrderWaybill"));
        Label importPartsLabel = new Label(ViewUtils.getMessage("field.template.importParts"));

        Upload repairOrderTicketUpload = createUpload("excel/" +
                ViewUtils.getMessage("file.template.repairOrderTicket") + ".xls");
        Upload repairOrderReportUpload = createUpload("excel/" +
                ViewUtils.getMessage("file.template.repairOrderReport") + ".xls");
        Upload saleOrderTicketUpload = createUpload("excel/" +
                ViewUtils.getMessage("file.template.saleOrderTicket") + ".xls");
        Upload saleOrderReceiptUpload = createUpload("excel/" +
                ViewUtils.getMessage("file.template.saleOrderReceipt") + ".xls");
        Upload saleOrderWaybillUpload = createUpload("excel/" +
                ViewUtils.getMessage("file.template.saleOrderWaybill") + ".xls");
        Upload importPartsUpload = createUpload("excel/" +
                ViewUtils.getMessage("file.template.importParts") + ".xls");

        Button downloadRepairOrderTicketButton = new Button(ViewUtils.getMessage("button.download"));
        Button downloadRepairOrderReportButton = new Button(ViewUtils.getMessage("button.download"));
        Button downloadSaleOrderTicketButton = new Button(ViewUtils.getMessage("button.download"));
        Button downloadSaleOrderReceiptButton = new Button(ViewUtils.getMessage("button.download"));
        Button downloadSaleOrderWaybillButton = new Button(ViewUtils.getMessage("button.download"));
        Button downloadImportPartsButton = new Button(ViewUtils.getMessage("button.download"));

        AdvancedFileDownloader repairOrderTicketDownloader = new AdvancedFileDownloader();
        repairOrderTicketDownloader.setFilePath(getClass().getClassLoader()
                .getResource("excel/" + ViewUtils.getMessage("file.template.repairOrderTicket") + ".xls").getFile());
        repairOrderTicketDownloader.extend(downloadRepairOrderTicketButton);

        AdvancedFileDownloader repairOrderReportDownloader = new AdvancedFileDownloader();
        repairOrderReportDownloader.setFilePath(getClass().getClassLoader()
                .getResource("excel/" + ViewUtils.getMessage("file.template.repairOrderReport") + ".xls").getFile());
        repairOrderReportDownloader.extend(downloadRepairOrderReportButton);

        AdvancedFileDownloader saleOrderTicketDownloader = new AdvancedFileDownloader();
        saleOrderTicketDownloader.setFilePath(getClass().getClassLoader()
                .getResource("excel/" + ViewUtils.getMessage("file.template.saleOrderTicket") + ".xls").getFile());
        saleOrderTicketDownloader.extend(downloadSaleOrderTicketButton);

        AdvancedFileDownloader saleOrderReceiptDownloader = new AdvancedFileDownloader();
        saleOrderReceiptDownloader.setFilePath(getClass().getClassLoader()
                .getResource("excel/" + ViewUtils.getMessage("file.template.saleOrderReceipt") + ".xls").getFile());
        saleOrderReceiptDownloader.extend(downloadSaleOrderReceiptButton);

        AdvancedFileDownloader saleOrderWaybillDownloader = new AdvancedFileDownloader();
        saleOrderWaybillDownloader.setFilePath(getClass().getClassLoader()
                .getResource("excel/" + ViewUtils.getMessage("file.template.saleOrderWaybill") + ".xls").getFile());
        saleOrderWaybillDownloader.extend(downloadSaleOrderWaybillButton);

        AdvancedFileDownloader importPartsDownloader = new AdvancedFileDownloader();
        importPartsDownloader.setFilePath(getClass().getClassLoader()
                .getResource("excel/" + ViewUtils.getMessage("file.template.importParts") + ".xls").getFile());
        importPartsDownloader.extend(downloadImportPartsButton);

        repairOrderTicketLayout.addComponent(repairOrderTicketLabel);
        repairOrderTicketLayout.addComponent(repairOrderTicketUpload);
        repairOrderTicketLayout.addComponent(downloadRepairOrderTicketButton);

        repairOrderReportLayout.addComponent(repairOrderReportLabel);
        repairOrderReportLayout.addComponent(repairOrderReportUpload);
        repairOrderReportLayout.addComponent(downloadRepairOrderReportButton);

        saleOrderTicketLayout.addComponent(saleOrderTicketLabel);
        saleOrderTicketLayout.addComponent(saleOrderTicketUpload);
        saleOrderTicketLayout.addComponent(downloadSaleOrderTicketButton);

        saleOrderReceiptLayout.addComponent(saleOrderReceiptLabel);
        saleOrderReceiptLayout.addComponent(saleOrderReceiptUpload);
        saleOrderReceiptLayout.addComponent(downloadSaleOrderReceiptButton);

        saleOrderWaybillLayout.addComponent(saleOrderWaybillLabel);
        saleOrderWaybillLayout.addComponent(saleOrderWaybillUpload);
        saleOrderWaybillLayout.addComponent(downloadSaleOrderWaybillButton);

        importPartsLayout.addComponent(importPartsLabel);
        importPartsLayout.addComponent(importPartsUpload);
        importPartsLayout.addComponent(downloadImportPartsButton);

        vertical.addComponent(repairOrderTicketLayout);
        vertical.addComponent(repairOrderReportLayout);
        vertical.addComponent(saleOrderTicketLayout);
        vertical.addComponent(saleOrderReceiptLayout);
        vertical.addComponent(saleOrderWaybillLayout);
        vertical.addComponent(importPartsLayout);

        baseLayout.addComponent(vertical);
    }

    private Upload createUpload(String path) {
        FileReceiver fileReceiver = new FileReceiver(path);
        Upload upload = new Upload(null, fileReceiver);
        upload.setImmediate(true);
        upload.setButtonCaption(ViewUtils.getMessage("button.upload"));
        upload.addFinishedListener(new MyFinishedListener());
        return upload;
    }

    private class MyFinishedListener implements Upload.FinishedListener {
        @Override
        public void uploadFinished(Upload.FinishedEvent event) {
            Notification.show(ViewUtils.getMessage("notifications.successUpload"),
                    Notification.Type.WARNING_MESSAGE);
        }
    }
}
