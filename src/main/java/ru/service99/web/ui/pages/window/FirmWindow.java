package ru.service99.web.ui.pages.window;

import com.vaadin.ui.*;
import ru.service99.SecurityUtils;
import ru.service99.domain.Authority;
import ru.service99.domain.Firm;
import ru.service99.service.FirmService;
import ru.service99.web.ui.ViewUtils;

public class FirmWindow extends Window {

    private FirmService firmService;

    private Firm firm;

    public FirmWindow(FirmService firmService) {
        setCaption(ViewUtils.getMessage("firmsPage.window.title.new"));
        this.firmService = firmService;
        init();
    }

    public FirmWindow(FirmService firmService, Firm firm) {
        this(firmService);
        setCaption(ViewUtils.getMessage("firmsPage.window.title.edit") + " " + firm.getId());
        this.firm = firm;
        init();
    }

    private void init() {
        center();
        setModal(true);

        VerticalLayout vertical = new VerticalLayout();
        vertical.setSpacing(true);
        vertical.setMargin(true);
        vertical.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

        HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.setSpacing(true);

        TextField nameTextField = new TextField(ViewUtils.getMessage("field.name"));
        TextField innTextField = new TextField(ViewUtils.getMessage("field.inn"));

        nameTextField.setWidth("330px");
        innTextField.setWidth("330px");

        if (firm != null) {
            nameTextField.setValue(firm.getName());
            innTextField.setValue(firm.getINN());
        }

        Button saveButton = new Button(ViewUtils.getMessage("button.save"));
        saveButton.addClickListener(clickEvent -> {
            if (!SecurityUtils.hasAuthority(Authority.редактирование_фирмы)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
            if (firm == null) firm = new Firm();
            firm.setName(nameTextField.getValue());
            firm.setINN(innTextField.getValue());
            try {
                firmService.save(firm);
                Notification.show(ViewUtils.getMessage("notifications.saved"), Notification.Type.TRAY_NOTIFICATION);
                close();
            } catch (Exception e) {
                StringBuilder builder = new StringBuilder(ViewUtils.getMessage("notifications.notSaved"));
                if (e.getLocalizedMessage() != null) {
                    builder.append("\n");
                    builder.append(e.getLocalizedMessage());
                } else {
                    for (StackTraceElement element : e.getStackTrace()) {
                        builder.append("\n");
                        builder.append(element);
                    }
                }
                Notification.show(builder.toString(), Notification.Type.ERROR_MESSAGE);
            }
        });

        Button cancelButton = new Button(ViewUtils.getMessage("button.cancel"));
        cancelButton.addClickListener(event -> close());

        Button removeButton = new Button(ViewUtils.getMessage("button.remove"));
        if (firm == null) removeButton.setEnabled(false);
        removeButton.addClickListener(event -> {
            if (!SecurityUtils.hasAuthority(Authority.редактирование_фирмы)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
            ConfirmWindow window = new ConfirmWindow(ViewUtils.getMessage("confirm.message.remove"));
            window.setOkClickListener(() -> {
                firm.setIsRemoved(true);
                try {
                    firmService.save(firm);
                    close();
                } catch (Exception e) {
                    firm.setIsRemoved(false);
                    StringBuilder builder = new StringBuilder(ViewUtils.getMessage("notifications.notSaved"));
                    if (e.getLocalizedMessage() != null) {
                        builder.append("\n");
                        builder.append(e.getLocalizedMessage());
                    } else {
                        for (StackTraceElement element : e.getStackTrace()) {
                            builder.append("\n");
                            builder.append(element);
                        }
                    }
                    Notification.show(builder.toString(), Notification.Type.ERROR_MESSAGE);
                }
            });
            getUI().addWindow(window);
        });

        buttonsLayout.addComponent(cancelButton);
        buttonsLayout.addComponent(removeButton);
        buttonsLayout.addComponent(saveButton);

        vertical.addComponent(nameTextField);
        vertical.addComponent(innTextField);
        vertical.addComponent(buttonsLayout);

        setContent(vertical);
    }
}
