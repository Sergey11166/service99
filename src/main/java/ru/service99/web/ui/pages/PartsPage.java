package ru.service99.web.ui.pages;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.service99.SecurityUtils;
import ru.service99.domain.*;
import ru.service99.service.ModelService;
import ru.service99.service.PartService;
import ru.service99.service.criteria.PartSearchCriteria;
import ru.service99.web.FileReceiver;
import ru.service99.web.FileUtils;
import ru.service99.web.ui.ViewUtils;
import ru.service99.web.ui.pages.window.PartWindow;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
@Scope("prototype")
@SpringView(name = PartsPage.NAME)
public class PartsPage extends BasePage {

    public static final String NAME = "parts";

    private static final String ID_COLUMN = "id";
    private static final String PART_NUMBER_COLUMN = "partNumber";
    private static final String NAME_COLUMN = "name";
    private static final String LAST_IN_PRICE_COLUMN = "lastInPrice";
    private static final String LAST_OPT_PRICE_COLUMN = "lastOptPrice";
    private static final String LAST_OUT_PRICE_COLUMN = "lastOutPrice";
    private static final String COMMENT_COLUMN = "comment";

    private static final String ID_VISIBLE_COLUMN = ViewUtils.getMessage("table.id");
    private static final String PART_NUMBER_VISIBLE_COLUMN = ViewUtils.getMessage("table.partNumber");
    private static final String NAME_VISIBLE_COLUMN = ViewUtils.getMessage("table.name");
    private static final String LAST_IN_PRICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.inPrice");
    private static final String LAST_OPT_PRICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.optPrice");
    private static final String LAST_OUT_PRICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.outPrice");
    private static final String COMMENT_VISIBLE_COLUMN = ViewUtils.getMessage("table.comment");

    private static final String[] fieldNamesWithInPrice = new String[]{ID_COLUMN, PART_NUMBER_COLUMN, NAME_COLUMN,
            LAST_IN_PRICE_COLUMN, LAST_OPT_PRICE_COLUMN, LAST_OUT_PRICE_COLUMN, COMMENT_COLUMN};

    private static final String[] fieldNamesWithoutInPrice = new String[]{ID_COLUMN, PART_NUMBER_COLUMN, NAME_COLUMN,
            LAST_OPT_PRICE_COLUMN, LAST_OUT_PRICE_COLUMN, COMMENT_COLUMN};

    @Autowired
    private PartService partService;
    @Autowired
    private ModelService modelService;

    private Table table = new Table();

    TextField nameTextField;
    TextField numberTextField;

    private ComboBox typeDeviceComboBox;
    private ComboBox producerComboBox;
    private ComboBox modelComboBox;

    @PostConstruct
    public void init() {
        initLayout();
        applyFilter();
    }

    private void initLayout() {
        HorizontalLayout horizontal = new HorizontalLayout();
        horizontal.setSpacing(true);
        horizontal.setSizeFull();

        VerticalLayout filterLayout = new VerticalLayout();
        filterLayout.setSpacing(true);

        VerticalLayout tableLayout = new VerticalLayout();
        tableLayout.setSpacing(true);
        tableLayout.setSizeFull();

        HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.setSpacing(true);

        nameTextField = new TextField(ViewUtils.getMessage("field.name"));
        numberTextField = new TextField(ViewUtils.getMessage("field.partnumber"));

        typeDeviceComboBox = createTypeDeviceComboBox();
        producerComboBox = createProducerComboBox();
        modelComboBox = createModelComboBox();

        Button applyButton = new Button(ViewUtils.getMessage("button.search"));
        applyButton.addClickListener(clickEvent -> applyFilter());

        Button addButton = new Button(ViewUtils.getMessage("button.new"));
        addButton.addClickListener(event -> {
            if (!SecurityUtils.hasAuthority(Authority.редактирование_детали)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
            for (Window window : getUI().getWindows()) if (window instanceof PartWindow) return;
            PartWindow window = new PartWindow(partService, modelService);
            window.addCloseListener(new MyCloseListener());
            getUI().addWindow(window);
        });

        Upload importUpload = createUpload("excel/" + ViewUtils.getMessage("file.tempImportParts") + ".xls");
        if (!SecurityUtils.hasAuthority(Authority.редактирование_детали)) {
            Notification.show(ViewUtils.getMessage("notifications.accessDenied"), Notification.Type.ERROR_MESSAGE);
            importUpload.setEnabled(false);
        }

        filterLayout.addComponent(nameTextField);
        filterLayout.addComponent(numberTextField);
        filterLayout.addComponent(typeDeviceComboBox);
        filterLayout.addComponent(producerComboBox);
        filterLayout.addComponent(modelComboBox);
        filterLayout.addComponent(applyButton);

        buttonsLayout.addComponent(addButton);
        buttonsLayout.addComponent(importUpload);

        tableLayout.addComponent(table);
        tableLayout.addComponent(buttonsLayout);
        tableLayout.setExpandRatio(table, 10);
        tableLayout.setExpandRatio(buttonsLayout, 1);

        horizontal.addComponent(filterLayout);
        horizontal.addComponent(tableLayout);
        horizontal.setExpandRatio(filterLayout, 1);
        horizontal.setExpandRatio(tableLayout, 5);

        baseLayout.addComponent(horizontal);
        baseLayout.setExpandRatio(horizontal, 8);
    }

    private ComboBox createTypeDeviceComboBox() {
        BeanItemContainer<TypeDevice> container = new BeanItemContainer<>(TypeDevice.class);
        List<TypeDevice> typeDevices = modelService.findAllTypeDevices();
        container.addAll(typeDevices);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.typeDevice"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.addValueChangeListener(event -> {
            Producer producer = (Producer) producerComboBox.getValue();
            if (producer == null) return;
            TypeDevice typeDevice = (TypeDevice) event.getProperty().getValue();
            List<Model> models;
            if (typeDevice != null) {
                models = modelService.findModelByProducerIdAndTypeDeviceId(
                        producer.getId(), typeDevice.getId());
            } else {
                models = modelService.findModelByProducerId(producer.getId());
            }
            BeanItemContainer<Model> modelContainer = new BeanItemContainer<>(Model.class);
            modelContainer.addAll(models);
            modelComboBox.setContainerDataSource(modelContainer);
        });
        return select;
    }

    private ComboBox createProducerComboBox() {
        BeanItemContainer<Producer> container = new BeanItemContainer<>(Producer.class);
        List<Producer> producers = modelService.findAllProducers();
        container.addAll(producers);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.producer"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.addValueChangeListener(event -> {
            TypeDevice typeDevice = (TypeDevice) typeDeviceComboBox.getValue();
            Producer producer = (Producer) event.getProperty().getValue();
            List<Model> models = new ArrayList<>();
            if (producer != null) {
                modelComboBox.setEnabled(true);
                if (typeDevice != null) {
                    models = modelService.findModelByProducerIdAndTypeDeviceId(
                            producer.getId(), typeDevice.getId());
                } else {
                    models = modelService.findModelByProducerId(producer.getId());
                }
            } else modelComboBox.setEnabled(false);
            BeanItemContainer<Model> modelContainer = new BeanItemContainer<>(Model.class);
            modelContainer.addAll(models);
            modelComboBox.setContainerDataSource(modelContainer);
        });
        return select;
    }

    private ComboBox createModelComboBox() {
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.model"));
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.setEnabled(false);
        return select;
    }

    private Upload createUpload(String path) {
        FileReceiver fileReceiver = new FileReceiver(path);
        Upload upload = new Upload(null, fileReceiver);
        upload.setImmediate(true);
        upload.setButtonCaption(ViewUtils.getMessage("field.importParts"));
        upload.addFinishedListener(new MyFinishedListener());
        return upload;
    }

    private void initTable(BeanItemContainer<Part> container) {
        table.setContainerDataSource(container);
        table.setSizeFull();
        table.setColumnCollapsingAllowed(true);
        if (SecurityUtils.hasAuthority(Authority.видеть_входную_цену)) {
            table.setVisibleColumns(fieldNamesWithInPrice);
            table.setColumnHeader(LAST_IN_PRICE_COLUMN, LAST_IN_PRICE_VISIBLE_COLUMN);
        } else {
            table.setVisibleColumns(fieldNamesWithoutInPrice);
        }
        table.setColumnHeader(ID_COLUMN, ID_VISIBLE_COLUMN);
        table.setColumnHeader(PART_NUMBER_COLUMN, PART_NUMBER_VISIBLE_COLUMN);
        table.setColumnHeader(NAME_COLUMN, NAME_VISIBLE_COLUMN);
        table.setColumnHeader(LAST_OPT_PRICE_COLUMN, LAST_OPT_PRICE_VISIBLE_COLUMN);
        table.setColumnHeader(LAST_OUT_PRICE_COLUMN, LAST_OUT_PRICE_VISIBLE_COLUMN);
        table.setColumnHeader(COMMENT_COLUMN, COMMENT_VISIBLE_COLUMN);
        table.setSelectable(true);
        table.setImmediate(true);
        table.addItemClickListener(event -> {
            if (!event.isDoubleClick()) return;
            for (Window window : getUI().getWindows()) if (window instanceof PartWindow) return;
            Part part = (Part) event.getItemId();
            PartWindow window = new PartWindow(partService, modelService, part);
            window.addCloseListener(new MyCloseListener());
            getUI().addWindow(window);
        });
    }

    private void applyFilter() {
        TypeDevice typeDevice = (TypeDevice) typeDeviceComboBox.getValue();
        Producer producer = (Producer) producerComboBox.getValue();
        Model model = (Model) modelComboBox.getValue();
        String name = nameTextField.getValue();
        String number = numberTextField.getValue();

        PartSearchCriteria criteria = new PartSearchCriteria();
        criteria.setName(name);
        criteria.setPartNumber(number);
        criteria.setModel(model);
        criteria.setProducer(producer);
        criteria.setTypeDevice(typeDevice);

        initTable(createContainer(partService.findPartsByCriteria(criteria, null).getContent()));
    }

    private BeanItemContainer<Part> createContainer(List<Part> parts) {
        BeanItemContainer<Part> container = new BeanItemContainer<>(Part.class);
        container.addAll(parts);
        return container;
    }

    private class MyCloseListener implements Window.CloseListener {
        @Override
        public void windowClose(Window.CloseEvent closeEvent) {
            //applyFilter();
        }
    }

    private class MyFinishedListener implements Upload.FinishedListener {
        @Override
        public void uploadFinished(Upload.FinishedEvent event) {
            if (!SecurityUtils.hasAuthority(Authority.поступление_деталей)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
            FileUtils.importParts(getClass(), partService);
            applyFilter();
        }
    }
}
