package ru.service99.web.ui.pages.window;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.*;
import ru.service99.SecurityUtils;
import ru.service99.domain.*;
import ru.service99.service.*;
import ru.service99.web.AdvancedFileDownloader;
import ru.service99.web.FileUtils;
import ru.service99.web.model.FreePart;
import ru.service99.web.ui.*;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

public class RepairWindow extends Window {

    private static final String PART_NUMBER_COLUMN = "part.partNumber";
    private static final String NAME_COLUMN = "part.name";
    private static final String COUNT_COLUMN = "count";
    private static final String NEEDED_COUNT_COLUMN = "neededCount";
    private static final String ORDERED_COUNT_COLUMN = "orderedCount";
    private static final String RECEIVED_COUNT_COLUMN = "receivedCount";
    private static final String STOCK_COLUMN = "stock.name";
    private static final String OPT_PRICE_COLUMN = "optPrice";
    private static final String OUT_PRICE_COLUMN = "outPrice";
    private static final String PART_COMMENT_COLUMN = "part.comment";
    private static final String COMMENT_COLUMN = "comment";

    private static final String PART_NUMBER_VISIBLE_COLUMN = ViewUtils.getMessage("table.partNumber");
    private static final String NAME_VISIBLE_COLUMN = ViewUtils.getMessage("table.name");
    private static final String COUNT_VISIBLE_COLUMN = ViewUtils.getMessage("table.count");
    private static final String NEEDED_VISIBLE_COUNT_COLUMN = ViewUtils.getMessage("table.neededCount");
    private static final String ORDERED_VISIBLE_COUNT_COLUMN = ViewUtils.getMessage("table.orderedCount");
    private static final String RECEIVED_VISIBLE_COUNT_COLUMN = ViewUtils.getMessage("table.receivedCount");
    private static final String STOCK_VISIBLE_COLUMN = ViewUtils.getMessage("table.stock");
    private static final String OPT_PRICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.optPrice");
    private static final String OUT_PRICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.outPrice");
    private static final String PART_COMMENT_VISIBLE_COLUMN = ViewUtils.getMessage("table.comment");
    private static final String COMMENT_VISIBLE_COLUMN = ViewUtils.getMessage("table.comment");

    private static final String[] mainFieldNames = new String[]{PART_NUMBER_COLUMN, NAME_COLUMN, COUNT_COLUMN,
            STOCK_COLUMN, OPT_PRICE_COLUMN, OUT_PRICE_COLUMN, PART_COMMENT_COLUMN};

    private static final String[] pendingFieldNames = new String[]{PART_NUMBER_COLUMN, NAME_COLUMN,
            NEEDED_COUNT_COLUMN, ORDERED_COUNT_COLUMN, RECEIVED_COUNT_COLUMN, COMMENT_COLUMN};

    private SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());

    private RepairOrderService repairOrderService;
    private ModelService modelService;
    private CustomerService customerService;
    private FirmService firmService;
    private UserService userService;
    private PartService partService;

    private RepairOrder order;
    private List<RepairOrder> prevRepairs;
    private Customer selectedCustomer;
    private Set<PartInstance> partInstances;
    private Set<PartPending> partsPending;
    private Date lastChangeDate;

    private HashMap<Long, Firm> firmMap = new HashMap<>();
    private HashMap<Long, User> masterMap = new HashMap<>();
    private HashMap<Long, TypeDevice> typeDeviceMap = new HashMap<>();
    private HashMap<Long, Producer> producerMap = new HashMap<>();
    private HashMap<Long, Model> modelMap = new HashMap<>();

    private Table mainTable;
    private Table pendingTable;

    //tab1
    private ComboBox typeDeviceComboBox;
    private ComboBox producerComboBox;
    private ComboBox modelComboBox;
    private TextField idTextField;
    private TextField imeiTextField;
    private TextField snTextField;
    private TextArea visibleDamageTextArea;
    private TextArea equipmentTextArea;
    private TextField customerTextField;
    private TextArea statedDefectTextArea;
    private TextArea receiverCommentTextArea;
    private TextField startCostTextField;
    private TextField endCostTextField;
    private ComboBox firmComboBox;
    private TextField partsPendingCostTextField;

    //tab2
    private TextArea masterCommentTextArea;
    private TextField newImeiTextField;
    private TextField newSnTextField;
    private TextArea discoveredDefectTextArea;
    private TextArea workDoneTextArea;
    private TextField partsCostTextField;
    private TextField workCostTextField;

    private TextArea commentForCustomerTextArea;
    private ComboBox statusComboBox;
    private ComboBox masterComboBox;

    private Button addPartButton;
    private Button removePartButton;
    private Button historyButton;
    private Button ticketButton;
    private Button reportButton;
    private HorizontalLayout modelLayout;

    public RepairWindow(
            RepairOrderService repairOrderService,
            ModelService modelService,
            CustomerService customerService,
            FirmService firmService,
            UserService userService,
            PartService partService) {
        setCaption(ViewUtils.getMessage("repairsPage.window.title.new"));
        this.repairOrderService = repairOrderService;
        this.modelService = modelService;
        this.customerService = customerService;
        this.firmService = firmService;
        this.userService = userService;
        this.partService = partService;
        this.prevRepairs = new ArrayList<>();
        this.partInstances = new HashSet<>();
        this.partsPending = new HashSet<>();
        init();
    }

    public RepairWindow(RepairOrderService repairOrderService,
                        ModelService modelService,
                        CustomerService customerService,
                        FirmService firmService,
                        UserService userService,
                        PartService partService,
                        RepairOrder order) {
        this(repairOrderService, modelService, customerService, firmService, userService, partService);
        setCaption(ViewUtils.getMessage("repairsPage.window.title.edit") + " " + order.getId());
        this.order = order;
        if (order.getDocument() != null) {
            partInstances.addAll(order.getDocument().getPartInstances());
        }
        if (order.getPartsPending() != null) {
            this.partsPending.addAll(order.getPartsPending());
        }
        this.lastChangeDate = repairOrderService.findLastRevisionById(order.getId()).getRevisionDate().toDate();
        init();
    }

    private void init() {
        initLayout();
        initMainTable(createMainContainer(new ArrayList<>(partInstances)));
        initPendingTable(createPendingContainer(new ArrayList<>(partsPending)));
    }

    private void initLayout() {
        center();
        setModal(true);
        setHeight("100%");

        VerticalLayout vertical = new VerticalLayout();
        vertical.setSpacing(true);
        vertical.setMargin(true);

        //startDate, receiver, placeReceive, currentPlace etc.
        HorizontalLayout horizontalLayout1 = new HorizontalLayout();
        horizontalLayout1.setSpacing(true);

        //typeDevice, producer, model, imei, sn etc.
        HorizontalLayout horizontalLayout2 = new HorizontalLayout();
        horizontalLayout2.setSpacing(true);

        //visibleDamage, equipment, customer
        HorizontalLayout horizontalLayout3 = new HorizontalLayout();
        horizontalLayout3.setSpacing(true);

        //statedDefect, receiverComment, cost, firm
        HorizontalLayout horizontalLayout4 = new HorizontalLayout();
        horizontalLayout4.setSpacing(true);

        //masterComment, discoveredDefect, workDone
        HorizontalLayout horizontalLayout5 = new HorizontalLayout();
        horizontalLayout5.setSpacing(true);

        //endCost, newImei, newSn
        AbsoluteLayout absoluteLayout6 = new AbsoluteLayout();
        absoluteLayout6.setWidth("985px");
        absoluteLayout6.setHeight("70px");

        //commentForCustomer, status, master
        HorizontalLayout horizontalLayout7 = new HorizontalLayout();
        horizontalLayout7.setSpacing(true);

        //buttons
        HorizontalLayout horizontalLayout8 = new HorizontalLayout();
        horizontalLayout8.setSpacing(true);

        HorizontalLayout customerLayout = new HorizontalLayout();
        customerLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);

        modelLayout = new HorizontalLayout();
        modelLayout.setEnabled(false);
        modelLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);

        VerticalLayout verticalLayout4_1 = new VerticalLayout();
        verticalLayout4_1.setSpacing(true);

        VerticalLayout verticalLayout4_2 = new VerticalLayout();
        verticalLayout4_2.setSpacing(true);

        HorizontalLayout tablePartButtonsLayout = new HorizontalLayout();
        tablePartButtonsLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);
        tablePartButtonsLayout.setSpacing(true);

        HorizontalLayout tablePartsPendingButtonsLayout = new HorizontalLayout();
        tablePartsPendingButtonsLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);
        tablePartsPendingButtonsLayout.setSpacing(true);

        VerticalLayout statusMasterLayout = new VerticalLayout();
        statusMasterLayout.setSpacing(true);

        VerticalLayout tab1 = new VerticalLayout();
        tab1.setSpacing(true);

        VerticalLayout tab2 = new VerticalLayout();
        tab2.setSpacing(true);

        VerticalLayout tab3 = new VerticalLayout();
        tab3.setSpacing(true);

        mainTable = new Table();
        mainTable.setWidth("100%");
        mainTable.setHeight("220px");

        pendingTable = new Table();
        pendingTable.setWidth("100%");
        pendingTable.setHeight("230px");

        TabSheet tabSheet = new TabSheet();
        tabSheet.setHeight("450px");

        idTextField = new TextField(ViewUtils.getMessage("field.repairNumber"));
        idTextField.setEnabled(false);
        TextField startDateTextField = new TextField(ViewUtils.getMessage("field.startDate"));
        startDateTextField.setEnabled(false);
        TextField currentPlaceTextField = new TextField(ViewUtils.getMessage("field.currentPlace"));
        currentPlaceTextField.setEnabled(false);
        TextField placeReceiveTextField = new TextField(ViewUtils.getMessage("field.placeReceive"));
        placeReceiveTextField.setEnabled(false);
        TextField receiverTextField = new TextField(ViewUtils.getMessage("field.receiver"));
        receiverTextField.setEnabled(false);

        typeDeviceComboBox = createTypeDeviceComboBox();
        producerComboBox = createProducerComboBox();
        modelComboBox = createModelComboBox();
        imeiTextField = new TextField(ViewUtils.getMessage("field.imei"));
        imeiTextField.addBlurListener(event -> {
            if (order != null || !prevRepairs.isEmpty()) return;
            checkRepetitions();
        });
        snTextField = new TextField(ViewUtils.getMessage("field.sn"));
        snTextField.addBlurListener(event -> {
            if (order != null || !prevRepairs.isEmpty()) return;
            checkRepetitions();
        });
        producerComboBox.setWidth("160px");
        visibleDamageTextArea = new TextArea(ViewUtils.getMessage("field.visibleDamage"));
        visibleDamageTextArea.setWidth("385px");
        visibleDamageTextArea.setRows(1);
        equipmentTextArea = new TextArea(ViewUtils.getMessage("field.equipment"));
        equipmentTextArea.setWidth("292px");
        equipmentTextArea.setRows(1);
        customerTextField = new TextField(ViewUtils.getMessage("field.customer"));
        customerTextField.setEnabled(false);
        customerTextField.setWidth("250px");
        statedDefectTextArea = new TextArea(ViewUtils.getMessage("field.statedDefect"));
        statedDefectTextArea.setWidth("785px");
        statedDefectTextArea.setRows(4);
        receiverCommentTextArea = new TextArea(ViewUtils.getMessage("field.receiverComment"));
        receiverCommentTextArea.setWidth("785px");
        receiverCommentTextArea.setRows(1);
        startCostTextField = new TextField(ViewUtils.getMessage("field.startCost"));
        endCostTextField = new TextField(ViewUtils.getMessage("field.endCost"));
        endCostTextField.setEnabled(false);
        partsCostTextField = new TextField(ViewUtils.getMessage("field.partsCost"));
        partsCostTextField.setEnabled(false);
        workCostTextField = new TextField(ViewUtils.getMessage("field.workCost"));
        workCostTextField.addBlurListener(event -> setEndCostTextField());
        partsPendingCostTextField = new TextField(ViewUtils.getMessage("field.partsPendingCost"));
        partsPendingCostTextField.setEnabled(false);
        firmComboBox = createFirmComboBox();

        masterCommentTextArea = new TextArea(ViewUtils.getMessage("field.masterComment"));
        masterCommentTextArea.setWidth("300px");
        masterCommentTextArea.setRows(3);
        discoveredDefectTextArea = new TextArea(ViewUtils.getMessage("field.discoveredDefect"));
        discoveredDefectTextArea.setWidth("210px");
        discoveredDefectTextArea.setRows(3);
        workDoneTextArea = new TextArea(ViewUtils.getMessage("field.workDone"));
        workDoneTextArea.setWidth("450px");
        workDoneTextArea.setRows(3);

        newImeiTextField = new TextField(ViewUtils.getMessage("field.newImei"));
        newSnTextField = new TextField(ViewUtils.getMessage("field.newSn"));
        commentForCustomerTextArea = new TextArea(ViewUtils.getMessage("field.commentForCustomer"));
        commentForCustomerTextArea.setWidth("785px");
        commentForCustomerTextArea.setRows(4);
        statusComboBox = createStatusComboBox();
        masterComboBox = createMasterComboBox();

        Button choiceCustomerButton = new Button("*");
        choiceCustomerButton.setWidth("30px");
        choiceCustomerButton.addClickListener(event -> {
            for (Window window : getUI().getWindows()) if (window instanceof SelectCustomerWindow) return;
            SelectCustomerWindow window = new SelectCustomerWindow(customerService);
            window.setCustomerChoiceListener(new MyCustomerChoiceListener());
            getUI().addWindow(window);
        });

        Button newModelButton = new Button("+");
        newModelButton.setWidth("30px");
        newModelButton.addClickListener(event -> {
            for (Window window : getUI().getWindows()) if (window instanceof ModelWindow) return;
            ModelWindow window = new ModelWindow(modelService);
            window.addCloseListener(new MyCloseListener());
            getUI().addWindow(window);
        });

        addPartButton = new Button("+");
        addPartButton.addClickListener(event -> {
            for (Window window : getUI().getWindows()) if (window instanceof SelectPartInstanceWindow) return;
            if (SecurityUtils.getCurrentUser().getAllowedStocks().isEmpty()) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"), Notification.Type.ERROR_MESSAGE);
                return;
            }
            SelectPartInstanceWindow window = new SelectPartInstanceWindow(modelService, partService,
                    null, partInstances);
            window.setPartChoiceListener(new MyOnPartChoiceListener());
            getUI().addWindow(window);
        });

        removePartButton = new Button("-");
        removePartButton.addClickListener(event -> removePartButtonClick());

        Button addPartPendingButton = new Button("+");
        addPartPendingButton.addClickListener(event -> {
            for (Window window : getUI().getWindows()) if (window instanceof SelectPartWindow) return;
            SelectPartWindow window = new SelectPartWindow(modelService, partService);
            window.setPartPendingChoiceListener(new MyOnPartPendingChoiceListener());
            getUI().addWindow(window);
        });

        Button removePartPendingButton = new Button("-");
        removePartPendingButton.addClickListener(event -> removePartPendingButtonClick());

        Button saveButton = new Button(ViewUtils.getMessage("button.save"));
        saveButton.addClickListener(clickEvent -> saveButtonClick());

        historyButton = new Button(ViewUtils.getMessage("button.history"));
        historyButton.addClickListener(event -> {
            if (order == null) return;
            for (Window window : getUI().getWindows()) if (window instanceof HistoryRepairWindow) return;
            HistoryRepairWindow window = new HistoryRepairWindow(repairOrderService, order.getId());
            getUI().addWindow(window);
        });

        AdvancedFileDownloader ticketDownloader = new AdvancedFileDownloader();
        URL urlTicketDownloader = getClass().getClassLoader().getResource(
                "excel/" + ViewUtils.getMessage("file.repairOrderTicket") + ".xls");
        assert urlTicketDownloader != null;
        ticketDownloader.setFilePath(urlTicketDownloader.getFile());
        ticketDownloader.addAdvancedDownloaderListener(event -> FileUtils.writeRepairOrderTicketExcel(getClass(), order));
        ticketButton = new Button(ViewUtils.getMessage("button.ticket"));
        ticketDownloader.extend(ticketButton);

        AdvancedFileDownloader reportDownloader = new AdvancedFileDownloader();
        URL urlReportDownloader = getClass().getClassLoader().getResource(
                "excel/" + ViewUtils.getMessage("file.repairOrderReport") + ".xls");
        assert urlReportDownloader != null;
        reportDownloader.setFilePath(urlReportDownloader.getFile());
        reportDownloader.addAdvancedDownloaderListener(event -> FileUtils.writeRepairOrderReportExcel(getClass(), order));
        reportButton = new Button(ViewUtils.getMessage("button.report"));
        reportDownloader.extend(reportButton);

        Button closeButton = new Button(ViewUtils.getMessage("button.close"));
        closeButton.addClickListener(event -> close());

        horizontalLayout1.addComponent(idTextField);
        horizontalLayout1.addComponent(startDateTextField);
        horizontalLayout1.addComponent(currentPlaceTextField);
        horizontalLayout1.addComponent(placeReceiveTextField);
        horizontalLayout1.addComponent(receiverTextField);

        horizontalLayout2.addComponent(typeDeviceComboBox);
        horizontalLayout2.addComponent(producerComboBox);
        horizontalLayout2.addComponent(modelComboBox);
        modelLayout.addComponent(modelComboBox);
        modelLayout.addComponent(newModelButton);
        horizontalLayout2.addComponent(modelLayout);
        horizontalLayout2.addComponent(imeiTextField);
        horizontalLayout2.addComponent(snTextField);

        horizontalLayout3.addComponent(visibleDamageTextArea);
        horizontalLayout3.addComponent(equipmentTextArea);
        customerLayout.addComponent(customerTextField);
        customerLayout.addComponent(choiceCustomerButton);
        horizontalLayout3.addComponent(customerLayout);

        verticalLayout4_1.addComponent(statedDefectTextArea);
        verticalLayout4_1.addComponent(receiverCommentTextArea);
        verticalLayout4_2.addComponent(firmComboBox);
        verticalLayout4_2.addComponent(startCostTextField);
        horizontalLayout4.addComponent(verticalLayout4_1);
        horizontalLayout4.addComponent(verticalLayout4_2);

        horizontalLayout5.addComponent(masterCommentTextArea);
        horizontalLayout5.addComponent(discoveredDefectTextArea);
        horizontalLayout5.addComponent(workDoneTextArea);

        absoluteLayout6.addComponent(endCostTextField, "left: 0px; bottom: 10px;");
        absoluteLayout6.addComponent(newImeiTextField, "right: 200px; bottom: 10px;");
        absoluteLayout6.addComponent(newSnTextField, "right: 0px; bottom: 10px;");

        horizontalLayout7.addComponent(commentForCustomerTextArea);
        statusMasterLayout.addComponent(statusComboBox);
        statusMasterLayout.addComponent(masterComboBox);
        horizontalLayout7.addComponent(statusMasterLayout);

        horizontalLayout8.addComponent(historyButton);
        horizontalLayout8.addComponent(ticketButton);
        horizontalLayout8.addComponent(reportButton);
        horizontalLayout8.addComponent(closeButton);
        horizontalLayout8.addComponent(saveButton);

        tablePartButtonsLayout.addComponent(addPartButton);
        tablePartButtonsLayout.addComponent(removePartButton);
        tablePartButtonsLayout.addComponent(partsCostTextField);
        tablePartButtonsLayout.addComponent(workCostTextField);

        tablePartsPendingButtonsLayout.addComponent(addPartPendingButton);
        tablePartsPendingButtonsLayout.addComponent(removePartPendingButton);
        tablePartsPendingButtonsLayout.addComponent(partsPendingCostTextField);

        tab1.addComponent(horizontalLayout2);
        tab1.addComponent(horizontalLayout3);
        tab1.addComponent(horizontalLayout4);
        tab2.addComponent(tablePartButtonsLayout);
        tab2.addComponent(mainTable);
        tab2.addComponent(horizontalLayout5);
        tab3.addComponent(tablePartsPendingButtonsLayout);
        tab3.addComponent(pendingTable);

        tabSheet.addTab(tab1, ViewUtils.getMessage("tab.mainInfo"));
        tabSheet.addTab(tab2, ViewUtils.getMessage("tab.parts"));
        tabSheet.addTab(tab3, ViewUtils.getMessage("tab.partsPending"));

        vertical.addComponent(horizontalLayout1);
        vertical.addComponent(tabSheet);
        vertical.addComponent(absoluteLayout6);
        vertical.addComponent(horizontalLayout7);
        vertical.addComponent(horizontalLayout8);

        setContent(vertical);

        if (order != null) {
            idTextField.setValue(order.getId().toString());
            startDateTextField.setValue(sdf.format(order.getStartDate()));
            currentPlaceTextField.setValue(order.getCurrentPlace().getName());
            if (checkIsMoving(order)) currentPlaceTextField.setValue(ViewUtils.getMessage("string.inWay"));
            placeReceiveTextField.setValue(order.getPlaceReceive().getName());
            receiverTextField.setValue(order.getReceiver().getName());
            typeDeviceComboBox.setValue(typeDeviceMap.get(order.getModel().getTypeDevice().getId()));
            producerComboBox.setValue(producerMap.get(order.getModel().getProducer().getId()));
            modelComboBox.setValue(modelMap.get(order.getModel().getId()));
            imeiTextField.setValue(order.getImei() == null ? "" : order.getImei());
            snTextField.setValue(order.getSn() == null ? "" : order.getSn());
            visibleDamageTextArea.setValue(order.getVisibleDamage() == null ? "" : order.getVisibleDamage());
            equipmentTextArea.setValue(order.getEquipment() == null ? "" : order.getEquipment());
            selectedCustomer = order.getCustomer();
            customerTextField.setValue(selectedCustomer.getName());
            customerTextField.setDescription(selectedCustomer.description());
            statedDefectTextArea.setValue(order.getStatedDefect());
            receiverCommentTextArea.setValue(order.getReceiverComment() == null ? "" : order.getReceiverComment());
            startCostTextField.setValue(order.getStartCost() == null ? "" : order.getStartCost().toString());
            Float endCostWork = order.getEndCost() == null ? 0 : order.getEndCost();
            endCostTextField.setValue(endCostWork == 0 ? "" : endCostWork.toString());
            setPartsCostTextField();
            setPartsPendingCostTextField();
            setWorkCostTextField();
            firmComboBox.setValue(firmMap.get(order.getFirm().getId()));
            discoveredDefectTextArea.setValue(order.getDiscoveredDefect() == null ? "" : order.getDiscoveredDefect());
            workDoneTextArea.setValue(order.getWorksDone() == null ? "" : order.getWorksDone());
            masterCommentTextArea.setValue(order.getMasterComment() == null ? "" : order.getMasterComment());
            newImeiTextField.setValue(order.getNewImei() == null ? "" : order.getNewImei());
            newSnTextField.setValue(order.getNewSn() == null ? "" : order.getNewSn());
            commentForCustomerTextArea.setValue(order.getCommentForCustomer() == null ? "" :
                    order.getCommentForCustomer());
            statusComboBox.setValue(order.getStatus());
            if (order.getMaster() != null) masterComboBox.setValue(masterMap.get(order.getMaster().getId()));
        } else {
            startDateTextField.setValue(sdf.format(new Date()));
            currentPlaceTextField.setValue(SecurityUtils.getCurrentUser().getPlace().getName());
            placeReceiveTextField.setValue(SecurityUtils.getCurrentUser().getPlace().getName());
            receiverTextField.setValue(SecurityUtils.getCurrentUser().getName());
            firmComboBox.setValue(firmMap.get(SecurityUtils.getCurrentUser().getFirm().getId()));
            statusComboBox.setValue(StatusRepairOrder.Новый);
        }
        setFields();
    }

    private void saveButtonClick() {
        if (isEditedAnotherUser()) {
            Notification.show(ViewUtils.getMessage("notifications.editedAnotherUser"), Notification.Type.ERROR_MESSAGE);
            return;
        }
        if (!SecurityUtils.hasAuthority(Authority.редактирование_ремонта)) {
            Notification.show(ViewUtils.getMessage("notifications.accessDenied"), Notification.Type.ERROR_MESSAGE);
            return;
        }
        Float startCost = null;
        try {
            if (!startCostTextField.getValue().isEmpty())
                startCost = Float.valueOf(startCostTextField.getValue());
        } catch (NumberFormatException e) {
            Notification.show(ViewUtils.getMessage("notifications.wrongCost"), Notification.Type.ERROR_MESSAGE);
            return;
        }
        Float endCost = null;
        try {
            if (!endCostTextField.getValue().isEmpty()) endCost = Float.valueOf(endCostTextField.getValue());
        } catch (NumberFormatException e) {
            Notification.show(ViewUtils.getMessage("notifications.wrongCost"), Notification.Type.ERROR_MESSAGE);
            return;
        }
        try {
            if (!imeiTextField.getValue().isEmpty()) {
                long imei = Long.parseLong(imeiTextField.getValue());
                if (!isImeiValid(imei)) {
                    Notification.show(ViewUtils.getMessage("notifications.wrongImei"), Notification.Type.ERROR_MESSAGE);
                    return;
                }
            }
        } catch (NumberFormatException e) {
            Notification.show(ViewUtils.getMessage("notifications.wrongImei"), Notification.Type.ERROR_MESSAGE);
            return;
        }
        try {
            if (!newImeiTextField.getValue().isEmpty()) {
                long newImei = Long.parseLong(imeiTextField.getValue());
                if (!isImeiValid(newImei)) {
                    Notification.show(ViewUtils.getMessage("notifications.wrongNewImei"), Notification.Type.ERROR_MESSAGE);
                    return;
                }
            }
        } catch (NumberFormatException e) {
            Notification.show(ViewUtils.getMessage("notifications.wrongNewImei"), Notification.Type.ERROR_MESSAGE);
            return;
        }
        if (order == null) order = new RepairOrder();
        Firm firm = (Firm) firmComboBox.getValue();
        String receiverComment = receiverCommentTextArea.getValue();
        Model model = (Model) modelComboBox.getValue();
        User master = (User) masterComboBox.getValue();
        StatusRepairOrder statusRepairOrder = (StatusRepairOrder) statusComboBox.getValue();
        String visibleDamage = visibleDamageTextArea.getValue();
        String equipment = equipmentTextArea.getValue();
        String imei = imeiTextField.getValue();
        String newImei = newImeiTextField.getValue();
        String sn = snTextField.getValue();
        String newSn = newSnTextField.getValue();
        String discoveredDefect = discoveredDefectTextArea.getValue();
        String workDone = workDoneTextArea.getValue();
        String commentForCustomer = commentForCustomerTextArea.getValue();
        String masterComment = masterCommentTextArea.getValue();

        order.setFirm(firm);
        order.setModel(model);
        order.setMaster(master);
        order.setStatus(statusRepairOrder);
        order.setStartCost(startCost);
        order.setEndCost(endCost);
        order.setCustomer(selectedCustomer);
        order.setReceiverComment(receiverComment.isEmpty() ? null : receiverComment);
        order.setVisibleDamage(visibleDamage.isEmpty() ? null : visibleDamage);
        order.setEquipment(equipment.isEmpty() ? null : equipment);
        order.setImei(imei.isEmpty() ? null : imei);
        order.setNewImei(newImei.isEmpty() ? null : newImei);
        order.setSn(sn.isEmpty() ? null : sn);
        order.setNewSn(newSn.isEmpty() ? null : newSn);
        order.setStatedDefect(statedDefectTextArea.getValue());
        order.setDiscoveredDefect(discoveredDefect.isEmpty() ? null : discoveredDefect);
        order.setWorksDone(workDone.isEmpty() ? null : workDone);
        order.setCommentForCustomer(commentForCustomer.isEmpty() ? null : commentForCustomer);
        order.setMasterComment(masterComment.isEmpty() ? null : masterComment);
        if (order.getDocument() != null) {
            order.getDocument().setPartInstances(partInstances);
        } else {
            if (!partInstances.isEmpty()) {
                DocumentOut doc = new DocumentOut();
                doc.setPartInstances(partInstances);
                order.setDocument(doc);
            }
        }
        order.setPartsPending(partsPending);
        try {
            RepairOrder savedOrder = repairOrderService.save(order);
            order = repairOrderService.findById(savedOrder.getId());
            if (order.getDocument() != null) partInstances = new HashSet<>(order.getDocument().getPartInstances());
            partsPending = new HashSet<>(order.getPartsPending());
            Notification.show(ViewUtils.getMessage("notifications.saved"), Notification.Type.TRAY_NOTIFICATION);
            setFields();
            lastChangeDate = repairOrderService.findLastRevisionById(order.getId()).getRevisionDate().toDate();
        } catch (Exception e) {
            StringBuilder builder = new StringBuilder(ViewUtils.getMessage("notifications.notSaved"));
            if (e.getLocalizedMessage() != null) {
                builder.append("\n");
                builder.append(e.getLocalizedMessage());
            } else {
                for (StackTraceElement element : e.getStackTrace()) {
                    builder.append("\n");
                    builder.append(element);
                }
            }
            Notification.show(builder.toString(), Notification.Type.ERROR_MESSAGE);
            e.printStackTrace();
        }
        initMainTable(createMainContainer(new ArrayList<>(partInstances)));
        initPendingTable(createPendingContainer(new ArrayList<>(partsPending)));
    }

    private void removePartButtonClick() {
        FreePart selected = (FreePart) mainTable.getValue();
        if (selected == null) return;
        for (PartInstance instance : partInstances) {
            if (Objects.equals(instance.getPart().getId(), selected.getPart().getId())) {
                partInstances.remove(instance);
                break;
            }
        }
        initMainTable(createMainContainer(new ArrayList<>(partInstances)));
        setPartsCostTextField();
        setEndCostTextField();
    }

    private void removePartPendingButtonClick() {
        PartPending selected = (PartPending) pendingTable.getValue();
        if (selected == null) return;
        for (PartPending partPending : partsPending) {
            if (Objects.equals(partPending.getPart().getId(), selected.getPart().getId())) {
                partsPending.remove(partPending);
                break;
            }
        }
        initPendingTable(createPendingContainer(new ArrayList<>(partsPending)));
        setPartsPendingCostTextField();
    }

    private ComboBox createStatusComboBox() {
        BeanItemContainer<StatusRepairOrder> container = new BeanItemContainer<>(StatusRepairOrder.class);
        container.addAll(Arrays.asList(StatusRepairOrder.values()));
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.status"), container);
        select.setNullSelectionAllowed(false);
        return select;
    }

    private ComboBox createTypeDeviceComboBox() {
        BeanItemContainer<TypeDevice> container = new BeanItemContainer<>(TypeDevice.class);
        List<TypeDevice> typeDevices = modelService.findAllTypeDevices();
        if (order != null) {
            if (order.getModel().getTypeDevice().getIsRemoved()) {
                typeDevices.add(order.getModel().getTypeDevice());
            }
        }
        typeDevices.forEach(typeDevice -> typeDeviceMap.put(typeDevice.getId(), typeDevice));
        container.addAll(typeDevices);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.typeDevice"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.addValueChangeListener(event -> {
            Producer producer = (Producer) producerComboBox.getValue();
            if (producer == null) return;
            TypeDevice typeDevice = (TypeDevice) event.getProperty().getValue();
            List<Model> models;
            if (typeDevice != null) {
                models = modelService.findModelByProducerIdAndTypeDeviceId(
                        producer.getId(), typeDevice.getId());
            } else {
                models = modelService.findModelByProducerId(producer.getId());
            }
            BeanItemContainer<Model> modelContainer = new BeanItemContainer<>(Model.class);
            if (order != null) {
                if (order.getModel().getIsRemoved()) models.add(order.getModel());
            }
            modelContainer.addAll(models);
            modelComboBox.setContainerDataSource(modelContainer);
            modelMap.clear();
            models.forEach(model -> modelMap.put(model.getId(), model));
        });
        return select;
    }

    private ComboBox createProducerComboBox() {
        BeanItemContainer<Producer> container = new BeanItemContainer<>(Producer.class);
        List<Producer> producers = modelService.findAllProducers();
        if (order != null) {
            if (order.getModel().getProducer().getIsRemoved()) {
                producers.add(order.getModel().getProducer());
            }
        }
        producers.forEach(producer -> producerMap.put(producer.getId(), producer));
        container.addAll(producers);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.producer"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.addValueChangeListener(event -> {
            TypeDevice typeDevice = (TypeDevice) typeDeviceComboBox.getValue();
            Producer producer = (Producer) event.getProperty().getValue();
            List<Model> models = new ArrayList<>();
            if (producer != null) {
                modelLayout.setEnabled(true);
                if (typeDevice != null) {
                    models = modelService.findModelByProducerIdAndTypeDeviceId(
                            producer.getId(), typeDevice.getId());
                } else {
                    models = modelService.findModelByProducerId(producer.getId());
                }
            } else modelLayout.setEnabled(false);
            BeanItemContainer<Model> modelContainer = new BeanItemContainer<>(Model.class);
            if (order != null) {
                if (order.getModel().getIsRemoved()) models.add(order.getModel());
            }
            modelContainer.addAll(models);
            modelComboBox.setContainerDataSource(modelContainer);
            modelMap.clear();
            models.forEach(model -> modelMap.put(model.getId(), model));
        });
        return select;
    }

    private ComboBox createModelComboBox() {
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.model"));
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        return select;
    }

    private ComboBox createFirmComboBox() {
        BeanItemContainer<Firm> container = new BeanItemContainer<>(Firm.class);
        List<Firm> firms = firmService.findAll();
        if (order != null) {
            if (order.getFirm().getIsRemoved()) firms.add(order.getFirm());
        }
        firms.forEach(firm -> firmMap.put(firm.getId(), firm));
        container.addAll(firms);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.firm"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.setNullSelectionAllowed(false);
        return select;
    }

    private ComboBox createMasterComboBox() {
        BeanItemContainer<User> container = new BeanItemContainer<>(User.class);
        List<User> users = userService.findAll();
        if (order != null) {
            User master = order.getMaster();
            if (master != null) {
                if (master.getIsRemoved()) users.add(master);
            }
        }
        users.forEach(user -> masterMap.put(user.getId(), user));
        container.addAll(users);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.master"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.setNullSelectionAllowed(false);
        return select;
    }

    private void initMainTable(BeanItemContainer<FreePart> container) {
        mainTable.setContainerDataSource(container);
        mainTable.setColumnCollapsingAllowed(true);
        mainTable.setColumnCollapsed(PART_COMMENT_COLUMN, true);
        mainTable.setColumnCollapsed(OUT_PRICE_COLUMN, true);
        mainTable.setVisibleColumns(mainFieldNames);
        mainTable.setColumnHeader(PART_NUMBER_COLUMN, PART_NUMBER_VISIBLE_COLUMN);
        mainTable.setColumnHeader(NAME_COLUMN, NAME_VISIBLE_COLUMN);
        mainTable.setColumnHeader(COUNT_COLUMN, COUNT_VISIBLE_COLUMN);
        mainTable.setColumnHeader(STOCK_COLUMN, STOCK_VISIBLE_COLUMN);
        mainTable.setColumnHeader(OPT_PRICE_COLUMN, OPT_PRICE_VISIBLE_COLUMN);
        mainTable.setColumnHeader(OUT_PRICE_COLUMN, OUT_PRICE_VISIBLE_COLUMN);
        mainTable.setColumnHeader(PART_COMMENT_COLUMN, PART_COMMENT_VISIBLE_COLUMN);
        mainTable.setSelectable(true);
        mainTable.setImmediate(true);
    }

    private void initPendingTable(BeanItemContainer<PartPending> container) {
        pendingTable.setContainerDataSource(container);
        pendingTable.setColumnCollapsingAllowed(true);
        pendingTable.setColumnCollapsed(COMMENT_COLUMN, true);
        pendingTable.setVisibleColumns(pendingFieldNames);
        pendingTable.setColumnHeader(PART_NUMBER_COLUMN, PART_NUMBER_VISIBLE_COLUMN);
        pendingTable.setColumnHeader(NAME_COLUMN, NAME_VISIBLE_COLUMN);
        pendingTable.setColumnHeader(NEEDED_COUNT_COLUMN, NEEDED_VISIBLE_COUNT_COLUMN);
        pendingTable.setColumnHeader(ORDERED_COUNT_COLUMN, ORDERED_VISIBLE_COUNT_COLUMN);
        pendingTable.setColumnHeader(RECEIVED_COUNT_COLUMN, RECEIVED_VISIBLE_COUNT_COLUMN);
        pendingTable.setColumnHeader(COMMENT_COLUMN, COMMENT_VISIBLE_COLUMN);
        pendingTable.setSelectable(true);
        pendingTable.setImmediate(true);
    }

    private BeanItemContainer<FreePart> createMainContainer(List<PartInstance> instances) {
        BeanItemContainer<FreePart> container = new BeanItemContainer<>(FreePart.class);
        container.addNestedContainerProperty(PART_NUMBER_COLUMN);
        container.addNestedContainerProperty(NAME_COLUMN);
        container.addNestedContainerProperty(STOCK_COLUMN);
        container.addNestedContainerProperty(OPT_PRICE_COLUMN);
        container.addNestedContainerProperty(OUT_PRICE_COLUMN);
        container.addNestedContainerProperty(PART_COMMENT_COLUMN);
        List<FreePart> freeParts = FreePart.instancesToFreeParts(instances);
        container.addAll(freeParts);
        container.sort(new Object[]{NAME_COLUMN, COUNT_COLUMN}, new boolean[]{true, true});
        return container;
    }

    private BeanItemContainer<PartPending> createPendingContainer(List<PartPending> partsPending) {
        BeanItemContainer<PartPending> container = new BeanItemContainer<>(PartPending.class);
        container.addNestedContainerProperty(PART_NUMBER_COLUMN);
        container.addNestedContainerProperty(NAME_COLUMN);
        container.addNestedContainerProperty(NEEDED_COUNT_COLUMN);
        container.addNestedContainerProperty(ORDERED_COUNT_COLUMN);
        container.addNestedContainerProperty(RECEIVED_COUNT_COLUMN);
        container.addNestedContainerProperty(COMMENT_COLUMN);
        container.addAll(partsPending);
        container.sort(new Object[]{NAME_COLUMN}, new boolean[]{true});
        return container;
    }

    private boolean isImeiValid(long imei) {
        return String.valueOf(imei).length() == 15;
    }

    private void setFields() {
        if (order != null) {
            idTextField.setValue(String.valueOf(order.getId()));
            historyButton.setEnabled(true);
            ticketButton.setEnabled(true);
            reportButton.setEnabled(order.getStatus() == StatusRepairOrder.Выдан);
            if (order.getStatus() == StatusRepairOrder.Выдан || order.getStatus() == StatusRepairOrder.Отменён) {
                statusComboBox.setEnabled(false);
                masterComboBox.setEnabled(false);
                workCostTextField.setEnabled(false);
                addPartButton.setEnabled(false);
                removePartButton.setEnabled(false);
            }
        } else {
            historyButton.setEnabled(false);
            ticketButton.setEnabled(false);
            reportButton.setEnabled(false);
        }
    }

    private void setPartsCostTextField() {
        DocumentOut doc = new DocumentOut();
        doc.setPartInstances(partInstances);
        Float partsCost = doc.getPartsCostOpt();
        partsCostTextField.setValue(String.valueOf(partsCost));
    }

    private void setPartsPendingCostTextField() {
        SalesOrder o = new SalesOrder();
        o.setPartsPending(partsPending);
        partsPendingCostTextField.setValue(String.valueOf(o.getPartsPendingCostOpt()));
    }

    private void setWorkCostTextField() {
        String endCostString = endCostTextField.getValue();
        if (endCostString.isEmpty()) {
            workCostTextField.setValue("0.0");
            return;
        }
        Float endCost = Float.valueOf(endCostString);
        Float partsCost = Float.valueOf(partsCostTextField.getValue());
        Float workCost = endCost - partsCost;
        workCostTextField.setValue(workCost.toString());
    }

    private void setEndCostTextField() {
        Float workCost = 0f;
        String workCostString = workCostTextField.getValue();
        try {
            if (!workCostString.isEmpty()) workCost = Float.valueOf(workCostString);
        } catch (NumberFormatException e) {
            Notification.show(ViewUtils.getMessage("notifications.wrongCost"), Notification.Type.ERROR_MESSAGE);
            return;
        }
        Float partsCost = Float.valueOf(partsCostTextField.getValue());
        Float endCost = partsCost + workCost;
        endCostTextField.setValue(endCost.toString());
    }

    private void setFieldsFromPrevLastRepair() {
        if (prevRepairs.isEmpty()) return;
        RepairOrder prevLastOrder = prevRepairs.get(prevRepairs.size() - 1);
        typeDeviceComboBox.setValue(typeDeviceMap.get(prevLastOrder.getModel().getTypeDevice().getId()));
        producerComboBox.setValue(producerMap.get(prevLastOrder.getModel().getProducer().getId()));
        modelComboBox.setValue(modelMap.get(prevLastOrder.getModel().getId()));
        if (prevLastOrder.getImei() != null) imeiTextField.setValue(prevLastOrder.getImei());
        if (prevLastOrder.getSn() != null) snTextField.setValue(prevLastOrder.getSn());
    }

    private void checkRepetitions() {
        prevRepairs.addAll(repairOrderService.findPreviousRepairs(imeiTextField.getValue(), snTextField.getValue()));
        if (!prevRepairs.isEmpty()) {
            StringBuilder builder = new StringBuilder(ViewUtils.getMessage("string.repetition"));
            for (RepairOrder o : prevRepairs) {
                builder.append(" ");
                builder.append(o.getId());
                builder.append(" - ");
                builder.append(sdf.format(o.getStartDate()));
                builder.append(", ");
            }
            commentForCustomerTextArea.setValue(builder.toString());
            ConfirmWindow window = new ConfirmWindow(ViewUtils.getMessage("confirm.message.loadPreviousRepairs"));
            window.setOkClickListener(new MyOkClickListener());
            getUI().addWindow(window);
        }
    }

    private boolean checkIsMoving(RepairOrder order) {
        Set<DocumentMoveDevices> documents = new HashSet<>(order.getDocumentMoveDevices());
        if (!documents.isEmpty()) {
            for (DocumentMoveDevices doc : documents) {
                if (doc.getStatus() != StatusDocument.Завершено) return true;
            }
        }
        return false;
    }

    private boolean isEditedAnotherUser() {
        if (order == null) return false;
        if (order.getId() == null) return false;
        Date date = repairOrderService.findLastRevisionById(order.getId()).getRevisionDate().toDate();
        return lastChangeDate.getTime() != date.getTime();
    }

    private class MyCustomerChoiceListener implements OnCustomerChoiceListener {
        @Override
        public void onChoice(Customer customer) {
            selectedCustomer = customer;
            customerTextField.setValue(selectedCustomer.getName());
            customerTextField.setDescription(selectedCustomer.description());
        }
    }

    private class MyOnPartChoiceListener implements OnPartChoiceListener {
        @Override
        public void onChoice(Set<PartInstance> instances) {
            partInstances.addAll(instances);
            initMainTable(createMainContainer(new ArrayList<>(partInstances)));
            setPartsCostTextField();
            setEndCostTextField();
        }
    }

    private class MyOnPartPendingChoiceListener implements OnPartPendingChoiceListener {
        @Override
        public void onChoice(PartPending partPending) {
            partsPending.add(partPending);
            initPendingTable(createPendingContainer(new ArrayList<>(partsPending)));
            setPartsPendingCostTextField();
        }
    }

    private class MyOkClickListener implements OkClickListener {
        @Override
        public void onOkClick() {
            setFieldsFromPrevLastRepair();
        }
    }

    private class MyCloseListener implements Window.CloseListener {
        @Override
        public void windowClose(Window.CloseEvent closeEvent) {
            TypeDevice typeDevice = (TypeDevice) typeDeviceComboBox.getValue();
            Producer producer = (Producer) producerComboBox.getValue();
            List<Model> models;
            if (typeDevice != null) {
                models = modelService.findModelByProducerIdAndTypeDeviceId(
                        producer.getId(), typeDevice.getId());
            } else {
                models = modelService.findModelByProducerId(producer.getId());
            }
            BeanItemContainer<Model> modelContainer = new BeanItemContainer<>(Model.class);
            modelContainer.addAll(models);
            modelComboBox.setContainerDataSource(modelContainer);
            modelMap.clear();
            models.forEach(model -> modelMap.put(model.getId(), model));
        }
    }
}
