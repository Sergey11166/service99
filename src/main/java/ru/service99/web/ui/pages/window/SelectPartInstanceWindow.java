package ru.service99.web.ui.pages.window;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.*;
import org.springframework.data.domain.Page;
import ru.service99.SecurityUtils;
import ru.service99.domain.*;
import ru.service99.service.ModelService;
import ru.service99.service.PartService;
import ru.service99.service.criteria.PartSearchCriteria;
import ru.service99.web.model.FreePart;
import ru.service99.web.ui.OnPartChoiceListener;
import ru.service99.web.ui.ViewUtils;

import java.util.*;

public class SelectPartInstanceWindow extends Window {

    private static final String PART_NUMBER_COLUMN = "part.partNumber";
    private static final String NAME_COLUMN = "part.name";
    private static final String COUNT_COLUMN = "count";
    private static final String STOCK_COLUMN = "stock.name";
    private static final String RESERVE_COLUMN = "reserve";
    private static final String IN_PRICE_COLUMN = "inPrice";
    private static final String OPT_PRICE_COLUMN = "optPrice";
    private static final String OUT_PRICE_COLUMN = "outPrice";
    private static final String COMMENT_COLUMN = "part.comment";

    private static final String PART_NUMBER_VISIBLE_COLUMN = ViewUtils.getMessage("table.partNumber");
    private static final String NAME_VISIBLE_COLUMN = ViewUtils.getMessage("table.name");
    private static final String COUNT_VISIBLE_COLUMN = ViewUtils.getMessage("table.count");
    private static final String STOCK_VISIBLE_COLUMN = ViewUtils.getMessage("table.stock");
    private static final String RESERVE_VISIBLE_COLUMN = ViewUtils.getMessage("table.reserve");
    private static final String IN_PRICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.inPrice");
    private static final String OPT_PRICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.optPrice");
    private static final String OUT_PRICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.outPrice");
    private static final String COMMENT_VISIBLE_COLUMN = ViewUtils.getMessage("table.comment");

    private static final String[] fieldNamesWithInPrice = new String[]{PART_NUMBER_COLUMN, NAME_COLUMN, STOCK_COLUMN,
            COUNT_COLUMN, RESERVE_COLUMN, IN_PRICE_COLUMN, OPT_PRICE_COLUMN, OUT_PRICE_COLUMN, COMMENT_COLUMN};
    private static final String[] fieldNamesWithoutInPrice = new String[]{PART_NUMBER_COLUMN, NAME_COLUMN, STOCK_COLUMN,
            COUNT_COLUMN, RESERVE_COLUMN, OPT_PRICE_COLUMN, OUT_PRICE_COLUMN, COMMENT_COLUMN};

    private PartService partService;
    private ModelService modelService;
    private Stock stock;
    private Set<PartInstance> selectedPartInstances;

    private Map<Long, Stock> stockMap = new HashMap<>();

    private OnPartChoiceListener partChoiceListener;

    private Table table = new Table();

    private TextField nameTextField;
    private TextField numberTextField;
    private TextField countTextField;

    private ComboBox typeDeviceComboBox;
    private ComboBox producerComboBox;
    private ComboBox modelComboBox;
    private ComboBox stockComboBox;

    public SelectPartInstanceWindow(ModelService modelService, PartService partService, Stock stock,
                                    Set<PartInstance> selectedPartInstances) {
        super(ViewUtils.getMessage("window.title.select"));
        this.partService = partService;
        this.modelService = modelService;
        this.stock = stock;
        this.selectedPartInstances = selectedPartInstances;
        init();
    }

    public void init() {
        initLayout();
        applyFilter();
    }

    private void initLayout() {
        center();
        setModal(true);
        setHeight("90%");
        setWidth("90%");

        HorizontalLayout horizontal = new HorizontalLayout();
        horizontal.setSpacing(true);
        horizontal.setMargin(true);
        horizontal.setSizeFull();

        VerticalLayout filterLayout = new VerticalLayout();
        filterLayout.setSpacing(true);

        HorizontalLayout fieldLayout = new HorizontalLayout();
        fieldLayout.setSpacing(true);
        fieldLayout.setDefaultComponentAlignment(Alignment.BOTTOM_CENTER);

        VerticalLayout tableLayout = new VerticalLayout();
        tableLayout.setSpacing(true);
        tableLayout.setSizeFull();

        nameTextField = new TextField(ViewUtils.getMessage("field.name"));
        numberTextField = new TextField(ViewUtils.getMessage("field.partnumber"));
        countTextField = new TextField(ViewUtils.getMessage("field.count"));
        countTextField.setValue("1");

        typeDeviceComboBox = createTypeDeviceComboBox();
        producerComboBox = createProducerPlaceComboBox();
        modelComboBox = createModelComboBox();
        stockComboBox = createStockComboBox(ViewUtils.getMessage("field.stock"));

        Button applyButton = new Button(ViewUtils.getMessage("button.search"));
        applyButton.addClickListener(clickEvent -> applyFilter());

        Button selectButton = new Button(ViewUtils.getMessage("button.add"));
        selectButton.addClickListener(event -> selectClick());

        filterLayout.addComponent(stockComboBox);
        filterLayout.addComponent(nameTextField);
        filterLayout.addComponent(numberTextField);
        filterLayout.addComponent(typeDeviceComboBox);
        filterLayout.addComponent(producerComboBox);
        filterLayout.addComponent(modelComboBox);
        filterLayout.addComponent(applyButton);

        fieldLayout.addComponent(countTextField);
        fieldLayout.addComponent(selectButton);

        tableLayout.addComponent(fieldLayout);
        tableLayout.addComponent(table);
        tableLayout.setExpandRatio(fieldLayout, 1);
        tableLayout.setExpandRatio(table, 8);

        horizontal.addComponent(filterLayout);
        horizontal.addComponent(tableLayout);
        horizontal.setExpandRatio(filterLayout, 1);
        horizontal.setExpandRatio(tableLayout, 5);

        setContent(horizontal);

        if (stock != null) {
            stockComboBox.setValue(stockMap.get(stock.getId()));
            stockComboBox.setEnabled(false);
        } else {
            stockComboBox.setValue(stockMap.get(SecurityUtils.getCurrentUser().getAllowedStocks().iterator().next().getId()));
        }
    }

    private void selectClick() {
        FreePart freePart = (FreePart) table.getValue();
        if (freePart == null) {
            Notification.show(ViewUtils.getMessage("notifications.selectPart"), Notification.Type.ERROR_MESSAGE);
            return;
        }
        int count;
        try {
            count = Integer.parseInt(countTextField.getValue());
        } catch (NumberFormatException e) {
            Notification.show(ViewUtils.getMessage("notifications.wrongCount"), Notification.Type.ERROR_MESSAGE);
            return;
        }
        if (count == 0) {
            Notification.show(ViewUtils.getMessage("notifications.selectCount"), Notification.Type.ERROR_MESSAGE);
            return;
        }
        if ((freePart.getCount() - freePart.getReserve()) < count) {
            Notification.show(ViewUtils.getMessage("notifications.notEnough"), Notification.Type.ERROR_MESSAGE);
            return;
        }
        PartSearchCriteria criteria = new PartSearchCriteria();
        criteria.setPartNumber(freePart.getPart().getPartNumber());
        criteria.setStock(freePart.getStock());
        criteria.setInPrice(freePart.getInPrice());
        criteria.setOptPrice(freePart.getOptPrice());
        criteria.setOutPrice(freePart.getOutPrice());
        List<PartInstance> allPartInstances = new ArrayList<>(partService
                .findFreeInstancesByCriteria(criteria, null).getContent());
        Set<PartInstance> partInstances = new HashSet<>(count);

        List<PartInstance> needRemove = new ArrayList<>();
        for (PartInstance instance : allPartInstances) {
            if (instance.isReserve()) {
                needRemove.add(instance);
                continue;
            }
            for (PartInstance selectedInstance : selectedPartInstances) {
                if (Objects.equals(instance.getId(), selectedInstance.getId())) needRemove.add(instance);
            }
        }
        allPartInstances.removeAll(needRemove);

        for (int i = 0; i < count; i++) {
            partInstances.add(allPartInstances.get(i));
        }
        partChoiceListener.onChoice(partInstances);
        close();
    }

    private ComboBox createTypeDeviceComboBox() {
        BeanItemContainer<TypeDevice> container = new BeanItemContainer<>(TypeDevice.class);
        List<TypeDevice> typeDevices = modelService.findAllTypeDevices();
        container.addAll(typeDevices);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.typeDevice"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.addValueChangeListener(event -> {
            Producer producer = (Producer) producerComboBox.getValue();
            if (producer == null) return;
            TypeDevice typeDevice = (TypeDevice) event.getProperty().getValue();
            List<Model> models;
            if (typeDevice != null) {
                models = modelService.findModelByProducerIdAndTypeDeviceId(
                        producer.getId(), typeDevice.getId());
            } else {
                models = modelService.findModelByProducerId(producer.getId());
            }
            BeanItemContainer<Model> modelContainer = new BeanItemContainer<>(Model.class);
            modelContainer.addAll(models);
            modelComboBox.setContainerDataSource(modelContainer);
        });
        return select;
    }

    private ComboBox createProducerPlaceComboBox() {
        BeanItemContainer<Producer> container = new BeanItemContainer<>(Producer.class);
        List<Producer> producers = modelService.findAllProducers();
        container.addAll(producers);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.producer"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.addValueChangeListener(event -> {
            TypeDevice typeDevice = (TypeDevice) typeDeviceComboBox.getValue();
            Producer producer = (Producer) event.getProperty().getValue();
            List<Model> models = new ArrayList<>();
            if (producer != null) {
                modelComboBox.setEnabled(true);
                if (typeDevice != null) {
                    models = modelService.findModelByProducerIdAndTypeDeviceId(
                            producer.getId(), typeDevice.getId());
                } else {
                    models = modelService.findModelByProducerId(producer.getId());
                }
            } else modelComboBox.setEnabled(false);
            BeanItemContainer<Model> modelContainer = new BeanItemContainer<>(Model.class);
            modelContainer.addAll(models);
            modelComboBox.setContainerDataSource(modelContainer);
        });
        return select;
    }

    private ComboBox createModelComboBox() {
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.model"));
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.setEnabled(false);
        return select;
    }

    private ComboBox createStockComboBox(String label) {
        BeanItemContainer<Stock> container = new BeanItemContainer<>(Stock.class);
        List<Stock> stocks = partService.findAllStocks();
        stocks.forEach(stock -> {
            if (this.stock == null) {
                if (SecurityUtils.hasAllowedStock(stock)) {
                    container.addBean(stock);
                    stockMap.put(stock.getId(), stock);
                }
            } else {
                container.addBean(stock);
                stockMap.put(stock.getId(), stock);
            }
        });
        ComboBox select = new ComboBox(label, container);
        select.setNullSelectionAllowed(false);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.setPageLength(15);
        return select;
    }

    private void initTable(BeanItemContainer<FreePart> container) {
        table.setContainerDataSource(container);
        table.setSizeFull();
        table.setColumnCollapsingAllowed(true);
        table.setColumnCollapsed(COMMENT_COLUMN, true);
        if (SecurityUtils.hasAuthority(Authority.видеть_входную_цену)) {
            table.setVisibleColumns(fieldNamesWithInPrice);
            table.setColumnHeader(IN_PRICE_COLUMN, IN_PRICE_VISIBLE_COLUMN);
        } else {
            table.setVisibleColumns(fieldNamesWithoutInPrice);
        }
        table.setColumnHeader(PART_NUMBER_COLUMN, PART_NUMBER_VISIBLE_COLUMN);
        table.setColumnHeader(NAME_COLUMN, NAME_VISIBLE_COLUMN);
        table.setColumnHeader(COUNT_COLUMN, COUNT_VISIBLE_COLUMN);
        table.setColumnHeader(STOCK_COLUMN, STOCK_VISIBLE_COLUMN);
        table.setColumnHeader(RESERVE_COLUMN, RESERVE_VISIBLE_COLUMN);
        table.setColumnHeader(IN_PRICE_COLUMN, IN_PRICE_VISIBLE_COLUMN);
        table.setColumnHeader(OPT_PRICE_COLUMN, OPT_PRICE_VISIBLE_COLUMN);
        table.setColumnHeader(OUT_PRICE_COLUMN, OUT_PRICE_VISIBLE_COLUMN);
        table.setColumnHeader(COMMENT_COLUMN, COMMENT_VISIBLE_COLUMN);
        table.setSelectable(true);
        table.setImmediate(true);
    }

    private void applyFilter() {
        Stock stock = (Stock) stockComboBox.getValue();
        TypeDevice typeDevice = (TypeDevice) typeDeviceComboBox.getValue();
        Producer producer = (Producer) producerComboBox.getValue();
        Model model = (Model) modelComboBox.getValue();
        String name = nameTextField.getValue();
        String number = numberTextField.getValue();

        PartSearchCriteria criteria = new PartSearchCriteria();
        criteria.setName(name);
        criteria.setPartNumber(number);
        criteria.setModel(model);
        criteria.setProducer(producer);
        criteria.setTypeDevice(typeDevice);
        criteria.setStock(stock);

        if (partService.getCountFreeInstancesByCriteria(criteria) > 1000) {
            Notification.show(ViewUtils.getMessage("notifications.tooMatchElements"), Notification.Type.ERROR_MESSAGE);
        } else {
            Page<PartInstance> page = partService.findFreeInstancesByCriteria(criteria, null);
            List<PartInstance> allPartInstances = new ArrayList<>(page.getContent());
            List<PartInstance> needRemove = new ArrayList<>();
            for (PartInstance instance : allPartInstances) {
                for (PartInstance movedInstance : selectedPartInstances) {
                    if (Objects.equals(instance.getId(), movedInstance.getId())) needRemove.add(instance);
                }
            }
            allPartInstances.removeAll(needRemove);
            initTable(createContainer(allPartInstances));
        }
    }

    private BeanItemContainer<FreePart> createContainer(List<PartInstance> instances) {
        BeanItemContainer<FreePart> container = new BeanItemContainer<>(FreePart.class);
        container.addNestedContainerProperty(PART_NUMBER_COLUMN);
        container.addNestedContainerProperty(NAME_COLUMN);
        container.addNestedContainerProperty(STOCK_COLUMN);
        container.addNestedContainerProperty(IN_PRICE_COLUMN);
        container.addNestedContainerProperty(OPT_PRICE_COLUMN);
        container.addNestedContainerProperty(OUT_PRICE_COLUMN);
        container.addNestedContainerProperty(COMMENT_COLUMN);
        List<FreePart> freeParts = FreePart.instancesToFreeParts(instances);
        container.addAll(freeParts);
        container.sort(new Object[]{NAME_COLUMN, COUNT_COLUMN}, new boolean[]{true, true});
        return container;
    }

    public void setPartChoiceListener(OnPartChoiceListener partChoiceListener) {
        this.partChoiceListener = partChoiceListener;
    }
}
