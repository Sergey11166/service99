package ru.service99.web.ui.pages.window;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.*;
import org.springframework.data.history.Revision;
import org.springframework.data.history.Revisions;
import ru.service99.domain.MyRevisionEntity;
import ru.service99.domain.RepairOrder;
import ru.service99.service.RepairOrderService;
import ru.service99.web.ui.ViewUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class HistoryRepairWindow extends Window {

    private List<MyRevisionEntity> myRevisionEntities;
    private Map<Integer, RepairOrder> integerRepairOrderMap;

    private static final String ID_COLUMN = "id";
    private static final String DATA_COLUMN = "revisionDate";
    private static final String AUTHOR_COLUMN = "auditor.name";

    private static final String ID_VISIBLE_COLUMN = ViewUtils.getMessage("table.id");
    private static final String DATA_VISIBLE_COLUMN = ViewUtils.getMessage("table.date");
    private static final String AUTHOR_VISIBLE_COLUMN = ViewUtils.getMessage("table.author");

    private static final String[] fieldNames = new String[]{ID_COLUMN, DATA_COLUMN, AUTHOR_COLUMN};

    private Table table;

    private TextField imeiTextField;
    private TextField snTextField;
    private TextField newImeiTextField;
    private TextField newSnTextField;
    private TextField currentPlaceTextField;
    private TextField masterTextField;
    private TextField statusTextField;
    private TextField modelTextField;
    private TextField customerTextField;
    private TextField startCostTextField;
    private TextField endCostTextField;
    private TextField firmTextField;
    private TextArea visibleDamageTextArea;
    private TextArea equipmentTextArea;
    private TextArea statedDefectTextArea;
    private TextArea receiverCommentTextArea;
    private TextArea discoveredDefectTextArea;
    private TextArea workDoneTextArea;
    private TextArea masterCommentTextArea;
    private TextArea commentForCustomerTextArea;
    private TextArea partsTextArea;
    private TextArea partsPendingTextArea;

    public HistoryRepairWindow(RepairOrderService repairOrderService, long repairId) {
        super(ViewUtils.getMessage("repairsPage.window.title.history") + " " + repairId);
        this.myRevisionEntities = repairOrderService.findMyRevisionEntitiesById(repairId);
        this.integerRepairOrderMap = createRevisionsMap(repairOrderService.findRevisionsById(repairId));
        init();
    }

    private void init() {
        initLayout();
        initTable(createContainer(myRevisionEntities));
    }

    private void initLayout() {
        center();
        setModal(true);
        setHeight(100, Unit.PERCENTAGE);

        HorizontalLayout container = new HorizontalLayout();
        container.setSpacing(true);
        container.setMargin(true);

        VerticalLayout leftContainer = new VerticalLayout();
        leftContainer.setSpacing(true);

        HorizontalLayout rightContainer = new HorizontalLayout();
        rightContainer.setSpacing(true);

        VerticalLayout rightLeftLayout = new VerticalLayout();
        rightLeftLayout.setSpacing(true);

        VerticalLayout rightRightLayout = new VerticalLayout();
        rightRightLayout.setSpacing(true);

        table = new Table();
        /*table = new Table(){
            @Override
            protected String formatPropertyValue(Object rowId, Object colId, Property property) {
                if (property.getType() == DateTime.class)
                    return sdf.format(((DateTime)property.getValue()).toDate());
                return super.formatPropertyValue(rowId, colId, property);
            }
        };*/

        imeiTextField = new TextField(ViewUtils.getMessage("field.imei"));
        snTextField = new TextField(ViewUtils.getMessage("field.sn"));
        newImeiTextField = new TextField(ViewUtils.getMessage("field.newImei"));
        newSnTextField = new TextField(ViewUtils.getMessage("field.newSn"));
        currentPlaceTextField = new TextField(ViewUtils.getMessage("field.currentPlace"));
        masterTextField = new TextField(ViewUtils.getMessage("field.master"));
        statusTextField = new TextField(ViewUtils.getMessage("field.status"));
        modelTextField = new TextField(ViewUtils.getMessage("field.model"));
        customerTextField = new TextField(ViewUtils.getMessage("field.customer"));
        startCostTextField = new TextField(ViewUtils.getMessage("field.startCost"));
        endCostTextField = new TextField(ViewUtils.getMessage("field.endCost"));
        firmTextField = new TextField(ViewUtils.getMessage("field.firm"));
        visibleDamageTextArea = new TextArea(ViewUtils.getMessage("field.visibleDamage"));
        equipmentTextArea = new TextArea(ViewUtils.getMessage("field.equipment"));
        statedDefectTextArea = new TextArea(ViewUtils.getMessage("field.statedDefect"));
        receiverCommentTextArea = new TextArea(ViewUtils.getMessage("field.receiverComment"));
        discoveredDefectTextArea = new TextArea(ViewUtils.getMessage("field.discoveredDefect"));
        workDoneTextArea = new TextArea(ViewUtils.getMessage("field.workDone"));
        masterCommentTextArea = new TextArea(ViewUtils.getMessage("field.masterComment"));
        commentForCustomerTextArea = new TextArea(ViewUtils.getMessage("field.commentForCustomer"));
        partsTextArea = new TextArea(ViewUtils.getMessage("field.listParts"));
        partsPendingTextArea = new TextArea(ViewUtils.getMessage("field.listPartsPending"));

        imeiTextField.setEnabled(false);
        snTextField.setEnabled(false);
        newImeiTextField.setEnabled(false);
        newSnTextField.setEnabled(false);
        currentPlaceTextField.setEnabled(false);
        masterTextField.setEnabled(false);
        statusTextField.setEnabled(false);
        modelTextField.setEnabled(false);
        customerTextField.setEnabled(false);
        startCostTextField.setEnabled(false);
        endCostTextField.setEnabled(false);
        firmTextField.setEnabled(false);
        visibleDamageTextArea.setEnabled(false);
        equipmentTextArea.setEnabled(false);
        statedDefectTextArea.setEnabled(false);
        receiverCommentTextArea.setEnabled(false);
        discoveredDefectTextArea.setEnabled(false);
        workDoneTextArea.setEnabled(false);
        masterCommentTextArea.setEnabled(false);
        commentForCustomerTextArea.setEnabled(false);
        partsTextArea.setEnabled(false);
        partsPendingTextArea.setEnabled(false);

        commentForCustomerTextArea.setWidth("450px");
        masterCommentTextArea.setWidth("450px");
        receiverCommentTextArea.setWidth("450");
        workDoneTextArea.setWidth("450px");
        table.setHeight("400px");
        table.setWidth("450px");
        commentForCustomerTextArea.setRows(3);
        receiverCommentTextArea.setRows(2);
        masterCommentTextArea.setRows(2);
        visibleDamageTextArea.setRows(1);
        equipmentTextArea.setRows(1);
        workDoneTextArea.setRows(2);
        partsTextArea.setRows(4);
        partsPendingTextArea.setRows(4);

        rightLeftLayout.addComponent(currentPlaceTextField);
        rightLeftLayout.addComponent(imeiTextField);
        rightLeftLayout.addComponent(snTextField);
        rightLeftLayout.addComponent(newImeiTextField);
        rightLeftLayout.addComponent(newSnTextField);
        rightLeftLayout.addComponent(masterTextField);
        rightLeftLayout.addComponent(equipmentTextArea);
        rightLeftLayout.addComponent(statedDefectTextArea);
        rightLeftLayout.addComponent(partsTextArea);

        rightRightLayout.addComponent(statusTextField);
        rightRightLayout.addComponent(modelTextField);
        rightRightLayout.addComponent(customerTextField);
        rightRightLayout.addComponent(startCostTextField);
        rightRightLayout.addComponent(endCostTextField);
        rightRightLayout.addComponent(firmTextField);
        rightRightLayout.addComponent(visibleDamageTextArea);
        rightRightLayout.addComponent(discoveredDefectTextArea);
        rightRightLayout.addComponent(partsPendingTextArea);

        leftContainer.addComponent(table);
        leftContainer.addComponent(receiverCommentTextArea);
        leftContainer.addComponent(workDoneTextArea);
        leftContainer.addComponent(masterCommentTextArea);
        leftContainer.addComponent(commentForCustomerTextArea);

        rightContainer.addComponent(rightLeftLayout);
        rightContainer.addComponent(rightRightLayout);

        container.addComponent(leftContainer);
        container.addComponent(rightContainer);

        setContent(container);
    }

    private Map<Integer, RepairOrder> createRevisionsMap(Revisions<Integer, RepairOrder> revisions) {
        Map<Integer, RepairOrder> result = new TreeMap<>();
        for (Revision<Integer, RepairOrder> rev : revisions) {
            result.put(rev.getRevisionNumber(), rev.getEntity());
        }
        return result;
    }

    private void updateContent(int key) {
        RepairOrder order = integerRepairOrderMap.get(key);
        currentPlaceTextField.setValue(order.getCurrentPlace().getName());
        modelTextField.setValue(order.getModel().getName());
        imeiTextField.setValue(order.getImei() == null ? "" : order.getImei());
        snTextField.setValue(order.getSn() == null ? "" : order.getSn());
        visibleDamageTextArea.setValue(order.getVisibleDamage() == null ? "" : order.getVisibleDamage());
        equipmentTextArea.setValue(order.getEquipment() == null ? "" : order.getEquipment());
        customerTextField.setValue(order.getCustomer().getName());
        statedDefectTextArea.setValue(order.getStatedDefect());
        receiverCommentTextArea.setValue(order.getReceiverComment() == null ? "" : order.getReceiverComment());
        startCostTextField.setValue(order.getStartCost() == null ? "" : order.getStartCost().toString());
        Float endCostWork = order.getEndCost() == null ? 0 : order.getEndCost();
        endCostTextField.setValue(endCostWork == 0 ? "" : endCostWork.toString());
        firmTextField.setValue(order.getFirm().getName());
        discoveredDefectTextArea.setValue(order.getDiscoveredDefect() == null ? "" : order.getDiscoveredDefect());
        workDoneTextArea.setValue(order.getWorksDone() == null ? "" : order.getWorksDone());
        masterCommentTextArea.setValue(order.getMasterComment() == null ? "" : order.getMasterComment());
        newImeiTextField.setValue(order.getNewImei() == null ? "" : order.getNewImei());
        newSnTextField.setValue(order.getNewSn() == null ? "" : order.getNewSn());
        commentForCustomerTextArea.setValue(order.getCommentForCustomer() == null ? "" :
                order.getCommentForCustomer());
        statusTextField.setValue(order.getStatus().toString());
        masterTextField.setValue(order.getMaster() == null ? "" : order.getMaster().getName());
        partsTextArea.setValue(order.getPartsString() == null ? "" : order.getPartsString());
        partsPendingTextArea.setValue(order.getPartsPendingString() == null ? "" : order.getPartsPendingString());
        paintFieldIfNeeded(key);
    }

    private void paintFieldIfNeeded(int keyOfMap) {
        List<Integer> keys = new ArrayList<>(integerRepairOrderMap.keySet());
        if (keys.get(0) == keyOfMap) {
            clearColorAllFields();
            return;
        }
        int keyPrev = keys.get(keys.indexOf(keyOfMap) - 1);
        RepairOrder orderPrev = integerRepairOrderMap.get(keyPrev);
        String imeiPrev = orderPrev.getImei();
        String snPrev = orderPrev.getSn();
        String newImeiPrev = orderPrev.getNewImei();
        String newSnPrev = orderPrev.getNewSn();
        String visibleDamagePrev = orderPrev.getVisibleDamage();
        String equipmentPrev = orderPrev.getEquipment();
        String statedDefectPrev = orderPrev.getStatedDefect();
        String discoveredDefectPrev = orderPrev.getDiscoveredDefect();
        String receiverCommentPrev = orderPrev.getReceiverComment();
        String workDonePrev = orderPrev.getWorksDone();
        String masterCommentPrev = orderPrev.getMasterComment();
        String commentForCustomerPrev = orderPrev.getCommentForCustomer();
        String currentPlacePrev = String.valueOf(orderPrev.getCurrentPlace().getId());
        String masterPrev = orderPrev.getMaster() != null ? String.valueOf(orderPrev.getMaster().getId()) : null;
        String modelPrev = String.valueOf(orderPrev.getModel().getId());
        String customerPrev = String.valueOf(orderPrev.getCustomer().getId());
        String firmPrev = String.valueOf(orderPrev.getFirm().getId());
        String statusPrev = String.valueOf(orderPrev.getStatus().ordinal());
        String startCostPrev = orderPrev.getStartCost() != null ? String.valueOf(orderPrev.getStartCost()) : null;
        String endCostPrev = orderPrev.getEndCost() != null ? String.valueOf(orderPrev.getEndCost()) : null;
        String partsStringPrev = orderPrev.getPartsString() != null ? orderPrev.getPartsString() : null;
        String partsPendingStringPrev = orderPrev.getPartsPendingString() != null ?
                orderPrev.getPartsPendingString() : null;

        RepairOrder order = integerRepairOrderMap.get(keyOfMap);
        String imei = order.getImei();
        String sn = order.getSn();
        String newImei = order.getNewImei();
        String newSn = order.getNewSn();
        String visibleDamage = order.getVisibleDamage();
        String equipment = order.getEquipment();
        String statedDefect = order.getStatedDefect();
        String discoveredDefect = order.getDiscoveredDefect();
        String receiverComment = order.getReceiverComment();
        String workDone = order.getWorksDone();
        String masterComment = order.getMasterComment();
        String commentForCustomer = order.getCommentForCustomer();
        String currentPlace = String.valueOf(order.getCurrentPlace().getId());
        String master = order.getMaster() != null ? String.valueOf(order.getMaster().getId()) : null;
        String model = String.valueOf(order.getModel().getId());
        String customer = String.valueOf(order.getCustomer().getId());
        String firm = String.valueOf(order.getFirm().getId());
        String status = String.valueOf(order.getStatus().ordinal());
        String startCost = order.getStartCost() != null ? String.valueOf(order.getStartCost()) : null;
        String endCost = order.getEndCost() != null ? String.valueOf(order.getEndCost()) : null;
        String partsString = order.getPartsString() != null ? order.getPartsString() : null;
        String partsPendingString = order.getPartsPendingString() != null ? order.getPartsPendingString() : null;

        paintField(imeiTextField, isChangeFields(imeiPrev, imei));
        paintField(snTextField, isChangeFields(snPrev, sn));
        paintField(newImeiTextField, isChangeFields(newImeiPrev, newImei));
        paintField(newSnTextField, isChangeFields(newSnPrev, newSn));
        paintField(currentPlaceTextField, isChangeFields(currentPlacePrev, currentPlace));
        paintField(masterTextField, isChangeFields(masterPrev, master));
        paintField(statusTextField, isChangeFields(statusPrev, status));
        paintField(modelTextField, isChangeFields(modelPrev, model));
        paintField(customerTextField, isChangeFields(customerPrev, customer));
        paintField(startCostTextField, isChangeFields(startCostPrev, startCost));
        paintField(endCostTextField, isChangeFields(endCostPrev, endCost));
        paintField(firmTextField, isChangeFields(firmPrev, firm));
        paintField(visibleDamageTextArea, isChangeFields(visibleDamagePrev, visibleDamage));
        paintField(equipmentTextArea, isChangeFields(equipmentPrev, equipment));
        paintField(statedDefectTextArea, isChangeFields(statedDefectPrev, statedDefect));
        paintField(receiverCommentTextArea, isChangeFields(receiverCommentPrev, receiverComment));
        paintField(discoveredDefectTextArea, isChangeFields(discoveredDefectPrev, discoveredDefect));
        paintField(workDoneTextArea, isChangeFields(workDonePrev, workDone));
        paintField(masterCommentTextArea, isChangeFields(masterCommentPrev, masterComment));
        paintField(commentForCustomerTextArea, isChangeFields(commentForCustomerPrev, commentForCustomer));
        paintField(partsTextArea, isChangeFields(partsStringPrev, partsString));
        paintField(partsPendingTextArea, isChangeFields(partsPendingStringPrev, partsPendingString));
    }

    private boolean isChangeFields(String val1, String val2) {
        return !(val1 == null && val2 == null) && (val1 == null || val2 == null || !val1.equals(val2));
    }

    private void paintField(AbstractTextField field, boolean isChanged) {
        if (isChanged) field.addStyleName("background_green");
        else field.removeStyleName("background_green");
    }

    private void clearColorAllFields() {
        paintField(imeiTextField, false);
        paintField(snTextField, false);
        paintField(newImeiTextField, false);
        paintField(newSnTextField, false);
        paintField(currentPlaceTextField, false);
        paintField(masterTextField, false);
        paintField(statusTextField, false);
        paintField(modelTextField, false);
        paintField(customerTextField, false);
        paintField(startCostTextField, false);
        paintField(endCostTextField, false);
        paintField(firmTextField, false);
        paintField(visibleDamageTextArea, false);
        paintField(equipmentTextArea, false);
        paintField(statedDefectTextArea, false);
        paintField(receiverCommentTextArea, false);
        paintField(discoveredDefectTextArea, false);
        paintField(workDoneTextArea, false);
        paintField(masterCommentTextArea, false);
        paintField(commentForCustomerTextArea, false);
        paintField(partsTextArea, false);
        paintField(partsPendingTextArea, false);
    }

    private void initTable(BeanItemContainer<MyRevisionEntity> container) {
        table.setContainerDataSource(container);
        table.setVisibleColumns(fieldNames);
        table.setColumnHeader(ID_COLUMN, ID_VISIBLE_COLUMN);
        table.setColumnHeader(DATA_COLUMN, DATA_VISIBLE_COLUMN);
        table.setColumnHeader(AUTHOR_COLUMN, AUTHOR_VISIBLE_COLUMN);
        table.setSelectable(true);
        table.setImmediate(true);
        table.addValueChangeListener(event -> {
            MyRevisionEntity myRevisionEntity = (MyRevisionEntity) table.getValue();
            if (myRevisionEntity == null) return;
            int key = myRevisionEntity.getId();
            updateContent(key);
        });
    }

    private BeanItemContainer<MyRevisionEntity> createContainer(List<MyRevisionEntity> myRevisionEntities) {
        BeanItemContainer<MyRevisionEntity> container = new BeanItemContainer<>(MyRevisionEntity.class);
        container.addNestedContainerProperty(AUTHOR_COLUMN);
        container.addAll(myRevisionEntities);
        container.sort(new Object[]{ID_COLUMN}, new boolean[]{true});
        return container;
    }
}
