package ru.service99.web.ui.pages.window;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.*;
import ru.service99.SecurityUtils;
import ru.service99.domain.*;
import ru.service99.service.DocumentService;
import ru.service99.service.PlaceService;
import ru.service99.service.RepairOrderService;
import ru.service99.web.ui.OnRepairChoiceListener;
import ru.service99.web.ui.ViewUtils;

import java.text.SimpleDateFormat;
import java.util.*;

public class MoveDevicesWindow extends Window {

    private static final String ID_COLUMN = "id";
    private static final String DATE_COLUMN = "startDate";
    private static final String IMEI_COLUMN = "imei";
    private static final String SN_COLUMN = "sn";
    private static final String PRODUCER_COLUMN = "model.producer.name";
    private static final String MODEL_COLUMN = "model.name";

    private static final String ID_VISIBLE_COLUMN = ViewUtils.getMessage("table.id");
    private static final String DATE_VISIBLE_COLUMN = ViewUtils.getMessage("table.startDate");
    private static final String IMEI_VISIBLE_COLUMN = ViewUtils.getMessage("table.imei");
    private static final String SN_VISIBLE_COLUMN = ViewUtils.getMessage("table.sn");
    private static final String PRODUCER_VISIBLE_COLUMN = ViewUtils.getMessage("table.producer");
    private static final String MODEL_VISIBLE_COLUMN = ViewUtils.getMessage("table.model");

    private static final String[] fieldNames = new String[]{ID_COLUMN, DATE_COLUMN, IMEI_COLUMN,
            SN_COLUMN, PRODUCER_COLUMN, MODEL_COLUMN};

    private SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());

    private DocumentService documentService;
    private RepairOrderService repairOrderService;
    private PlaceService placeService;

    private DocumentMoveDevices documentMoveDevices;
    private Set<RepairOrder> repairOrders;

    private Table table;

    private ComboBox placeFromComboBox;
    private ComboBox placeToComboBox;
    private ComboBox statusComboBox;
    private TextField commentTextField;
    private Button saveButton;
    private Button addButton;
    private Button removeButton;

    private HashMap<Long, Place> placeFromMap = new HashMap<>();
    private HashMap<Long, Place> placeToMap = new HashMap<>();

    public MoveDevicesWindow(DocumentService documentService, RepairOrderService repairOrderService,
                             PlaceService placeService) {
        setCaption(ViewUtils.getMessage("movesDevicesPage.window.title.new"));
        this.documentService = documentService;
        this.repairOrderService = repairOrderService;
        this.placeService = placeService;
        this.repairOrders = new HashSet<>();
        init();
    }

    public MoveDevicesWindow(DocumentService documentService, RepairOrderService repairOrderService,
                             PlaceService placeService, DocumentMoveDevices documentMoveDevices) {
        this(documentService, repairOrderService, placeService);
        setCaption(ViewUtils.getMessage("movesDevicesPage.window.title.edit") + " " + documentMoveDevices.getId());
        this.documentMoveDevices = documentMoveDevices;
        this.repairOrders.addAll(documentMoveDevices.getRepairOrders());
        init();
    }

    public void init() {
        initLayout();
        initTable(createContainer(new ArrayList<>(repairOrders)));
    }

    private void initLayout() {
        center();
        setModal(true);
        setHeight(100, Unit.PERCENTAGE);

        VerticalLayout vertical = new VerticalLayout();
        vertical.setSpacing(true);
        vertical.setMargin(true);

        HorizontalLayout fieldLayout = new HorizontalLayout();
        fieldLayout.setSpacing(true);
        fieldLayout.setDefaultComponentAlignment(Alignment.BOTTOM_CENTER);

        HorizontalLayout buttonLayout = new HorizontalLayout();
        buttonLayout.setSpacing(true);
        buttonLayout.setSizeFull();
        buttonLayout.setDefaultComponentAlignment(Alignment.BOTTOM_RIGHT);

        table = new Table();
        table.setWidth(100, Unit.PERCENTAGE);

        statusComboBox = createStatusComboBox();
        placeFromComboBox = createPlaceFromComboBox();
        placeToComboBox = createPlaceToComboBox();

        Label authorDocumentLabel = new Label();
        Label dateDocumentLabel = new Label();

        commentTextField = new TextField(ViewUtils.getMessage("field.comment"));
        commentTextField.setSizeFull();

        addButton = new Button("+");
        addButton.addClickListener(clickEvent -> clickAddButton());

        removeButton = new Button("-");
        removeButton.addClickListener(clickEvent -> clickRemoveButton());

        saveButton = new Button(ViewUtils.getMessage("button.save"));
        saveButton.addClickListener(clickEvent -> saveDocument());

        fieldLayout.addComponent(dateDocumentLabel);
        fieldLayout.addComponent(authorDocumentLabel);
        fieldLayout.addComponent(placeFromComboBox);
        fieldLayout.addComponent(placeToComboBox);
        fieldLayout.addComponent(statusComboBox);
        fieldLayout.addComponent(addButton);
        fieldLayout.addComponent(removeButton);

        buttonLayout.addComponent(saveButton);

        vertical.addComponent(fieldLayout);
        vertical.addComponent(commentTextField);
        vertical.addComponent(table);
        vertical.addComponent(buttonLayout);

        setContent(vertical);

        if (documentMoveDevices != null) {
            dateDocumentLabel.setValue(ViewUtils.getMessage("field.date") + " " +
                    sdf.format(documentMoveDevices.getDate()));
            authorDocumentLabel.setValue(ViewUtils.getMessage("field.author") + " "
                    + documentMoveDevices.getAuthor().getName());
            setFields();
        } else {
            dateDocumentLabel.setValue(ViewUtils.getMessage("field.date") + " " +
                    sdf.format(new Date()));
            authorDocumentLabel.setValue(ViewUtils.getMessage("field.author") + " "
                    + SecurityUtils.getCurrentUser().getName());
            statusComboBox.setEnabled(false);
        }
    }

    private ComboBox createStatusComboBox() {
        BeanItemContainer<StatusDocument> container = new BeanItemContainer<>(StatusDocument.class);
        container.addAll(Arrays.asList(StatusDocument.values()));
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.status"), container);
        select.setNullSelectionAllowed(false);
        return select;
    }

    private ComboBox createPlaceFromComboBox() {
        BeanItemContainer<Place> container = new BeanItemContainer<>(Place.class);
        List<Place> places = placeService.findAllPlaces();
        places.forEach(place -> placeFromMap.put(place.getId(), place));
        container.addAll(places);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.placeFrom"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.setNullSelectionAllowed(false);
        return select;
    }

    private ComboBox createPlaceToComboBox() {
        BeanItemContainer<Place> container = new BeanItemContainer<>(Place.class);
        List<Place> places = placeService.findAllPlaces();
        places.forEach(place -> placeToMap.put(place.getId(), place));
        container.addAll(places);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.placeTo"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.setNullSelectionAllowed(false);
        return select;
    }

    private void setFields() {
        statusComboBox.select(documentMoveDevices.getStatus());
        if (documentMoveDevices.getStatus() == StatusDocument.Завершено) {
            statusComboBox.setEnabled(false);
            commentTextField.setEnabled(false);
            saveButton.setEnabled(false);
            addButton.setEnabled(false);
            removeButton.setEnabled(false);
            table.setSelectable(false);
        } else {
            statusComboBox.setEnabled(true);
            commentTextField.setEnabled(true);
            saveButton.setEnabled(true);
            addButton.setEnabled(true);
            removeButton.setEnabled(true);
            table.setSelectable(true);
        }
        placeToComboBox.setEnabled(false);
        placeFromComboBox.setEnabled(false);
        placeFromComboBox.select(placeFromMap.get(documentMoveDevices.getPlaceFrom().getId()));
        placeToComboBox.select(placeToMap.get(documentMoveDevices.getPlaceTo().getId()));
        commentTextField.setValue(documentMoveDevices.getComment());
    }

    private void clickAddButton() {
        Place place = (Place) placeFromComboBox.getValue();
        if (place == null) {
            Notification.show(ViewUtils.getMessage("notifications.selectPlaceFrom"), Notification.Type.ERROR_MESSAGE);
            return;
        }
        for (Window window : getUI().getWindows()) if (window instanceof SelectRepairWindow) return;
        SelectRepairWindow window = new SelectRepairWindow(repairOrderService, repairOrders, place);
        window.setRepairChoiceListener(new MyOnRepairChoiceListener());
        getUI().addWindow(window);
    }

    private void clickRemoveButton() {
        RepairOrder selected = (RepairOrder) table.getValue();
        if (selected == null) return;
        for (RepairOrder order : repairOrders) {
            if (Objects.equals(order.getId(), selected.getId())) {
                repairOrders.remove(order);
                break;
            }
        }
        initTable(createContainer(new ArrayList<>(repairOrders)));
    }

    private void saveDocument() {
        if (!SecurityUtils.hasAuthority(Authority.перемещение_устройств)) {
            Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                    Notification.Type.ERROR_MESSAGE);
            return;
        }
        if (documentMoveDevices == null) documentMoveDevices = new DocumentMoveDevices();
        Place placeFrom = (Place) placeFromComboBox.getValue();
        Place placeTo = (Place) placeToComboBox.getValue();
        StatusDocument status = (StatusDocument) statusComboBox.getValue();
        documentMoveDevices.setPlaceFrom(placeFrom);
        documentMoveDevices.setPlaceTo(placeTo);
        documentMoveDevices.setStatus(status);
        documentMoveDevices.setComment(commentTextField.getValue());
        documentMoveDevices.setRepairOrders(repairOrders);

        try {
            DocumentMoveDevices savedDocument = documentService.saveDocumentMoveDevices(documentMoveDevices);
            documentMoveDevices = documentService.findDocumentMoveDevicesById(savedDocument.getId());
            repairOrders = new HashSet<>(documentMoveDevices.getRepairOrders());
            Notification.show(ViewUtils.getMessage("notifications.saved"), Notification.Type.TRAY_NOTIFICATION);
            setFields();
        } catch (Exception e) {
            StringBuilder builder = new StringBuilder(ViewUtils.getMessage("notifications.notSaved"));
            if (e.getLocalizedMessage() != null) {
                builder.append("\n");
                builder.append(e.getLocalizedMessage());
            } else {
                for (StackTraceElement element : e.getStackTrace()) {
                    builder.append("\n");
                    builder.append(element);
                }
            }
            Notification.show(builder.toString(), Notification.Type.ERROR_MESSAGE);
        }
    }

    private void initTable(BeanItemContainer<RepairOrder> container) {
        table.setContainerDataSource(container);
        table.setColumnCollapsingAllowed(true);
        table.setVisibleColumns(fieldNames);
        table.setColumnHeader(ID_COLUMN, ID_VISIBLE_COLUMN);
        table.setColumnHeader(DATE_COLUMN, DATE_VISIBLE_COLUMN);
        table.setColumnHeader(IMEI_COLUMN, IMEI_VISIBLE_COLUMN);
        table.setColumnHeader(SN_COLUMN, SN_VISIBLE_COLUMN);
        table.setColumnHeader(PRODUCER_COLUMN, PRODUCER_VISIBLE_COLUMN);
        table.setColumnHeader(MODEL_COLUMN, MODEL_VISIBLE_COLUMN);
        table.setSelectable(true);
        table.setImmediate(true);
    }

    private BeanItemContainer<RepairOrder> createContainer(List<RepairOrder> repairOrders) {
        BeanItemContainer<RepairOrder> container = new BeanItemContainer<>(RepairOrder.class);
        container.addNestedContainerProperty(PRODUCER_COLUMN);
        container.addNestedContainerProperty(MODEL_COLUMN);
        container.addAll(repairOrders);
        //container.sort(new Object[]{ID_COLUMN}, new boolean[]{true});
        return container;
    }

    private class MyOnRepairChoiceListener implements OnRepairChoiceListener {
        @Override
        public void onChoice(RepairOrder order) {
            repairOrders.add(order);
            initTable(createContainer(new ArrayList<>(repairOrders)));
        }
    }
}
