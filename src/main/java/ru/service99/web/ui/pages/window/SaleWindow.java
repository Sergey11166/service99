package ru.service99.web.ui.pages.window;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.*;
import ru.service99.SecurityUtils;
import ru.service99.domain.*;
import ru.service99.service.*;
import ru.service99.web.AdvancedFileDownloader;
import ru.service99.web.FileUtils;
import ru.service99.web.model.FreePart;
import ru.service99.web.ui.OnCustomerChoiceListener;
import ru.service99.web.ui.OnPartChoiceListener;
import ru.service99.web.ui.OnPartPendingChoiceListener;
import ru.service99.web.ui.ViewUtils;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

public class SaleWindow extends Window {

    private static final String PART_NUMBER_COLUMN = "part.partNumber";
    private static final String NAME_COLUMN = "part.name";
    private static final String COUNT_COLUMN = "count";
    private static final String NEEDED_COUNT_COLUMN = "neededCount";
    private static final String ORDERED_COUNT_COLUMN = "orderedCount";
    private static final String RECEIVED_COUNT_COLUMN = "receivedCount";
    private static final String STOCK_COLUMN = "stock.name";
    private static final String OUT_PRICE_COLUMN = "outPrice";
    private static final String OPT_PRICE_COLUMN = "optPrice";
    private static final String IN_PRICE_COLUMN = "inPrice";
    private static final String PART_COMMENT_COLUMN = "part.comment";
    private static final String COMMENT_COLUMN = "comment";

    private static final String PART_NUMBER_VISIBLE_COLUMN = ViewUtils.getMessage("table.partNumber");
    private static final String NAME_VISIBLE_COLUMN = ViewUtils.getMessage("table.name");
    private static final String COUNT_VISIBLE_COLUMN = ViewUtils.getMessage("table.count");
    private static final String NEEDED_VISIBLE_COUNT_COLUMN = ViewUtils.getMessage("table.neededCount");
    private static final String ORDERED_VISIBLE_COUNT_COLUMN = ViewUtils.getMessage("table.orderedCount");
    private static final String RECEIVED_VISIBLE_COUNT_COLUMN = ViewUtils.getMessage("table.receivedCount");
    private static final String STOCK_VISIBLE_COLUMN = ViewUtils.getMessage("table.stock");
    private static final String OUT_PRICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.outPrice");
    private static final String OPT_PRICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.optPrice");
    private static final String IN_PRICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.inPrice");
    private static final String PART_COMMENT_VISIBLE_COLUMN = ViewUtils.getMessage("table.comment");
    private static final String COMMENT_VISIBLE_COLUMN = ViewUtils.getMessage("table.comment");

    private static final String[] mainFieldNamesWithInPrice = new String[]{PART_NUMBER_COLUMN, NAME_COLUMN, COUNT_COLUMN,
            STOCK_COLUMN, OUT_PRICE_COLUMN, OPT_PRICE_COLUMN, IN_PRICE_COLUMN, PART_COMMENT_COLUMN};
    private static final String[] mainFieldNamesWithoutInPrice = new String[]{PART_NUMBER_COLUMN, NAME_COLUMN, COUNT_COLUMN,
            STOCK_COLUMN, OUT_PRICE_COLUMN, OPT_PRICE_COLUMN, PART_COMMENT_COLUMN};

    private static final String[] pendingFieldNames = new String[]{PART_NUMBER_COLUMN, NAME_COLUMN,
            NEEDED_COUNT_COLUMN, ORDERED_COUNT_COLUMN, RECEIVED_COUNT_COLUMN, COMMENT_COLUMN};

    private SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());

    private SalesOrderService salesOrderService;
    private CustomerService customerService;
    private FirmService firmService;
    private UserService userService;
    private PartService partService;
    private ModelService modelService;

    private SalesOrder order;
    private Customer selectedCustomer;
    private Set<PartInstance> partInstances;
    private Set<PartPending> partsPending;
    private Date lastChangeDate;

    private HashMap<Long, Firm> firmMap = new HashMap<>();
    private HashMap<Long, User> managerMap = new HashMap<>();

    private Table mainTable;
    private Table pendingTable;

    private TextField idTextField;
    private TextField customerTextField;
    private TextArea receiverCommentTextArea;
    private TextField partsCostTextField;
    private TextField partsPendingCostTextField;
    private ComboBox firmComboBox;
    private ComboBox typeComboBox;

    private TextArea manageCommentTextArea;
    private ComboBox statusComboBox;
    private ComboBox managerComboBox;

    private Button addPartButton;
    private Button removePartButton;
    private Button addPartPendingButton;
    private Button removePartPendingButton;
    private Button historyButton;
    private Button ticketButton;
    private Button receiptButton;
    private Button waybillButton;

    public SaleWindow(
            SalesOrderService salesOrderService,
            CustomerService customerService,
            FirmService firmService,
            UserService userService,
            ModelService modelService,
            PartService partService) {
        setCaption(ViewUtils.getMessage("salesPage.window.title.new"));
        this.salesOrderService = salesOrderService;
        this.customerService = customerService;
        this.firmService = firmService;
        this.userService = userService;
        this.modelService = modelService;
        this.partService = partService;
        this.partInstances = new HashSet<>();
        this.partsPending = new HashSet<>();
        init();
    }

    public SaleWindow(SalesOrderService salesOrderService,
                      CustomerService customerService,
                      FirmService firmService,
                      UserService userService,
                      ModelService modelService,
                      PartService partService,
                      SalesOrder order) {
        this(salesOrderService, customerService, firmService, userService, modelService, partService);
        setCaption(ViewUtils.getMessage("salesPage.window.title.edit") + " " + order.getId());
        this.order = order;
        if (order.getDocument() != null) {
            this.partInstances.addAll(order.getDocument().getPartInstances());
        }
        if (order.getPartsPending() != null) {
            this.partsPending.addAll(order.getPartsPending());
        }
        this.lastChangeDate = salesOrderService.findLastRevisionById(order.getId()).getRevisionDate().toDate();
        init();
    }

    private void init() {
        initLayout();
        initMainTable(createMainContainer(new ArrayList<>(partInstances)));
        initPendingTable(createPendingContainer(new ArrayList<>(partsPending)));
    }

    private void initLayout() {
        center();
        setModal(true);
        setHeight(100, Unit.PERCENTAGE);

        VerticalLayout vertical = new VerticalLayout();
        vertical.setSpacing(true);
        vertical.setMargin(true);

        //startDate, placeReceive, receiver
        HorizontalLayout horizontalLayout1 = new HorizontalLayout();
        horizontalLayout1.setSpacing(true);

        //receiverComment, customer
        HorizontalLayout horizontalLayout2 = new HorizontalLayout();
        horizontalLayout2.setSpacing(true);

        //commentForCustomer, firm, type, status, manager
        HorizontalLayout horizontalLayout3 = new HorizontalLayout();
        horizontalLayout3.setSpacing(true);

        //buttons
        HorizontalLayout horizontalLayout4 = new HorizontalLayout();
        horizontalLayout4.setSpacing(true);

        HorizontalLayout customerLayout = new HorizontalLayout();
        customerLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);

        HorizontalLayout tablePartButtonsLayout = new HorizontalLayout();
        tablePartButtonsLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);
        tablePartButtonsLayout.setSpacing(true);

        HorizontalLayout tablePartsPendingButtonsLayout = new HorizontalLayout();
        tablePartsPendingButtonsLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);
        tablePartsPendingButtonsLayout.setSpacing(true);

        VerticalLayout statusManagerLayout = new VerticalLayout();
        statusManagerLayout.setSpacing(true);

        VerticalLayout tab1 = new VerticalLayout();
        tab1.setSpacing(true);

        VerticalLayout tab2 = new VerticalLayout();
        tab2.setSpacing(true);

        mainTable = new Table();
        mainTable.setWidth(100, Unit.PERCENTAGE);
        mainTable.setHeight("230px");

        pendingTable = new Table();
        pendingTable.setWidth(100, Unit.PERCENTAGE);
        pendingTable.setHeight("230px");

        TabSheet tabSheet = new TabSheet();
        tabSheet.setHeight("350px");

        idTextField = new TextField(ViewUtils.getMessage("field.saleNumber"));
        idTextField.setEnabled(false);
        TextField startDateTextField = new TextField(ViewUtils.getMessage("field.startDate"));
        startDateTextField.setEnabled(false);
        TextField placeReceiveTextField = new TextField(ViewUtils.getMessage("field.placeReceive"));
        placeReceiveTextField.setEnabled(false);
        TextField receiverTextField = new TextField(ViewUtils.getMessage("field.receiver"));
        receiverTextField.setEnabled(false);

        customerTextField = new TextField(ViewUtils.getMessage("field.customer"));
        customerTextField.setEnabled(false);
        customerTextField.setWidth("250px");
        receiverCommentTextArea = new TextArea(ViewUtils.getMessage("field.receiverComment"));
        receiverCommentTextArea.setWidth("685px");
        receiverCommentTextArea.setRows(2);
        partsCostTextField = new TextField(ViewUtils.getMessage("field.partsCost"));
        partsCostTextField.setEnabled(false);
        partsPendingCostTextField = new TextField(ViewUtils.getMessage("field.partsPendingCost"));
        partsPendingCostTextField.setEnabled(false);
        firmComboBox = createFirmComboBox();
        typeComboBox = createTypeComboBox();
        manageCommentTextArea = new TextArea(ViewUtils.getMessage("field.managerComment"));
        manageCommentTextArea.setWidth("785px");
        statusComboBox = createStatusComboBox();
        managerComboBox = createManagerComboBox();

        Button choiceCustomerButton = new Button("*");
        choiceCustomerButton.setWidth("15px");
        choiceCustomerButton.addClickListener(event -> {
            for (Window window : getUI().getWindows()) if (window instanceof SelectCustomerWindow) return;
            SelectCustomerWindow window = new SelectCustomerWindow(customerService);
            window.setCustomerChoiceListener(new MyCustomerChoiceListener());
            getUI().addWindow(window);
        });

        addPartButton = new Button("+");
        addPartButton.addClickListener(event -> {
            for (Window window : getUI().getWindows()) if (window instanceof SelectPartInstanceWindow) return;
            if (SecurityUtils.getCurrentUser().getAllowedStocks().isEmpty()) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"), Notification.Type.ERROR_MESSAGE);
                return;
            }
            SelectPartInstanceWindow window = new SelectPartInstanceWindow(modelService, partService,
                    null, partInstances);
            window.setPartChoiceListener(new MyOnPartChoiceListener());
            getUI().addWindow(window);
        });

        removePartButton = new Button("-");
        removePartButton.addClickListener(event -> removePartButtonClick());

        addPartPendingButton = new Button("+");
        addPartPendingButton.addClickListener(event -> {
            for (Window window : getUI().getWindows()) if (window instanceof SelectPartWindow) return;
            SelectPartWindow window = new SelectPartWindow(modelService, partService);
            window.setPartPendingChoiceListener(new MyOnPartPendingChoiceListener());
            getUI().addWindow(window);
        });

        removePartPendingButton = new Button("-");
        removePartPendingButton.addClickListener(event -> removePartPendingButtonClick());

        Button saveButton = new Button(ViewUtils.getMessage("button.save"));
        saveButton.addClickListener(clickEvent -> saveButtonClick());

        historyButton = new Button(ViewUtils.getMessage("button.history"));
        historyButton.addClickListener(event -> {
            if (order == null) return;
            for (Window window : getUI().getWindows()) if (window instanceof HistorySaleWindow) return;
            HistorySaleWindow window = new HistorySaleWindow(salesOrderService, order.getId());
            getUI().addWindow(window);
        });

        AdvancedFileDownloader ticketDownloader = new AdvancedFileDownloader();
        URL urlTicketDownloader = getClass().getClassLoader().getResource(
                "excel/" + ViewUtils.getMessage("file.saleOrderTicket") + ".xls");
        assert urlTicketDownloader != null;
        ticketDownloader.setFilePath(urlTicketDownloader.getFile());
        ticketDownloader.addAdvancedDownloaderListener(event -> FileUtils.writeSaleOrderTicketExcel(getClass(), order));
        ticketButton = new Button(ViewUtils.getMessage("button.ticket"));
        ticketDownloader.extend(ticketButton);

        AdvancedFileDownloader receiptDownloader = new AdvancedFileDownloader();
        URL urlReceiptDownloader = getClass().getClassLoader().getResource(
                "excel/" + ViewUtils.getMessage("file.saleOrderReceipt") + ".xls");
        assert urlReceiptDownloader != null;
        receiptDownloader.setFilePath(urlReceiptDownloader.getFile());
        receiptDownloader.addAdvancedDownloaderListener(event -> FileUtils.writeSaleOrderReceiptExcel(getClass(), order));
        receiptButton = new Button(ViewUtils.getMessage("button.receipt"));
        receiptDownloader.extend(receiptButton);

        AdvancedFileDownloader waybillDownloader = new AdvancedFileDownloader();
        URL urlWaybillDownloader = getClass().getClassLoader().getResource(
                "excel/" + ViewUtils.getMessage("file.saleOrderWaybill") + ".xls");
        assert urlWaybillDownloader != null;
        waybillDownloader.setFilePath(urlWaybillDownloader.getFile());
        waybillDownloader.addAdvancedDownloaderListener(event -> FileUtils.writeSaleOrderWaybillExcel(getClass(), order));
        waybillButton = new Button(ViewUtils.getMessage("button.waybill"));
        waybillDownloader.extend(waybillButton);

        Button closeButton = new Button(ViewUtils.getMessage("button.close"));
        closeButton.addClickListener(event -> close());

        horizontalLayout1.addComponent(idTextField);
        horizontalLayout1.addComponent(startDateTextField);
        horizontalLayout1.addComponent(placeReceiveTextField);
        horizontalLayout1.addComponent(receiverTextField);

        horizontalLayout2.addComponent(receiverCommentTextArea);
        customerLayout.addComponent(customerTextField);
        customerLayout.addComponent(choiceCustomerButton);
        horizontalLayout2.addComponent(customerLayout);

        horizontalLayout3.addComponent(manageCommentTextArea);
        statusManagerLayout.addComponent(firmComboBox);
        statusManagerLayout.addComponent(statusComboBox);
        statusManagerLayout.addComponent(managerComboBox);
        horizontalLayout3.addComponent(statusManagerLayout);

        horizontalLayout4.addComponent(historyButton);
        horizontalLayout4.addComponent(ticketButton);
        horizontalLayout4.addComponent(receiptButton);
        horizontalLayout4.addComponent(waybillButton);
        horizontalLayout4.addComponent(closeButton);
        horizontalLayout4.addComponent(saveButton);

        tablePartButtonsLayout.addComponent(addPartButton);
        tablePartButtonsLayout.addComponent(removePartButton);
        tablePartButtonsLayout.addComponent(partsCostTextField);
        tablePartButtonsLayout.addComponent(typeComboBox);

        tablePartsPendingButtonsLayout.addComponent(addPartPendingButton);
        tablePartsPendingButtonsLayout.addComponent(removePartPendingButton);
        tablePartsPendingButtonsLayout.addComponent(partsPendingCostTextField);

        tab1.addComponent(tablePartButtonsLayout);
        tab1.addComponent(mainTable);

        tab2.addComponent(tablePartsPendingButtonsLayout);
        tab2.addComponent(pendingTable);

        tabSheet.addTab(tab1, ViewUtils.getMessage("tab.mainInfo"));
        tabSheet.addTab(tab2, ViewUtils.getMessage("tab.partsPending"));

        vertical.addComponent(horizontalLayout1);
        vertical.addComponent(tabSheet);
        vertical.addComponent(horizontalLayout2);
        vertical.addComponent(horizontalLayout3);
        vertical.addComponent(horizontalLayout4);

        setContent(vertical);

        if (order != null) {
            idTextField.setValue(order.getId().toString());
            startDateTextField.setValue(sdf.format(order.getStartDate()));
            placeReceiveTextField.setValue(order.getPlaceReceive() == null ? "" : order.getPlaceReceive().getName());
            receiverTextField.setValue(order.getReceiver() == null ? "" : order.getReceiver().getName());
            selectedCustomer = order.getCustomer();
            customerTextField.setValue(selectedCustomer.getName());
            customerTextField.setDescription(selectedCustomer.description());
            receiverCommentTextArea.setValue(order.getReceiverComment() == null ? "" : order.getReceiverComment());
            typeComboBox.setValue(order.getType());
            setPartsCostTextField();
            setPartsPendingCostTextField();
            firmComboBox.setValue(firmMap.get(order.getFirm().getId()));
            manageCommentTextArea.setValue(order.getManagerComment() == null ? "" :
                    order.getManagerComment());
            statusComboBox.setValue(order.getStatus());
            if (order.getManager() != null) managerComboBox.setValue(managerMap.get(order.getManager().getId()));
        } else {
            startDateTextField.setValue(sdf.format(new Date()));
            placeReceiveTextField.setValue(SecurityUtils.getCurrentUser().getPlace().getName());
            receiverTextField.setValue(SecurityUtils.getCurrentUser().getName());
            firmComboBox.setValue(firmMap.get(SecurityUtils.getCurrentUser().getFirm().getId()));
            statusComboBox.setValue(StatusSalesOrder.Новый);
            typeComboBox.setValue(TypeSale.Розница);
        }
        setFields();
    }

    private void saveButtonClick() {
        if (isEditedAnotherUser()) {
            Notification.show(ViewUtils.getMessage("notifications.editedAnotherUser"),
                    Notification.Type.ERROR_MESSAGE);
            return;
        }
        if (!SecurityUtils.hasAuthority(Authority.редактирование_ремонта)) {
            Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                    Notification.Type.ERROR_MESSAGE);
            return;
        }
        if (order == null) order = new SalesOrder();
        Firm firm = (Firm) firmComboBox.getValue();
        TypeSale type = (TypeSale) typeComboBox.getValue();
        String receiverComment = receiverCommentTextArea.getValue();
        User manager = (User) managerComboBox.getValue();
        StatusSalesOrder statusSalesOrder = (StatusSalesOrder) statusComboBox.getValue();
        String managerComment = manageCommentTextArea.getValue();

        order.setFirm(firm);
        order.setType(type);
        order.setManager(manager);
        order.setStatus(statusSalesOrder);
        order.setCustomer(selectedCustomer);
        order.setReceiverComment(receiverComment.isEmpty() ? null : receiverComment);
        order.setManagerComment(managerComment.isEmpty() ? null : managerComment);
        if (order.getDocument() != null) {
            order.getDocument().setPartInstances(partInstances);
        } else {
            if (!partInstances.isEmpty()) {
                DocumentOut doc = new DocumentOut();
                doc.setPartInstances(partInstances);
                order.setDocument(doc);
            }
        }
        order.setPartsPending(partsPending);
        try {
            SalesOrder savedOrder = salesOrderService.save(order);
            order = salesOrderService.findById(savedOrder.getId());
            if (order.getDocument() != null) partInstances = new HashSet<>(order.getDocument().getPartInstances());
            //Notification.show(ViewUtils.getMessage("notifications.saved"), Notification.Type.TRAY_NOTIFICATION);
            partsPending = new HashSet<>(order.getPartsPending());
            setFields();
            lastChangeDate = salesOrderService.findLastRevisionById(order.getId()).getRevisionDate().toDate();
        } catch (Exception e) {
            StringBuilder builder = new StringBuilder(ViewUtils.getMessage("notifications.notSaved"));
            if (e.getLocalizedMessage() != null) {
                builder.append("\n");
                builder.append(e.getLocalizedMessage());
            } else {
                for (StackTraceElement element : e.getStackTrace()) {
                    builder.append("\n");
                    builder.append(element);
                }
            }
            Notification.show(builder.toString(), Notification.Type.ERROR_MESSAGE);
            e.printStackTrace();
        }
        initMainTable(createMainContainer(new ArrayList<>(partInstances)));
        initPendingTable(createPendingContainer(new ArrayList<>(partsPending)));
    }

    private void removePartButtonClick() {
        FreePart selected = (FreePart) mainTable.getValue();
        if (selected == null) return;
        for (PartInstance instance : partInstances) {
            if (Objects.equals(instance.getPart().getId(), selected.getPart().getId())) {
                partInstances.remove(instance);
                break;
            }
        }
        initMainTable(createMainContainer(new ArrayList<>(partInstances)));
    }

    private void removePartPendingButtonClick() {
        PartPending selected = (PartPending) pendingTable.getValue();
        if (selected == null) return;
        for (PartPending partPending : partsPending) {
            if (Objects.equals(partPending.getPart().getId(), selected.getPart().getId())) {
                partsPending.remove(partPending);
                break;
            }
        }
        initPendingTable(createPendingContainer(new ArrayList<>(partsPending)));
    }

    private ComboBox createStatusComboBox() {
        BeanItemContainer<StatusSalesOrder> container = new BeanItemContainer<>(StatusSalesOrder.class);
        container.addAll(Arrays.asList(StatusSalesOrder.values()));
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.status"), container);
        select.setNullSelectionAllowed(false);
        return select;
    }

    private ComboBox createTypeComboBox() {
        BeanItemContainer<TypeSale> container = new BeanItemContainer<>(TypeSale.class);
        if (order == null) {
            if (SecurityUtils.hasAuthority(Authority.видеть_входную_цену)) container.addBean(TypeSale.Вх_цена);
        } else {
            container.addBean(TypeSale.Вх_цена);
        }
        container.addBean(TypeSale.Опт);
        container.addBean(TypeSale.Розница);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.typeSale"), container);
        select.addValueChangeListener(event -> {
            setPartsCostTextField();
            setPartsPendingCostTextField();
        });
        return select;
    }

    private ComboBox createFirmComboBox() {
        BeanItemContainer<Firm> container = new BeanItemContainer<>(Firm.class);
        List<Firm> firms = firmService.findAll();
        if (order != null) {
            if (order.getFirm().getIsRemoved()) firms.add(order.getFirm());
        }
        firms.forEach(firm -> firmMap.put(firm.getId(), firm));
        container.addAll(firms);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.firm"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.setNullSelectionAllowed(false);
        return select;
    }

    private ComboBox createManagerComboBox() {
        BeanItemContainer<User> container = new BeanItemContainer<>(User.class);
        List<User> users = userService.findAll();
        if (order != null) {
            User master = order.getManager();
            if (master != null) {
                if (master.getIsRemoved()) users.add(master);
            }
        }
        users.forEach(user -> managerMap.put(user.getId(), user));
        container.addAll(users);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.manager"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.setNullSelectionAllowed(false);
        return select;
    }

    private void initMainTable(BeanItemContainer<FreePart> container) {
        mainTable.setContainerDataSource(container);
        mainTable.setColumnCollapsingAllowed(true);
        mainTable.setColumnCollapsed(PART_COMMENT_COLUMN, true);
        if (SecurityUtils.hasAuthority(Authority.видеть_входную_цену)) {
            mainTable.setVisibleColumns(mainFieldNamesWithInPrice);
            mainTable.setColumnHeader(IN_PRICE_COLUMN, IN_PRICE_VISIBLE_COLUMN);
        } else {
            mainTable.setVisibleColumns(mainFieldNamesWithoutInPrice);
        }
        mainTable.setColumnHeader(PART_NUMBER_COLUMN, PART_NUMBER_VISIBLE_COLUMN);
        mainTable.setColumnHeader(NAME_COLUMN, NAME_VISIBLE_COLUMN);
        mainTable.setColumnHeader(COUNT_COLUMN, COUNT_VISIBLE_COLUMN);
        mainTable.setColumnHeader(STOCK_COLUMN, STOCK_VISIBLE_COLUMN);
        mainTable.setColumnHeader(OUT_PRICE_COLUMN, OUT_PRICE_VISIBLE_COLUMN);
        mainTable.setColumnHeader(OPT_PRICE_COLUMN, OPT_PRICE_VISIBLE_COLUMN);
        mainTable.setColumnHeader(IN_PRICE_COLUMN, IN_PRICE_VISIBLE_COLUMN);
        mainTable.setColumnHeader(PART_COMMENT_COLUMN, PART_COMMENT_VISIBLE_COLUMN);
        mainTable.setSelectable(true);
        mainTable.setImmediate(true);
    }

    private void initPendingTable(BeanItemContainer<PartPending> container) {
        pendingTable.setContainerDataSource(container);
        pendingTable.setColumnCollapsingAllowed(true);
        pendingTable.setColumnCollapsed(COMMENT_COLUMN, true);
        pendingTable.setVisibleColumns(pendingFieldNames);
        pendingTable.setColumnHeader(PART_NUMBER_COLUMN, PART_NUMBER_VISIBLE_COLUMN);
        pendingTable.setColumnHeader(NAME_COLUMN, NAME_VISIBLE_COLUMN);
        pendingTable.setColumnHeader(NEEDED_COUNT_COLUMN, NEEDED_VISIBLE_COUNT_COLUMN);
        pendingTable.setColumnHeader(ORDERED_COUNT_COLUMN, ORDERED_VISIBLE_COUNT_COLUMN);
        pendingTable.setColumnHeader(RECEIVED_COUNT_COLUMN, RECEIVED_VISIBLE_COUNT_COLUMN);
        pendingTable.setColumnHeader(COMMENT_COLUMN, COMMENT_VISIBLE_COLUMN);
        pendingTable.setSelectable(true);
        pendingTable.setImmediate(true);
    }

    private BeanItemContainer<FreePart> createMainContainer(List<PartInstance> instances) {
        BeanItemContainer<FreePart> container = new BeanItemContainer<>(FreePart.class);
        container.addNestedContainerProperty(PART_NUMBER_COLUMN);
        container.addNestedContainerProperty(NAME_COLUMN);
        container.addNestedContainerProperty(STOCK_COLUMN);
        container.addNestedContainerProperty(OUT_PRICE_COLUMN);
        container.addNestedContainerProperty(OPT_PRICE_COLUMN);
        container.addNestedContainerProperty(IN_PRICE_COLUMN);
        container.addNestedContainerProperty(PART_COMMENT_COLUMN);
        List<FreePart> freeParts = FreePart.instancesToFreeParts(instances);
        container.addAll(freeParts);
        container.sort(new Object[]{NAME_COLUMN, COUNT_COLUMN}, new boolean[]{true, true});
        return container;
    }

    private BeanItemContainer<PartPending> createPendingContainer(List<PartPending> partsPending) {
        BeanItemContainer<PartPending> container = new BeanItemContainer<>(PartPending.class);
        container.addNestedContainerProperty(PART_NUMBER_COLUMN);
        container.addNestedContainerProperty(NAME_COLUMN);
        container.addNestedContainerProperty(NEEDED_COUNT_COLUMN);
        container.addNestedContainerProperty(ORDERED_COUNT_COLUMN);
        container.addNestedContainerProperty(RECEIVED_COUNT_COLUMN);
        container.addNestedContainerProperty(COMMENT_COLUMN);
        container.addAll(partsPending);
        container.sort(new Object[]{NAME_COLUMN}, new boolean[]{true});
        return container;
    }

    private void setPartsCostTextField() {
        Float partsCost = 0F;
        DocumentOut doc = new DocumentOut();
        doc.setPartInstances(partInstances);
        TypeSale type = (TypeSale) typeComboBox.getValue();
        switch (type) {
            case Розница:
                partsCost = doc.getPartsCostOut();
                break;
            case Опт:
                partsCost = doc.getPartsCostOpt();
                break;
            case Вх_цена:
                partsCost = doc.getPartsCostIn();
                break;
        }
        if (type == TypeSale.Вх_цена && !SecurityUtils.hasAuthority(Authority.видеть_входную_цену)) {
            partsCostTextField.setValue(ViewUtils.getMessage("notifications.hided"));
        } else {
            partsCostTextField.setValue(String.valueOf(partsCost));
        }
    }

    private void setPartsPendingCostTextField() {
        Float partsCost = 0F;
        TypeSale type = (TypeSale) typeComboBox.getValue();
        SalesOrder o = new SalesOrder();
        o.setPartsPending(partsPending);
        switch (type) {
            case Розница:
                partsCost = o.getPartsPendingCostOut();
                break;
            case Опт:
                partsCost = o.getPartsPendingCostOpt();
                break;
            case Вх_цена:
                partsCost = o.getPartsPendingCostIn();
                break;
        }
        if (type == TypeSale.Вх_цена && !SecurityUtils.hasAuthority(Authority.видеть_входную_цену)) {
            partsPendingCostTextField.setValue(ViewUtils.getMessage("notifications.hided"));
        } else {
            partsPendingCostTextField.setValue(String.valueOf(partsCost));
        }
    }

    private void setFields() {
        if (order != null) {
            idTextField.setValue(String.valueOf(order.getId()));
            historyButton.setEnabled(true);
            ticketButton.setEnabled(true);
            receiptButton.setEnabled(!(order.getStatus() == StatusSalesOrder.Новый ||
                    order.getStatus() == StatusSalesOrder.Ож_детали));
            waybillButton.setEnabled(!(order.getStatus() == StatusSalesOrder.Новый ||
                    order.getStatus() == StatusSalesOrder.Ож_детали));
            if (order.getStatus() == StatusSalesOrder.Выдан || order.getStatus() == StatusSalesOrder.Отменён) {
                firmComboBox.setEnabled(false);
                statusComboBox.setEnabled(false);
                typeComboBox.setEnabled(false);
                managerComboBox.setEnabled(false);
                addPartButton.setEnabled(false);
                removePartButton.setEnabled(false);
                addPartPendingButton.setEnabled(false);
                removePartPendingButton.setEnabled(false);
            }
        } else {
            historyButton.setEnabled(false);
            ticketButton.setEnabled(false);
            receiptButton.setEnabled(false);
            waybillButton.setEnabled(false);
        }
    }

    private boolean isEditedAnotherUser() {
        if (order == null) return false;
        if (order.getId() == null) return false;
        Date date = salesOrderService.findLastRevisionById(order.getId()).getRevisionDate().toDate();
        return lastChangeDate.getTime() != date.getTime();
    }

    private class MyCustomerChoiceListener implements OnCustomerChoiceListener {
        @Override
        public void onChoice(Customer customer) {
            selectedCustomer = customer;
            customerTextField.setValue(selectedCustomer.getName());
            customerTextField.setDescription(selectedCustomer.description());
        }
    }

    private class MyOnPartChoiceListener implements OnPartChoiceListener {
        @Override
        public void onChoice(Set<PartInstance> instances) {
            partInstances.addAll(instances);
            initMainTable(createMainContainer(new ArrayList<>(partInstances)));
            setPartsCostTextField();
        }
    }

    private class MyOnPartPendingChoiceListener implements OnPartPendingChoiceListener {
        @Override
        public void onChoice(PartPending partPending) {
            partsPending.add(partPending);
            initPendingTable(createPendingContainer(new ArrayList<>(partsPending)));
            setPartsPendingCostTextField();
        }
    }
}
