package ru.service99.web.ui.pages;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.service99.SecurityUtils;
import ru.service99.domain.Authority;
import ru.service99.domain.Customer;
import ru.service99.service.CustomerService;
import ru.service99.web.ui.ViewUtils;
import ru.service99.web.ui.pages.window.CustomerWindow;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
@Scope("prototype")
@SpringView(name = CustomersPage.NAME)
public class CustomersPage extends BasePage {

    public static final String NAME = "customers";

    @Autowired
    private CustomerService customerService;

    private Table table = new Table();
    private TextField searchTextField;

    private static final String ID_COLUMN = "id";
    private static final String NAME_COLUMN = "name";
    private static final String PHONE_COLUMN = "phoneNumber";
    private static final String ADDRESS_COLUMN = "address";
    private static final String COMMENT_COLUMN = "comment";

    private static final String ID_VISIBLE_COLUMN = ViewUtils.getMessage("table.id");
    private static final String NAME_VISIBLE_COLUMN = ViewUtils.getMessage("table.userName");
    private static final String PHONE_VISIBLE_COLUMN = ViewUtils.getMessage("table.phone");
    private static final String ADDRESS_VISIBLE_COLUMN = ViewUtils.getMessage("table.address");
    private static final String COMMENT_VISIBLE_COLUMN = ViewUtils.getMessage("table.comment");

    private static final String[] fieldNames = new String[]{ID_COLUMN, NAME_COLUMN, PHONE_COLUMN,
            ADDRESS_COLUMN, COMMENT_COLUMN};

    @PostConstruct
    public void init() {
        initLayout();
        applyFilter();
    }

    private void initLayout() {
        HorizontalLayout filterLayout = new HorizontalLayout();
        filterLayout.setSpacing(true);
        filterLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);

        searchTextField = new TextField(ViewUtils.getMessage("field.search"));
        Button applyButton = new Button(ViewUtils.getMessage("button.search"));
        applyButton.addClickListener(clickEvent -> applyFilter());

        Button addButton = new Button(ViewUtils.getMessage("button.new"));
        addButton.addClickListener(event -> {
            if (!SecurityUtils.hasAuthority(Authority.редактирование_клиента)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
            for (Window window : getUI().getWindows()) if (window instanceof CustomerWindow) return;
            CustomerWindow window = new CustomerWindow(customerService);
            window.addCloseListener(new MyCloseListener());
            getUI().addWindow(window);
        });

        filterLayout.addComponent(searchTextField);
        filterLayout.addComponent(applyButton);

        baseLayout.addComponent(filterLayout);
        baseLayout.addComponent(table);
        baseLayout.addComponent(addButton);

        baseLayout.setExpandRatio(filterLayout, 1);
        baseLayout.setExpandRatio(table, 8);
        baseLayout.setExpandRatio(addButton, 1);
    }

    private void applyFilter() {
        String searchName = "%" + searchTextField.getValue() + "%";
        List<Customer> customers;
        customers = customerService.findByName(searchName, null).getContent();
        initTable(createContainer(customers));
    }

    private void initTable(BeanItemContainer<Customer> container) {
        table.setContainerDataSource(container);
        table.setSizeFull();
        table.setColumnCollapsingAllowed(true);
        table.setVisibleColumns(fieldNames);
        table.setColumnHeader(ID_COLUMN, ID_VISIBLE_COLUMN);
        table.setColumnHeader(NAME_COLUMN, NAME_VISIBLE_COLUMN);
        table.setColumnHeader(PHONE_COLUMN, PHONE_VISIBLE_COLUMN);
        table.setColumnHeader(ADDRESS_COLUMN, ADDRESS_VISIBLE_COLUMN);
        table.setColumnHeader(COMMENT_COLUMN, COMMENT_VISIBLE_COLUMN);
        table.setSelectable(true);
        table.setImmediate(true);
        table.addItemClickListener(event -> {
            if (!event.isDoubleClick()) return;
            for (Window window : getUI().getWindows()) if (window instanceof CustomerWindow) return;
            Customer customer = (Customer) event.getItemId();
            CustomerWindow window = new CustomerWindow(customerService, customer);
            window.addCloseListener(new MyCloseListener());
            getUI().addWindow(window);
        });
    }

    private BeanItemContainer<Customer> createContainer(List<Customer> customers) {
        BeanItemContainer<Customer> container = new BeanItemContainer<>(Customer.class);
        container.addAll(customers);
        return container;
    }

    private class MyCloseListener implements Window.CloseListener {
        @Override
        public void windowClose(Window.CloseEvent closeEvent) {
            //applyFilter();
        }
    }
}
