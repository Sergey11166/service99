package ru.service99.web.ui.pages;

import com.vaadin.spring.annotation.SpringView;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Scope("prototype")
@SpringView(name = MainPage.NAME)
public class MainPage extends BasePage {

    public static final String NAME = "";

    @PostConstruct
    public void init() {
    }
}
