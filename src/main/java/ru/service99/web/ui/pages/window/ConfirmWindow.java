package ru.service99.web.ui.pages.window;

import com.vaadin.ui.*;
import ru.service99.web.ui.OkClickListener;
import ru.service99.web.ui.ViewUtils;

public class ConfirmWindow extends Window {

    private String message;
    private OkClickListener okClickListener;

    public ConfirmWindow(String message) {
        super(ViewUtils.getMessage("window.title.confirm"));
        this.message = message;
        init();
    }

    private void init() {
        center();
        setModal(true);

        VerticalLayout vertical = new VerticalLayout();
        vertical.setSpacing(true);
        vertical.setMargin(true);
        vertical.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

        HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.setSpacing(true);

        Label messageLabel = new Label(message);

        vertical.addComponent(messageLabel);

        Button okButton = new Button(ViewUtils.getMessage("button.ok"));
        okButton.addClickListener(event -> {
            okClickListener.onOkClick();
            close();
        });

        Button cancelButton = new Button(ViewUtils.getMessage("button.cancel"));
        cancelButton.addClickListener(event -> close());

        buttonsLayout.addComponent(cancelButton);
        buttonsLayout.addComponent(okButton);
        vertical.addComponent(buttonsLayout);
        setContent(vertical);
    }

    public void setOkClickListener(OkClickListener okClickListener) {
        this.okClickListener = okClickListener;
    }
}
