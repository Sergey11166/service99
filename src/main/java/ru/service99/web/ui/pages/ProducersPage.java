package ru.service99.web.ui.pages;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.Window;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.service99.SecurityUtils;
import ru.service99.domain.Authority;
import ru.service99.domain.Producer;
import ru.service99.service.ModelService;
import ru.service99.web.ui.ViewUtils;
import ru.service99.web.ui.pages.window.ProducerWindow;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
@Scope("prototype")
@SpringView(name = ProducersPage.NAME)
public class ProducersPage extends BasePage {

    public static final String NAME = "producers";

    @Autowired
    private ModelService modelService;

    private Table table = new Table();

    private static final String ID_COLUMN = "id";
    private static final String NAME_COLUMN = "name";

    private static final String ID_VISIBLE_COLUMN = ViewUtils.getMessage("table.id");
    private static final String NAME_VISIBLE_COLUMN = ViewUtils.getMessage("table.name");

    private static final String[] fieldNames = new String[]{ID_COLUMN, NAME_COLUMN};

    @PostConstruct
    public void init() {
        initLayout();
        initTable(createContainer(modelService.findAllProducers()));
    }

    private void initLayout() {
        Button addButton = new Button(ViewUtils.getMessage("button.new"));
        addButton.addClickListener(event -> {
            if (!SecurityUtils.hasAuthority(Authority.редактирование_производителя)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
            for (Window window : getUI().getWindows()) if (window instanceof ProducerWindow) return;
            ProducerWindow window = new ProducerWindow(modelService);
            window.addCloseListener(new MyCloseListener());
            getUI().addWindow(window);
        });

        baseLayout.addComponent(table);
        baseLayout.addComponent(addButton);
        baseLayout.setExpandRatio(table, 8);
        baseLayout.setExpandRatio(addButton, 1);
    }

    private void initTable(BeanItemContainer<Producer> container) {
        table.setContainerDataSource(container);
        table.setHeight("100%");
        table.setVisibleColumns(fieldNames);
        table.setColumnHeader(ID_COLUMN, ID_VISIBLE_COLUMN);
        table.setColumnHeader(NAME_COLUMN, NAME_VISIBLE_COLUMN);
        table.setSelectable(true);
        table.setImmediate(true);
        table.addItemClickListener(event -> {
            if (!event.isDoubleClick()) return;
            for (Window window : getUI().getWindows()) if (window instanceof ProducerWindow) return;
            Producer producer = (Producer) event.getItemId();
            ProducerWindow window = new ProducerWindow(modelService, producer);
            window.addCloseListener(new MyCloseListener());
            getUI().addWindow(window);
        });
    }

    private BeanItemContainer<Producer> createContainer(List<Producer> producers) {
        BeanItemContainer<Producer> container = new BeanItemContainer<>(Producer.class);
        container.addAll(producers);
        return container;
    }

    private class MyCloseListener implements Window.CloseListener {
        @Override
        public void windowClose(Window.CloseEvent closeEvent) {
            //initTable(createContainer(modelService.findAllProducers()));
        }
    }
}
