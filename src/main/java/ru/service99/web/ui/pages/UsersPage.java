package ru.service99.web.ui.pages;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.Window;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.service99.SecurityUtils;
import ru.service99.domain.Authority;
import ru.service99.domain.User;
import ru.service99.service.FirmService;
import ru.service99.service.PartService;
import ru.service99.service.PlaceService;
import ru.service99.service.UserService;
import ru.service99.web.ui.ViewUtils;
import ru.service99.web.ui.pages.window.UserWindow;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
@Scope("prototype")
@SpringView(name = UsersPage.NAME)
public class UsersPage extends BasePage {

    public static final String NAME = "users";

    @Autowired
    private FirmService firmService;
    @Autowired
    private PlaceService placeService;
    @Autowired
    private UserService userService;
    @Autowired
    private PartService partService;

    private Table table = new Table();

    private static final String ID_COLUMN = "id";
    private static final String NAME_COLUMN = "name";
    private static final String FIRM_COLUMN = "firm.name";
    private static final String PLACE_COLUMN = "place.name";

    private static final String ID_VISIBLE_COLUMN = ViewUtils.getMessage("table.id");
    private static final String NAME_VISIBLE_COLUMN = ViewUtils.getMessage("table.userName");
    private static final String FIRM_VISIBLE_COLUMN = ViewUtils.getMessage("table.firm");
    private static final String PLACE_VISIBLE_COLUMN = ViewUtils.getMessage("table.place");

    private static final String[] fieldNames = new String[]{ID_COLUMN, NAME_COLUMN, FIRM_COLUMN,
            PLACE_COLUMN};

    @PostConstruct
    public void init() {
        if (!SecurityUtils.hasAuthority(Authority.управление_пользователями)) {
            Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                    Notification.Type.ERROR_MESSAGE);
            return;
        }
        initLayout();
        initTable(createContainer(userService.findAll()));
    }

    private void initLayout() {
        Button addButton = new Button(ViewUtils.getMessage("button.new"));
        addButton.addClickListener(event -> {
            for (Window window : getUI().getWindows()) if (window instanceof UserWindow) return;
            UserWindow window = new UserWindow(userService, firmService, placeService, partService);
            window.addCloseListener(new MyCloseListener());
            getUI().addWindow(window);
        });

        baseLayout.addComponent(table);
        baseLayout.addComponent(addButton);

        baseLayout.setExpandRatio(table, 8);
        baseLayout.setExpandRatio(addButton, 1);
    }

    private void initTable(BeanItemContainer<User> container) {
        table.setContainerDataSource(container);
        table.setHeight("100%");
        table.setVisibleColumns(fieldNames);
        table.setColumnHeader(ID_COLUMN, ID_VISIBLE_COLUMN);
        table.setColumnHeader(NAME_COLUMN, NAME_VISIBLE_COLUMN);
        table.setColumnHeader(FIRM_COLUMN, FIRM_VISIBLE_COLUMN);
        table.setColumnHeader(PLACE_COLUMN, PLACE_VISIBLE_COLUMN);
        table.setSelectable(true);
        table.setImmediate(true);
        table.addItemClickListener(event -> {
            if (!event.isDoubleClick()) return;
            for (Window window : getUI().getWindows()) if (window instanceof UserWindow) return;
            User user = (User) event.getItemId();
            UserWindow window = new UserWindow(userService, firmService, placeService, partService, user);
            window.addCloseListener(new MyCloseListener());
            getUI().addWindow(window);
        });
    }

    private BeanItemContainer<User> createContainer(List<User> users) {
        BeanItemContainer<User> container = new BeanItemContainer<>(User.class);
        container.addNestedContainerProperty(FIRM_COLUMN);
        container.addNestedContainerProperty(PLACE_COLUMN);
        container.addAll(users);
        return container;
    }

    private class MyCloseListener implements Window.CloseListener {
        @Override
        public void windowClose(Window.CloseEvent closeEvent) {
            initTable(createContainer(userService.findAll()));
        }
    }
}
