package ru.service99.web.ui.pages;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.service99.SecurityUtils;
import ru.service99.domain.Authority;
import ru.service99.domain.Model;
import ru.service99.domain.Producer;
import ru.service99.domain.TypeDevice;
import ru.service99.service.ModelService;
import ru.service99.web.ui.ViewUtils;
import ru.service99.web.ui.pages.window.ModelWindow;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
@Scope("prototype")
@SpringView(name = ModelsPage.NAME)
public class ModelsPage extends BasePage {

    public static final String NAME = "models";

    @Autowired
    private ModelService modelService;

    private Table table = new Table();
    ComboBox typeDeviceComboBox;
    ComboBox producerComboBox;

    private static final String ID_COLUMN = "id";
    private static final String TYPE_DEVICE_COLUMN = "typeDevice.name";
    private static final String PRODUCER_COLUMN = "producer.name";
    private static final String NAME_COLUMN = "name";

    private static final String ID_VISIBLE_COLUMN = ViewUtils.getMessage("table.id");
    private static final String NAME_VISIBLE_COLUMN = ViewUtils.getMessage("table.model");
    private static final String TYPE_DEVICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.typeDevice");
    private static final String PRODUCER_VISIBLE_COLUMN = ViewUtils.getMessage("table.producer");

    private static final String[] fieldNames = new String[]{ID_COLUMN, TYPE_DEVICE_COLUMN,
            PRODUCER_COLUMN, NAME_COLUMN};

    @PostConstruct
    public void init() {
        initLayout();
        applyFilter();
    }

    private void initLayout() {
        HorizontalLayout filterLayout = new HorizontalLayout();
        filterLayout.setSpacing(true);
        filterLayout.setDefaultComponentAlignment(Alignment.BOTTOM_CENTER);

        typeDeviceComboBox = createTypeDeviceComboBox();
        producerComboBox = createProducerPlaceComboBox();
        Button applyButton = new Button(ViewUtils.getMessage("button.search"));
        applyButton.addClickListener(clickEvent -> applyFilter());

        filterLayout.addComponent(typeDeviceComboBox);
        filterLayout.addComponent(producerComboBox);
        filterLayout.addComponent(applyButton);

        Button addButton = new Button(ViewUtils.getMessage("button.new"));
        addButton.addClickListener(event -> {
            if (!SecurityUtils.hasAuthority(Authority.редактирование_модели)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
            for (Window window : getUI().getWindows()) if (window instanceof ModelWindow) return;
            ModelWindow window = new ModelWindow(modelService);
            window.addCloseListener(new MyCloseListener());
            getUI().addWindow(window);
        });

        baseLayout.addComponent(filterLayout);
        baseLayout.addComponent(table);
        baseLayout.addComponent(addButton);

        baseLayout.setExpandRatio(filterLayout, 1);
        baseLayout.setExpandRatio(table, 8);
        baseLayout.setExpandRatio(addButton, 1);
    }

    private void applyFilter() {
        TypeDevice typeDevice = (TypeDevice) typeDeviceComboBox.getValue();
        Producer producer = (Producer) producerComboBox.getValue();
        List<Model> models;
        if (typeDevice != null && producer != null) {
            models = modelService.findModelByProducerIdAndTypeDeviceId(
                    producer.getId(), typeDevice.getId());
        } else if (typeDevice == null && producer != null) {
            models = modelService.findModelByProducerId(producer.getId());
        } else if (typeDevice != null) {
            models = modelService.findModelByTypeDeviceId(typeDevice.getId());
        } else {
            models = modelService.findAllModels();
        }
        initTable(createContainer(models));
    }

    private ComboBox createTypeDeviceComboBox() {
        BeanItemContainer<TypeDevice> container = new BeanItemContainer<>(TypeDevice.class);
        List<TypeDevice> typeDevices = modelService.findAllTypeDevices();
        container.addAll(typeDevices);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.typeDevice"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        return select;
    }

    private ComboBox createProducerPlaceComboBox() {
        BeanItemContainer<Producer> container = new BeanItemContainer<>(Producer.class);
        List<Producer> producers = modelService.findAllProducers();
        container.addAll(producers);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.producer"), container);
        select.setItemCaptionPropertyId("name");
        return select;
    }

    private void initTable(BeanItemContainer<Model> container) {
        table.setContainerDataSource(container);
        table.setHeight("100%");
        table.setVisibleColumns(fieldNames);
        table.setColumnHeader(ID_COLUMN, ID_VISIBLE_COLUMN);
        table.setColumnHeader(NAME_COLUMN, NAME_VISIBLE_COLUMN);
        table.setColumnHeader(TYPE_DEVICE_COLUMN, TYPE_DEVICE_VISIBLE_COLUMN);
        table.setColumnHeader(PRODUCER_COLUMN, PRODUCER_VISIBLE_COLUMN);
        table.setSelectable(true);
        table.setImmediate(true);
        table.addItemClickListener(event -> {
            if (!event.isDoubleClick()) return;
            for (Window window : getUI().getWindows()) if (window instanceof ModelWindow) return;
            Model model = (Model) event.getItemId();
            ModelWindow window = new ModelWindow(modelService, model);
            window.addCloseListener(new MyCloseListener());
            getUI().addWindow(window);
        });
    }

    private BeanItemContainer<Model> createContainer(List<Model> models) {
        BeanItemContainer<Model> container = new BeanItemContainer<>(Model.class);
        container.addNestedContainerProperty(TYPE_DEVICE_COLUMN);
        container.addNestedContainerProperty(PRODUCER_COLUMN);
        container.addAll(models);
        return container;
    }

    private class MyCloseListener implements Window.CloseListener {
        @Override
        public void windowClose(Window.CloseEvent closeEvent) {
            //applyFilter();
        }
    }
}
