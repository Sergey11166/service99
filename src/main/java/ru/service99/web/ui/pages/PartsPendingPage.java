package ru.service99.web.ui.pages;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ShortcutAction;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import ru.service99.domain.*;
import ru.service99.service.ModelService;
import ru.service99.service.PartService;
import ru.service99.service.UserService;
import ru.service99.service.criteria.PartPendingSearchCriteria;
import ru.service99.web.ui.ViewUtils;
import ru.service99.web.ui.pages.window.PartPendingWindow;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.Calendar;

@Component
@Scope("prototype")
@SpringView(name = PartsPendingPage.NAME)
public class PartsPendingPage extends BasePage {

    public static final String NAME = "partsPending";

    @Autowired
    private UserService userService;
    @Autowired
    private PartService partService;
    @Autowired
    private ModelService modelService;

    private Table table = new Table();

    private DateField startDateField1;
    private DateField startDateField2;

    private DateField orderedDateField1;
    private DateField orderedDateField2;

    private TextField partNumberTextField;
    private ComboBox modelComboBox;

    private ComboBox producerComboBox;
    private ComboBox typeDeviceComboBox;

    private ComboBox managerComboBox;
    private ComboBox receiverComboBox;

    private TextField idTextField;
    private ComboBox typeComboBox;

    private Label countLabel;
    private ComboBox countComboBox;

    private static final String ID_COLUMN = "id";
    private static final String START_DATE_COLUMN = "startDate";
    private static final String ORDERED_DATE_COLUMN = "orderedDate";
    private static final String PART_NAME_COLUMN = "part.name";
    private static final String TYPE_COLUMN = "type";
    private static final String RECEIVER_COLUMN = "receiver.name";
    private static final String MANAGER_COLUMN = "manager.name";
    private static final String NEEDED_COUNT_COLUMN = "neededCount";
    private static final String ORDERED_COUNT_COLUMN = "orderedCount";
    private static final String RECEIVED_COUNT_COLUMN = "receivedCount";
    private static final String REPAIR_ORDER_COLUMN = "repairOrder.id";
    private static final String SALE_ORDER_COLUMN = "saleOrder.id";
    private static final String COMMENT_COLUMN = "comment";


    private static final String ID_VISIBLE_COLUMN = ViewUtils.getMessage("table.id");
    private static final String START_DATE_VISIBLE_COLUMN = ViewUtils.getMessage("table.startDate");
    private static final String ORDERED_DATE_VISIBLE_COLUMN = ViewUtils.getMessage("table.orderedDate");
    private static final String PART_NAME_VISIBLE_COLUMN = ViewUtils.getMessage("table.partName");
    private static final String TYPE_VISIBLE_COLUMN = ViewUtils.getMessage("table.type");
    private static final String RECEIVER_VISIBLE_COLUMN = ViewUtils.getMessage("table.author");
    private static final String MANAGER_VISIBLE_COLUMN = ViewUtils.getMessage("table.manager");
    private static final String NEEDED_COUNT_VISIBLE_COLUMN = ViewUtils.getMessage("table.neededCount");
    private static final String ORDERED_COUNT_VISIBLE_COLUMN = ViewUtils.getMessage("table.orderedCount");
    private static final String RECEIVED_COUNT_VISIBLE_COLUMN = ViewUtils.getMessage("table.receivedCount");
    private static final String REPAIR_ORDER_VISIBLE_COLUMN = ViewUtils.getMessage("table.repair");
    private static final String SALE_ORDER_VISIBLE_COLUMN = ViewUtils.getMessage("table.sale");
    private static final String COMMENT_VISIBLE_COLUMN = ViewUtils.getMessage("table.comment");

    private static final String[] fieldNames = new String[]{ID_COLUMN, START_DATE_COLUMN, ORDERED_DATE_COLUMN,
            PART_NAME_COLUMN, TYPE_COLUMN, RECEIVER_COLUMN, MANAGER_COLUMN, NEEDED_COUNT_COLUMN, ORDERED_COUNT_COLUMN,
            RECEIVED_COUNT_COLUMN, REPAIR_ORDER_COLUMN, SALE_ORDER_COLUMN, COMMENT_COLUMN};

    @PostConstruct
    public void init() {
        initLayout();
        applyFilter();
    }

    private void initLayout() {
        HorizontalLayout filterContainer = new HorizontalLayout();
        filterContainer.setSpacing(true);

        VerticalLayout filterLayout1 = new VerticalLayout();
        filterLayout1.setSpacing(true);

        VerticalLayout filterLayout2 = new VerticalLayout();
        filterLayout2.setSpacing(true);

        VerticalLayout filterLayout3 = new VerticalLayout();
        filterLayout3.setSpacing(true);

        VerticalLayout filterLayout4 = new VerticalLayout();
        filterLayout4.setSpacing(true);

        VerticalLayout filterLayout5 = new VerticalLayout();
        filterLayout5.setSpacing(true);

        VerticalLayout filterLayout6 = new VerticalLayout();
        filterLayout6.setSpacing(true);

        HorizontalLayout idLayout = new HorizontalLayout();
        idLayout.setSpacing(true);
        idLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);

        HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.setSpacing(true);
        buttonsLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);

        startDateField1 = new DateField(ViewUtils.getMessage("field.startDate1"));
        startDateField1.setWidth("120px");
        startDateField2 = new DateField(ViewUtils.getMessage("field.startDate2"));
        startDateField2.setWidth("120px");

        orderedDateField1 = new DateField(ViewUtils.getMessage("field.orderedDate1"));
        orderedDateField1.setWidth("120px");
        orderedDateField2 = new DateField(ViewUtils.getMessage("field.orderedDate2"));
        orderedDateField2.setWidth("120px");

        typeComboBox = createTypeComboBox();
        typeComboBox.setWidth("150px");

        typeDeviceComboBox = createTypeDeviceComboBox();
        typeDeviceComboBox.setWidth("150px");

        producerComboBox = createProducerComboBox();
        producerComboBox.setWidth("150px");

        modelComboBox = createModelComboBox();
        modelComboBox.setWidth("150px");

        managerComboBox = createManagerComboBox();
        managerComboBox.setWidth("150px");

        receiverComboBox = createReceiverComboBox();
        receiverComboBox.setWidth("150px");

        idTextField = new TextField(ViewUtils.getMessage("field.pendingNumber"));
        idTextField.setWidth("100");
        idTextField.focus();

        partNumberTextField = new TextField(ViewUtils.getMessage("field.partnumber"));
        partNumberTextField.setWidth("240");

        countLabel = new Label();
        countComboBox = createCountComboBox();
        countComboBox.setWidth("100px");

        Button applyButton = new Button(ViewUtils.getMessage("button.select"));
        applyButton.addClickListener(clickEvent -> applyFilter());
        applyButton.setWidth("105px");
        applyButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);

        Button addButton = new Button(ViewUtils.getMessage("button.new"));
        addButton.addClickListener(event -> {
            for (Window window : getUI().getWindows()) if (window instanceof PartPendingWindow) return;
            PartPendingWindow window = new PartPendingWindow(partService, modelService);
            window.addCloseListener(new WindowCloseListener());
            getUI().addWindow(window);
        });

        Button clearButton = new Button(ViewUtils.getMessage("button.clear"));
        clearButton.addClickListener(event -> cleanButtonClick());

        /*AdvancedFileDownloader ticketDownloader = new AdvancedFileDownloader();
        ticketDownloader.setFilePath(getClass().getClassLoader()
                .getResource("excel/" + ViewUtils.getMessage("file.sales") + ".xls").getFile());
        ticketDownloader.addAdvancedDownloaderListener(event -> FileUtils.writeSalesExcel(getClass(), orders));
        */
        Button excelButton = new Button(ViewUtils.getMessage("button.excel"));
        //ticketDownloader.extend(excelButton);

        filterLayout1.addComponent(startDateField1);
        filterLayout1.addComponent(startDateField2);

        filterLayout2.addComponent(orderedDateField1);
        filterLayout2.addComponent(orderedDateField2);

        filterLayout3.addComponent(typeComboBox);
        filterLayout3.addComponent(typeDeviceComboBox);

        filterLayout4.addComponent(producerComboBox);
        filterLayout4.addComponent(modelComboBox);

        filterLayout5.addComponent(receiverComboBox);
        filterLayout5.addComponent(managerComboBox);

        idLayout.addComponent(idTextField);
        idLayout.addComponent(applyButton);

        filterLayout6.addComponent(idLayout);
        filterLayout6.addComponent(partNumberTextField);

        buttonsLayout.addComponent(addButton);
        buttonsLayout.addComponent(clearButton);
        buttonsLayout.addComponent(excelButton);
        buttonsLayout.addComponent(countLabel);
        buttonsLayout.addComponent(countComboBox);

        filterContainer.addComponent(filterLayout1);
        filterContainer.addComponent(filterLayout2);
        filterContainer.addComponent(filterLayout3);
        filterContainer.addComponent(filterLayout4);
        filterContainer.addComponent(filterLayout5);
        filterContainer.addComponent(filterLayout6);

        baseLayout.addComponent(filterContainer);
        baseLayout.addComponent(buttonsLayout);
        baseLayout.addComponent(table);

        baseLayout.setExpandRatio(filterContainer, 2.5f);
        baseLayout.setExpandRatio(buttonsLayout, 1);
        baseLayout.setExpandRatio(table, 10);
    }

    private void cleanButtonClick() {
        startDateField1.setValue(null);
        startDateField2.setValue(null);
        orderedDateField1.setValue(null);
        orderedDateField2.setValue(null);
        typeComboBox.setValue(null);
        partNumberTextField.setValue("");
        managerComboBox.setValue(null);
        receiverComboBox.setValue(null);
        typeDeviceComboBox.setValue(null);
        producerComboBox.setValue(null);
        modelComboBox.setValue(null);
        idTextField.setValue("");
    }

    private ComboBox createTypeComboBox() {
        BeanItemContainer<TypePartPending> container = new BeanItemContainer<>(TypePartPending.class);
        container.addAll(Arrays.asList(TypePartPending.values()));
        return new ComboBox(ViewUtils.getMessage("field.typePartPending"), container);
    }

    private ComboBox createManagerComboBox() {
        BeanItemContainer<User> container = new BeanItemContainer<>(User.class);
        List<User> users = userService.findAll();
        container.addAll(users);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.manager"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        return select;
    }

    private ComboBox createReceiverComboBox() {
        BeanItemContainer<User> container = new BeanItemContainer<>(User.class);
        List<User> users = userService.findAll();
        container.addAll(users);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.author"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        return select;
    }

    private ComboBox createTypeDeviceComboBox() {
        BeanItemContainer<TypeDevice> container = new BeanItemContainer<>(TypeDevice.class);
        List<TypeDevice> typeDevices = modelService.findAllTypeDevices();
        container.addAll(typeDevices);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.typeDevice"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.addValueChangeListener(event -> {
            Producer producer = (Producer) producerComboBox.getValue();
            if (producer == null) return;
            TypeDevice typeDevice = (TypeDevice) event.getProperty().getValue();
            List<Model> models;
            if (typeDevice != null) {
                models = modelService.findModelByProducerIdAndTypeDeviceId(
                        producer.getId(), typeDevice.getId());
            } else {
                models = modelService.findModelByProducerId(producer.getId());
            }
            BeanItemContainer<Model> modelContainer = new BeanItemContainer<>(Model.class);
            modelContainer.addAll(models);
            modelComboBox.setContainerDataSource(modelContainer);
        });
        return select;
    }

    private ComboBox createProducerComboBox() {
        BeanItemContainer<Producer> container = new BeanItemContainer<>(Producer.class);
        List<Producer> producers = modelService.findAllProducers();
        container.addAll(producers);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.producer"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.addValueChangeListener(event -> {
            TypeDevice typeDevice = (TypeDevice) typeDeviceComboBox.getValue();
            Producer producer = (Producer) event.getProperty().getValue();
            List<Model> models = new ArrayList<>();
            if (producer != null) {
                modelComboBox.setEnabled(true);
                if (typeDevice != null) {
                    models = modelService.findModelByProducerIdAndTypeDeviceId(
                            producer.getId(), typeDevice.getId());
                } else {
                    models = modelService.findModelByProducerId(producer.getId());
                }
            } else modelComboBox.setEnabled(false);
            BeanItemContainer<Model> modelContainer = new BeanItemContainer<>(Model.class);
            modelContainer.addAll(models);
            modelComboBox.setContainerDataSource(modelContainer);
        });
        return select;
    }

    private ComboBox createModelComboBox() {
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.model"));
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.setEnabled(false);
        return select;
    }

    private ComboBox createCountComboBox() {
        ComboBox select = new ComboBox();
        select.setNullSelectionAllowed(false);
        select.addItem("100");
        select.addItem("500");
        select.addItem("1000");
        select.select("100");
        select.addValueChangeListener(event -> applyFilter());
        return select;
    }

    private void applyFilter() {
        Long id = null;
        if (!idTextField.getValue().isEmpty()) {
            try {
                id = Long.parseLong(idTextField.getValue());
            } catch (NumberFormatException e) {
                Notification.show(ViewUtils.getMessage("notifications.wrongPartPendingNumber"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
        }

        PartPendingSearchCriteria criteria = new PartPendingSearchCriteria();

        criteria.setId(id);
        criteria.setStartDate1(startDateField1.getValue());
        criteria.setOrderedDate1(orderedDateField1.getValue());
        criteria.setType((TypePartPending) typeComboBox.getValue());
        criteria.setManager((User) managerComboBox.getValue());
        criteria.setReceiver((User) receiverComboBox.getValue());
        criteria.setPartNumber(partNumberTextField.getValue());
        criteria.setTypeDevice((TypeDevice) typeDeviceComboBox.getValue());
        criteria.setProducer((Producer) producerComboBox.getValue());
        criteria.setModel((Model) modelComboBox.getValue());

        Date startDate2 = startDateField2.getValue();
        if (startDate2 != null) {
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(startDate2);
            calendar.add(Calendar.DATE, 1);
            criteria.setStartDate2(calendar.getTime());
        }

        Date orderedDate2 = orderedDateField2.getValue();
        if (orderedDate2 != null) {
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(orderedDate2);
            calendar.add(Calendar.DATE, 1);
            criteria.setOrderedDate2(calendar.getTime());
        }

        int count = Integer.parseInt((String) countComboBox.getValue());

        Page<PartPending> page = partService.findPartsPendingByCriteria(criteria,
                new PageRequest(0, count, new Sort(Sort.Direction.DESC, "id")));
        List<PartPending> partsPending = page.getContent();
        initTable(createContainer(partsPending));
        countLabel.setValue(
                ViewUtils.getMessage("field.showCount") + " " + partsPending.size() + " " +
                        ViewUtils.getMessage("string.of") + " " + page.getTotalElements());
    }

    private void initTable(BeanItemContainer<PartPending> container) {
        table.setContainerDataSource(container);
        table.setSizeFull();
        table.setColumnCollapsingAllowed(true);
        table.setColumnCollapsed(COMMENT_COLUMN, true);
        table.setColumnCollapsed(START_DATE_COLUMN, true);
        table.setColumnCollapsed(ORDERED_DATE_COLUMN, true);
        TypePartPending type = (TypePartPending) typeComboBox.getValue();
        if (type != null) {
            if (type == TypePartPending.ремонт) table.setColumnCollapsed(REPAIR_ORDER_COLUMN, false);
            else table.setColumnCollapsed(REPAIR_ORDER_COLUMN, true);
            if (type == TypePartPending.продажа) table.setColumnCollapsed(SALE_ORDER_COLUMN, false);
            else table.setColumnCollapsed(SALE_ORDER_COLUMN, true);
        } else {
            table.setColumnCollapsed(REPAIR_ORDER_COLUMN, false);
            table.setColumnCollapsed(SALE_ORDER_COLUMN, false);
        }
        table.setVisibleColumns(fieldNames);
        table.setColumnHeader(ID_COLUMN, ID_VISIBLE_COLUMN);
        table.setColumnHeader(START_DATE_COLUMN, START_DATE_VISIBLE_COLUMN);
        table.setColumnHeader(ORDERED_DATE_COLUMN, ORDERED_DATE_VISIBLE_COLUMN);
        table.setColumnHeader(PART_NAME_COLUMN, PART_NAME_VISIBLE_COLUMN);
        table.setColumnHeader(TYPE_COLUMN, TYPE_VISIBLE_COLUMN);
        table.setColumnHeader(MANAGER_COLUMN, MANAGER_VISIBLE_COLUMN);
        table.setColumnHeader(RECEIVER_COLUMN, RECEIVER_VISIBLE_COLUMN);
        table.setColumnHeader(NEEDED_COUNT_COLUMN, NEEDED_COUNT_VISIBLE_COLUMN);
        table.setColumnHeader(ORDERED_COUNT_COLUMN, ORDERED_COUNT_VISIBLE_COLUMN);
        table.setColumnHeader(RECEIVED_COUNT_COLUMN, RECEIVED_COUNT_VISIBLE_COLUMN);
        table.setColumnHeader(REPAIR_ORDER_COLUMN, REPAIR_ORDER_VISIBLE_COLUMN);
        table.setColumnHeader(SALE_ORDER_COLUMN, SALE_ORDER_VISIBLE_COLUMN);
        table.setColumnHeader(COMMENT_COLUMN, COMMENT_VISIBLE_COLUMN);
        table.setSelectable(true);
        table.setImmediate(true);
        table.addItemClickListener(event -> {
            if (!event.isDoubleClick()) return;
            for (Window window : getUI().getWindows()) if (window instanceof PartPendingWindow) return;
            PartPending partPending = (PartPending) event.getItemId();
            PartPendingWindow window = new PartPendingWindow(partService, modelService, partPending);
            window.addCloseListener(new WindowCloseListener());
            getUI().addWindow(window);
        });
    }

    private BeanItemContainer<PartPending> createContainer(List<PartPending> partsPending) {
        BeanItemContainer<PartPending> container = new BeanItemContainer<>(PartPending.class);
        container.addAll(partsPending);
        container.addNestedContainerProperty(PART_NAME_COLUMN);
        container.addNestedContainerProperty(MANAGER_COLUMN);
        container.addNestedContainerProperty(RECEIVER_COLUMN);
        container.addNestedContainerProperty(REPAIR_ORDER_COLUMN);
        container.addNestedContainerProperty(SALE_ORDER_COLUMN);
        return container;
    }

    private class WindowCloseListener implements Window.CloseListener {
        @Override
        public void windowClose(Window.CloseEvent closeEvent) {
            //applyFilter();
        }
    }
}
