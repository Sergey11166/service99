package ru.service99.web.ui.pages;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ShortcutAction;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import ru.service99.SecurityUtils;
import ru.service99.domain.*;
import ru.service99.service.*;
import ru.service99.service.criteria.RepairOrderSearchCriteria;
import ru.service99.web.AdvancedFileDownloader;
import ru.service99.web.FileUtils;
import ru.service99.web.ui.OnCustomerChoiceListener;
import ru.service99.web.ui.ViewUtils;
import ru.service99.web.ui.pages.window.RepairWindow;
import ru.service99.web.ui.pages.window.SelectCustomerWindow;

import javax.annotation.PostConstruct;
import java.net.URL;
import java.util.*;
import java.util.Calendar;

@Component
@Scope("prototype")
@SpringView(name = RepairsPage.NAME)
public class RepairsPage extends BasePage {

    public static final String NAME = "repairs";

    @Autowired
    private RepairOrderService repairOrderService;
    @Autowired
    private ModelService modelService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private PlaceService placeService;
    @Autowired
    private UserService userService;
    @Autowired
    private FirmService firmService;
    @Autowired
    private PartService partService;

    private List<RepairOrder> orders;

    private Table table = new Table();

    private DateField startDateField1;
    private DateField startDateField2;

    private DateField endDateField1;
    private DateField endDateField2;

    private ComboBox statusComboBox;
    private ComboBox typeDeviceComboBox;

    private ComboBox producerComboBox;
    private ComboBox modelComboBox;

    private ComboBox placeReceiveComboBox;
    private ComboBox currentPlaceComboBox;

    private ComboBox masterComboBox;
    private ComboBox receiverComboBox;

    private TextField imeiStTextField;
    private ComboBox customerComboBox;

    private TextField idTextField;

    private Label countLabel;
    private ComboBox countComboBox;

    private static final String ID_COLUMN = "id";
    private static final String START_DATE_COLUMN = "startDate";
    private static final String END_DATE_COLUMN = "endDate";
    private static final String TYPE_DEVICE_COLUMN = "model.typeDevice.name";
    private static final String PRODUCER_COLUMN = "model.producer.name";
    private static final String MODEL_COLUMN = "model.name";
    private static final String IMEI_COLUMN = "imei";
    private static final String SN_COLUMN = "sn";
    private static final String STATUS_COLUMN = "status";
    private static final String CUSTOMER_COLUMN = "customer.name";
    private static final String MASTER_COLUMN = "master.name";
    private static final String RECEIVER_COLUMN = "receiver.name";

    private static final String ID_VISIBLE_COLUMN = ViewUtils.getMessage("table.id");
    private static final String START_DATE_VISIBLE_COLUMN = ViewUtils.getMessage("table.startDate");
    private static final String END_DATE_VISIBLE_COLUMN = ViewUtils.getMessage("table.endDate");
    private static final String TYPE_DEVICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.typeDevice");
    private static final String PRODUCER_VISIBLE_COLUMN = ViewUtils.getMessage("table.producer");
    private static final String MODEL_VISIBLE_COLUMN = ViewUtils.getMessage("table.model");
    private static final String IMEI_VISIBLE_COLUMN = ViewUtils.getMessage("table.imei");
    private static final String SN_VISIBLE_COLUMN = ViewUtils.getMessage("table.sn");
    private static final String STATUS_VISIBLE_COLUMN = ViewUtils.getMessage("table.status");
    private static final String CUSTOMER_VISIBLE_COLUMN = ViewUtils.getMessage("table.customer");
    private static final String MASTER_VISIBLE_COLUMN = ViewUtils.getMessage("table.master");
    private static final String RECEIVER_VISIBLE_COLUMN = ViewUtils.getMessage("table.receiver");

    private static final String[] fieldNames = new String[]{ID_COLUMN, START_DATE_COLUMN, END_DATE_COLUMN,
            TYPE_DEVICE_COLUMN, PRODUCER_COLUMN, MODEL_COLUMN, IMEI_COLUMN, SN_COLUMN, STATUS_COLUMN,
            CUSTOMER_COLUMN, MASTER_COLUMN, RECEIVER_COLUMN};

    @PostConstruct
    public void init() {
        initLayout();
        applyFilter();
    }

    private void initLayout() {
        HorizontalLayout filterContainer = new HorizontalLayout();
        filterContainer.setSpacing(true);

        VerticalLayout filterLayout1 = new VerticalLayout();
        filterLayout1.setSpacing(true);

        VerticalLayout filterLayout2 = new VerticalLayout();
        filterLayout2.setSpacing(true);

        VerticalLayout filterLayout3 = new VerticalLayout();
        filterLayout3.setSpacing(true);

        VerticalLayout filterLayout4 = new VerticalLayout();
        filterLayout4.setSpacing(true);

        VerticalLayout filterLayout5 = new VerticalLayout();
        filterLayout5.setSpacing(true);

        VerticalLayout filterLayout6 = new VerticalLayout();
        filterLayout6.setSpacing(true);

        VerticalLayout filterLayout7 = new VerticalLayout();
        filterLayout7.setSpacing(true);

        VerticalLayout filterLayout8 = new VerticalLayout();
        filterLayout8.setSpacing(true);

        HorizontalLayout customerLayout = new HorizontalLayout();
        customerLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);

        HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.setSpacing(true);
        buttonsLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);

        startDateField1 = new DateField(ViewUtils.getMessage("field.startDate1"));
        startDateField1.setWidth("120px");
        startDateField2 = new DateField(ViewUtils.getMessage("field.startDate2"));
        startDateField2.setWidth("120px");

        endDateField1 = new DateField(ViewUtils.getMessage("field.endDate1"));
        endDateField1.setWidth("120px");
        endDateField2 = new DateField(ViewUtils.getMessage("field.endDate2"));
        endDateField2.setWidth("120px");

        statusComboBox = createStatusComboBox();
        statusComboBox.setWidth("150px");
        typeDeviceComboBox = createTypeDeviceComboBox();
        typeDeviceComboBox.setWidth("150px");

        producerComboBox = createProducerComboBox();
        producerComboBox.setWidth("150px");
        modelComboBox = createModelComboBox();
        modelComboBox.setWidth("150px");

        placeReceiveComboBox = createPlaceReceiveComboBox();
        placeReceiveComboBox.setWidth("140px");
        currentPlaceComboBox = createCurrentPlaceComboBox();
        currentPlaceComboBox.setWidth("140px");

        masterComboBox = createMasterComboBox();
        masterComboBox.setWidth("150px");
        receiverComboBox = createReceiverComboBox();
        receiverComboBox.setWidth("150px");

        imeiStTextField = new TextField(ViewUtils.getMessage("field.imeiSn"));
        imeiStTextField.setWidth("200px");
        customerComboBox = createCustomerComboBox();
        customerComboBox.setWidth("170px");

        idTextField = new TextField(ViewUtils.getMessage("field.repairNumber"));
        idTextField.setWidth("100px");
        idTextField.focus();

        countLabel = new Label();
        countComboBox = createCountComboBox();
        countComboBox.setWidth("100px");

        Button applyButton = new Button(ViewUtils.getMessage("button.select"));
        applyButton.addClickListener(clickEvent -> applyFilter());
        applyButton.setHeight("60px");
        applyButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);

        Button addButton = new Button(ViewUtils.getMessage("button.new"));
        addButton.addClickListener(event -> {
            if (!SecurityUtils.hasAuthority(Authority.новый_ремонт)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"), Notification.Type.ERROR_MESSAGE);
                return;
            }
            for (Window window : getUI().getWindows()) if (window instanceof RepairWindow) return;
            RepairWindow window = new RepairWindow(
                    repairOrderService,
                    modelService,
                    customerService,
                    firmService,
                    userService,
                    partService);
            window.addCloseListener(new WindowCloseListener());
            getUI().addWindow(window);
        });

        Button choiceCustomerButton = new Button("*");
        choiceCustomerButton.setWidth("15px");
        choiceCustomerButton.addClickListener(event -> {
            for (Window window : getUI().getWindows()) if (window instanceof SelectCustomerWindow) return;
            SelectCustomerWindow window = new SelectCustomerWindow(customerService);
            window.setCustomerChoiceListener(new MyCustomerChoiceListener());
            getUI().addWindow(window);
        });

        Button clearButton = new Button(ViewUtils.getMessage("button.clear"));
        clearButton.addClickListener(event -> cleanButtonClick());

        AdvancedFileDownloader repairsDownloader = new AdvancedFileDownloader();
        URL url = getClass().getClassLoader().getResource("excel/" + ViewUtils.getMessage("file.repairs") + ".xls");
        assert url != null;
        repairsDownloader.setFilePath(url.getFile());
        repairsDownloader.addAdvancedDownloaderListener(event -> FileUtils.writeRepairsExcel(getClass(), orders));
        Button excelButton = new Button(ViewUtils.getMessage("button.excel"));
        repairsDownloader.extend(excelButton);
        if (!SecurityUtils.hasAuthority(Authority.загрузка_отчетов)) excelButton.setEnabled(false);

        filterLayout1.addComponent(startDateField1);
        filterLayout1.addComponent(startDateField2);

        filterLayout2.addComponent(endDateField1);
        filterLayout2.addComponent(endDateField2);

        filterLayout3.addComponent(statusComboBox);
        filterLayout3.addComponent(typeDeviceComboBox);

        filterLayout4.addComponent(producerComboBox);
        filterLayout4.addComponent(modelComboBox);

        filterLayout5.addComponent(placeReceiveComboBox);
        filterLayout5.addComponent(currentPlaceComboBox);

        filterLayout6.addComponent(masterComboBox);
        filterLayout6.addComponent(receiverComboBox);

        customerLayout.addComponent(customerComboBox);
        customerLayout.addComponent(choiceCustomerButton);

        filterLayout7.addComponent(imeiStTextField);
        filterLayout7.addComponent(customerLayout);

        filterLayout8.addComponent(idTextField);
        filterLayout8.addComponent(applyButton);

        buttonsLayout.addComponent(addButton);
        buttonsLayout.addComponent(clearButton);
        buttonsLayout.addComponent(excelButton);
        buttonsLayout.addComponent(countLabel);
        buttonsLayout.addComponent(countComboBox);

        filterContainer.addComponent(filterLayout1);
        filterContainer.addComponent(filterLayout2);
        filterContainer.addComponent(filterLayout3);
        filterContainer.addComponent(filterLayout4);
        filterContainer.addComponent(filterLayout5);
        filterContainer.addComponent(filterLayout6);
        filterContainer.addComponent(filterLayout7);
        filterContainer.addComponent(filterLayout8);

        baseLayout.addComponent(filterContainer);
        baseLayout.addComponent(buttonsLayout);
        baseLayout.addComponent(table);

        baseLayout.setExpandRatio(filterContainer, 2.5f);
        baseLayout.setExpandRatio(buttonsLayout, 1);
        baseLayout.setExpandRatio(table, 10);
    }

    private void cleanButtonClick() {
        startDateField1.setValue(null);
        startDateField2.setValue(null);
        endDateField1.setValue(null);
        endDateField2.setValue(null);
        statusComboBox.setValue(null);
        typeDeviceComboBox.setValue(null);
        producerComboBox.setValue(null);
        modelComboBox.setValue(null);
        modelComboBox.setEnabled(false);
        customerComboBox.setValue(null);
        masterComboBox.setValue(null);
        receiverComboBox.setValue(null);
        imeiStTextField.setValue("");
        placeReceiveComboBox.setValue(null);
        currentPlaceComboBox.setValue(null);
        idTextField.setValue("");
    }

    private ComboBox createStatusComboBox() {
        BeanItemContainer<StatusRepairOrder> container = new BeanItemContainer<>(StatusRepairOrder.class);
        container.addAll(Arrays.asList(StatusRepairOrder.values()));
        return new ComboBox(ViewUtils.getMessage("field.status"), container);
    }

    private ComboBox createTypeDeviceComboBox() {
        BeanItemContainer<TypeDevice> container = new BeanItemContainer<>(TypeDevice.class);
        List<TypeDevice> typeDevices = modelService.findAllTypeDevices();
        container.addAll(typeDevices);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.typeDevice"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.addValueChangeListener(event -> {
            Producer producer = (Producer) producerComboBox.getValue();
            if (producer == null) return;
            TypeDevice typeDevice = (TypeDevice) event.getProperty().getValue();
            List<Model> models;
            if (typeDevice != null) {
                models = modelService.findModelByProducerIdAndTypeDeviceId(
                        producer.getId(), typeDevice.getId());
            } else {
                models = modelService.findModelByProducerId(producer.getId());
            }
            BeanItemContainer<Model> modelContainer = new BeanItemContainer<>(Model.class);
            modelContainer.addAll(models);
            modelComboBox.setContainerDataSource(modelContainer);
        });
        return select;
    }

    private ComboBox createProducerComboBox() {
        BeanItemContainer<Producer> container = new BeanItemContainer<>(Producer.class);
        List<Producer> producers = modelService.findAllProducers();
        container.addAll(producers);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.producer"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.addValueChangeListener(event -> {
            TypeDevice typeDevice = (TypeDevice) typeDeviceComboBox.getValue();
            Producer producer = (Producer) event.getProperty().getValue();
            List<Model> models = new ArrayList<>();
            if (producer != null) {
                modelComboBox.setEnabled(true);
                if (typeDevice != null) {
                    models = modelService.findModelByProducerIdAndTypeDeviceId(
                            producer.getId(), typeDevice.getId());
                } else {
                    models = modelService.findModelByProducerId(producer.getId());
                }
            } else modelComboBox.setEnabled(false);
            BeanItemContainer<Model> modelContainer = new BeanItemContainer<>(Model.class);
            modelContainer.addAll(models);
            modelComboBox.setContainerDataSource(modelContainer);
        });
        return select;
    }

    private ComboBox createModelComboBox() {
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.model"));
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.setEnabled(false);
        return select;
    }

    private ComboBox createCustomerComboBox() {
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.customer"));
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        return select;
    }

    private ComboBox createMasterComboBox() {
        BeanItemContainer<User> container = new BeanItemContainer<>(User.class);
        List<User> users = userService.findAll();
        container.addAll(users);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.master"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        return select;
    }

    private ComboBox createReceiverComboBox() {
        BeanItemContainer<User> container = new BeanItemContainer<>(User.class);
        List<User> users = userService.findAll();
        container.addAll(users);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.receiver"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        return select;
    }

    private ComboBox createPlaceReceiveComboBox() {
        BeanItemContainer<Place> container = new BeanItemContainer<>(Place.class);
        List<Place> places = placeService.findAllPlaces();
        container.addAll(places);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.placeReceive"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        return select;
    }

    private ComboBox createCurrentPlaceComboBox() {
        BeanItemContainer<Place> container = new BeanItemContainer<>(Place.class);
        List<Place> places = placeService.findAllPlaces();
        container.addAll(places);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.currentPlace"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        return select;
    }

    private ComboBox createCountComboBox() {
        ComboBox select = new ComboBox();
        select.setNullSelectionAllowed(false);
        select.addItem("100");
        select.addItem("500");
        select.addItem("1000");
        select.select("100");
        select.addValueChangeListener(event -> applyFilter());
        return select;
    }

    private void applyFilter() {
        Long id = null;
        if (!idTextField.getValue().isEmpty()) {
            try {
                id = Long.parseLong(idTextField.getValue());
            } catch (NumberFormatException e) {
                Notification.show(ViewUtils.getMessage("notifications.wrongRepairNumber"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
        }

        RepairOrderSearchCriteria criteria = new RepairOrderSearchCriteria();

        criteria.setId(id);
        criteria.setStartDate1(startDateField1.getValue());
        criteria.setEndDate1(endDateField1.getValue());
        criteria.setImeiSn("%" + imeiStTextField.getValue() + "%");
        criteria.setStatus((StatusRepairOrder) statusComboBox.getValue());
        criteria.setTypeDevice((TypeDevice) typeDeviceComboBox.getValue());
        criteria.setProducer((Producer) producerComboBox.getValue());
        criteria.setModel((Model) modelComboBox.getValue());
        criteria.setCustomer((Customer) customerComboBox.getValue());
        criteria.setMaster((User) masterComboBox.getValue());
        criteria.setReceiver((User) receiverComboBox.getValue());
        criteria.setPlaceReceive((Place) placeReceiveComboBox.getValue());
        criteria.setCurrentPlace((Place) currentPlaceComboBox.getValue());

        Date startDate2 = startDateField2.getValue();
        if (startDate2 != null) {
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(startDate2);
            calendar.add(Calendar.DATE, 1);
            criteria.setStartDate2(calendar.getTime());
        }

        Date endDate2 = endDateField2.getValue();
        if (endDate2 != null) {
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(endDate2);
            calendar.add(Calendar.DATE, 1);
            criteria.setEndDate2(calendar.getTime());
        }

        int count = Integer.parseInt((String) countComboBox.getValue());

        Page<RepairOrder> page = repairOrderService.findByCriteria(criteria,
                new PageRequest(0, count, new Sort(Sort.Direction.DESC, "id")));
        orders = page.getContent();
        initTable(createContainer(orders));
        countLabel.setValue(ViewUtils.getMessage("field.showCount") + " " + orders.size() + " " +
                ViewUtils.getMessage("string.of") + " " + page.getTotalElements());
    }

    private void initTable(BeanItemContainer<RepairOrder> container) {
        table.setContainerDataSource(container);
        table.setSizeFull();
        table.setColumnCollapsingAllowed(true);
        //table.setColumnCollapsed(MASTER_COLUMN, true);
        //table.setColumnCollapsed(CUSTOMER_COLUMN, true);
        //table.setColumnCollapsed(RECEIVER_COLUMN, true);
        //table.setColumnCollapsed(END_DATE_COLUMN, true);
        //table.setColumnCollapsed(TYPE_DEVICE_COLUMN, true);
        table.setVisibleColumns(fieldNames);
        table.setColumnHeader(ID_COLUMN, ID_VISIBLE_COLUMN);
        table.setColumnHeader(START_DATE_COLUMN, START_DATE_VISIBLE_COLUMN);
        table.setColumnHeader(END_DATE_COLUMN, END_DATE_VISIBLE_COLUMN);
        table.setColumnHeader(TYPE_DEVICE_COLUMN, TYPE_DEVICE_VISIBLE_COLUMN);
        table.setColumnHeader(PRODUCER_COLUMN, PRODUCER_VISIBLE_COLUMN);
        table.setColumnHeader(MODEL_COLUMN, MODEL_VISIBLE_COLUMN);
        table.setColumnHeader(IMEI_COLUMN, IMEI_VISIBLE_COLUMN);
        table.setColumnHeader(SN_COLUMN, SN_VISIBLE_COLUMN);
        table.setColumnHeader(STATUS_COLUMN, STATUS_VISIBLE_COLUMN);
        table.setColumnHeader(CUSTOMER_COLUMN, CUSTOMER_VISIBLE_COLUMN);
        table.setColumnHeader(MASTER_COLUMN, MASTER_VISIBLE_COLUMN);
        table.setColumnHeader(RECEIVER_COLUMN, RECEIVER_VISIBLE_COLUMN);
        table.setSelectable(true);
        table.setImmediate(true);
        table.addItemClickListener(event -> {
            if (!event.isDoubleClick()) return;
            for (Window window : getUI().getWindows()) if (window instanceof RepairWindow) return;
            RepairOrder order = (RepairOrder) event.getItemId();
            RepairWindow window = new RepairWindow(
                    repairOrderService,
                    modelService,
                    customerService,
                    firmService,
                    userService,
                    partService,
                    order);
            window.addCloseListener(new WindowCloseListener());
            getUI().addWindow(window);
        });
    }

    private BeanItemContainer<RepairOrder> createContainer(List<RepairOrder> repairOrders) {
        BeanItemContainer<RepairOrder> container = new BeanItemContainer<>(RepairOrder.class);
        container.addAll(repairOrders);
        container.addNestedContainerProperty(TYPE_DEVICE_COLUMN);
        container.addNestedContainerProperty(PRODUCER_COLUMN);
        container.addNestedContainerProperty(MODEL_COLUMN);
        container.addNestedContainerProperty(CUSTOMER_COLUMN);
        container.addNestedContainerProperty(MASTER_COLUMN);
        container.addNestedContainerProperty(RECEIVER_COLUMN);
        return container;
    }

    private class WindowCloseListener implements Window.CloseListener {
        @Override
        public void windowClose(Window.CloseEvent closeEvent) {
            //applyFilter();
        }
    }

    private class MyCustomerChoiceListener implements OnCustomerChoiceListener {
        @Override
        public void onChoice(Customer customer) {
            BeanItemContainer<Customer> container = new BeanItemContainer<>(Customer.class);
            List<Customer> customers = new ArrayList<>(1);
            customers.add(customer);
            container.addAll(customers);
            customerComboBox.setContainerDataSource(container);
            customerComboBox.select(customer);
            customerComboBox.setDescription(customer.description());
        }
    }
}
