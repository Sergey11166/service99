package ru.service99.web.ui.pages;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.Window;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.service99.SecurityUtils;
import ru.service99.domain.Authority;
import ru.service99.domain.Firm;
import ru.service99.service.FirmService;
import ru.service99.web.ui.ViewUtils;
import ru.service99.web.ui.pages.window.FirmWindow;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
@Scope("prototype")
@SpringView(name = FirmsPage.NAME)
public class FirmsPage extends BasePage {

    public static final String NAME = "firms";

    private static final String ID_COLUMN = "id";
    private static final String NAME_COLUMN = "name";
    private static final String INN_COLUMN = "INN";

    private static final String ID_VISIBLE_COLUMN = ViewUtils.getMessage("table.id");
    private static final String NAME_VISIBLE_COLUMN = ViewUtils.getMessage("table.name");
    private static final String INN_VISIBLE_COLUMN = ViewUtils.getMessage("table.inn");

    private static final String[] fieldNames = new String[]{ID_COLUMN, NAME_COLUMN, INN_COLUMN};

    @Autowired
    private FirmService firmService;

    private Table table = new Table();

    @PostConstruct
    public void init() {
        initLayout();
        initTable(createContainer(firmService.findAll()));
    }

    private void initLayout() {
        Button addButton = new Button(ViewUtils.getMessage("button.new"));
        addButton.addClickListener(event -> {
            if (!SecurityUtils.hasAuthority(Authority.редактирование_фирмы)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
            for (Window window : getUI().getWindows()) if (window instanceof FirmWindow) return;
            FirmWindow window = new FirmWindow(firmService);
            window.addCloseListener(new MyCloseListener());
            getUI().addWindow(window);
        });

        baseLayout.addComponent(table);
        baseLayout.addComponent(addButton);

        baseLayout.setExpandRatio(table, 8);
        baseLayout.setExpandRatio(addButton, 1);
    }

    private void initTable(BeanItemContainer<Firm> container) {
        table.setContainerDataSource(container);
        table.setHeight("100%");
        table.setVisibleColumns(fieldNames);
        table.setColumnHeader(ID_COLUMN, ID_VISIBLE_COLUMN);
        table.setColumnHeader(NAME_COLUMN, NAME_VISIBLE_COLUMN);
        table.setColumnHeader(INN_COLUMN, INN_VISIBLE_COLUMN);
        table.setSelectable(true);
        table.setImmediate(true);
        table.addItemClickListener(event -> {
            if (!event.isDoubleClick()) return;
            for (Window window : getUI().getWindows()) if (window instanceof FirmWindow) return;
            Firm firm = (Firm) event.getItemId();
            FirmWindow window = new FirmWindow(firmService, firm);
            window.addCloseListener(new MyCloseListener());
            getUI().addWindow(window);
        });
    }

    private BeanItemContainer<Firm> createContainer(List<Firm> firms) {
        BeanItemContainer<Firm> container = new BeanItemContainer<>(Firm.class);
        container.addAll(firms);
        return container;
    }

    private class MyCloseListener implements Window.CloseListener {
        @Override
        public void windowClose(Window.CloseEvent closeEvent) {
            initTable(createContainer(firmService.findAll()));
        }
    }
}
