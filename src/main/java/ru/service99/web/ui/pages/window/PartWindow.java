package ru.service99.web.ui.pages.window;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.shared.ui.MultiSelectMode;
import com.vaadin.ui.*;
import ru.service99.SecurityUtils;
import ru.service99.domain.*;
import ru.service99.service.ModelService;
import ru.service99.service.PartService;
import ru.service99.web.ui.ViewUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class PartWindow extends Window {

    private static final String ID_COLUMN = "id";
    private static final String TYPE_DEVICE_COLUMN = "typeDevice.name";
    private static final String PRODUCER_COLUMN = "producer.name";
    private static final String NAME_COLUMN = "name";

    private static final String ID_VISIBLE_COLUMN = ViewUtils.getMessage("table.id");
    private static final String TYPE_DEVICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.typeDevice");
    private static final String PRODUCER_VISIBLE_COLUMN = ViewUtils.getMessage("table.producer");
    private static final String NAME_VISIBLE_COLUMN = ViewUtils.getMessage("table.name");

    private static final String[] fieldNames = new String[]{ID_COLUMN, TYPE_DEVICE_COLUMN,
            PRODUCER_COLUMN, NAME_COLUMN};

    private ModelService modelService;
    private PartService partService;

    private Part part;

    private Table table = new Table();

    private ComboBox typeDeviceComboBox;
    private ComboBox producerComboBox;

    public PartWindow(PartService partService, ModelService modelService) {
        setCaption(ViewUtils.getMessage("partsPage.window.title.new"));
        this.modelService = modelService;
        this.partService = partService;
        init();
    }

    public PartWindow(PartService partService, ModelService modelService, Part part) {
        this(partService, modelService);
        setCaption(ViewUtils.getMessage("partsPage.window.title.edit") + " " + part.getPartNumber());
        this.part = part;
        init();
    }

    private void init() {
        initLayout();
        if (part != null) initTable(createContainer(new ArrayList<>(part.getModels())));
        else initTable(createContainer(new ArrayList<>(modelService.findAllModels())));
    }

    private void initLayout() {
        center();
        setModal(true);
        setHeight("90%");
        setWidth("90%");

        HorizontalLayout horizontal = new HorizontalLayout();
        horizontal.setSpacing(true);
        horizontal.setMargin(true);
        horizontal.setWidth("100%");

        VerticalLayout fieldsLayout = new VerticalLayout();
        fieldsLayout.setSpacing(true);

        HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.setSpacing(true);

        VerticalLayout tableLayout = new VerticalLayout();
        tableLayout.setSpacing(true);

        HorizontalLayout filterLayout = new HorizontalLayout();
        filterLayout.setSpacing(true);
        filterLayout.setDefaultComponentAlignment(Alignment.BOTTOM_CENTER);

        TextField nameTextField = new TextField(ViewUtils.getMessage("field.name"));
        TextField numberTextField = new TextField(ViewUtils.getMessage("field.partnumber"));
        TextField inPriceTextField = new TextField(ViewUtils.getMessage("field.inPrice"));
        TextField optPriceTextField = new TextField(ViewUtils.getMessage("field.optPrice"));
        TextField outPriceTextField = new TextField(ViewUtils.getMessage("field.outPrice"));
        if (!SecurityUtils.hasAuthority(Authority.видеть_входную_цену)) {
            inPriceTextField.setEnabled(false);
            optPriceTextField.setEnabled(false);
            outPriceTextField.setEnabled(false);
        }
        inPriceTextField.setValue("0");
        optPriceTextField.setValue("0");
        outPriceTextField.setValue("0");
        TextArea commentTextArea = new TextArea(ViewUtils.getMessage("field.comment"));
        commentTextArea.setRows(5);
        commentTextArea.setImmediate(true);
        commentTextArea.setSizeFull();
        typeDeviceComboBox = createTypeDeviceComboBox();
        producerComboBox = createProducerPlaceComboBox();

        if (part != null) {
            nameTextField.setValue(part.getName());
            numberTextField.setValue(part.getPartNumber());
            if (SecurityUtils.hasAuthority(Authority.видеть_входную_цену)) {
                inPriceTextField.setValue(String.valueOf(part.getLastInPrice()));
            } else {
                inPriceTextField.setValue(ViewUtils.getMessage("notifications.hided"));
            }
            optPriceTextField.setValue(String.valueOf(part.getLastOptPrice()));
            outPriceTextField.setValue(String.valueOf(part.getLastOutPrice()));
            numberTextField.setEnabled(false);
            commentTextArea.setValue(part.getComment());
        }

        Button saveButton = new Button(ViewUtils.getMessage("button.save"));
        saveButton.addClickListener(event -> {
            if (!SecurityUtils.hasAuthority(Authority.редактирование_детали)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
            if (part == null) part = new Part();

            float inPrice = 0;
            float optPrice = 0;
            float outPrice = 0;
            try {
                if (SecurityUtils.hasAuthority(Authority.видеть_входную_цену)) {
                    inPrice = Float.valueOf(inPriceTextField.getValue());
                    optPrice = Float.valueOf(optPriceTextField.getValue());
                    outPrice = Float.valueOf(outPriceTextField.getValue());
                }
                if (inPrice < 0 || optPrice < 0 || outPrice < 0) {
                    throw new NumberFormatException("price must be > 0");
                }
            } catch (NumberFormatException e) {
                Notification.show(ViewUtils.getMessage("notifications.wrongPrice"), Notification.Type.ERROR_MESSAGE);
                return;
            }

            String comment = commentTextArea.getValue();

            part.setName(nameTextField.getValue());
            part.setPartNumber(numberTextField.getValue());
            part.setComment(comment);
            if (SecurityUtils.hasAuthority(Authority.видеть_входную_цену)) {
                part.setLastInPrice(inPrice);
                part.setLastOptPrice(optPrice);
                part.setLastOutPrice(outPrice);
            }
            //noinspection unchecked
            part.setModels((Set<Model>) table.getValue());
            try {
                partService.savePart(part);
                Notification.show(ViewUtils.getMessage("notifications.saved"), Notification.Type.TRAY_NOTIFICATION);
                close();
            } catch (Exception e) {
                StringBuilder builder = new StringBuilder(ViewUtils.getMessage("notifications.notSaved"));
                if (e.getLocalizedMessage() != null) {
                    builder.append("\n");
                    builder.append(e.getLocalizedMessage());
                } else {
                    for (StackTraceElement element : e.getStackTrace()) {
                        builder.append("\n");
                        builder.append(element);
                    }
                }
                Notification.show(builder.toString(), Notification.Type.ERROR_MESSAGE);
                e.printStackTrace();
            }
        });

        Button removeButton = new Button(ViewUtils.getMessage("button.remove"));
        if (part == null) removeButton.setEnabled(false);
        removeButton.addClickListener(event -> {
            if (!SecurityUtils.hasAuthority(Authority.редактирование_детали)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
            ConfirmWindow window = new ConfirmWindow(ViewUtils.getMessage("confirm.message.remove"));
            window.setOkClickListener(() -> {
                part.setIsRemoved(true);
                try {
                    partService.savePart(part);
                    close();
                } catch (Exception e) {
                    part.setIsRemoved(false);
                    StringBuilder builder = new StringBuilder(ViewUtils.getMessage("notifications.notSaved"));
                    if (e.getLocalizedMessage() != null) {
                        builder.append("\n");
                        builder.append(e.getLocalizedMessage());
                    } else {
                        for (StackTraceElement element : e.getStackTrace()) {
                            builder.append("\n");
                            builder.append(element);
                        }
                    }
                    Notification.show(builder.toString(), Notification.Type.ERROR_MESSAGE);
                }
            });
            getUI().addWindow(window);
        });

        Button applyButton = new Button(ViewUtils.getMessage("button.ok"));
        applyButton.addClickListener(clickEvent -> applyFilter());

        Button cancelButton = new Button(ViewUtils.getMessage("button.close"));
        cancelButton.addClickListener(event -> close());

        fieldsLayout.addComponent(numberTextField);
        fieldsLayout.addComponent(nameTextField);
        fieldsLayout.addComponent(inPriceTextField);
        fieldsLayout.addComponent(optPriceTextField);
        fieldsLayout.addComponent(outPriceTextField);
        fieldsLayout.addComponent(commentTextArea);
        buttonsLayout.addComponent(cancelButton);
        buttonsLayout.addComponent(saveButton);
        fieldsLayout.addComponent(buttonsLayout);
        fieldsLayout.addComponent(removeButton);

        filterLayout.addComponent(typeDeviceComboBox);
        filterLayout.addComponent(producerComboBox);
        filterLayout.addComponent(applyButton);

        tableLayout.addComponent(filterLayout);
        tableLayout.addComponent(table);

        horizontal.addComponent(fieldsLayout);
        horizontal.addComponent(tableLayout);
        horizontal.setExpandRatio(fieldsLayout, 1);
        horizontal.setExpandRatio(tableLayout, 4);

        setContent(horizontal);
    }


    private ComboBox createTypeDeviceComboBox() {
        BeanItemContainer<TypeDevice> container = new BeanItemContainer<>(TypeDevice.class);
        List<TypeDevice> typeDevices = modelService.findAllTypeDevices();
        container.addAll(typeDevices);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.typeDevice"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        return select;
    }

    private ComboBox createProducerPlaceComboBox() {
        BeanItemContainer<Producer> container = new BeanItemContainer<>(Producer.class);
        List<Producer> producers = modelService.findAllProducers();
        container.addAll(producers);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.producer"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        return select;
    }

    private void initTable(BeanItemContainer<Model> container) {
        table.setContainerDataSource(container);
        table.setSizeFull();
        table.setColumnCollapsingAllowed(true);
        table.setVisibleColumns(fieldNames);
        table.setColumnHeader(ID_COLUMN, ID_VISIBLE_COLUMN);
        table.setColumnHeader(TYPE_DEVICE_COLUMN, TYPE_DEVICE_VISIBLE_COLUMN);
        table.setColumnHeader(PRODUCER_COLUMN, PRODUCER_VISIBLE_COLUMN);
        table.setColumnHeader(NAME_COLUMN, NAME_VISIBLE_COLUMN);
        table.setSelectable(true);
        table.setImmediate(true);
        table.setMultiSelect(true);
        table.setMultiSelectMode(MultiSelectMode.SIMPLE);

        if (part != null) {
            container.getItemIds().forEach(item -> part.getModels().forEach(model -> {
                if (Objects.equals(model.getId(), item.getId())) table.select(item);
            }));
        }
    }

    private void applyFilter() {
        TypeDevice typeDevice = (TypeDevice) typeDeviceComboBox.getValue();
        Producer producer = (Producer) producerComboBox.getValue();
        List<Model> models;
        if (typeDevice != null && producer != null) {
            models = modelService.findModelByProducerIdAndTypeDeviceId(
                    producer.getId(), typeDevice.getId());
        } else if (typeDevice == null && producer != null) {
            models = modelService.findModelByProducerId(producer.getId());
        } else if (typeDevice != null) {
            models = modelService.findModelByTypeDeviceId(typeDevice.getId());
        } else {
            models = modelService.findAllModels();
        }
        initTable(createContainer(models));
    }

    private BeanItemContainer<Model> createContainer(List<Model> models) {
        BeanItemContainer<Model> container = new BeanItemContainer<>(Model.class);
        container.addNestedContainerProperty(TYPE_DEVICE_COLUMN);
        container.addNestedContainerProperty(PRODUCER_COLUMN);
        container.addAll(models);
        return container;
    }
}
