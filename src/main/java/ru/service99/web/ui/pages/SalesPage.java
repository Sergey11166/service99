package ru.service99.web.ui.pages;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ShortcutAction;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import ru.service99.SecurityUtils;
import ru.service99.domain.*;
import ru.service99.service.*;
import ru.service99.service.criteria.SalesOrderSearchCriteria;
import ru.service99.web.AdvancedFileDownloader;
import ru.service99.web.FileUtils;
import ru.service99.web.ui.OnCustomerChoiceListener;
import ru.service99.web.ui.ViewUtils;
import ru.service99.web.ui.pages.window.SaleWindow;
import ru.service99.web.ui.pages.window.SelectCustomerWindow;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.Calendar;

@Component
@Scope("prototype")
@SpringView(name = SalesPage.NAME)
public class SalesPage extends BasePage {

    public static final String NAME = "sales";

    @Autowired
    private SalesOrderService salesOrderService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private PlaceService placeService;
    @Autowired
    private UserService userService;
    @Autowired
    private FirmService firmService;
    @Autowired
    private PartService partService;
    @Autowired
    private ModelService modelService;

    private List<SalesOrder> orders;

    private Table table = new Table();

    private DateField startDateField1;
    private DateField startDateField2;

    private DateField endDateField1;
    private DateField endDateField2;

    private ComboBox statusComboBox;
    private ComboBox placeReceiveComboBox;

    private ComboBox managerComboBox;
    private ComboBox receiverComboBox;

    private TextField idTextField;
    private ComboBox typeComboBox;
    private ComboBox customerComboBox;

    private Label countLabel;
    private ComboBox countComboBox;

    private static final String ID_COLUMN = "id";
    private static final String START_DATE_COLUMN = "startDate";
    private static final String END_DATE_COLUMN = "endDate";
    private static final String STATUS_COLUMN = "status";
    private static final String TYPE_COLUMN = "type";
    private static final String CUSTOMER_COLUMN = "customer.name";
    private static final String MANAGER_COLUMN = "manager.name";
    private static final String PLACE_RECEIVE_COLUMN = "placeReceive.name";
    private static final String RECEIVER_COLUMN = "receiver.name";

    private static final String ID_VISIBLE_COLUMN = ViewUtils.getMessage("table.id");
    private static final String START_DATE_VISIBLE_COLUMN = ViewUtils.getMessage("table.startDate");
    private static final String END_DATE_VISIBLE_COLUMN = ViewUtils.getMessage("table.endDate");
    private static final String STATUS_VISIBLE_COLUMN = ViewUtils.getMessage("table.status");
    private static final String TYPE_VISIBLE_COLUMN = ViewUtils.getMessage("table.typeSale");
    private static final String CUSTOMER_VISIBLE_COLUMN = ViewUtils.getMessage("table.customer");
    private static final String MANAGER_VISIBLE_COLUMN = ViewUtils.getMessage("table.manager");
    private static final String PLACE_RECEIVE_VISIBLE_COLUMN = ViewUtils.getMessage("table.placeReceive");
    private static final String RECEIVER_VISIBLE_COLUMN = ViewUtils.getMessage("table.receiver");

    private static final String[] fieldNames = new String[]{ID_COLUMN, START_DATE_COLUMN, END_DATE_COLUMN,
            STATUS_COLUMN, TYPE_COLUMN, CUSTOMER_COLUMN, MANAGER_COLUMN, PLACE_RECEIVE_COLUMN, RECEIVER_COLUMN};

    @PostConstruct
    public void init() {
        initLayout();
        applyFilter();
    }

    private void initLayout() {
        HorizontalLayout filterContainer = new HorizontalLayout();
        filterContainer.setSpacing(true);

        VerticalLayout filterLayout1 = new VerticalLayout();
        filterLayout1.setSpacing(true);

        VerticalLayout filterLayout2 = new VerticalLayout();
        filterLayout2.setSpacing(true);

        VerticalLayout filterLayout3 = new VerticalLayout();
        filterLayout3.setSpacing(true);

        VerticalLayout filterLayout4 = new VerticalLayout();
        filterLayout4.setSpacing(true);

        VerticalLayout filterLayout5 = new VerticalLayout();
        filterLayout5.setSpacing(true);

        HorizontalLayout customerLayout = new HorizontalLayout();
        customerLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);

        HorizontalLayout idLayout = new HorizontalLayout();
        idLayout.setSpacing(true);
        idLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);

        HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.setSpacing(true);
        buttonsLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);

        startDateField1 = new DateField(ViewUtils.getMessage("field.startDate1"));
        startDateField1.setWidth("120px");
        startDateField2 = new DateField(ViewUtils.getMessage("field.startDate2"));
        startDateField2.setWidth("120px");

        endDateField1 = new DateField(ViewUtils.getMessage("field.endDate1"));
        endDateField1.setWidth("120px");
        endDateField2 = new DateField(ViewUtils.getMessage("field.endDate2"));
        endDateField2.setWidth("120px");

        statusComboBox = createStatusComboBox();
        statusComboBox.setWidth("160px");

        placeReceiveComboBox = createPlaceComboBox(ViewUtils.getMessage("field.placeReceive"));
        placeReceiveComboBox.setWidth("160px");

        managerComboBox = createManagerComboBox();
        receiverComboBox = createReceiverComboBox();

        customerComboBox = createCustomerComboBox();
        customerComboBox.setWidth("300px");

        idTextField = new TextField(ViewUtils.getMessage("field.saleNumber"));
        idTextField.setWidth("100");
        idTextField.focus();

        typeComboBox = createTypeComboBox();
        typeComboBox.setWidth("100px");

        countLabel = new Label();
        countComboBox = createCountComboBox();
        countComboBox.setWidth("100px");

        Button applyButton = new Button(ViewUtils.getMessage("button.select"));
        applyButton.addClickListener(clickEvent -> applyFilter());
        applyButton.setSizeFull();
        applyButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);

        Button addButton = new Button(ViewUtils.getMessage("button.new"));
        addButton.addClickListener(event -> {
            if (!SecurityUtils.hasAuthority(Authority.новая_продажа)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
            for (Window window : getUI().getWindows()) if (window instanceof SaleWindow) return;
            SaleWindow window = new SaleWindow(
                    salesOrderService,
                    customerService,
                    firmService,
                    userService,
                    modelService,
                    partService);
            window.addCloseListener(new WindowCloseListener());
            getUI().addWindow(window);
        });

        Button choiceCustomerButton = new Button("*");
        choiceCustomerButton.setWidth("15px");
        choiceCustomerButton.addClickListener(event -> {
            SelectCustomerWindow window = new SelectCustomerWindow(customerService);
            window.setCustomerChoiceListener(new MyCustomerChoiceListener());
            getUI().addWindow(window);
        });

        Button clearButton = new Button(ViewUtils.getMessage("button.clear"));
        clearButton.addClickListener(event -> cleanButtonClick());

        AdvancedFileDownloader salesDownloader = new AdvancedFileDownloader();
        salesDownloader.setFilePath(getClass().getClassLoader()
                .getResource("excel/" + ViewUtils.getMessage("file.sales") + ".xls").getFile());
        salesDownloader.addAdvancedDownloaderListener(event -> FileUtils.writeSalesExcel(getClass(), orders));
        Button excelButton = new Button(ViewUtils.getMessage("button.excel"));
        salesDownloader.extend(excelButton);
        if (!SecurityUtils.hasAuthority(Authority.загрузка_отчетов)) excelButton.setEnabled(false);

        filterLayout1.addComponent(startDateField1);
        filterLayout1.addComponent(startDateField2);

        filterLayout2.addComponent(endDateField1);
        filterLayout2.addComponent(endDateField2);

        filterLayout3.addComponent(statusComboBox);
        filterLayout3.addComponent(placeReceiveComboBox);

        filterLayout4.addComponent(receiverComboBox);
        filterLayout4.addComponent(managerComboBox);

        idLayout.addComponent(typeComboBox);
        idLayout.addComponent(idTextField);
        idLayout.addComponent(applyButton);

        customerLayout.addComponent(customerComboBox);
        customerLayout.addComponent(choiceCustomerButton);

        filterLayout5.addComponent(idLayout);
        filterLayout5.addComponent(customerLayout);

        buttonsLayout.addComponent(addButton);
        buttonsLayout.addComponent(clearButton);
        buttonsLayout.addComponent(excelButton);
        buttonsLayout.addComponent(countLabel);
        buttonsLayout.addComponent(countComboBox);

        filterContainer.addComponent(filterLayout1);
        filterContainer.addComponent(filterLayout2);
        filterContainer.addComponent(filterLayout3);
        filterContainer.addComponent(filterLayout4);
        filterContainer.addComponent(filterLayout5);

        baseLayout.addComponent(filterContainer);
        baseLayout.addComponent(buttonsLayout);
        baseLayout.addComponent(table);

        baseLayout.setExpandRatio(filterContainer, 2.5f);
        baseLayout.setExpandRatio(buttonsLayout, 1);
        baseLayout.setExpandRatio(table, 10);
    }

    private void cleanButtonClick() {
        startDateField1.setValue(null);
        startDateField2.setValue(null);
        endDateField1.setValue(null);
        endDateField2.setValue(null);
        statusComboBox.setValue(null);
        customerComboBox.setValue(null);
        managerComboBox.setValue(null);
        receiverComboBox.setValue(null);
        placeReceiveComboBox.setValue(null);
        idTextField.setValue("");
    }

    private ComboBox createStatusComboBox() {
        BeanItemContainer<StatusSalesOrder> container = new BeanItemContainer<>(StatusSalesOrder.class);
        container.addAll(Arrays.asList(StatusSalesOrder.values()));
        return new ComboBox(ViewUtils.getMessage("field.status"), container);
    }

    private ComboBox createTypeComboBox() {
        BeanItemContainer<TypeSale> container = new BeanItemContainer<>(TypeSale.class);
        container.addAll(Arrays.asList(TypeSale.values()));
        return new ComboBox(ViewUtils.getMessage("field.typeSale"), container);
    }

    private ComboBox createCustomerComboBox() {
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.customer"));
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        return select;
    }

    private ComboBox createManagerComboBox() {
        BeanItemContainer<User> container = new BeanItemContainer<>(User.class);
        List<User> users = userService.findAll();
        container.addAll(users);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.manager"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        return select;
    }

    private ComboBox createReceiverComboBox() {
        BeanItemContainer<User> container = new BeanItemContainer<>(User.class);
        List<User> users = userService.findAll();
        container.addAll(users);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.receiver"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        return select;
    }

    private ComboBox createPlaceComboBox(String caption) {
        BeanItemContainer<Place> container = new BeanItemContainer<>(Place.class);
        List<Place> places = placeService.findAllPlaces();
        container.addAll(places);
        ComboBox select = new ComboBox(caption, container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        return select;
    }

    private ComboBox createCountComboBox() {
        ComboBox select = new ComboBox();
        select.setNullSelectionAllowed(false);
        select.addItem("100");
        select.addItem("500");
        select.addItem("1000");
        select.select("100");
        select.addValueChangeListener(event -> applyFilter());
        return select;
    }

    private void applyFilter() {
        Long id = null;
        if (!idTextField.getValue().isEmpty()) {
            try {
                id = Long.parseLong(idTextField.getValue());
            } catch (NumberFormatException e) {
                Notification.show(ViewUtils.getMessage("notifications.wrongSaleNumber"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
        }

        SalesOrderSearchCriteria criteria = new SalesOrderSearchCriteria();

        criteria.setId(id);
        criteria.setStartDate1(startDateField1.getValue());
        criteria.setEndDate1(endDateField1.getValue());
        criteria.setStatus((StatusSalesOrder) statusComboBox.getValue());
        criteria.setType((TypeSale) typeComboBox.getValue());
        criteria.setManager((User) managerComboBox.getValue());
        criteria.setReceiver((User) receiverComboBox.getValue());
        criteria.setPlaceReceive((Place) placeReceiveComboBox.getValue());

        Date startDate2 = startDateField2.getValue();
        if (startDate2 != null) {
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(startDate2);
            calendar.add(Calendar.DATE, 1);
            criteria.setStartDate2(calendar.getTime());
        }

        Date endDate2 = endDateField2.getValue();
        if (endDate2 != null) {
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(endDate2);
            calendar.add(Calendar.DATE, 1);
            criteria.setEndDate2(calendar.getTime());
        }

        int count = Integer.parseInt((String) countComboBox.getValue());

        Page<SalesOrder> page = salesOrderService.findByCriteria(criteria,
                new PageRequest(0, count, new Sort(Sort.Direction.DESC, "id")));
        orders = page.getContent();
        initTable(createContainer(orders));
        countLabel.setValue(
                ViewUtils.getMessage("field.showCount") + " " + orders.size() + " " +
                        ViewUtils.getMessage("string.of") + " " + page.getTotalElements());
    }

    private void initTable(BeanItemContainer<SalesOrder> container) {
        table.setContainerDataSource(container);
        table.setSizeFull();
        table.setColumnCollapsingAllowed(true);
        //table.setColumnCollapsed(END_DATE_COLUMN, true);
        table.setVisibleColumns(fieldNames);
        table.setColumnHeader(ID_COLUMN, ID_VISIBLE_COLUMN);
        table.setColumnHeader(START_DATE_COLUMN, START_DATE_VISIBLE_COLUMN);
        table.setColumnHeader(END_DATE_COLUMN, END_DATE_VISIBLE_COLUMN);
        table.setColumnHeader(STATUS_COLUMN, STATUS_VISIBLE_COLUMN);
        table.setColumnHeader(TYPE_COLUMN, TYPE_VISIBLE_COLUMN);
        table.setColumnHeader(CUSTOMER_COLUMN, CUSTOMER_VISIBLE_COLUMN);
        table.setColumnHeader(MANAGER_COLUMN, MANAGER_VISIBLE_COLUMN);
        table.setColumnHeader(RECEIVER_COLUMN, RECEIVER_VISIBLE_COLUMN);
        table.setColumnHeader(PLACE_RECEIVE_COLUMN, PLACE_RECEIVE_VISIBLE_COLUMN);
        table.setSelectable(true);
        table.setImmediate(true);
        table.addItemClickListener(event -> {
            if (!event.isDoubleClick()) return;
            for (Window window : getUI().getWindows()) if (window instanceof SaleWindow) return;
            SalesOrder order = (SalesOrder) event.getItemId();
            SaleWindow window = new SaleWindow(
                    salesOrderService,
                    customerService,
                    firmService,
                    userService,
                    modelService,
                    partService,
                    order);
            window.addCloseListener(new WindowCloseListener());
            getUI().addWindow(window);
        });
    }

    private BeanItemContainer<SalesOrder> createContainer(List<SalesOrder> salesOrders) {
        BeanItemContainer<SalesOrder> container = new BeanItemContainer<>(SalesOrder.class);
        container.addAll(salesOrders);
        container.addNestedContainerProperty(CUSTOMER_COLUMN);
        container.addNestedContainerProperty(MANAGER_COLUMN);
        container.addNestedContainerProperty(RECEIVER_COLUMN);
        container.addNestedContainerProperty(PLACE_RECEIVE_COLUMN);
        return container;
    }

    private class WindowCloseListener implements Window.CloseListener {
        @Override
        public void windowClose(Window.CloseEvent closeEvent) {
            //applyFilter();
        }
    }

    private class MyCustomerChoiceListener implements OnCustomerChoiceListener {
        @Override
        public void onChoice(Customer customer) {
            BeanItemContainer<Customer> container = new BeanItemContainer<>(Customer.class);
            List<Customer> customers = new ArrayList<>(1);
            customers.add(customer);
            container.addAll(customers);
            customerComboBox.setContainerDataSource(container);
            customerComboBox.select(customer);
            customerComboBox.setDescription(customer.description());
        }
    }
}
