package ru.service99.web.ui.pages.window;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.*;
import org.springframework.data.history.Revision;
import org.springframework.data.history.Revisions;
import ru.service99.domain.MyRevisionEntity;
import ru.service99.domain.SalesOrder;
import ru.service99.service.SalesOrderService;
import ru.service99.web.ui.ViewUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class HistorySaleWindow extends Window {

    private List<MyRevisionEntity> myRevisionEntities;
    private Map<Integer, SalesOrder> integerSaleOrderMap;

    private static final String ID_COLUMN = "id";
    private static final String DATA_COLUMN = "revisionDate";
    private static final String AUTHOR_COLUMN = "auditor.name";

    private static final String ID_VISIBLE_COLUMN = ViewUtils.getMessage("table.id");
    private static final String DATA_VISIBLE_COLUMN = ViewUtils.getMessage("table.date");
    private static final String AUTHOR_VISIBLE_COLUMN = ViewUtils.getMessage("table.author");

    private static final String[] fieldNames = new String[]{ID_COLUMN, DATA_COLUMN, AUTHOR_COLUMN};

    private Table table;

    private TextField managerTextField;
    private TextField statusTextField;
    private TextField customerTextField;
    private TextField firmTextField;
    private TextField typeTextField;
    private TextArea receiverCommentTextArea;
    private TextArea managerCommentTextArea;
    private TextArea partsTextArea;
    private TextArea partsPendingTextArea;

    public HistorySaleWindow(SalesOrderService salesOrderService, long saleId) {
        super(ViewUtils.getMessage("salesPage.window.title.history") + " " + saleId);
        this.myRevisionEntities = salesOrderService.findMyRevisionEntitiesById(saleId);
        this.integerSaleOrderMap = createRevisionsMap(salesOrderService.findRevisionsById(saleId));
        init();
    }

    private void init() {
        initLayout();
        initTable(createContainer(myRevisionEntities));
    }

    private void initLayout() {
        center();
        setModal(true);
        setHeight(100, Unit.PERCENTAGE);

        HorizontalLayout container = new HorizontalLayout();
        container.setSpacing(true);
        container.setMargin(true);

        VerticalLayout leftContainer = new VerticalLayout();
        leftContainer.setSpacing(true);

        HorizontalLayout rightContainer = new HorizontalLayout();
        rightContainer.setSpacing(true);

        VerticalLayout rightLeftLayout = new VerticalLayout();
        rightLeftLayout.setSpacing(true);

        VerticalLayout rightRightLayout = new VerticalLayout();
        rightRightLayout.setSpacing(true);

        table = new Table();

        managerTextField = new TextField(ViewUtils.getMessage("field.manager"));
        statusTextField = new TextField(ViewUtils.getMessage("field.status"));
        customerTextField = new TextField(ViewUtils.getMessage("field.customer"));
        firmTextField = new TextField(ViewUtils.getMessage("field.firm"));
        typeTextField = new TextField(ViewUtils.getMessage("field.typeSale"));
        receiverCommentTextArea = new TextArea(ViewUtils.getMessage("field.receiverComment"));
        managerCommentTextArea = new TextArea(ViewUtils.getMessage("field.managerComment"));
        partsTextArea = new TextArea(ViewUtils.getMessage("field.listParts"));
        partsPendingTextArea = new TextArea(ViewUtils.getMessage("field.listPartsPending"));

        managerTextField.setEnabled(false);
        statusTextField.setEnabled(false);
        customerTextField.setEnabled(false);
        firmTextField.setEnabled(false);
        typeTextField.setEnabled(false);
        receiverCommentTextArea.setEnabled(false);
        managerCommentTextArea.setEnabled(false);
        partsTextArea.setEnabled(false);
        partsPendingTextArea.setEnabled(false);

        managerCommentTextArea.setWidth("450px");
        receiverCommentTextArea.setWidth("450");
        table.setHeight("400px");
        table.setWidth("450px");
        receiverCommentTextArea.setRows(2);
        managerCommentTextArea.setRows(2);
        partsTextArea.setRows(7);
        partsPendingTextArea.setRows(4);

        rightLeftLayout.addComponent(managerTextField);
        rightLeftLayout.addComponent(firmTextField);
        rightLeftLayout.addComponent(partsTextArea);

        rightRightLayout.addComponent(typeTextField);
        rightRightLayout.addComponent(statusTextField);
        rightRightLayout.addComponent(customerTextField);
        rightRightLayout.addComponent(partsPendingTextArea);

        leftContainer.addComponent(table);
        leftContainer.addComponent(receiverCommentTextArea);
        leftContainer.addComponent(managerCommentTextArea);

        rightContainer.addComponent(rightLeftLayout);
        rightContainer.addComponent(rightRightLayout);

        container.addComponent(leftContainer);
        container.addComponent(rightContainer);

        setContent(container);
    }

    private Map<Integer, SalesOrder> createRevisionsMap(Revisions<Integer, SalesOrder> revisions) {
        Map<Integer, SalesOrder> result = new TreeMap<>();
        for (Revision<Integer, SalesOrder> rev : revisions) {
            result.put(rev.getRevisionNumber(), rev.getEntity());
        }
        return result;
    }

    private void updateContent(int key) {
        SalesOrder order = integerSaleOrderMap.get(key);
        customerTextField.setValue(order.getCustomer().getName());
        receiverCommentTextArea.setValue(order.getReceiverComment() == null ? "" : order.getReceiverComment());
        firmTextField.setValue(order.getFirm().getName());
        typeTextField.setValue(order.getType().toString());
        managerCommentTextArea.setValue(order.getManagerComment() == null ? "" : order.getManagerComment());
        statusTextField.setValue(order.getStatus().toString());
        managerTextField.setValue(order.getManager() == null ? "" : order.getManager().getName());
        partsTextArea.setValue(order.getPartsString() == null ? "" : order.getPartsString());
        partsPendingTextArea.setValue(order.getPartsPendingString() == null ? "" : order.getPartsPendingString());
        paintFieldIfNeeded(key);
    }

    private void paintFieldIfNeeded(int keyOfMap) {
        List<Integer> keys = new ArrayList<>(integerSaleOrderMap.keySet());
        if (keys.get(0) == keyOfMap) {
            clearColorAllFields();
            return;
        }
        int keyPrev = keys.get(keys.indexOf(keyOfMap) - 1);
        SalesOrder orderPrev = integerSaleOrderMap.get(keyPrev);
        String receiverCommentPrev = orderPrev.getReceiverComment();
        String managerCommentPrev = orderPrev.getManagerComment();
        String managerPrev = orderPrev.getManager() != null ? String.valueOf(orderPrev.getManager().getId()) : null;
        String customerPrev = String.valueOf(orderPrev.getCustomer().getId());
        String firmPrev = String.valueOf(orderPrev.getFirm().getId());
        String typePrev = String.valueOf(orderPrev.getType());
        String statusPrev = String.valueOf(orderPrev.getStatus().ordinal());
        String partsStringPrev = orderPrev.getPartsString() != null ? orderPrev.getPartsString() : null;
        String partsPendingStringPrev = orderPrev.getPartsPendingString() != null ?
                orderPrev.getPartsPendingString() : null;

        SalesOrder order = integerSaleOrderMap.get(keyOfMap);
        String receiverComment = order.getReceiverComment();
        String managerComment = order.getManagerComment();
        String manager = order.getManager() != null ? String.valueOf(order.getManager().getId()) : null;
        String customer = String.valueOf(order.getCustomer().getId());
        String firm = String.valueOf(order.getFirm().getId());
        String type = String.valueOf(order.getType());
        String status = String.valueOf(order.getStatus().ordinal());
        String partsString = order.getPartsString() != null ? order.getPartsString() : null;
        String partsPendingString = order.getPartsPendingString() != null ? order.getPartsPendingString() : null;

        paintField(managerTextField, isChangeFields(managerPrev, manager));
        paintField(statusTextField, isChangeFields(statusPrev, status));
        paintField(customerTextField, isChangeFields(customerPrev, customer));
        paintField(firmTextField, isChangeFields(firmPrev, firm));
        paintField(typeTextField, isChangeFields(typePrev, type));
        paintField(receiverCommentTextArea, isChangeFields(receiverCommentPrev, receiverComment));
        paintField(managerCommentTextArea, isChangeFields(managerCommentPrev, managerComment));
        paintField(partsTextArea, isChangeFields(partsStringPrev, partsString));
        paintField(partsPendingTextArea, isChangeFields(partsPendingStringPrev, partsPendingString));
    }

    private boolean isChangeFields(String val1, String val2) {
        return !(val1 == null && val2 == null) && (val1 == null || val2 == null || !val1.equals(val2));
    }

    private void paintField(AbstractTextField field, boolean isChanged) {
        if (isChanged) field.addStyleName("background_green");
        else field.removeStyleName("background_green");
    }

    private void clearColorAllFields() {
        paintField(managerTextField, false);
        paintField(statusTextField, false);
        paintField(customerTextField, false);
        paintField(firmTextField, false);
        paintField(typeTextField, false);
        paintField(receiverCommentTextArea, false);
        paintField(managerCommentTextArea, false);
        paintField(partsTextArea, false);
        paintField(partsPendingTextArea, false);
    }

    private void initTable(BeanItemContainer<MyRevisionEntity> container) {
        table.setContainerDataSource(container);
        table.setVisibleColumns(fieldNames);
        table.setColumnHeader(ID_COLUMN, ID_VISIBLE_COLUMN);
        table.setColumnHeader(DATA_COLUMN, DATA_VISIBLE_COLUMN);
        table.setColumnHeader(AUTHOR_COLUMN, AUTHOR_VISIBLE_COLUMN);
        table.setSelectable(true);
        table.setImmediate(true);
        table.addValueChangeListener(event -> {
            MyRevisionEntity myRevisionEntity = (MyRevisionEntity) table.getValue();
            if (myRevisionEntity == null) return;
            int key = myRevisionEntity.getId();
            updateContent(key);
        });
    }

    private BeanItemContainer<MyRevisionEntity> createContainer(List<MyRevisionEntity> myRevisionEntities) {
        BeanItemContainer<MyRevisionEntity> container = new BeanItemContainer<>(MyRevisionEntity.class);
        container.addNestedContainerProperty(AUTHOR_COLUMN);
        container.addAll(myRevisionEntities);
        container.sort(new Object[]{ID_COLUMN}, new boolean[]{true});
        return container;
    }
}
