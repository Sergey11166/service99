package ru.service99.web.ui.pages.window;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.*;
import ru.service99.domain.*;
import ru.service99.service.RepairOrderService;
import ru.service99.service.criteria.RepairOrderSearchCriteria;
import ru.service99.web.ui.OnRepairChoiceListener;
import ru.service99.web.ui.ViewUtils;

import java.util.*;

public class SelectRepairWindow extends Window {

    private static final String ID_COLUMN = "id";
    private static final String DATE_COLUMN = "startDate";
    private static final String IMEI_COLUMN = "imei";
    private static final String SN_COLUMN = "sn";
    private static final String PRODUCER_COLUMN = "model.producer.name";
    private static final String MODEL_COLUMN = "model.name";

    private static final String ID_VISIBLE_COLUMN = ViewUtils.getMessage("table.id");
    private static final String DATE_VISIBLE_COLUMN = ViewUtils.getMessage("table.startDate");
    private static final String IMEI_VISIBLE_COLUMN = ViewUtils.getMessage("table.imei");
    private static final String SN_VISIBLE_COLUMN = ViewUtils.getMessage("table.sn");
    private static final String PRODUCER_VISIBLE_COLUMN = ViewUtils.getMessage("table.producer");
    private static final String MODEL_VISIBLE_COLUMN = ViewUtils.getMessage("table.model");

    private static final String[] fieldNames = new String[]{ID_COLUMN, DATE_COLUMN, IMEI_COLUMN,
            SN_COLUMN, PRODUCER_COLUMN, MODEL_COLUMN};

    private RepairOrderService repairOrderService;
    private Set<RepairOrder> selectedRepairOrders;
    private Place place;

    private OnRepairChoiceListener repairChoiceListener;

    private Table table = new Table();

    private TextField idTextField;
    private TextField numberTextField;

    public SelectRepairWindow(RepairOrderService repairOrderService, Set<RepairOrder> selectedRepairOrders, Place place) {
        super(ViewUtils.getMessage("movesDevicesPage.window.title.select"));
        this.repairOrderService = repairOrderService;
        this.selectedRepairOrders = selectedRepairOrders;
        this.place = place;
        init();
    }

    public void init() {
        initLayout();
        applyFilter();
    }

    private void initLayout() {
        center();
        setModal(true);
        setHeight(90, Unit.PERCENTAGE);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSpacing(true);
        verticalLayout.setMargin(true);

        HorizontalLayout filterLayout = new HorizontalLayout();
        filterLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);
        filterLayout.setSpacing(true);

        idTextField = new TextField(ViewUtils.getMessage("field.repairNumber"));
        numberTextField = new TextField(ViewUtils.getMessage("field.imeiSn"));

        Button applyButton = new Button(ViewUtils.getMessage("button.search"));
        applyButton.addClickListener(clickEvent -> applyFilter());

        Button selectButton = new Button(ViewUtils.getMessage("button.add"));
        selectButton.addClickListener(event -> selectClick());

        filterLayout.addComponent(idTextField);
        filterLayout.addComponent(numberTextField);
        filterLayout.addComponent(applyButton);
        filterLayout.addComponent(selectButton);

        verticalLayout.addComponent(filterLayout);
        verticalLayout.addComponent(table);

        setContent(verticalLayout);
    }

    private void selectClick() {
        RepairOrder repairOrder = (RepairOrder) table.getValue();
        if (repairOrder == null) {
            Notification.show(ViewUtils.getMessage("notifications.selectDevice"), Notification.Type.ERROR_MESSAGE);
            return;
        }
        Set<DocumentMoveDevices> documents = new HashSet<>(repairOrder.getDocumentMoveDevices());
        if (!documents.isEmpty()) {
            for (DocumentMoveDevices doc : documents) {
                if (doc.getStatus() != StatusDocument.Завершено) {
                    Notification.show(ViewUtils.getMessage("notifications.deviceIsMoving"), Notification.Type.ERROR_MESSAGE);
                    return;
                }
            }
        }
        repairChoiceListener.onChoice(repairOrder);
        close();
    }

    private void initTable(BeanItemContainer<RepairOrder> container) {
        table.setContainerDataSource(container);
        table.setColumnCollapsingAllowed(true);
        table.setVisibleColumns(fieldNames);
        table.setColumnHeader(ID_COLUMN, ID_VISIBLE_COLUMN);
        table.setColumnHeader(DATE_COLUMN, DATE_VISIBLE_COLUMN);
        table.setColumnHeader(IMEI_COLUMN, IMEI_VISIBLE_COLUMN);
        table.setColumnHeader(SN_COLUMN, SN_VISIBLE_COLUMN);
        table.setColumnHeader(PRODUCER_COLUMN, PRODUCER_VISIBLE_COLUMN);
        table.setColumnHeader(MODEL_COLUMN, MODEL_VISIBLE_COLUMN);
        table.setSelectable(true);
        table.setImmediate(true);
    }

    private void applyFilter() {
        String number = numberTextField.getValue();
        Long id = null;
        if (!idTextField.getValue().isEmpty()) {
            try {
                id = Long.parseLong(idTextField.getValue());
            } catch (NumberFormatException e) {
                Notification.show(ViewUtils.getMessage("notifications.wrongRepairNumber"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
        }

        Set<StatusRepairOrder> allowedStatuses = new HashSet<>(Arrays.asList(StatusRepairOrder.values()));
        allowedStatuses.remove(StatusRepairOrder.Отменён);
        allowedStatuses.remove(StatusRepairOrder.Выдан);

        RepairOrderSearchCriteria criteria = new RepairOrderSearchCriteria();
        criteria.setId(id);
        criteria.setCurrentPlace(place);
        criteria.setImeiSn(number);
        criteria.setStatuses(allowedStatuses);

        List<RepairOrder> allRepairOrders = new ArrayList<>(
                repairOrderService.findByCriteria(criteria, null).getContent());
        List<RepairOrder> needRemove = new ArrayList<>();
        for (RepairOrder order : allRepairOrders) {
            for (RepairOrder movedOrder : selectedRepairOrders) {
                if (Objects.equals(order.getId(), movedOrder.getId())) needRemove.add(order);
            }
        }
        allRepairOrders.removeAll(needRemove);
        initTable(createContainer(allRepairOrders));
    }

    private BeanItemContainer<RepairOrder> createContainer(List<RepairOrder> orders) {
        BeanItemContainer<RepairOrder> container = new BeanItemContainer<>(RepairOrder.class);
        container.addNestedContainerProperty(PRODUCER_COLUMN);
        container.addNestedContainerProperty(MODEL_COLUMN);
        container.addAll(orders);
        container.sort(new Object[]{ID_COLUMN}, new boolean[]{true});
        return container;
    }

    public void setRepairChoiceListener(OnRepairChoiceListener repairChoiceListener) {
        this.repairChoiceListener = repairChoiceListener;
    }
}
