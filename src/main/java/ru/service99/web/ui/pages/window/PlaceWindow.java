package ru.service99.web.ui.pages.window;

import com.vaadin.ui.*;
import ru.service99.SecurityUtils;
import ru.service99.domain.Authority;
import ru.service99.domain.Place;
import ru.service99.service.PlaceService;
import ru.service99.web.ui.ViewUtils;

public class PlaceWindow extends Window {

    private PlaceService placeService;

    private Place place;

    public PlaceWindow(PlaceService placeService) {
        this.placeService = placeService;
        setCaption(ViewUtils.getMessage("placesPage.window.title.new"));
        init();
    }

    public PlaceWindow(PlaceService placeService, Place place) {
        this(placeService);
        this.place = place;
        setCaption(ViewUtils.getMessage("placesPage.window.title.edit") + " " + place.getId());
        init();
    }

    private void init() {
        center();
        setModal(true);

        VerticalLayout vertical = new VerticalLayout();
        vertical.setSpacing(true);
        vertical.setMargin(true);
        vertical.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

        HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.setSpacing(true);

        TextField nameTextField = new TextField(ViewUtils.getMessage("field.name"));
        TextField addressTextField = new TextField(ViewUtils.getMessage("field.address"));
        TextField contactTextField = new TextField(ViewUtils.getMessage("field.contact"));

        nameTextField.setWidth("330px");
        addressTextField.setWidth("330px");
        contactTextField.setWidth("330px");

        if (place != null) {
            nameTextField.setValue(place.getName());
            addressTextField.setValue(place.getAddress());
            contactTextField.setValue(place.getContact());
        }

        Button saveButton = new Button(ViewUtils.getMessage("button.save"));
        saveButton.addClickListener(clickEvent -> {
            if (!SecurityUtils.hasAuthority(Authority.редактирование_площадки)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
            if (place == null) place = new Place();
            place.setName(nameTextField.getValue());
            place.setAddress(addressTextField.getValue());
            place.setContact(contactTextField.getValue());
            try {
                placeService.savePlace(place);
                Notification.show(ViewUtils.getMessage("notifications.saved"), Notification.Type.TRAY_NOTIFICATION);
                close();
            } catch (Exception e) {
                StringBuilder builder = new StringBuilder(ViewUtils.getMessage("notifications.notSaved"));
                if (e.getLocalizedMessage() != null) {
                    builder.append("\n");
                    builder.append(e.getLocalizedMessage());
                } else {
                    for (StackTraceElement element : e.getStackTrace()) {
                        builder.append("\n");
                        builder.append(element);
                    }
                }
                Notification.show(builder.toString(), Notification.Type.ERROR_MESSAGE);
            }
        });

        Button removeButton = new Button(ViewUtils.getMessage("button.remove"));
        if (place == null) removeButton.setEnabled(false);
        removeButton.addClickListener(event -> {
            if (!SecurityUtils.hasAuthority(Authority.редактирование_фирмы)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
            ConfirmWindow window = new ConfirmWindow(ViewUtils.getMessage("confirm.message.remove"));
            window.setOkClickListener(() -> {
                place.setIsRemoved(true);
                try {
                    placeService.savePlace(place);
                    close();
                } catch (Exception e) {
                    place.setIsRemoved(false);
                    StringBuilder builder = new StringBuilder(ViewUtils.getMessage("notifications.notSaved"));
                    if (e.getLocalizedMessage() != null) {
                        builder.append("\n");
                        builder.append(e.getLocalizedMessage());
                    } else {
                        for (StackTraceElement element : e.getStackTrace()) {
                            builder.append("\n");
                            builder.append(element);
                        }
                    }
                    Notification.show(builder.toString(), Notification.Type.ERROR_MESSAGE);
                }
            });
            getUI().addWindow(window);
        });

        Button cancelButton = new Button(ViewUtils.getMessage("button.cancel"));
        cancelButton.addClickListener(event -> close());

        buttonsLayout.addComponent(cancelButton);
        buttonsLayout.addComponent(removeButton);
        buttonsLayout.addComponent(saveButton);

        vertical.addComponent(nameTextField);
        vertical.addComponent(addressTextField);
        vertical.addComponent(contactTextField);
        vertical.addComponent(buttonsLayout);

        setContent(vertical);
    }
}
