package ru.service99.web.ui.pages;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.ui.*;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.service99.SecurityUtils;
import ru.service99.domain.User;
import ru.service99.web.ui.ViewUtils;

@Component
@Scope("prototype")
public class BasePage extends Panel implements View {

    private Label usernameLabel = new Label();
    protected VerticalLayout baseLayout = new VerticalLayout();

    @Override
    public void enter(ViewChangeEvent event) {
        User user = SecurityUtils.getCurrentUser();
        if (user != null) {
            usernameLabel.setValue(user.getName());
        }
        setContent(baseLayout);
    }

    public BasePage() {
        setSizeFull();
        baseLayout.setSpacing(true);
        baseLayout.setMargin(true);
        baseLayout.setSizeFull();

        // header
        Panel headerPanel = new Panel();

        HorizontalLayout headerLayout = new HorizontalLayout();
        headerLayout.setSizeFull();
        headerLayout.setMargin(true);
        headerLayout.setSpacing(true);
        headerLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);

        // menu
        HorizontalLayout menuLayout = new HorizontalLayout();
        menuLayout.setSpacing(true);

        MenuBar menuBar = new MenuBar();
        menuBar.setWidth("100%");
        MenuBar.Command menuCommands = this::selectItemMenu;

        MenuBar.MenuItem catalogs = menuBar.addItem(ViewUtils.getMessage("basePage.menu.catalogs"), null, null);
        catalogs.addItem(ViewUtils.getMessage("basePage.menu.catalogs.parts"), menuCommands);
        catalogs.addItem(ViewUtils.getMessage("basePage.menu.catalogs.models"), menuCommands);
        catalogs.addItem(ViewUtils.getMessage("basePage.menu.catalogs.producers"), menuCommands);
        catalogs.addItem(ViewUtils.getMessage("basePage.menu.catalogs.typeDevices"), menuCommands);
        catalogs.addItem(ViewUtils.getMessage("basePage.menu.catalogs.firms"), menuCommands);
        catalogs.addItem(ViewUtils.getMessage("basePage.menu.catalogs.stocks"), menuCommands);
        catalogs.addItem(ViewUtils.getMessage("basePage.menu.catalogs.places"), menuCommands);
        catalogs.addItem(ViewUtils.getMessage("basePage.menu.catalogs.customers"), menuCommands);
        catalogs.addItem(ViewUtils.getMessage("basePage.menu.catalogs.users"), menuCommands);
        catalogs.addItem(ViewUtils.getMessage("basePage.menu.catalogs.templates"), menuCommands);

        MenuBar.MenuItem parts = menuBar.addItem(ViewUtils.getMessage("basePage.menu.parts"), null, null);
        parts.addItem(ViewUtils.getMessage("basePage.menu.parts.freeParts"), menuCommands);
        parts.addItem(ViewUtils.getMessage("basePage.menu.parts.receiptsParts"), menuCommands);
        parts.addItem(ViewUtils.getMessage("basePage.menu.parts.movesParts"), menuCommands);
        parts.addItem(ViewUtils.getMessage("basePage.menu.parts.spendingParts"), menuCommands);
        parts.addItem(ViewUtils.getMessage("basePage.menu.parts.partsPending"), menuCommands);
        parts.addItem(ViewUtils.getMessage("basePage.menu.parts.repairsPending"), menuCommands);
        parts.addItem(ViewUtils.getMessage("basePage.menu.parts.salesPending"), menuCommands);

        MenuBar.MenuItem repairs = menuBar.addItem(ViewUtils.getMessage("basePage.menu.logs"), null, null);
        repairs.addItem(ViewUtils.getMessage("basePage.menu.logs.repairs"), menuCommands);
        repairs.addItem(ViewUtils.getMessage("basePage.menu.logs.sales"), menuCommands);
        repairs.addItem(ViewUtils.getMessage("basePage.menu.logs.movesDevices"), menuCommands);

        menuLayout.addComponent(menuBar);
        headerLayout.addComponent(menuLayout);

        // login, roles and logout button
        HorizontalLayout usernameLayout = new HorizontalLayout();
        usernameLayout.setSpacing(true);
        usernameLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);

        /*ComboBox themeComboBox = new ComboBox("Theme",
                Arrays.asList(new String[]{"liferay", "valo", "reindeer", "runo", "chameleon" }));
        themeComboBox.addValueChangeListener(event -> {
            String theme = (String) event.getProperty().getValue();
            getUI().setTheme(theme);
        });
        usernameLayout.addComponent(themeComboBox);*/

        usernameLayout.addComponent(new Label(ViewUtils.getMessage("basePage.login")));
        usernameLayout.addComponent(usernameLabel);

        Button logoutButton = new Button(ViewUtils.getMessage("basePage.logout"));
        logoutButton.addClickListener(event -> Page.getCurrent().open("/logout", null));
        usernameLayout.addComponent(logoutButton);

        headerLayout.addComponent(usernameLayout);
        headerLayout.setComponentAlignment(usernameLayout, Alignment.MIDDLE_RIGHT);

        headerPanel.setContent(headerLayout);

        baseLayout.addComponent(headerPanel);
        baseLayout.setExpandRatio(headerPanel, 1);
    }

    private void selectItemMenu(MenuBar.MenuItem item) {
        switch (item.getId()) {
            case 3:
                //Notification.show("Catalogs->Parts, id=" + item.getId(), Notification.Type.TRAY_NOTIFICATION);
                getUI().getNavigator().navigateTo(PartsPage.NAME);
                break;
            case 4:
                //Notification.show("Catalogs->Models, id=" + item.getId(), Notification.Type.TRAY_NOTIFICATION);
                getUI().getNavigator().navigateTo(ModelsPage.NAME);
                break;
            case 5:
                //Notification.show("Catalogs->Producers, id=" + item.getId(), Notification.Type.TRAY_NOTIFICATION);
                getUI().getNavigator().navigateTo(ProducersPage.NAME);
                break;
            case 6:
                //Notification.show("Catalogs->Type Devices, id=" + item.getId(), Notification.Type.TRAY_NOTIFICATION);
                getUI().getNavigator().navigateTo(TypeDevicesPage.NAME);
                break;
            case 7:
                //Notification.show("Catalogs->Firms, id=" + item.getId(), Notification.Type.TRAY_NOTIFICATION);
                getUI().getNavigator().navigateTo(FirmsPage.NAME);
                break;
            case 8:
                //Notification.show("Catalogs->Firms, id=" + item.getId(), Notification.Type.TRAY_NOTIFICATION);
                getUI().getNavigator().navigateTo(StocksPage.NAME);
                break;
            case 9:
                //Notification.show("Catalogs->Places, id=" + item.getId(), Notification.Type.TRAY_NOTIFICATION);
                getUI().getNavigator().navigateTo(PlacesPage.NAME);
                break;
            case 10:
                //Notification.show("Catalogs->Customers, id=" + item.getId(), Notification.Type.TRAY_NOTIFICATION);
                getUI().getNavigator().navigateTo(CustomersPage.NAME);
                break;
            case 11:
                //Notification.show("Catalogs->Users, id=" + item.getId(), Notification.Type.TRAY_NOTIFICATION);
                getUI().getNavigator().navigateTo(UsersPage.NAME);
                break;
            case 12:
                //Notification.show("Catalogs->Users, id=" + item.getId(), Notification.Type.TRAY_NOTIFICATION);
                getUI().getNavigator().navigateTo(TemplatesPage.NAME);
                break;
            case 14:
                //Notification.show("Parts->Free parts, id=" + item.getId(), Notification.Type.TRAY_NOTIFICATION);
                getUI().getNavigator().navigateTo(FreePartsPage.NAME);
                break;
            case 15:
                //Notification.show("Parts->Receipts parts, id=" + item.getId(), Notification.Type.TRAY_NOTIFICATION);
                getUI().getNavigator().navigateTo(ReceiptsPartsPage.NAME);
                break;
            case 16:
                //Notification.show("Parts->Move parts, id=" + item.getId(), Notification.Type.TRAY_NOTIFICATION);
                getUI().getNavigator().navigateTo(MovesPartsPage.NAME);
                break;
            case 17:
                //Notification.show("Parts->Spending parts, id=" + item.getId(), Notification.Type.TRAY_NOTIFICATION);
                getUI().getNavigator().navigateTo(SpendingPartsPage.NAME);
                break;
            case 18:
                //Notification.show("Parts->Parts pending, id=" + item.getId(), Notification.Type.TRAY_NOTIFICATION);
                getUI().getNavigator().navigateTo(PartsPendingPage.NAME);
                break;
            case 19:
                //Notification.show("Parts->Repairs pending, id=" + item.getId(), Notification.Type.TRAY_NOTIFICATION);
                getUI().getNavigator().navigateTo(RepairsPendingPage.NAME);
                break;
            case 20:
                //Notification.show("Parts->Sales pending, id=" + item.getId(), Notification.Type.TRAY_NOTIFICATION);
                getUI().getNavigator().navigateTo(SalesPendingPage.NAME);
                break;
            case 22:
                //Notification.show("Logs->Repairs, id=" + item.getId(), Notification.Type.TRAY_NOTIFICATION);
                getUI().getNavigator().navigateTo(RepairsPage.NAME);
                break;
            case 23:
                //Notification.show("Logs->Sales, id=" + item.getId(), Notification.Type.TRAY_NOTIFICATION);
                getUI().getNavigator().navigateTo(SalesPage.NAME);
                break;
            case 24:
                //Notification.show("Logs->Moves devices, id=" + item.getId(), Notification.Type.TRAY_NOTIFICATION);
                getUI().getNavigator().navigateTo(MovesDevicesPage.NAME);
                break;
        }
    }
}
