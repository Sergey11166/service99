package ru.service99.web.ui.pages.window;

import com.vaadin.ui.*;
import ru.service99.SecurityUtils;
import ru.service99.domain.Authority;
import ru.service99.domain.Stock;
import ru.service99.service.PartService;
import ru.service99.web.ui.ViewUtils;

public class StockWindow extends Window {

    private PartService partService;

    private Stock stock;

    public StockWindow(PartService partService) {
        setCaption(ViewUtils.getMessage("stocksPage.window.title.new"));
        this.partService = partService;
        init();
    }

    public StockWindow(PartService partService, Stock stock) {
        this(partService);
        setCaption(ViewUtils.getMessage("stocksPage.window.title.edit") + " " + stock.getId());
        this.stock = stock;
        init();
    }

    private void init() {
        center();
        setModal(true);

        VerticalLayout vertical = new VerticalLayout();
        vertical.setSpacing(true);
        vertical.setMargin(true);
        vertical.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

        HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.setSpacing(true);

        TextField nameTextField = new TextField(ViewUtils.getMessage("field.name"));
        nameTextField.setWidth("330px");

        if (stock != null) nameTextField.setValue(stock.getName());

        Button saveButton = new Button(ViewUtils.getMessage("button.save"));
        saveButton.addClickListener(clickEvent -> {
            if (!SecurityUtils.hasAuthority(Authority.редактирование_склада)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
            if (stock == null) stock = new Stock();
            stock.setName(nameTextField.getValue());
            try {
                partService.saveStock(stock);
                Notification.show(ViewUtils.getMessage("notifications.saved"), Notification.Type.TRAY_NOTIFICATION);
                close();
            } catch (Exception e) {
                StringBuilder builder = new StringBuilder(ViewUtils.getMessage("notifications.notSaved"));
                if (e.getLocalizedMessage() != null) {
                    builder.append("\n");
                    builder.append(e.getLocalizedMessage());
                } else {
                    for (StackTraceElement element : e.getStackTrace()) {
                        builder.append("\n");
                        builder.append(element);
                    }
                }
                Notification.show(builder.toString(), Notification.Type.ERROR_MESSAGE);
            }
        });

        Button removeButton = new Button(ViewUtils.getMessage("button.remove"));
        if (stock == null) removeButton.setEnabled(false);
        removeButton.addClickListener(event -> {
            if (!SecurityUtils.hasAuthority(Authority.редактирование_фирмы)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
            ConfirmWindow window = new ConfirmWindow(ViewUtils.getMessage("confirm.message.remove"));
            window.setOkClickListener(() -> {
                stock.setIsRemoved(true);
                try {
                    partService.saveStock(stock);
                    close();
                } catch (Exception e) {
                    stock.setIsRemoved(false);
                    StringBuilder builder = new StringBuilder(ViewUtils.getMessage("notifications.notSaved"));
                    if (e.getLocalizedMessage() != null) {
                        builder.append("\n");
                        builder.append(e.getLocalizedMessage());
                    } else {
                        for (StackTraceElement element : e.getStackTrace()) {
                            builder.append("\n");
                            builder.append(element);
                        }
                    }
                    Notification.show(builder.toString(), Notification.Type.ERROR_MESSAGE);
                }
            });
            getUI().addWindow(window);
        });

        Button cancelButton = new Button(ViewUtils.getMessage("button.cancel"));
        cancelButton.addClickListener(event -> close());

        buttonsLayout.addComponent(cancelButton);
        buttonsLayout.addComponent(removeButton);
        buttonsLayout.addComponent(saveButton);

        vertical.addComponent(nameTextField);
        vertical.addComponent(buttonsLayout);

        setContent(vertical);
    }
}
