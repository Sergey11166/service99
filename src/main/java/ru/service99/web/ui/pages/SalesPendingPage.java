package ru.service99.web.ui.pages;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ShortcutAction;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.service99.SecurityUtils;
import ru.service99.domain.PartPending;
import ru.service99.domain.SalesOrder;
import ru.service99.domain.StatusSalesOrder;
import ru.service99.domain.User;
import ru.service99.service.*;
import ru.service99.service.criteria.PartSearchCriteria;
import ru.service99.service.criteria.SalesOrderSearchCriteria;
import ru.service99.web.model.SalePending;
import ru.service99.web.ui.ViewUtils;
import ru.service99.web.ui.pages.window.SaleWindow;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

@Component
@Scope("prototype")
@SpringView(name = SalesPendingPage.NAME)
public class SalesPendingPage extends BasePage {

    public static final String NAME = "salesPending";

    @Autowired
    private SalesOrderService salesOrderService;
    @Autowired
    private PartService partService;
    @Autowired
    private UserService userService;
    @Autowired
    private ModelService modelService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private FirmService firmService;

    private Table table = new Table();

    private ComboBox managerComboBox;

    private HashMap<Long, User> managerMap = new HashMap<>();

    private static final String ID_COLUMN = "order.id";
    private static final String MANAGER_COLUMN = "order.manager.name";
    private static final String PARTNUMBER_COLUMN = "partPending.part.partNumber";
    private static final String PART_NAME_COLUMN = "partPending.part.name";
    private static final String NEEDED_COUNT_COLUMN = "partPending.neededCount";
    private static final String FREE_COUNT_COLUMN = "freeCount";

    private static final String ID_VISIBLE_COLUMN = ViewUtils.getMessage("table.id");
    private static final String MANAGER_VISIBLE_COLUMN = ViewUtils.getMessage("table.manager");
    private static final String PARTNUMBER_VISIBLE_COLUMN = ViewUtils.getMessage("table.partNumber");
    private static final String PART_NAME_VISIBLE_COLUMN = ViewUtils.getMessage("table.partName");
    private static final String NEEDED_COUNT_VISIBLE_COLUMN = ViewUtils.getMessage("table.neededCount");
    private static final String FREE_COUNT_VISIBLE_COLUMN = ViewUtils.getMessage("table.freeCount");

    private static final String[] fieldNames = new String[]{ID_COLUMN, MANAGER_COLUMN,
            PARTNUMBER_COLUMN, PART_NAME_COLUMN, NEEDED_COUNT_COLUMN, FREE_COUNT_COLUMN};

    @PostConstruct
    public void init() {
        initLayout();
        applyFilter();
    }

    private void initLayout() {
        HorizontalLayout filterContainer = new HorizontalLayout();
        filterContainer.setSpacing(true);

        managerComboBox = createManagerComboBox();
        managerComboBox.setWidth("150px");
        managerComboBox.select(managerMap.get(SecurityUtils.getCurrentUser().getId()));

        Button applyButton = new Button(ViewUtils.getMessage("button.select"));
        applyButton.addClickListener(clickEvent -> applyFilter());
        applyButton.setSizeFull();
        applyButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);

        filterContainer.addComponent(managerComboBox);
        filterContainer.addComponent(applyButton);

        baseLayout.addComponent(filterContainer);
        baseLayout.addComponent(table);

        baseLayout.setExpandRatio(filterContainer, 1);
        baseLayout.setExpandRatio(table, 10);
    }

    private ComboBox createManagerComboBox() {
        BeanItemContainer<User> container = new BeanItemContainer<>(User.class);
        List<User> users = userService.findAll();
        users.forEach(user -> managerMap.put(user.getId(), user));
        container.addAll(users);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.master"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        return select;
    }

    private void applyFilter() {
        TreeSet<SalePending> salesPending = new TreeSet<>();
        SalesOrderSearchCriteria salesCriteria = new SalesOrderSearchCriteria();
        salesCriteria.setStatus(StatusSalesOrder.Ож_детали);
        salesCriteria.setManager((User) managerComboBox.getValue());
        List<SalesOrder> salesOrders = salesOrderService.findByCriteria(salesCriteria, null).getContent();
        for (SalesOrder so : salesOrders) {
            if (so.getPartsPending().isEmpty()) continue;
            for (PartPending pp : so.getPartsPending()) {
                PartSearchCriteria partCriteria = new PartSearchCriteria();
                partCriteria.setPartId(pp.getPart().getId());
                long freeCount = partService.getCountFreeInstancesByCriteria(partCriteria);
                if (freeCount > 0) {
                    SalePending salePending = new SalePending();
                    salePending.setOrder(so);
                    salePending.setPartPending(pp);
                    salePending.setFreeCount(freeCount);
                    salesPending.add(salePending);
                }
            }
        }
        initTable(createContainer(new ArrayList<>(salesPending)));
    }

    private void initTable(BeanItemContainer<SalePending> container) {
        table.setContainerDataSource(container);
        table.setSizeFull();
        table.setColumnCollapsingAllowed(true);
        table.setVisibleColumns(fieldNames);
        table.setColumnHeader(ID_COLUMN, ID_VISIBLE_COLUMN);
        table.setColumnHeader(MANAGER_COLUMN, MANAGER_VISIBLE_COLUMN);
        table.setColumnHeader(PARTNUMBER_COLUMN, PARTNUMBER_VISIBLE_COLUMN);
        table.setColumnHeader(PART_NAME_COLUMN, PART_NAME_VISIBLE_COLUMN);
        table.setColumnHeader(NEEDED_COUNT_COLUMN, NEEDED_COUNT_VISIBLE_COLUMN);
        table.setColumnHeader(FREE_COUNT_COLUMN, FREE_COUNT_VISIBLE_COLUMN);
        table.setSelectable(true);
        table.setImmediate(true);
        table.addItemClickListener(event -> {
            if (!event.isDoubleClick()) return;
            for (Window window : getUI().getWindows()) if (window instanceof SaleWindow) return;
            SalePending order = (SalePending) event.getItemId();
            SaleWindow window = new SaleWindow(
                    salesOrderService,
                    customerService,
                    firmService,
                    userService,
                    modelService,
                    partService,
                    order.getOrder());
            window.addCloseListener(new WindowCloseListener());
            getUI().addWindow(window);
        });
    }

    private BeanItemContainer<SalePending> createContainer(List<SalePending> salesPending) {
        BeanItemContainer<SalePending> container = new BeanItemContainer<>(SalePending.class);
        container.addAll(salesPending);
        container.addNestedContainerProperty(ID_COLUMN);
        container.addNestedContainerProperty(MANAGER_COLUMN);
        container.addNestedContainerProperty(PARTNUMBER_COLUMN);
        container.addNestedContainerProperty(PART_NAME_COLUMN);
        container.addNestedContainerProperty(NEEDED_COUNT_COLUMN);
        return container;
    }

    private class WindowCloseListener implements Window.CloseListener {
        @Override
        public void windowClose(Window.CloseEvent closeEvent) {
            //applyFilter();
        }
    }
}
