package ru.service99.web.ui.pages.window;

import com.vaadin.ui.*;
import ru.service99.domain.Part;
import ru.service99.domain.PartInstance;
import ru.service99.web.ui.OnPriceChangeListener;
import ru.service99.web.ui.ViewUtils;

import java.util.Set;

public class PriceChangeWindow extends Window {

    private Set<PartInstance> partInstances;
    private OnPriceChangeListener onPriceChangeListener;

    private TextField inPriceTextField;
    private TextField optPriceTextField;
    private TextField outPriceTextField;

    public PriceChangeWindow(Set<PartInstance> partInstances) {
        setCaption(ViewUtils.getMessage("receiptsPartsPage.window.priceChangeWindow.title.edit"));
        this.partInstances = partInstances;
        init();
    }

    private void init() {
        center();
        setModal(true);
        setWidth("450px");

        VerticalLayout vertical = new VerticalLayout();
        vertical.setSpacing(true);
        vertical.setMargin(true);

        HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.setSpacing(true);

        inPriceTextField = new TextField(ViewUtils.getMessage("field.inPrice"));
        optPriceTextField = new TextField(ViewUtils.getMessage("field.optPrice"));
        outPriceTextField = new TextField(ViewUtils.getMessage("field.outPrice"));

        Part part = partInstances.iterator().next().getPart();
        inPriceTextField.setValue(String.valueOf(part.getLastInPrice()));
        optPriceTextField.setValue(String.valueOf(part.getLastOptPrice()));
        outPriceTextField.setValue(String.valueOf(part.getLastOutPrice()));

        Button saveButton = new Button(ViewUtils.getMessage("button.ok"));
        saveButton.addClickListener(clickEvent -> {
            float inPrice;
            float optPrice;
            float outPrice;
            try {
                inPrice = Float.valueOf(inPriceTextField.getValue());
                optPrice = Float.valueOf(optPriceTextField.getValue());
                outPrice = Float.valueOf(outPriceTextField.getValue());
                if (inPrice < 0 || optPrice < 0 || outPrice < 0) {
                    throw new NumberFormatException("price must be > 0");
                }
            } catch (NumberFormatException e) {
                Notification.show(ViewUtils.getMessage("notifications.wrongPrice"), Notification.Type.ERROR_MESSAGE);
                return;
            }
            for (PartInstance pi : partInstances) {
                pi.setInPrice(inPrice);
                pi.setOptPrice(optPrice);
                pi.setOutPrice(outPrice);
            }
            onPriceChangeListener.onChange(partInstances);
            close();
        });

        Button cancelButton = new Button(ViewUtils.getMessage("button.cancel"));
        cancelButton.addClickListener(event -> close());

        buttonsLayout.addComponent(cancelButton);
        buttonsLayout.addComponent(saveButton);

        vertical.addComponent(inPriceTextField);
        vertical.addComponent(optPriceTextField);
        vertical.addComponent(outPriceTextField);
        vertical.addComponent(buttonsLayout);

        setContent(vertical);
    }

    public void setPriceChangeListener(OnPriceChangeListener onPriceChangeListener) {
        this.onPriceChangeListener = onPriceChangeListener;
    }
}
