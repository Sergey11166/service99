package ru.service99.web.ui.pages.window;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import ru.service99.SecurityUtils;
import ru.service99.domain.Authority;
import ru.service99.domain.Customer;
import ru.service99.service.CustomerService;
import ru.service99.web.ui.OnCustomerChoiceListener;
import ru.service99.web.ui.ViewUtils;

import java.util.List;

public class SelectCustomerWindow extends Window {

    private static final String ID_COLUMN = "id";
    private static final String NAME_COLUMN = "name";
    private static final String PHONE_COLUMN = "phoneNumber";
    private static final String ADDRESS_COLUMN = "address";
    private static final String COMMENT_COLUMN = "comment";

    private static final String ID_VISIBLE_COLUMN = ViewUtils.getMessage("table.id");
    private static final String NAME_VISIBLE_COLUMN = ViewUtils.getMessage("table.userName");
    private static final String PHONE_VISIBLE_COLUMN = ViewUtils.getMessage("table.phone");
    private static final String ADDRESS_VISIBLE_COLUMN = ViewUtils.getMessage("table.address");
    private static final String COMMENT_VISIBLE_COLUMN = ViewUtils.getMessage("table.comment");

    private static final String[] fieldNames = new String[]{ID_COLUMN, NAME_COLUMN, PHONE_COLUMN,
            ADDRESS_COLUMN, COMMENT_COLUMN};

    private CustomerService customerService;

    private OnCustomerChoiceListener customerChoiceListener;

    private Table table = new Table();

    private TextField searchTextField;

    private Label countLabel = new Label();

    public SelectCustomerWindow(CustomerService customerService) {
        super(ViewUtils.getMessage("customersPage.window.title.select"));
        this.customerService = customerService;
        init();
    }

    public void init() {
        initLayout();
        applyFilter();
    }

    private void initLayout() {
        center();
        setModal(true);
        setHeight("90%");
        setWidth("90%");

        VerticalLayout vertical = new VerticalLayout();
        vertical.setSpacing(true);
        vertical.setMargin(true);
        vertical.setSizeFull();

        HorizontalLayout filterLayout = new HorizontalLayout();
        filterLayout.setSpacing(true);
        filterLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);

        searchTextField = new TextField(ViewUtils.getMessage("field.search"));

        Button applyButton = new Button(ViewUtils.getMessage("button.search"));
        applyButton.addClickListener(clickEvent -> applyFilter());

        Button selectButton = new Button(ViewUtils.getMessage("button.select"));
        selectButton.addClickListener(event -> selectClick());

        Button addButton = new Button(ViewUtils.getMessage("button.new"));
        addButton.addClickListener(event -> {
            if (!SecurityUtils.hasAuthority(Authority.редактирование_клиента)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
            for (Window window : getUI().getWindows()) if (window instanceof CustomerWindow) return;
            CustomerWindow window = new CustomerWindow(customerService);
            window.addCloseListener(new MyCloseListener());
            getUI().addWindow(window);
        });

        filterLayout.addComponent(searchTextField);
        filterLayout.addComponent(applyButton);
        filterLayout.addComponent(selectButton);

        vertical.addComponent(filterLayout);
        vertical.addComponent(addButton);
        vertical.addComponent(countLabel);
        vertical.addComponent(table);

        vertical.setExpandRatio(filterLayout, 1.5f);
        vertical.setExpandRatio(addButton, 1);
        vertical.setExpandRatio(countLabel, 1);
        vertical.setExpandRatio(table, 10);

        setContent(vertical);
    }

    private void selectClick() {
        Customer customer = (Customer) table.getValue();
        if (customer == null) {
            Notification.show(ViewUtils.getMessage("notifications.selectCustomer"), Notification.Type.ERROR_MESSAGE);
            return;
        }
        customerChoiceListener.onChoice(customer);
        close();
    }

    private void initTable(BeanItemContainer<Customer> container) {
        table.setContainerDataSource(container);
        table.setSizeFull();
        table.setColumnCollapsingAllowed(true);
        table.setVisibleColumns(fieldNames);
        table.setColumnHeader(ID_COLUMN, ID_VISIBLE_COLUMN);
        table.setColumnHeader(NAME_COLUMN, NAME_VISIBLE_COLUMN);
        table.setColumnHeader(PHONE_COLUMN, PHONE_VISIBLE_COLUMN);
        table.setColumnHeader(ADDRESS_COLUMN, ADDRESS_VISIBLE_COLUMN);
        table.setColumnHeader(COMMENT_COLUMN, COMMENT_VISIBLE_COLUMN);
        table.setSelectable(true);
        table.setImmediate(true);
    }

    private void applyFilter() {
        String searchName = "%" + searchTextField.getValue() + "%";
        List<Customer> customers;
        Page<Customer> page = customerService.findByName(searchName, new PageRequest(0, 100, new Sort("name")));
        customers = page.getContent();
        initTable(createContainer(customers));
        countLabel.setValue(
                ViewUtils.getMessage("field.showCount") + " " + customers.size() + " " +
                        ViewUtils.getMessage("string.of") + " " + page.getTotalElements());
    }

    private BeanItemContainer<Customer> createContainer(List<Customer> customers) {
        BeanItemContainer<Customer> container = new BeanItemContainer<>(Customer.class);
        container.addAll(customers);
        return container;
    }

    public void setCustomerChoiceListener(OnCustomerChoiceListener customerChoiceListener) {
        this.customerChoiceListener = customerChoiceListener;
    }

    private class MyCloseListener implements Window.CloseListener {
        @Override
        public void windowClose(Window.CloseEvent closeEvent) {
            applyFilter();
        }
    }
}
