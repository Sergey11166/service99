package ru.service99.web.ui.pages.window;

import com.vaadin.ui.*;
import ru.service99.SecurityUtils;
import ru.service99.domain.Authority;
import ru.service99.domain.PartPending;
import ru.service99.domain.TypePartPending;
import ru.service99.service.ModelService;
import ru.service99.service.PartService;
import ru.service99.web.ui.OnPartPendingChoiceListener;
import ru.service99.web.ui.ViewUtils;

public class PartPendingWindow extends Window {

    private PartService partService;
    private ModelService modelService;

    private PartPending partPending;

    private TextField partNumberTextField;
    private TextField partNameTextField;
    private TextField neededCountTextField;
    private TextField managerTextField;
    private Button choicePartButton;
    private Button removeButton;


    public PartPendingWindow(PartService partService, ModelService modelService) {
        setCaption(ViewUtils.getMessage("partsPendingPage.window.title.new"));
        this.partService = partService;
        this.modelService = modelService;
        init();
    }

    public PartPendingWindow(PartService partService, ModelService modelService, PartPending partPending) {
        this(partService, modelService);
        setCaption(ViewUtils.getMessage("partsPendingPage.window.title.edit") + " " + partPending.getId());
        this.partPending = partPending;
        init();
    }

    private void init() {
        center();
        setModal(true);
        setWidth("450px");

        VerticalLayout vertical = new VerticalLayout();
        vertical.setSpacing(true);
        vertical.setMargin(true);

        HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.setSpacing(true);

        HorizontalLayout partNumberLayout = new HorizontalLayout();
        partNumberLayout.setSpacing(true);
        partNumberLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);

        HorizontalLayout usersLayout = new HorizontalLayout();
        usersLayout.setSpacing(true);
        usersLayout.setEnabled(false);

        HorizontalLayout countsLayout = new HorizontalLayout();
        countsLayout.setSpacing(true);

        TextField authorTextField = new TextField(ViewUtils.getMessage("field.author"));
        managerTextField = new TextField(ViewUtils.getMessage("field.manager"));
        partNumberTextField = new TextField(ViewUtils.getMessage("field.partnumber"));
        partNameTextField = new TextField(ViewUtils.getMessage("field.name"));
        neededCountTextField = new TextField(ViewUtils.getMessage("field.neededCount"));
        TextField orderedCountTextField = new TextField(ViewUtils.getMessage("field.orderedCount"));
        TextField receivedCountTextField = new TextField(ViewUtils.getMessage("field.receivedCount"));
        TextArea commentTextArea = new TextArea(ViewUtils.getMessage("field.comment"));

        neededCountTextField.setWidth("125px");
        orderedCountTextField.setWidth("125px");
        receivedCountTextField.setWidth("125px");

        partNameTextField.setEnabled(false);
        partNameTextField.setSizeFull();
        partNumberTextField.setEnabled(false);
        commentTextArea.setRows(2);
        commentTextArea.setSizeFull();

        Button saveButton = new Button(ViewUtils.getMessage("button.save"));
        saveButton.addClickListener(clickEvent -> {
            if (partPending == null) {
                Notification.show(ViewUtils.getMessage("notifications.selectPart"), Notification.Type.ERROR_MESSAGE);
                return;
            }
            int neededCount;
            int orderedCount;
            int receivedCount;
            try {
                neededCount = Integer.parseInt(neededCountTextField.getValue());
                orderedCount = Integer.parseInt(orderedCountTextField.getValue());
                receivedCount = Integer.parseInt(receivedCountTextField.getValue());
            } catch (NumberFormatException e) {
                Notification.show(ViewUtils.getMessage("notifications.wrongCount"), Notification.Type.ERROR_MESSAGE);
                return;
            }
            if (partPending.getType() == null) partPending.setType(TypePartPending.запас);
            if (partPending.getOrderedCount() != orderedCount || partPending.getReceivedCount() != receivedCount) {
                switch (partPending.getType()) {
                    case ремонт:
                        if (!SecurityUtils.hasAuthority(Authority.заказ_деталей_для_ремонтов)) {
                            Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                                    Notification.Type.ERROR_MESSAGE);
                            return;
                        }
                        break;
                    case продажа:
                        if (!SecurityUtils.hasAuthority(Authority.заказ_деталей_для_продаж)) {
                            Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                                    Notification.Type.ERROR_MESSAGE);
                            return;
                        }
                        break;
                    case запас:
                        if (!SecurityUtils.hasAuthority(Authority.заказ_деталей_прозапас)) {
                            Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                                    Notification.Type.ERROR_MESSAGE);
                            return;
                        }
                        break;
                }
            }
            partPending.setNeededCount(neededCount);
            partPending.setOrderedCount(orderedCount);
            partPending.setReceivedCount(receivedCount);
            partPending.setComment(commentTextArea.getValue());
            try {
                PartPending savedPartPending = partService.savePartPending(partPending);
                partPending = partService.findPartPendingById(savedPartPending.getId());
                setFields();
            } catch (Exception e) {
                StringBuilder builder = new StringBuilder(ViewUtils.getMessage("notifications.notSaved"));
                if (e.getLocalizedMessage() != null) {
                    builder.append("\n");
                    builder.append(e.getLocalizedMessage());
                } else {
                    for (StackTraceElement element : e.getStackTrace()) {
                        builder.append("\n");
                        builder.append(element);
                    }
                }
                Notification.show(builder.toString(), Notification.Type.ERROR_MESSAGE);
            }
        });
        Button closeButton = new Button(ViewUtils.getMessage("button.close"));
        closeButton.addClickListener(event -> close());

        removeButton = new Button(ViewUtils.getMessage("button.remove"));
        removeButton.setEnabled(false);
        removeButton.addClickListener(event1 -> {
            ConfirmWindow window = new ConfirmWindow(ViewUtils.getMessage("confirm.message.remove"));
            window.setOkClickListener(() -> {
                try {
                    partService.removePartPending(partPending);
                    close();
                } catch (Exception e) {
                    StringBuilder builder = new StringBuilder(ViewUtils.getMessage("notifications.notSaved"));
                    if (e.getLocalizedMessage() != null) {
                        builder.append("\n");
                        builder.append(e.getLocalizedMessage());
                    } else {
                        for (StackTraceElement element : e.getStackTrace()) {
                            builder.append("\n");
                            builder.append(element);
                        }
                    }
                    Notification.show(builder.toString(), Notification.Type.ERROR_MESSAGE);
                }
            });
            getUI().addWindow(window);
        });

        choicePartButton = new Button(ViewUtils.getMessage("button.select"));
        choicePartButton.addClickListener(event -> {
            for (Window window : getUI().getWindows()) if (window instanceof SelectPartWindow) return;
            SelectPartWindow window = new SelectPartWindow(modelService, partService);
            window.setPartPendingChoiceListener(new MyOnPartPendingChoiceListener());
            getUI().addWindow(window);
        });

        buttonsLayout.addComponent(closeButton);
        buttonsLayout.addComponent(removeButton);
        buttonsLayout.addComponent(saveButton);

        partNumberLayout.addComponent(partNumberTextField);
        partNumberLayout.addComponent(choicePartButton);

        usersLayout.addComponent(authorTextField);
        usersLayout.addComponent(managerTextField);

        countsLayout.addComponent(neededCountTextField);
        countsLayout.addComponent(orderedCountTextField);
        countsLayout.addComponent(receivedCountTextField);

        vertical.addComponent(usersLayout);
        vertical.addComponent(partNumberLayout);
        vertical.addComponent(partNameTextField);
        vertical.addComponent(countsLayout);
        vertical.addComponent(commentTextArea);
        vertical.addComponent(buttonsLayout);

        setContent(vertical);

        if (partPending != null) {
            setPartFields();
            neededCountTextField.setValue(String.valueOf(partPending.getNeededCount() != null ?
                    partPending.getNeededCount() : ""));
            orderedCountTextField.setValue(String.valueOf(partPending.getOrderedCount() != null ?
                    partPending.getOrderedCount() : ""));
            receivedCountTextField.setValue(String.valueOf(partPending.getReceivedCount() != null ?
                    partPending.getReceivedCount() : ""));
            commentTextArea.setValue(partPending.getComment() != null ? partPending.getComment() : "");
            authorTextField.setValue(partPending.getReceiver().getName());
            managerTextField.setValue(partPending.getManager() != null ? partPending.getManager().getName() : "");
        } else {
            authorTextField.setValue(SecurityUtils.getCurrentUser().getName());
            neededCountTextField.setValue("0");
            orderedCountTextField.setValue("0");
            receivedCountTextField.setValue("0");
        }
        setFields();
    }

    private void setPartFields() {
        if (partPending != null) {
            partNumberTextField.setValue(partPending.getPart().getPartNumber());
            partNameTextField.setValue(partPending.getPart().getName());
            neededCountTextField.setValue(String.valueOf(partPending.getNeededCount()));
        }
    }

    private void setFields() {
        if (partPending != null) {
            if (partPending.getOrderedCount() != 0) {
                choicePartButton.setEnabled(false);
            } else choicePartButton.setEnabled(true);
            managerTextField.setValue(partPending.getManager() != null ? partPending.getManager().getName() : "");
            if (partPending.getId() != null) removeButton.setEnabled(true);
        } else {
            choicePartButton.setEnabled(true);
            removeButton.setEnabled(false);
        }
    }

    private class MyOnPartPendingChoiceListener implements OnPartPendingChoiceListener {
        @Override
        public void onChoice(PartPending pp) {
            partPending = pp;
            setPartFields();
        }
    }
}
