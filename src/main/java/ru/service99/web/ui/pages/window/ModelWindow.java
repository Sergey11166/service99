package ru.service99.web.ui.pages.window;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.*;
import ru.service99.SecurityUtils;
import ru.service99.domain.Authority;
import ru.service99.domain.Model;
import ru.service99.domain.Producer;
import ru.service99.domain.TypeDevice;
import ru.service99.service.ModelService;
import ru.service99.web.ui.ViewUtils;

import java.util.HashMap;
import java.util.List;

public class ModelWindow extends Window {

    private ModelService modelService;

    private Model model;

    private HashMap<Long, TypeDevice> typeMap = new HashMap<>();
    private HashMap<Long, Producer> producerMap = new HashMap<>();

    public ModelWindow(ModelService modelService) {
        setCaption(ViewUtils.getMessage("modelsPage.window.title.new"));
        this.modelService = modelService;
        init();
    }

    public ModelWindow(ModelService modelService, Model model) {
        this(modelService);
        setCaption(ViewUtils.getMessage("modelsPage.window.title.edit") + " " + model.getId());
        this.model = model;
        init();
    }

    private void init() {
        center();
        setModal(true);

        VerticalLayout vertical = new VerticalLayout();
        vertical.setSpacing(true);
        vertical.setMargin(true);
        vertical.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

        HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.setSpacing(true);

        TextField nameTextField = new TextField(ViewUtils.getMessage("field.name"));
        ComboBox typeDeviceComboBox = createTypeDeviceComboBox();
        ComboBox producerComboBox = createProducerPlaceComboBox();

        nameTextField.setWidth("330px");
        typeDeviceComboBox.setWidth("330px");
        producerComboBox.setWidth("330px");

        if (model != null) {
            nameTextField.setValue(model.getName());
            typeDeviceComboBox.select(typeMap.get(model.getTypeDevice().getId()));
            producerComboBox.select(producerMap.get(model.getProducer().getId()));
        }


        Button saveButton = new Button(ViewUtils.getMessage("button.save"));
        saveButton.addClickListener(event -> {
            if (!SecurityUtils.hasAuthority(Authority.редактирование_модели)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
            if (model == null) model = new Model();
            model.setName(nameTextField.getValue());
            model.setTypeDevice((TypeDevice) typeDeviceComboBox.getValue());
            model.setProducer((Producer) producerComboBox.getValue());
            try {
                modelService.saveModel(model);
                Notification.show(ViewUtils.getMessage("notifications.saved"), Notification.Type.TRAY_NOTIFICATION);
                close();
            } catch (Exception e) {
                StringBuilder builder = new StringBuilder(ViewUtils.getMessage("notifications.notSaved"));
                if (e.getLocalizedMessage() != null) {
                    builder.append("\n");
                    builder.append(e.getLocalizedMessage());
                } else {
                    for (StackTraceElement element : e.getStackTrace()) {
                        builder.append("\n");
                        builder.append(element);
                    }
                }
                Notification.show(builder.toString(), Notification.Type.ERROR_MESSAGE);
            }
        });

        Button removeButton = new Button(ViewUtils.getMessage("button.remove"));
        if (model == null) removeButton.setEnabled(false);
        removeButton.addClickListener(event -> {
            if (!SecurityUtils.hasAuthority(Authority.редактирование_модели)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
            ConfirmWindow window = new ConfirmWindow(ViewUtils.getMessage("confirm.message.remove"));
            window.setOkClickListener(() -> {
                model.setIsRemoved(true);
                try {
                    modelService.saveModel(model);
                    close();
                } catch (Exception e) {
                    model.setIsRemoved(false);
                    StringBuilder builder = new StringBuilder(ViewUtils.getMessage("notifications.notSaved"));
                    if (e.getLocalizedMessage() != null) {
                        builder.append("\n");
                        builder.append(e.getLocalizedMessage());
                    } else {
                        for (StackTraceElement element : e.getStackTrace()) {
                            builder.append("\n");
                            builder.append(element);
                        }
                    }
                    Notification.show(builder.toString(), Notification.Type.ERROR_MESSAGE);
                }
            });
            getUI().addWindow(window);
        });

        Button cancelButton = new Button(ViewUtils.getMessage("button.cancel"));
        cancelButton.addClickListener(event -> close());

        vertical.addComponent(nameTextField);
        vertical.addComponent(typeDeviceComboBox);
        vertical.addComponent(producerComboBox);

        buttonsLayout.addComponent(cancelButton);
        buttonsLayout.addComponent(removeButton);
        buttonsLayout.addComponent(saveButton);

        vertical.addComponent(buttonsLayout);

        setContent(vertical);
    }

    private ComboBox createTypeDeviceComboBox() {
        BeanItemContainer<TypeDevice> container = new BeanItemContainer<>(TypeDevice.class);
        List<TypeDevice> typeDevices = modelService.findAllTypeDevices();
        typeDevices.forEach(typeDevice -> typeMap.put(typeDevice.getId(), typeDevice));
        container.addAll(typeDevices);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.typeDevice"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.setNullSelectionAllowed(false);
        return select;
    }

    private ComboBox createProducerPlaceComboBox() {
        BeanItemContainer<Producer> container = new BeanItemContainer<>(Producer.class);
        List<Producer> producers = modelService.findAllProducers();
        producers.forEach(producer -> producerMap.put(producer.getId(), producer));
        container.addAll(producers);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.producer"), container);
        select.setItemCaptionPropertyId("name");
        select.setNullSelectionAllowed(false);
        return select;
    }
}
