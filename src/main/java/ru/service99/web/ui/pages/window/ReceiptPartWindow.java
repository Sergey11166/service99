package ru.service99.web.ui.pages.window;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.*;
import ru.service99.SecurityUtils;
import ru.service99.domain.*;
import ru.service99.service.DocumentService;
import ru.service99.service.ModelService;
import ru.service99.service.PartService;
import ru.service99.web.model.FreePart;
import ru.service99.web.ui.OnPartChoiceListener;
import ru.service99.web.ui.OnPriceChangeListener;
import ru.service99.web.ui.ViewUtils;

import java.text.SimpleDateFormat;
import java.util.*;

public class ReceiptPartWindow extends Window {

    private static final String PART_NUMBER_COLUMN = "part.partNumber";
    private static final String NAME_COLUMN = "part.name";
    private static final String COUNT_COLUMN = "count";
    private static final String IN_PRICE_COLUMN = "inPrice";
    private static final String OPT_PRICE_COLUMN = "optPrice";
    private static final String OUT_PRICE_COLUMN = "outPrice";
    private static final String COMMENT_COLUMN = "part.comment";

    private static final String PART_NUMBER_VISIBLE_COLUMN = ViewUtils.getMessage("table.partNumber");
    private static final String NAME_VISIBLE_COLUMN = ViewUtils.getMessage("table.name");
    private static final String COUNT_VISIBLE_COLUMN = ViewUtils.getMessage("table.count");
    private static final String IN_PRICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.inPrice");
    private static final String OPT_PRICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.optPrice");
    private static final String OUT_PRICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.outPrice");
    private static final String COMMENT_VISIBLE_COLUMN = ViewUtils.getMessage("table.comment");

    private static final String[] fieldNamesWithInPrice = new String[]{PART_NUMBER_COLUMN, NAME_COLUMN, COUNT_COLUMN,
            IN_PRICE_COLUMN, OPT_PRICE_COLUMN, OUT_PRICE_COLUMN, COMMENT_COLUMN};
    private static final String[] fieldNamesWithoutInPrice = new String[]{PART_NUMBER_COLUMN, NAME_COLUMN, COUNT_COLUMN,
            OPT_PRICE_COLUMN, OUT_PRICE_COLUMN, COMMENT_COLUMN};

    private SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yy", Locale.getDefault());

    private DocumentService documentService;
    private ModelService modelService;
    private PartService partService;

    private DocumentIn documentIn;
    private Set<PartInstance> partInstances;

    private Table table = new Table();

    private ComboBox stockComboBox;
    private ComboBox statusComboBox;
    private TextField commentTextField;
    private HorizontalLayout fieldLayout;

    private HashMap<Long, Stock> stockMap = new HashMap<>();

    public ReceiptPartWindow(DocumentService documentService, ModelService modelService, PartService partService) {
        setCaption(ViewUtils.getMessage("receiptsPartsPage.window.title.new"));
        this.documentService = documentService;
        this.modelService = modelService;
        this.partService = partService;
        this.partInstances = new HashSet<>();
        init();
    }

    public ReceiptPartWindow(DocumentService documentService, ModelService modelService, PartService partService,
                             DocumentIn documentIn) {
        this(documentService, modelService, partService);
        setCaption(ViewUtils.getMessage("receiptsPartsPage.window.title.edit") + " " + documentIn.getId());
        this.documentIn = documentIn;
        this.partInstances.addAll(documentIn.getPartInstances());
        init();
    }

    public void init() {
        initLayout();
        if (documentIn != null) initTable(createContainer(new ArrayList<>(partInstances)));
        else initTable(createContainer(new ArrayList<>()));
    }

    private void initLayout() {
        center();
        setModal(true);
        setHeight("90%");
        setWidth("90%");

        VerticalLayout vertical = new VerticalLayout();
        vertical.setSpacing(true);
        vertical.setMargin(true);
        vertical.setSizeFull();

        fieldLayout = new HorizontalLayout();
        fieldLayout.setSpacing(true);
        fieldLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);

        statusComboBox = createStatusComboBox();
        stockComboBox = createStockComboBox();

        Label authorDocumentLabel;
        Label dateDocumentLabel;
        commentTextField = new TextField(ViewUtils.getMessage("field.comment"));
        commentTextField.setSizeFull();

        Button addButton = new Button("+");
        addButton.addClickListener(clickEvent -> clickAddButton());

        Button removeButton = new Button("-");
        removeButton.addClickListener(clickEvent -> clickRemoveButton());

        Button saveButton = new Button(ViewUtils.getMessage("button.save"));
        saveButton.addClickListener(clickEvent -> saveDocument());

        if (documentIn != null) {
            dateDocumentLabel = new Label(ViewUtils.getMessage("field.date") + " " +
                    sdf.format(documentIn.getDate()));
            authorDocumentLabel = new Label(ViewUtils.getMessage("field.author") + " "
                    + documentIn.getAuthor().getName());
            setFields();
        } else {
            dateDocumentLabel = new Label(ViewUtils.getMessage("field.date") + " " +
                    sdf.format(new Date()));
            authorDocumentLabel = new Label(ViewUtils.getMessage("field.author") + " "
                    + SecurityUtils.getCurrentUser().getName());
            statusComboBox.setEnabled(false);
        }

        fieldLayout.addComponent(dateDocumentLabel);
        fieldLayout.addComponent(authorDocumentLabel);
        fieldLayout.addComponent(stockComboBox);
        fieldLayout.addComponent(statusComboBox);
        fieldLayout.addComponent(addButton);
        fieldLayout.addComponent(removeButton);

        vertical.addComponent(fieldLayout);
        vertical.addComponent(commentTextField);
        vertical.addComponent(table);
        vertical.addComponent(saveButton);

        vertical.setExpandRatio(fieldLayout, 1);
        vertical.setExpandRatio(commentTextField, 1);
        vertical.setExpandRatio(table, 6);
        vertical.setExpandRatio(saveButton, 1);

        setContent(vertical);
    }

    private ComboBox createStatusComboBox() {
        BeanItemContainer<StatusDocument> container = new BeanItemContainer<>(StatusDocument.class);
        StatusDocument[] statuses = StatusDocument.values();
        container.addBean(statuses[0]);
        container.addBean(statuses[2]);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.status"), container);
        select.setNullSelectionAllowed(false);
        return select;
    }

    private ComboBox createStockComboBox() {
        BeanItemContainer<Stock> container = new BeanItemContainer<>(Stock.class);
        List<Stock> stocks = partService.findAllStocks();
        stocks.forEach(stock -> stockMap.put(stock.getId(), stock));
        container.addAll(stocks);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.stock"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.setNullSelectionAllowed(false);
        return select;
    }

    private void setFields() {
        statusComboBox.select(documentIn.getStatus());
        if (documentIn.getStatus() == StatusDocument.Завершено) {
            statusComboBox.setEnabled(false);
            fieldLayout.setEnabled(false);
        } else {
            statusComboBox.setEnabled(true);
            fieldLayout.setEnabled(true);
        }
        stockComboBox.setEnabled(false);
        stockComboBox.select(stockMap.get(documentIn.getStockTo().getId()));
        commentTextField.setValue(documentIn.getComment());
    }

    private void clickAddButton() {
        for (Window window : getUI().getWindows()) if (window instanceof SelectPartWindow) return;
        SelectPartWindow window = new SelectPartWindow(modelService, partService);
        window.setPartChoiceListener(new MyOnPartChoiceListener());
        getUI().addWindow(window);
    }

    private void clickRemoveButton() {
        FreePart selected = (FreePart) table.getValue();
        if (selected == null) return;
        for (PartInstance instance : partInstances) {
            if (Objects.equals(instance.getPart().getId(), selected.getPart().getId())) {
                partInstances.remove(instance);
                break;
            }
        }
        initTable(createContainer(new ArrayList<>(partInstances)));
    }

    private void saveDocument() {
        if (!SecurityUtils.hasAuthority(Authority.поступление_деталей)) {
            Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                    Notification.Type.ERROR_MESSAGE);
            return;
        }
        if (documentIn == null) documentIn = new DocumentIn();
        Stock stock = (Stock) stockComboBox.getValue();
        StatusDocument status = (StatusDocument) statusComboBox.getValue();

        documentIn.setStatus(status);
        documentIn.setStockTo(stock);
        documentIn.setComment(commentTextField.getValue());
        documentIn.setPartInstances(partInstances);
        try {
            DocumentIn savedDocument = documentService.saveDocumentIn(documentIn);
            documentIn = documentService.findDocumentInById(savedDocument.getId());
            partInstances = new HashSet<>(documentIn.getPartInstances());
            Notification.show(ViewUtils.getMessage("notifications.saved"), Notification.Type.TRAY_NOTIFICATION);
            setFields();
        } catch (Exception e) {
            StringBuilder builder = new StringBuilder(ViewUtils.getMessage("notifications.notSaved"));
            if (e.getLocalizedMessage() != null) {
                builder.append("\n");
                builder.append(e.getLocalizedMessage());
            } else {
                for (StackTraceElement element : e.getStackTrace()) {
                    builder.append("\n");
                    builder.append(element);
                }
            }
            Notification.show(builder.toString(), Notification.Type.ERROR_MESSAGE);
        }
    }

    private void initTable(BeanItemContainer<FreePart> container) {
        table.setContainerDataSource(container);
        table.setSizeFull();
        table.setColumnCollapsingAllowed(true);
        if (SecurityUtils.hasAuthority(Authority.видеть_входную_цену)) {
            table.setVisibleColumns(fieldNamesWithInPrice);
            table.setColumnHeader(IN_PRICE_COLUMN, IN_PRICE_VISIBLE_COLUMN);
        } else {
            table.setVisibleColumns(fieldNamesWithoutInPrice);
        }
        table.setColumnHeader(PART_NUMBER_COLUMN, PART_NUMBER_VISIBLE_COLUMN);
        table.setColumnHeader(NAME_COLUMN, NAME_VISIBLE_COLUMN);
        table.setColumnHeader(COUNT_COLUMN, COUNT_VISIBLE_COLUMN);
        table.setColumnHeader(IN_PRICE_COLUMN, IN_PRICE_VISIBLE_COLUMN);
        table.setColumnHeader(OPT_PRICE_COLUMN, OPT_PRICE_VISIBLE_COLUMN);
        table.setColumnHeader(OUT_PRICE_COLUMN, OUT_PRICE_VISIBLE_COLUMN);
        table.setColumnHeader(COMMENT_COLUMN, COMMENT_VISIBLE_COLUMN);
        table.setSelectable(true);
        table.setImmediate(true);
    }

    private BeanItemContainer<FreePart> createContainer(List<PartInstance> instances) {
        Stock stock = (Stock) stockComboBox.getValue();
        for (PartInstance instance : instances) {
            instance.setStock(stock);
        }
        BeanItemContainer<FreePart> container = new BeanItemContainer<>(FreePart.class);
        container.addNestedContainerProperty(PART_NUMBER_COLUMN);
        container.addNestedContainerProperty(NAME_COLUMN);
        container.addNestedContainerProperty(COMMENT_COLUMN);
        List<FreePart> freeParts = FreePart.instancesToFreeParts(instances);
        container.addAll(freeParts);
        container.sort(new Object[]{NAME_COLUMN, COUNT_COLUMN}, new boolean[]{true, true});
        return container;
    }

    private class MyOnPartChoiceListener implements OnPartChoiceListener {
        @Override
        public void onChoice(Set<PartInstance> instances) {
            if (instances.isEmpty()) return;
            for (Window window : getUI().getWindows()) if (window instanceof PriceChangeWindow) return;
            PriceChangeWindow window = new PriceChangeWindow(instances);
            window.setPriceChangeListener(new MyOnPriceChangeListener());
            getUI().addWindow(window);
        }
    }

    private class MyOnPriceChangeListener implements OnPriceChangeListener {
        @Override
        public void onChange(Set<PartInstance> instances) {
            if (partInstances != null) partInstances.addAll(instances);
            else partInstances = instances;
            initTable(createContainer(new ArrayList<>(partInstances)));
        }
    }
}
