package ru.service99.web.ui.pages;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.Window;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.service99.SecurityUtils;
import ru.service99.domain.Authority;
import ru.service99.domain.Place;
import ru.service99.service.PlaceService;
import ru.service99.web.ui.ViewUtils;
import ru.service99.web.ui.pages.window.PlaceWindow;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
@Scope("prototype")
@SpringView(name = PlacesPage.NAME)
public class PlacesPage extends BasePage {

    public static final String NAME = "places";

    @Autowired
    private PlaceService placeService;

    private Table table = new Table();

    private static final String ID_COLUMN = "id";
    private static final String NAME_COLUMN = "name";
    private static final String ADDRESS_COLUMN = "address";
    private static final String CONTACT_COLUMN = "contact";

    private static final String ID_VISIBLE_COLUMN = ViewUtils.getMessage("table.id");
    private static final String NAME_VISIBLE_COLUMN = ViewUtils.getMessage("table.name");
    private static final String ADDRESS_VISIBLE_COLUMN = ViewUtils.getMessage("table.address");
    private static final String CONTACT_VISIBLE_COLUMN = ViewUtils.getMessage("table.contact");

    private static final String[] fieldNames = new String[]{ID_COLUMN, NAME_COLUMN, ADDRESS_COLUMN, CONTACT_COLUMN};

    @PostConstruct
    public void init() {
        initLayout();
        initTable(createContainer(placeService.findAllPlaces()));
    }

    private void initLayout() {
        Button addButton = new Button(ViewUtils.getMessage("button.new"));
        addButton.addClickListener(event -> {
            if (!SecurityUtils.hasAuthority(Authority.редактирование_площадки)) {
                Notification.show(ViewUtils.getMessage("notifications.accessDenied"),
                        Notification.Type.ERROR_MESSAGE);
                return;
            }
            for (Window window : getUI().getWindows()) if (window instanceof PlaceWindow) return;
            PlaceWindow window = new PlaceWindow(placeService);
            window.addCloseListener(new MyCloseListener());
            getUI().addWindow(window);
        });

        baseLayout.addComponent(table);
        baseLayout.addComponent(addButton);
        baseLayout.setExpandRatio(table, 8);
        baseLayout.setExpandRatio(addButton, 1);
    }

    private void initTable(BeanItemContainer<Place> container) {
        table.setContainerDataSource(container);
        table.setHeight("100%");
        table.setVisibleColumns(fieldNames);
        table.setColumnHeader(ID_COLUMN, ID_VISIBLE_COLUMN);
        table.setColumnHeader(NAME_COLUMN, NAME_VISIBLE_COLUMN);
        table.setColumnHeader(ADDRESS_COLUMN, ADDRESS_VISIBLE_COLUMN);
        table.setColumnHeader(CONTACT_COLUMN, CONTACT_VISIBLE_COLUMN);
        table.setSelectable(true);
        table.setImmediate(true);
        table.addItemClickListener(event -> {
            if (!event.isDoubleClick()) return;
            for (Window window : getUI().getWindows()) if (window instanceof PlaceWindow) return;
            Place place = (Place) event.getItemId();
            PlaceWindow window = new PlaceWindow(placeService, place);
            window.addCloseListener(new MyCloseListener());
            getUI().addWindow(window);
        });
    }

    private BeanItemContainer<Place> createContainer(List<Place> places) {
        BeanItemContainer<Place> container = new BeanItemContainer<>(Place.class);
        container.addAll(places);
        return container;
    }

    private class MyCloseListener implements Window.CloseListener {
        @Override
        public void windowClose(Window.CloseEvent closeEvent) {
            initTable(createContainer(placeService.findAllPlaces()));
        }
    }
}
