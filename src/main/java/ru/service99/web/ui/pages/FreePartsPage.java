package ru.service99.web.ui.pages;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.service99.SecurityUtils;
import ru.service99.domain.*;
import ru.service99.service.ModelService;
import ru.service99.service.PartService;
import ru.service99.service.criteria.PartSearchCriteria;
import ru.service99.web.model.FreePart;
import ru.service99.web.ui.ViewUtils;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
@Scope("prototype")
@SpringView(name = FreePartsPage.NAME)
public class FreePartsPage extends BasePage {

    public static final String NAME = "free-parts";

    private static final String PART_NUMBER_COLUMN = "part.partNumber";
    private static final String NAME_COLUMN = "part.name";
    private static final String COUNT_COLUMN = "count";
    private static final String STOCK_COLUMN = "stock.name";
    private static final String IN_PRICE_COLUMN = "inPrice";
    private static final String OPT_PRICE_COLUMN = "optPrice";
    private static final String OUT_PRICE_COLUMN = "outPrice";
    private static final String COMMENT_COLUMN = "part.comment";

    private static final String PART_NUMBER_VISIBLE_COLUMN = ViewUtils.getMessage("table.partNumber");
    private static final String NAME_VISIBLE_COLUMN = ViewUtils.getMessage("table.name");
    private static final String COUNT_VISIBLE_COLUMN = ViewUtils.getMessage("table.count");
    private static final String STOCK_VISIBLE_COLUMN = ViewUtils.getMessage("table.stock");
    private static final String IN_PRICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.inPrice");
    private static final String OPT_PRICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.optPrice");
    private static final String OUT_PRICE_VISIBLE_COLUMN = ViewUtils.getMessage("table.outPrice");
    private static final String COMMENT_VISIBLE_COLUMN = ViewUtils.getMessage("table.comment");

    private static final String[] fieldNamesWithInPrice = new String[]{PART_NUMBER_COLUMN, NAME_COLUMN, COUNT_COLUMN,
            STOCK_COLUMN, IN_PRICE_COLUMN, OPT_PRICE_COLUMN, OUT_PRICE_COLUMN, COMMENT_COLUMN};

    private static final String[] fieldNamesWithoutInPrice = new String[]{PART_NUMBER_COLUMN, NAME_COLUMN, COUNT_COLUMN,
            STOCK_COLUMN, OPT_PRICE_COLUMN, OUT_PRICE_COLUMN, COMMENT_COLUMN};

    @Autowired
    private PartService partService;
    @Autowired
    private ModelService modelService;

    private Table table = new Table();

    private TextField nameTextField;
    private TextField numberTextField;

    private ComboBox stockComboBox;
    private ComboBox typeDeviceComboBox;
    private ComboBox producerComboBox;
    private ComboBox modelComboBox;

    //private Label countLabel;
    //private ComboBox countComboBox;

    @PostConstruct
    public void init() {
        initLayout();
        applyFilter();
    }

    private void initLayout() {
        HorizontalLayout horizontal = new HorizontalLayout();
        horizontal.setSpacing(true);
        horizontal.setSizeFull();

        VerticalLayout filterLayout = new VerticalLayout();
        filterLayout.setSpacing(true);

        //HorizontalLayout countLayout = new HorizontalLayout();
        //countLayout.setSpacing(true);

        //VerticalLayout tableLayout = new VerticalLayout();
        //filterLayout.setSpacing(true);

        nameTextField = new TextField(ViewUtils.getMessage("field.name"));
        numberTextField = new TextField(ViewUtils.getMessage("field.partnumber"));

        stockComboBox = createStockComboBox();
        typeDeviceComboBox = createTypeDeviceComboBox();
        producerComboBox = createProducerPlaceComboBox();
        modelComboBox = createModelComboBox();

        //countLabel = new Label();
        //countComboBox = createCountComboBox();
        //countComboBox.setWidth("100px");

        Button applyButton = new Button(ViewUtils.getMessage("button.search"));
        applyButton.addClickListener(clickEvent -> applyFilter());

        filterLayout.addComponent(nameTextField);
        filterLayout.addComponent(numberTextField);
        filterLayout.addComponent(stockComboBox);
        filterLayout.addComponent(typeDeviceComboBox);
        filterLayout.addComponent(producerComboBox);
        filterLayout.addComponent(modelComboBox);
        filterLayout.addComponent(applyButton);

        //countLayout.addComponent(countLabel);
        //countLayout.addComponent(countComboBox);

        //tableLayout.addComponent(countLayout);
        //tableLayout.addComponent(table);

        horizontal.addComponent(filterLayout);
        horizontal.addComponent(table);
        horizontal.setExpandRatio(filterLayout, 1);
        horizontal.setExpandRatio(table, 5);

        baseLayout.addComponent(horizontal);
        baseLayout.setExpandRatio(horizontal, 8);
    }

    private ComboBox createStockComboBox() {
        BeanItemContainer<Stock> container = new BeanItemContainer<>(Stock.class);
        List<Stock> stocks = partService.findAllStocks();
        container.addAll(stocks);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.stock"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        return select;
    }

    private ComboBox createTypeDeviceComboBox() {
        BeanItemContainer<TypeDevice> container = new BeanItemContainer<>(TypeDevice.class);
        List<TypeDevice> typeDevices = modelService.findAllTypeDevices();
        container.addAll(typeDevices);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.typeDevice"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.addValueChangeListener(event -> {
            Producer producer = (Producer) producerComboBox.getValue();
            if (producer == null) return;
            TypeDevice typeDevice = (TypeDevice) event.getProperty().getValue();
            List<Model> models;
            if (typeDevice != null) {
                models = modelService.findModelByProducerIdAndTypeDeviceId(producer.getId(), typeDevice.getId());
            } else {
                models = modelService.findModelByProducerId(producer.getId());
            }
            BeanItemContainer<Model> modelContainer = new BeanItemContainer<>(Model.class);
            modelContainer.addAll(models);
            modelComboBox.setContainerDataSource(modelContainer);
        });
        return select;
    }

    private ComboBox createProducerPlaceComboBox() {
        BeanItemContainer<Producer> container = new BeanItemContainer<>(Producer.class);
        List<Producer> producers = modelService.findAllProducers();
        container.addAll(producers);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.producer"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.addValueChangeListener(event -> {
            TypeDevice typeDevice = (TypeDevice) typeDeviceComboBox.getValue();
            Producer producer = (Producer) event.getProperty().getValue();
            List<Model> models = new ArrayList<>();
            if (producer != null) {
                modelComboBox.setEnabled(true);
                if (typeDevice != null) {
                    models = modelService.findModelByProducerIdAndTypeDeviceId(
                            producer.getId(), typeDevice.getId());
                } else {
                    models = modelService.findModelByProducerId(producer.getId());
                }
            } else modelComboBox.setEnabled(false);
            BeanItemContainer<Model> modelContainer = new BeanItemContainer<>(Model.class);
            modelContainer.addAll(models);
            modelComboBox.setContainerDataSource(modelContainer);
        });
        return select;
    }

    private ComboBox createModelComboBox() {
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.model"));
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.setEnabled(false);
        return select;
    }

    /*private ComboBox createCountComboBox(){
        ComboBox select = new ComboBox();
        select.setNullSelectionAllowed(false);
        select.addItem("1000");
        select.addItem("3000");
        select.addItem("5000");
        select.select("1000");
        select.addValueChangeListener(event -> applyFilter());
        return select;
    }*/

    private void initTable(BeanItemContainer<FreePart> container) {
        table.setContainerDataSource(container);
        table.setSizeFull();
        table.setColumnCollapsingAllowed(true);
        if (SecurityUtils.hasAuthority(Authority.видеть_входную_цену)) {
            table.setVisibleColumns(fieldNamesWithInPrice);
            table.setColumnHeader(IN_PRICE_COLUMN, IN_PRICE_VISIBLE_COLUMN);
        } else {
            table.setVisibleColumns(fieldNamesWithoutInPrice);
        }
        table.setColumnHeader(PART_NUMBER_COLUMN, PART_NUMBER_VISIBLE_COLUMN);
        table.setColumnHeader(NAME_COLUMN, NAME_VISIBLE_COLUMN);
        table.setColumnHeader(COUNT_COLUMN, COUNT_VISIBLE_COLUMN);
        table.setColumnHeader(STOCK_COLUMN, STOCK_VISIBLE_COLUMN);
        table.setColumnHeader(IN_PRICE_COLUMN, IN_PRICE_VISIBLE_COLUMN);
        table.setColumnHeader(OPT_PRICE_COLUMN, OPT_PRICE_VISIBLE_COLUMN);
        table.setColumnHeader(OUT_PRICE_COLUMN, OUT_PRICE_VISIBLE_COLUMN);
        table.setColumnHeader(COMMENT_COLUMN, COMMENT_VISIBLE_COLUMN);
        table.setImmediate(true);
    }

    private void applyFilter() {
        Stock stock = (Stock) stockComboBox.getValue();
        TypeDevice typeDevice = (TypeDevice) typeDeviceComboBox.getValue();
        Producer producer = (Producer) producerComboBox.getValue();
        Model model = (Model) modelComboBox.getValue();
        String name = nameTextField.getValue();
        String number = numberTextField.getValue();

        PartSearchCriteria criteria = new PartSearchCriteria();
        criteria.setName(name);
        criteria.setPartNumber(number);
        criteria.setModel(model);
        criteria.setProducer(producer);
        criteria.setTypeDevice(typeDevice);
        criteria.setStock(stock);

        long count = partService.getCountFreeInstancesByCriteria(criteria);
        if (count > 1000) {
            Notification.show(ViewUtils.getMessage("notifications.tooMatchElements"), Notification.Type.ERROR_MESSAGE);
        } else {
            initTable(createContainer(partService.findFreeInstancesByCriteria(criteria, null).getContent()));
        }
    }

    private BeanItemContainer<FreePart> createContainer(List<PartInstance> instances) {
        BeanItemContainer<FreePart> container = new BeanItemContainer<>(FreePart.class);
        container.addNestedContainerProperty(PART_NUMBER_COLUMN);
        container.addNestedContainerProperty(NAME_COLUMN);
        container.addNestedContainerProperty(STOCK_COLUMN);
        container.addNestedContainerProperty(COMMENT_COLUMN);
        List<FreePart> freeParts = FreePart.instancesToFreeParts(instances);
        container.addAll(freeParts);
        container.sort(new Object[]{NAME_COLUMN, COUNT_COLUMN}, new boolean[]{true, true});
        return container;
    }
}