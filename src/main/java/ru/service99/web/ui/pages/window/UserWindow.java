package ru.service99.web.ui.pages.window;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.shared.ui.MultiSelectMode;
import com.vaadin.ui.*;
import ru.service99.domain.*;
import ru.service99.service.FirmService;
import ru.service99.service.PartService;
import ru.service99.service.PlaceService;
import ru.service99.service.UserService;
import ru.service99.web.ui.ViewUtils;

import java.util.*;

public class UserWindow extends Window {

    private static final String NAME_COLUMN = "name";

    private static final String[] fieldNames = new String[]{NAME_COLUMN};

    private UserService userService;
    private FirmService firmService;
    private PlaceService placeService;
    private PartService partService;

    private Table authoritiesTable = new Table(ViewUtils.getMessage("field.authorities"));
    private Table stocksTable = new Table(ViewUtils.getMessage("field.allowedStocks"));

    private User user;

    private HashMap<Long, Firm> firmMap = new HashMap<>();
    private HashMap<Long, Place> placeMap = new HashMap<>();

    public UserWindow(UserService userService, FirmService firmService, PlaceService placeService,
                      PartService partService) {
        super(ViewUtils.getMessage("usersPage.window.title.new"));
        this.userService = userService;
        this.firmService = firmService;
        this.placeService = placeService;
        this.partService = partService;
        init();
    }

    public UserWindow(UserService userService, FirmService firmService, PlaceService placeService,
                      PartService partService, User user) {
        super(ViewUtils.getMessage("usersPage.window.title.edit") + " " + user.getId());
        this.userService = userService;
        this.firmService = firmService;
        this.placeService = placeService;
        this.partService = partService;
        this.user = user;
        init();
    }

    private void init() {
        center();
        setModal(true);
        initTables();

        VerticalLayout vertical = new VerticalLayout();
        vertical.setSpacing(true);
        vertical.setMargin(true);

        HorizontalLayout contentLayout = new HorizontalLayout();
        contentLayout.setSpacing(true);

        VerticalLayout fieldLayout = new VerticalLayout();
        fieldLayout.setSpacing(true);

        HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.setSpacing(true);

        TextField nameTextField = new TextField(ViewUtils.getMessage("field.userName"));
        TextField loginTextField = new TextField(ViewUtils.getMessage("field.login"));
        TextField passwordTextField = new TextField(ViewUtils.getMessage("field.password"));
        ComboBox firmComboBox = createFirmComboBox();
        ComboBox placeComboBox = createPlaceComboBox();

        if (user != null) {
            nameTextField.setValue(user.getName());
            loginTextField.setValue(user.getLogin());
            passwordTextField.setValue(user.getPassword());
            firmComboBox.select(firmMap.get(user.getFirm().getId()));
            placeComboBox.select(placeMap.get(user.getPlace().getId()));
        }

        Button saveButton = new Button(ViewUtils.getMessage("button.save"));
        saveButton.addClickListener(event -> {
            if (user == null) user = new User();
            user.setName(nameTextField.getValue());
            user.setLogin(loginTextField.getValue());
            user.setPassword(passwordTextField.getValue());
            user.setPlace((Place) placeComboBox.getValue());
            user.setFirm((Firm) firmComboBox.getValue());
            //noinspection unchecked
            user.setAuthorities((Set<Authority>) authoritiesTable.getValue());
            //noinspection unchecked
            user.setAllowedStocks((Set<Stock>) stocksTable.getValue());
            try {
                userService.save(user);
                Notification.show(ViewUtils.getMessage("notifications.saved"), Notification.Type.TRAY_NOTIFICATION);
            } catch (Exception e) {
                StringBuilder builder = new StringBuilder(ViewUtils.getMessage("notifications.notSaved"));
                if (e.getLocalizedMessage() != null) {
                    builder.append("\n");
                    builder.append(e.getLocalizedMessage());
                } else {
                    for (StackTraceElement element : e.getStackTrace()) {
                        builder.append("\n");
                        builder.append(element);
                    }
                }
                Notification.show(builder.toString(), Notification.Type.ERROR_MESSAGE);
            }
        });

        Button removeButton = new Button(ViewUtils.getMessage("button.remove"));
        if (user == null) removeButton.setEnabled(false);
        removeButton.addClickListener(event -> {
            ConfirmWindow window = new ConfirmWindow(ViewUtils.getMessage("confirm.message.remove"));
            window.setOkClickListener(() -> {
                user.setIsRemoved(true);
                try {
                    userService.save(user);
                    close();
                } catch (Exception e) {
                    user.setIsRemoved(false);
                    StringBuilder builder = new StringBuilder(ViewUtils.getMessage("notifications.notSaved"));
                    if (e.getLocalizedMessage() != null) {
                        builder.append("\n");
                        builder.append(e.getLocalizedMessage());
                    } else {
                        for (StackTraceElement element : e.getStackTrace()) {
                            builder.append("\n");
                            builder.append(element);
                        }
                    }
                    Notification.show(builder.toString(), Notification.Type.ERROR_MESSAGE);
                }
            });
            getUI().addWindow(window);
        });

        Button cancelButton = new Button(ViewUtils.getMessage("button.close"));
        cancelButton.addClickListener(event -> close());

        fieldLayout.addComponent(nameTextField);
        fieldLayout.addComponent(loginTextField);
        fieldLayout.addComponent(passwordTextField);
        fieldLayout.addComponent(firmComboBox);
        fieldLayout.addComponent(placeComboBox);

        contentLayout.addComponent(fieldLayout);
        contentLayout.addComponent(authoritiesTable);
        contentLayout.addComponent(stocksTable);

        buttonsLayout.addComponent(cancelButton);
        buttonsLayout.addComponent(removeButton);
        buttonsLayout.addComponent(saveButton);

        vertical.addComponent(contentLayout);
        vertical.addComponent(buttonsLayout);
        setContent(vertical);
    }

    private ComboBox createFirmComboBox() {
        BeanItemContainer<Firm> container = new BeanItemContainer<>(Firm.class);
        List<Firm> firms = firmService.findAll();
        firms.forEach(firm -> firmMap.put(firm.getId(), firm));
        container.addAll(firms);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.firm"), container);
        select.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        select.setItemCaptionPropertyId("name");
        select.setNullSelectionAllowed(false);
        return select;
    }

    private ComboBox createPlaceComboBox() {
        BeanItemContainer<Place> container = new BeanItemContainer<>(Place.class);
        List<Place> places = placeService.findAllPlaces();
        places.forEach(place -> placeMap.put(place.getId(), place));
        container.addAll(places);
        ComboBox select = new ComboBox(ViewUtils.getMessage("field.place"), container);
        select.setItemCaptionPropertyId("name");
        select.setNullSelectionAllowed(false);
        return select;
    }

    private void initTables() {
        authoritiesTable.setHeight("350px");
        authoritiesTable.addContainerProperty("auth", Authority.class, null);
        for (Authority authority : Authority.values()) {
            //noinspection unchecked
            authoritiesTable.addItem(authority).getItemProperty("auth").setValue(authority);
        }
        authoritiesTable.setColumnHeaderMode(Table.ColumnHeaderMode.HIDDEN);
        authoritiesTable.setSelectable(true);
        authoritiesTable.setImmediate(true);
        authoritiesTable.setMultiSelect(true);
        authoritiesTable.setMultiSelectMode(MultiSelectMode.SIMPLE);
        if (user != null) {
            for (Authority authority : Authority.values()) {
                if (user.getAuthorities().contains(authority)) authoritiesTable.select(authority);
            }
        }

        BeanItemContainer<Stock> stockContainer = new BeanItemContainer<>(Stock.class);
        stockContainer.addAll(partService.findAllStocks());
        stocksTable.setContainerDataSource(stockContainer);
        stocksTable.setHeight("350px");
        stocksTable.setVisibleColumns(fieldNames);
        stocksTable.setColumnHeaderMode(Table.ColumnHeaderMode.HIDDEN);
        stocksTable.setSelectable(true);
        stocksTable.setImmediate(true);
        stocksTable.setMultiSelect(true);
        stocksTable.setMultiSelectMode(MultiSelectMode.SIMPLE);
        if (user != null) {
            for (Stock stock : stockContainer.getItemIds()) {
                for (Stock s : new HashSet<>(user.getAllowedStocks())) {
                    if (Objects.equals(s.getId(), stock.getId())) {
                        stocksTable.select(stock);
                        break;
                    }
                }
            }
        }
    }
}
