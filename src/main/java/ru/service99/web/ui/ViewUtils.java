package ru.service99.web.ui;

import com.vaadin.server.ExternalResource;
import com.vaadin.ui.Link;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class ViewUtils {

    public static Link createPageLink(String caption, String page) {
        return new Link(caption, new ExternalResource("#!" + page));
    }

    public static String getMessage(String key) {
        ResourceBundle rb = ResourceBundle.getBundle("messages");
        try {
            return rb.getString(key);
        } catch (MissingResourceException e) {
            return key;
        }
    }
}
