package ru.service99.web.ui;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.DefaultErrorHandler;
import com.vaadin.server.ErrorHandler;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.UI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.service99.service.SampleDataGenerator;

@Component
@SpringUI()
@Scope("prototype")
@Theme("service99crm")
public class Service99crmUI extends UI implements ErrorHandler {

    @Autowired
    private SpringViewProvider viewProvider;
    @Autowired
    private SampleDataGenerator sampleDataGenerator;


    @Override
    protected void init(VaadinRequest vaadinRequest) {
        //sampleDataGenerator.generateSampleData();
        VaadinSession.getCurrent().setErrorHandler(this);
        setSizeFull();
        Navigator navigator = new Navigator(this, this);
        navigator.addProvider(viewProvider);
    }

    @Override
    public void error(com.vaadin.server.ErrorEvent errorEvent) {
        /*Throwable cause = ExceptionUtils.getRootCause(errorEvent.getThrowable());
        if (cause instanceof AccessDeniedException) {
            AccessDeniedException exception = (AccessDeniedException) cause;

            Label label = new Label(exception.getMessage());
            label.setWidth(-1, Unit.PERCENTAGE);

            Link goToMain = ViewUtils.createPageLink("Go to main", "");

            VerticalLayout layout = new VerticalLayout();
            layout.addComponent(label);
            layout.addComponent(goToMain);
            layout.setComponentAlignment(label, Alignment.MIDDLE_CENTER);
            layout.setComponentAlignment(goToMain, Alignment.MIDDLE_CENTER);

            VerticalLayout mainLayout = new VerticalLayout();
            mainLayout.setSizeFull();
            mainLayout.addComponent(layout);
            mainLayout.setComponentAlignment(layout, Alignment.MIDDLE_CENTER);

            setContent(mainLayout);
            Notification.show(exception.getMessage(), Notification.Type.ERROR_MESSAGE);
            return;
        }*/
        DefaultErrorHandler.doDefault(errorEvent);
    }
}