package ru.service99.web.ui;

import ru.service99.domain.Customer;

public interface OnCustomerChoiceListener {
    void onChoice(Customer customer);
}
