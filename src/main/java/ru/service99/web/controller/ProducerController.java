package ru.service99.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.service99.service.ModelService;
import ru.service99.web.model.ProducerShop;

import java.util.List;

@Controller
public class ProducerController {

    @Autowired
    private ModelService modelService;

    @RequestMapping(value = "/api/producers", method = RequestMethod.GET)
    public ResponseEntity<List<ProducerShop>> getAllProducers() {
        List<ProducerShop> producers = ProducerShop.producersToProducersShop(modelService.findAllProducers());
        return new ResponseEntity<>(producers, HttpStatus.OK);
    }
}
