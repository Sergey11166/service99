package ru.service99.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.service99.service.ModelService;
import ru.service99.web.model.TypeDeviceShop;

import java.util.List;

@Controller
public class TypeDevicesController {

    @Autowired
    private ModelService modelService;

    @RequestMapping(value = "/api/typeDevices", method = RequestMethod.GET)
    public ResponseEntity<List<TypeDeviceShop>> getAllTypeDevices() {
        List<TypeDeviceShop> typeDevices = TypeDeviceShop.typeDeviceToTypeDeviceShop(modelService.findAllTypeDevices());
        return new ResponseEntity<>(typeDevices, HttpStatus.OK);
    }
}
