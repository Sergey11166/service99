package ru.service99.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.service99.domain.Model;
import ru.service99.domain.Producer;
import ru.service99.domain.Stock;
import ru.service99.domain.TypeDevice;
import ru.service99.service.PartService;
import ru.service99.service.criteria.PartSearchCriteria;
import ru.service99.web.model.PartShop;

import java.util.List;

@Controller
public class PartsController {

    @Autowired
    private PartService partService;

    @RequestMapping(value = "/api/parts", method = RequestMethod.GET)
    public ResponseEntity<List<PartShop>> getFreeParts(
            @RequestParam long p, // producerId
            @RequestParam long t, //typeDeviceId
            @RequestParam long m, //modelId
            @RequestParam String n //searchName
    ) {
        PartSearchCriteria criteria = new PartSearchCriteria();
        if (p != 0) {
            Producer producer = new Producer();
            producer.setId(p);
            criteria.setProducer(producer);
        }
        if (t != 0) {
            TypeDevice typeDevice = new TypeDevice();
            typeDevice.setId(t);
            criteria.setTypeDevice(typeDevice);
        }
        if (m != 0) {
            Model model = new Model();
            model.setId(m);
            criteria.setModel(model);
        }
        criteria.setName(n);
        Stock stock = new Stock();
        stock.setId(Constant.SHOP_STOCK);
        criteria.setStock(stock);

        PageRequest request = new PageRequest(0, 10000, new Sort("partName"));

        List<PartShop> partsShop = PartShop
                .instancesToPartsShop(partService.findFreeInstancesByCriteria(criteria, request).getContent());

        return new ResponseEntity<>(partsShop, HttpStatus.OK);
    }
}
