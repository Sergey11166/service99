package ru.service99.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.service99.service.RepairOrderService;
import ru.service99.web.model.RepairStatus;

@Controller
public class RepairStatusController {

    @Autowired
    private RepairOrderService orderService;

    @RequestMapping(value = "/api/status/repair", method = RequestMethod.GET)
    public ResponseEntity<RepairStatus> getRepairStatusById(@RequestParam long id) {
        RepairStatus repairStatus = RepairStatus.repairOrderToRepairStatus(orderService.findById(id));
        return new ResponseEntity<>(repairStatus, HttpStatus.OK);
    }
}
