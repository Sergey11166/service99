package ru.service99.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.service99.domain.*;
import ru.service99.service.CustomerService;
import ru.service99.service.FirmService;
import ru.service99.service.PartService;
import ru.service99.service.SalesOrderService;
import ru.service99.service.criteria.PartSearchCriteria;
import ru.service99.web.model.PartShop;
import ru.service99.web.model.SaleOrderShop;
import ru.service99.web.ui.ViewUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
public class SaleOrderController {

    @Autowired
    private SalesOrderService salesOrderService;
    @Autowired
    private PartService partService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private FirmService firmService;

    @RequestMapping(value = "/api/sale", method = RequestMethod.POST)
    public ResponseEntity<SaleOrderShop> newSale(@RequestBody SaleOrderShop sale) {

        validateSaleOrderShop(sale);

        Set<PartInstance> partInstances = new HashSet<>();
        for (PartShop ps : sale.getParts()) {
            PartSearchCriteria criteria = new PartSearchCriteria();
            criteria.setPartId(ps.getPartId());
            criteria.setStock(partService.findStockById(Constant.SHOP_STOCK));
            List<PartInstance> exIns = partService.findFreeInstancesByCriteria(criteria, null).getContent();
            for (int i = 0; i < ps.getCount(); i++) {
                partInstances.add(exIns.get(i));
            }
        }
        DocumentOut documentOut = new DocumentOut();
        documentOut.setPartInstances(partInstances);

        Customer customer = new Customer();
        customer.setName(sale.getCustomerName());
        customer.setPhoneNumber(sale.getCustomerPhone());
        customer.setAddress(sale.getCustomerAddress());
        customer.setComment(Constant.COMMENT_CUSTOMER);

        Customer savedCustomer = customerService.save(customer);

        SalesOrder salesOrder = new SalesOrder();
        salesOrder.setCustomer(savedCustomer);
        salesOrder.setType(TypeSale.Розница);
        salesOrder.setFirm(firmService.findById(Constant.SHOP_FIRM));
        salesOrder.setDocument(documentOut);
        salesOrder.setReceiverComment(sale.getComment());
        SalesOrder savedOrder = salesOrderService.save(salesOrder);

        SaleOrderShop savedOrderShop = new SaleOrderShop();
        savedOrderShop.setId(savedOrder.getId());
        savedOrderShop.setCustomerAddress(savedOrder.getCustomer().getAddress());
        savedOrderShop.setCustomerName(savedOrder.getCustomer().getName());
        savedOrderShop.setCustomerPhone(savedOrder.getCustomer().getPhoneNumber());
        savedOrderShop.setComment(savedOrder.getReceiverComment());
        savedOrderShop.setParts(new HashSet<>(PartShop
                .instancesToPartsShop(savedOrder.getDocument().getPartInstances())));
        return new ResponseEntity<>(savedOrderShop, HttpStatus.CREATED);
    }

    private void validateSaleOrderShop(SaleOrderShop sale) {

        if (sale.getCustomerPhone().isEmpty()) {
            throw new IllegalStateException(ViewUtils.getMessage("api.message.customerPhoneIsEmpty"));
        }
        if (sale.getParts().isEmpty()) {
            throw new IllegalStateException(ViewUtils.getMessage("api.message.partsIsEmpty"));
        }
        for (PartShop p : sale.getParts()) {
            PartSearchCriteria criteria = new PartSearchCriteria();
            criteria.setPartId(p.getPartId());
            criteria.setStock(partService.findStockById(Constant.SHOP_STOCK));
            if (partService.findFreeInstancesByCriteria(criteria, null).getTotalElements() < p.getCount()) {
                throw new IllegalStateException(ViewUtils.getMessage("api.message.notEnough"));
            }
        }
    }
}
