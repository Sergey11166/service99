package ru.service99.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.service99.service.ModelService;
import ru.service99.web.model.ModelShop;

import java.util.List;

@Controller
public class ModelsController {

    @Autowired
    private ModelService modelService;

    @RequestMapping(value = "/api/models", method = RequestMethod.GET)
    public ResponseEntity<List<ModelShop>> getModelsByTypeDeviceAndProducer(
            @RequestParam long p, // producerId
            @RequestParam long t //typeDeviceId
    ) {
        List<ModelShop> models;
        if (p != 0 && t != 0) {
            models = ModelShop.modelsToModelsShop(modelService.findModelByProducerIdAndTypeDeviceId(p, t));
        } else if (p == 0 && t == 0) {
            models = ModelShop.modelsToModelsShop(modelService.findAllModels());
        } else if (p == 0) {
            models = ModelShop.modelsToModelsShop(modelService.findModelByTypeDeviceId(t));
        } else {
            models = ModelShop.modelsToModelsShop(modelService.findModelByProducerId(p));
        }
        return new ResponseEntity<>(models, HttpStatus.OK);
    }
}
