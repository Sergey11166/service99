package ru.service99.web.controller;

public final class Constant {

    public static final Long SHOP_STOCK = 6L;
    public static final Long SHOP_FIRM = 1L;
    public static final String COMMENT_CUSTOMER = "WEB SHOP";
}
