package ru.service99.web;

import com.vaadin.server.*;
import com.vaadin.ui.AbstractComponent;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Logger;

public class AdvancedFileDownloader extends FileDownloader {

    /**
     * http://stackoverflow.com/a/22730778
     */
    private static final long serialVersionUID = 7914516170514586601L;
    private static final boolean DEBUG_MODE = true;

    private static final Logger logger = Logger.getLogger(AdvancedFileDownloader.class.getName());

    private AbstractComponent extendedComponent;

    private AdvancedDownloaderListener dynamicDownloaderListener;
    private DownloaderEvent downloadEvent;

    public abstract class DownloaderEvent {
        public abstract AbstractComponent getExtendedComponent();

        public abstract void setExtendedComponent(
                AbstractComponent extendedComponent);
    }

    public interface AdvancedDownloaderListener {
        void beforeDownload(DownloaderEvent downloadEvent);
    }

    public void fireEvent() {
        if (DEBUG_MODE) {
            logger.info("inside fireEvent");
        }
        if (this.dynamicDownloaderListener != null
                && this.downloadEvent != null) {
            if (DEBUG_MODE) {
                logger.info("beforeDownload is going to be invoked");
            }
            this.dynamicDownloaderListener.beforeDownload(this.downloadEvent);
        }
    }

    public void addAdvancedDownloaderListener(
            AdvancedDownloaderListener listener) {
        if (listener != null) {
            DownloaderEvent downloadEvent = new DownloaderEvent() {
                private AbstractComponent extendedComponent;

                @Override
                public void setExtendedComponent(
                        AbstractComponent extendedComponent) {
                    this.extendedComponent = extendedComponent;
                }

                @Override
                public AbstractComponent getExtendedComponent() {
                    return this.extendedComponent;
                }
            };
            downloadEvent.setExtendedComponent(AdvancedFileDownloader.this.extendedComponent);
            this.dynamicDownloaderListener = listener;
            this.downloadEvent = downloadEvent;

        }
    }

    private static class FileResourceUtil {

        private String filePath;

        private String fileName = "";

        private File file;

        public void setFilePath(String filePath) {
            this.filePath = filePath;
            file = new File(filePath);

            if (file.exists() && !file.isDirectory()) {
                fileName = file.getName();
            }
        }

        @SuppressWarnings("serial")
        public StreamResource getResource() {
            return new StreamResource(() -> {
                if (filePath != null && file != null) {
                    if (file.exists() && !file.isDirectory()) {
                        try {
                            return new FileInputStream(file);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                            return null;
                        }
                    } else {
                        return null;
                    }
                }
                return null;
            }, FileResourceUtil.this.fileName) {
                @Override
                public String getFilename() {
                    return FileResourceUtil.this.fileName;
                }
            };
        }

    }

    private FileResourceUtil resource;

    private AdvancedFileDownloader(FileResourceUtil resource) {
        super(resource == null ? (resource = new FileResourceUtil())
                .getResource() : resource.getResource());

        AdvancedFileDownloader.this.resource = resource;
    }

    public AdvancedFileDownloader() {
        this(null);
    }

    public void setFilePath(String filePath) {
        if (resource != null && filePath != null) {
            this.resource.setFilePath(filePath);
        }
    }

    @Override
    public boolean handleConnectorRequest(VaadinRequest request, VaadinResponse response, String path)
            throws IOException {
        if (!path.matches("dl(/.*)?")) {
            return false;
        }
        VaadinSession session = getSession();
        session.lock();
        AdvancedFileDownloader.this.fireEvent();
        DownloadStream stream;
        try {
            Resource resource = getFileDownloadResource();
            if (!(resource instanceof ConnectorResource)) {
                return false;
            }
            stream = ((ConnectorResource) resource).getStream();
            if (stream.getParameter("Content-Disposition") == null) {
                stream.setParameter("Content-Disposition",
                        "attachment; filename=\"" + stream.getFileName() + "\"");
            }
            if (isOverrideContentType()) {
                stream.setContentType("application/octet-stream;charset=UTF-8");
            }
        } finally {
            session.unlock();
        }
        stream.writeResponse(request, response);
        return true;
    }
}