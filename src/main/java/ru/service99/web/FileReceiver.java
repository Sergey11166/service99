package ru.service99.web;

import com.vaadin.ui.Notification;
import com.vaadin.ui.Upload;
import ru.service99.web.ui.ViewUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

public class FileReceiver implements Upload.Receiver {

    private String path;

    public FileReceiver(String path) {
        this.path = path;
    }

    @Override
    public OutputStream receiveUpload(String filename, String mimeType) {
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(new File(getClass().getClassLoader().getResource(path).getFile()));
        } catch (FileNotFoundException e) {
            StringBuilder builder = new StringBuilder(ViewUtils.getMessage("notifications.errorUpload"));
            if (e.getLocalizedMessage() != null) {
                builder.append("\n");
                builder.append(e.getLocalizedMessage());
            } else {
                for (StackTraceElement element : e.getStackTrace()) {
                    builder.append("\n");
                    builder.append(element);
                }
            }
            Notification.show(builder.toString(), Notification.Type.ERROR_MESSAGE);
            return null;
        }
        return fos;
    }
}
