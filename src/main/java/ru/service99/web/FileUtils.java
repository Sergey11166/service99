package ru.service99.web;

import com.vaadin.ui.Notification;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import ru.service99.domain.*;
import ru.service99.service.PartService;
import ru.service99.service.criteria.PartSearchCriteria;
import ru.service99.web.model.FreePart;
import ru.service99.web.ui.ViewUtils;

import java.io.*;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

public class FileUtils {

    public static void writeRepairsExcel(Class<?> cls, List<RepairOrder> orders) {
        final String[] fieldNames = new String[]{
                ViewUtils.getMessage("table.id"),
                ViewUtils.getMessage("table.startDate"),
                ViewUtils.getMessage("table.endDate"),
                ViewUtils.getMessage("table.typeDevice"),
                ViewUtils.getMessage("table.producer"),
                ViewUtils.getMessage("table.model"),
                ViewUtils.getMessage("table.imei"),
                ViewUtils.getMessage("table.sn"),
                ViewUtils.getMessage("table.receiver"),
                ViewUtils.getMessage("table.master"),
                ViewUtils.getMessage("table.customer"),
                ViewUtils.getMessage("table.placeReceive"),
                ViewUtils.getMessage("table.firm"),
                ViewUtils.getMessage("table.partNumber"),
                ViewUtils.getMessage("table.partName"),
                ViewUtils.getMessage("table.partCost"),
                ViewUtils.getMessage("table.partsCost"),
                ViewUtils.getMessage("table.workCost"),
                ViewUtils.getMessage("table.endCost"),};
        File writeFile = new File(cls.getClassLoader()
                .getResource("excel/" + ViewUtils.getMessage("file.repairs") + ".xls").getFile());
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet();
        Row titleRow = sheet.createRow(0);
        for (int c = 0; c < fieldNames.length; c++) {
            Cell cell = titleRow.createCell(c);
            cell.setCellValue(fieldNames[c]);
        }
        int rowIndex = 1;
        for (RepairOrder order : orders) {
            DocumentOut document = order.getDocument();
            ArrayList<PartInstance> instances = new ArrayList<>();
            if (document != null) instances.addAll(document.getPartInstances());
            Row row = sheet.createRow(rowIndex);
            for (int c = 0; c < fieldNames.length; c++) {
                Cell cell = row.createCell(c);
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
                switch (c) {
                    case 0:
                        cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                        cell.setCellValue(order.getId());
                        break;
                    case 1:
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        cell.setCellValue(sdf.format(order.getStartDate()));
                        break;
                    case 2:
                        Date date = order.getEndDate();
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        if (date != null) cell.setCellValue(sdf.format(date));
                        break;
                    case 3:
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        cell.setCellValue(order.getModel().getTypeDevice().getName());
                        break;
                    case 4:
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        cell.setCellValue(order.getModel().getProducer().getName());
                        break;
                    case 5:
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        cell.setCellValue(order.getModel().getName());
                        break;
                    case 6:
                        String imei = order.getImei();
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        if (imei != null) cell.setCellValue(imei);
                        break;
                    case 7:
                        String sn = order.getSn();
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        if (sn != null) cell.setCellValue(sn);
                        break;
                    case 8:
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        cell.setCellValue(order.getReceiver().getName());
                        break;
                    case 9:
                        User master = order.getMaster();
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        if (master != null) cell.setCellValue(master.getName());
                        break;
                    case 10:
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        cell.setCellValue(order.getCustomer().getName());
                        break;
                    case 11:
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        cell.setCellValue(order.getPlaceReceive().getName());
                        break;
                    case 12:
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        cell.setCellValue(order.getFirm().getName());
                        break;
                    case 13:
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        if (document == null || instances.isEmpty()) break;
                        cell.setCellValue(instances.get(0).getPart().getPartNumber());
                        break;
                    case 14:
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        if (document == null || instances.isEmpty()) break;
                        cell.setCellValue(instances.get(0).getPart().getName());
                        break;
                    case 15:
                        cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                        if (document == null || instances.isEmpty()) break;
                        cell.setCellValue(instances.get(0).getOptPrice());
                        break;
                    case 16:
                        cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                        if (document == null || instances.isEmpty()) break;
                        cell.setCellValue(document.getPartsCostOpt());
                        break;
                    case 17:
                        cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                        Float totalCost = order.getEndCost();
                        if (totalCost == null) totalCost = 0F;
                        if (document == null || instances.isEmpty()) cell.setCellValue(totalCost);
                        else cell.setCellValue(totalCost - document.getPartsCostOpt());
                        break;
                    case 18:
                        cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                        Float endCost = order.getEndCost();
                        if (endCost != null) cell.setCellValue(endCost);
                        else cell.setCellValue(0);
                        break;
                }
            }
            rowIndex++;
            if (instances.size() > 1) {
                for (int p = 1; p < instances.size(); p++) {
                    Row partRow = sheet.createRow(rowIndex);
                    Cell partNumberCell = partRow.createCell(13);
                    Cell partNameCell = partRow.createCell(14);
                    Cell partCostCell = partRow.createCell(15);
                    partNumberCell.setCellType(Cell.CELL_TYPE_STRING);
                    partNameCell.setCellType(Cell.CELL_TYPE_STRING);
                    partCostCell.setCellType(Cell.CELL_TYPE_NUMERIC);
                    partNumberCell.setCellValue(instances.get(p).getPart().getPartNumber());
                    partNameCell.setCellValue(instances.get(p).getPart().getName());
                    partCostCell.setCellValue(instances.get(p).getOptPrice());
                    rowIndex++;
                }
            }
        }
        writeExcelFile(writeFile, workbook);
    }

    public static void writeSalesExcel(Class<?> cls, List<SalesOrder> orders) {
        final String[] fieldNames = new String[]{
                ViewUtils.getMessage("table.id"),
                ViewUtils.getMessage("table.startDate"),
                ViewUtils.getMessage("table.endDate"),
                ViewUtils.getMessage("table.type"),
                ViewUtils.getMessage("table.receiver"),
                ViewUtils.getMessage("table.manager"),
                ViewUtils.getMessage("table.customer"),
                ViewUtils.getMessage("table.placeReceive"),
                ViewUtils.getMessage("table.firm"),
                ViewUtils.getMessage("table.partNumber"),
                ViewUtils.getMessage("table.partName"),
                ViewUtils.getMessage("table.inPrice"),
                ViewUtils.getMessage("table.realPrice"),
                ViewUtils.getMessage("table.partsCostIn"),
                ViewUtils.getMessage("table.partsCostReal")};
        File writeFile = new File(cls.getClassLoader()
                .getResource("excel/" + ViewUtils.getMessage("file.sales") + ".xls").getFile());
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet();
        Row titleRow = sheet.createRow(0);
        for (int c = 0; c < fieldNames.length; c++) {
            Cell cell = titleRow.createCell(c);
            cell.setCellValue(fieldNames[c]);
        }
        int rowIndex = 1;
        for (SalesOrder order : orders) {
            DocumentOut document = order.getDocument();
            ArrayList<PartInstance> instances = new ArrayList<>();
            if (document != null) instances.addAll(document.getPartInstances());
            Row row = sheet.createRow(rowIndex);
            for (int c = 0; c < fieldNames.length; c++) {
                Cell cell = row.createCell(c);
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
                switch (c) {
                    case 0:
                        cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                        cell.setCellValue(order.getId());
                        break;
                    case 1:
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        cell.setCellValue(sdf.format(order.getStartDate()));
                        break;
                    case 2:
                        Date date = order.getEndDate();
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        if (date != null) cell.setCellValue(sdf.format(date));
                        break;
                    case 3:
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        cell.setCellValue(order.getType().toString());
                        break;
                    case 4:
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        cell.setCellValue(order.getReceiver().getName());
                        break;
                    case 5:
                        User manager = order.getManager();
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        if (manager != null) cell.setCellValue(manager.getName());
                        break;
                    case 6:
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        cell.setCellValue(order.getCustomer().getName());
                        break;
                    case 7:
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        cell.setCellValue(order.getPlaceReceive().getName());
                        break;
                    case 8:
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        cell.setCellValue(order.getFirm().getName());
                        break;
                    case 9:
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        if (document == null || instances.isEmpty()) break;
                        cell.setCellValue(instances.get(0).getPart().getPartNumber());
                        break;
                    case 10:
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        if (document == null || instances.isEmpty()) break;
                        cell.setCellValue(instances.get(0).getPart().getName());
                        break;
                    case 11:
                        cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                        if (document == null || instances.isEmpty()) break;
                        cell.setCellValue(instances.get(0).getInPrice());
                        cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                        break;
                    case 12:
                        cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                        if (document == null || instances.isEmpty()) break;
                        switch (order.getType()) {
                            case Розница:
                                cell.setCellValue(instances.get(0).getOutPrice());
                                break;
                            case Опт:
                                cell.setCellValue(instances.get(0).getOptPrice());
                                break;
                            case Вх_цена:
                                cell.setCellValue(instances.get(0).getInPrice());
                                break;
                        }
                        break;
                    case 13:
                        cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                        if (document == null || instances.isEmpty()) break;
                        cell.setCellValue(document.getPartsCostIn());
                        break;
                    case 14:
                        cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                        if (document == null || instances.isEmpty()) break;
                        switch (order.getType()) {
                            case Розница:
                                cell.setCellValue(document.getPartsCostOut());
                                break;
                            case Опт:
                                cell.setCellValue(document.getPartsCostOpt());
                                break;
                            case Вх_цена:
                                cell.setCellValue(document.getPartsCostIn());
                                break;
                        }
                        break;
                }
            }
            rowIndex++;
            if (instances.size() > 1) {
                for (int p = 1; p < instances.size(); p++) {
                    Row partRow = sheet.createRow(rowIndex);
                    Cell partNumberCell = partRow.createCell(9);
                    Cell partNameCell = partRow.createCell(10);
                    Cell partCostInCell = partRow.createCell(11);
                    Cell partCostCell = partRow.createCell(12);
                    partNumberCell.setCellType(Cell.CELL_TYPE_STRING);
                    partNameCell.setCellType(Cell.CELL_TYPE_STRING);
                    partCostCell.setCellType(Cell.CELL_TYPE_NUMERIC);
                    partNumberCell.setCellValue(instances.get(p).getPart().getPartNumber());
                    partNameCell.setCellValue(instances.get(p).getPart().getName());
                    partCostInCell.setCellValue(instances.get(p).getInPrice());
                    switch (order.getType()) {
                        case Розница:
                            partCostCell.setCellValue(instances.get(p).getOutPrice());
                            break;
                        case Опт:
                            partCostCell.setCellValue(instances.get(p).getOptPrice());
                            break;
                        case Вх_цена:
                            partCostCell.setCellValue(instances.get(p).getInPrice());
                            break;
                    }
                    rowIndex++;
                }
            }
        }
        writeExcelFile(writeFile, workbook);
    }

    public static void writeRepairOrderTicketExcel(Class<?> cls, RepairOrder order) {
        if (order == null || cls == null) return;
        ClassLoader classLoader = cls.getClassLoader();
        URL urlRead = classLoader.getResource("excel/" + ViewUtils.getMessage("file.template.repairOrderTicket") + ".xls");
        URL urlWrite = classLoader.getResource("excel/" + ViewUtils.getMessage("file.repairOrderTicket") + ".xls");
        assert urlRead != null;
        assert urlWrite != null;
        File readFile = new File(urlRead.getFile());
        File writeFile = new File(urlWrite.getFile());
        HSSFWorkbook workbook = readExcelFile(readFile);
        HSSFSheet sheet = workbook.getSheetAt(0);
        for (Row row : sheet) {
            Iterator<Cell> cellIterator = row.cellIterator();
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                    String value = cell.getStringCellValue();
                    if (value.startsWith("#")) {
                        replaceTagValueRepairOrder(cell, order);
                    }
                }
            }
        }
        writeExcelFile(writeFile, workbook);
    }

    public static void writeRepairOrderReportExcel(Class<?> cls, RepairOrder order) {
        if (order == null || cls == null) return;
        ClassLoader classLoader = cls.getClassLoader();
        URL urlRead = classLoader.getResource("excel/" + ViewUtils.getMessage("file.template.repairOrderReport") + ".xls");
        URL urlWrite = classLoader.getResource("excel/" + ViewUtils.getMessage("file.repairOrderReport") + ".xls");
        assert urlRead != null;
        assert urlWrite != null;
        File readFile = new File(urlRead.getFile());
        File writeFile = new File(urlWrite.getFile());
        HSSFWorkbook workbook = readExcelFile(readFile);
        HSSFSheet sheet = workbook.getSheetAt(0);
        for (Row row : sheet) {
            Iterator<Cell> cellIterator = row.cellIterator();
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                    String value = cell.getStringCellValue();
                    if (value.startsWith("#")) {
                        replaceTagValueRepairOrder(cell, order);
                    }
                }
            }
        }
        writeExcelFile(writeFile, workbook);
    }

    public static void writeSaleOrderReceiptExcel(Class<?> cls, SalesOrder order) {
        if (order == null || cls == null) return;
        ClassLoader classLoader = cls.getClassLoader();
        URL urlRead = classLoader.getResource("excel/" + ViewUtils.getMessage("file.template.saleOrderReceipt") + ".xls");
        URL urlWrite = classLoader.getResource("excel/" + ViewUtils.getMessage("file.saleOrderReceipt") + ".xls");
        assert urlRead != null;
        assert urlWrite != null;
        File readFile = new File(urlRead.getFile());
        File writeFile = new File(urlWrite.getFile());
        writeSaleOrder(readFile, writeFile, order);
    }

    public static void writeSaleOrderWaybillExcel(Class<?> cls, SalesOrder order) {
        if (order == null || cls == null) return;
        ClassLoader classLoader = cls.getClassLoader();
        URL urlRead = classLoader.getResource("excel/" + ViewUtils.getMessage("file.template.saleOrderWaybill") + ".xls");
        URL urlWrite = classLoader.getResource("excel/" + ViewUtils.getMessage("file.saleOrderWaybill") + ".xls");
        assert urlRead != null;
        assert urlWrite != null;
        File readFile = new File(urlRead.getFile());
        File writeFile = new File(urlWrite.getFile());
        writeSaleOrder(readFile, writeFile, order);
    }

    public static void writeSaleOrderTicketExcel(Class<?> cls, SalesOrder order) {
        if (order == null || cls == null) return;
        ClassLoader classLoader = cls.getClassLoader();
        URL urlRead = classLoader.getResource("excel/" + ViewUtils.getMessage("file.template.saleOrderTicket") + ".xls");
        URL urlWrite = classLoader.getResource("excel/" + ViewUtils.getMessage("file.saleOrderTicket") + ".xls");
        assert urlRead != null;
        assert urlWrite != null;
        File readFile = new File(urlRead.getFile());
        File writeFile = new File(urlWrite.getFile());
        writeSaleOrder(readFile, writeFile, order);
    }

    private static void writeSaleOrder(File readFile, File writeFile, SalesOrder order) {
        HSSFWorkbook workbook = readExcelFile(readFile);
        HSSFSheet sheet = workbook.getSheetAt(0);
        Cell partsCell = null;
        for (Row row : sheet) {
            Iterator<Cell> cellIterator = row.cellIterator();
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                    String value = cell.getStringCellValue();
                    if (value.startsWith("#")) {
                        if (value.equals("#parts")) partsCell = cell;
                        else replaceTagValueSaleOrder(cell, order);
                    }
                }
            }
        }

        List<PartPending> parts = new ArrayList<>();
        if (order.getStatus() == StatusSalesOrder.Выдан || order.getStatus() == StatusSalesOrder.Готов_к_выдаче) {
            if (order.getDocument() != null)
                parts.addAll(PartPending.freePartToPartsPending(FreePart
                        .instancesToFreeParts(new ArrayList<>(order.getDocument().getPartInstances()))));
        } else {
            if (order.getStatus() == StatusSalesOrder.Новый || order.getStatus() == StatusSalesOrder.Ож_детали) {
                if (order.getPartsPending() != null)
                    parts.addAll(new ArrayList<>(new TreeSet<>(order.getPartsPending())));
            }
        }

        if (partsCell != null) {
            int currentRow = partsCell.getRowIndex();
            float totalPartsCost = 0;
            int countLine = 1;
            for (PartPending part : parts) {
                sheet.shiftRows(partsCell.getRowIndex(), sheet.getLastRowNum(), 1, false, true);
                HSSFRow newRow = sheet.createRow(currentRow);

                Cell numRowCell = newRow.createCell(partsCell.getColumnIndex());
                numRowCell.setCellStyle(partsCell.getCellStyle());
                numRowCell.setCellValue(countLine);

                Cell partNumberCell = newRow.createCell(partsCell.getColumnIndex() + 1);
                partNumberCell.setCellStyle(partsCell.getCellStyle());
                partNumberCell.setCellValue(part.getPart().getPartNumber());

                Cell nameCell = newRow.createCell(partsCell.getColumnIndex() + 2);
                nameCell.setCellStyle(partsCell.getCellStyle());
                nameCell.setCellValue(part.getPart().getName());

                Cell countCell = newRow.createCell(partsCell.getColumnIndex() + 3);
                countCell.setCellStyle(partsCell.getCellStyle());
                countCell.setCellValue(part.getNeededCount());

                float price = 0;
                switch (order.getType()) {
                    case Розница:
                        price = part.getPart().getLastOutPrice();
                        break;
                    case Опт:
                        price = part.getPart().getLastOptPrice();
                        break;
                    case Вх_цена:
                        price = part.getPart().getLastInPrice();
                        break;
                }
                totalPartsCost += price;

                Cell oncePriceCell = newRow.createCell(partsCell.getColumnIndex() + 4);
                oncePriceCell.setCellStyle(partsCell.getCellStyle());
                oncePriceCell.setCellValue(price);

                Cell totalPriceCell = newRow.createCell(partsCell.getColumnIndex() + 5);
                totalPriceCell.setCellStyle(partsCell.getCellStyle());
                totalPriceCell.setCellValue(part.getNeededCount() * price);

                currentRow++;
                countLine++;
            }
            HSSFRow newRow = sheet.createRow(currentRow);

            Cell totalCell = newRow.createCell(partsCell.getColumnIndex() + 4);
            totalCell.setCellStyle(partsCell.getCellStyle());
            totalCell.setCellValue(ViewUtils.getMessage("string.total"));

            Cell totalValueCell = newRow.createCell(partsCell.getColumnIndex() + 5);
            totalValueCell.setCellStyle(partsCell.getCellStyle());
            totalValueCell.setCellValue(totalPartsCost);
        }
        writeExcelFile(writeFile, workbook);
    }

    private static void replaceTagValueSaleOrder(Cell cell, SalesOrder order) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        switch (cell.getStringCellValue()) {
            case "#id":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getId());
                break;
            case "#startDate":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(sdf.format(order.getStartDate()));
                break;
            case "#endtDate":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                Date endDate = order.getEndDate();
                if (endDate != null) cell.setCellValue(sdf.format(endDate));
                else cell.setCellValue("");
                break;
            case "#receiver":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getReceiver().getName());
                break;
            case "#placeAddress":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getPlaceReceive().getAddress());
                break;
            case "#placeContact":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getPlaceReceive().getContact());
                break;
            case "#customer":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getCustomer().getName());
                break;
            case "#customerNumber":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getCustomer().getPhoneNumber());
                break;
            case "#customerAddress":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getCustomer().getAddress());
                break;
            case "#firm":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getFirm().getName());
                break;
            case "#INN":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getFirm().getINN());
                break;
            case "#receiverComment":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getReceiverComment());
                break;
            case "#manager":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                User manager = order.getManager();
                if (manager != null) cell.setCellValue(manager.getName());
                else cell.setCellValue("");
                break;
            default:
                cell.setCellValue("#ERROR");
        }
    }

    private static void replaceTagValueRepairOrder(Cell cell, RepairOrder order) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        switch (cell.getStringCellValue()) {
            case "#id":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getId());
                break;
            case "#startDate":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(sdf.format(order.getStartDate()));
                break;
            case "#endtDate":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                Date endDate = order.getEndDate();
                if (endDate != null) cell.setCellValue(sdf.format(endDate));
                else cell.setCellValue("");
                break;
            case "#receiver":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getReceiver().getName());
                break;
            case "#placeAddress":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getPlaceReceive().getAddress());
                break;
            case "#placeContact":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getPlaceReceive().getContact());
                break;
            case "#customer":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getCustomer().getName());
                break;
            case "#customerNumber":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getCustomer().getPhoneNumber());
                break;
            case "#customerAddress":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getCustomer().getAddress());
                break;
            case "#firm":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getFirm().getName());
                break;
            case "#INN":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getFirm().getINN());
                break;
            case "#receiverComment":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getReceiverComment());
                break;
            case "#device":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getModel().getTypeDevice().getName() + " " +
                        order.getModel().getProducer().getName() + " " + order.getModel().getName());
                break;
            case "#typeDevice":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getModel().getTypeDevice().getName());
                break;
            case "#producer":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getModel().getProducer().getName());
                break;
            case "#model":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getModel().getName());
                break;
            case "#master":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                User master = order.getMaster();
                if (master != null) cell.setCellValue(master.getName());
                else cell.setCellValue("");
                break;
            case "#startCost":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                Float startCost = order.getStartCost();
                if (startCost != null) cell.setCellValue(order.getStartCost());
                else cell.setCellValue("");
                break;
            case "#endCost":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                Float endCost = order.getEndCost();
                if (endCost != null) cell.setCellValue(endCost);
                else cell.setCellValue("");
                break;
            case "#visibleDamage":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getVisibleDamage());
                break;
            case "#equipment":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getEquipment());
                break;
            case "#IMEI":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getImei());
                break;
            case "#newIMEI":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getNewImei());
                break;
            case "#SN":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getSn());
                break;
            case "#newSN":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getNewSn());
                break;
            case "#statedDefect":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getStatedDefect());
                break;
            case "#discoveredDefect":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getDiscoveredDefect());
                break;
            case "#worksDone":
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(order.getWorksDone());
                break;
            default:
                cell.setCellValue("#ERROR");
        }
    }


    public static void importParts(Class<?> cls, PartService partService) {
        ClassLoader classLoader = cls.getClassLoader();
        URL url = classLoader.getResource("excel/" + ViewUtils.getMessage("file.tempImportParts") + ".xls");
        assert url != null;
        File file = new File(url.getFile());
        HSSFWorkbook workbook = readExcelFile(file);
        if (workbook == null) return;
        HSSFSheet sheet = workbook.getSheetAt(0);

        //check excel file
        for (Row row : sheet) {

            //keep header
            if (row.getRowNum() == 0) continue;

            //check empty row
            boolean isRowEmpty = true;
            Iterator<Cell> cellIterator = row.cellIterator();
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                cell.setCellType(Cell.CELL_TYPE_STRING);
                if (!cell.getStringCellValue().isEmpty()) isRowEmpty = false;
            }
            if (isRowEmpty) continue;

            Cell partNumberCell = row.getCell(0);
            Cell partNameCell = row.getCell(1);
            Cell inPriceCell = row.getCell(2);
            Cell optPriceCell = row.getCell(3);
            Cell outPriceCell = row.getCell(4);
            Cell commentCell = row.getCell(5);
            String partNumberCellValue = partNumberCell == null ? "" : partNumberCell.getStringCellValue();
            String partNameCellValue = partNameCell == null ? "" : partNameCell.getStringCellValue();
            String inPriceCellValue = inPriceCell == null ? "" : inPriceCell.getStringCellValue();
            String optPriceCellValue = optPriceCell == null ? "" : optPriceCell.getStringCellValue();
            String outPriceCellValue = outPriceCell == null ? "" : outPriceCell.getStringCellValue();
            String commentCellValue = commentCell == null ? "" : commentCell.getStringCellValue();

            //replace empty cellPrice, 0
            if (inPriceCellValue.isEmpty()) {
                inPriceCellValue = "0";
                row.createCell(2).setCellValue("0");
            }
            if (optPriceCellValue.isEmpty()) {
                optPriceCellValue = "0";
                row.createCell(3).setCellValue("0");
            }
            if (outPriceCellValue.isEmpty()) {
                outPriceCellValue = "0";
                row.createCell(4).setCellValue("0");
            }
            if (commentCellValue.isEmpty()) {
                row.createCell(5).setCellValue("");
            }

            //validate cellsValue
            try {
                if (partNumberCellValue.isEmpty()) throw new Exception();
                if (partNameCellValue.isEmpty()) throw new Exception();
                if (Float.parseFloat(inPriceCellValue) < 0) throw new Exception();
                if (Float.parseFloat(optPriceCellValue) < 0) throw new Exception();
                if (Float.parseFloat(outPriceCellValue) < 0) throw new Exception();
            } catch (Exception e) {
                Notification.show(ViewUtils
                                .getMessage("notifications.errorInRowExcel") + " " + (row.getRowNum() + 1),
                        Notification.Type.ERROR_MESSAGE);
                e.printStackTrace();
                return;
            }
        }

        //Notification.show("check success!", Notification.Type.TRAY_NOTIFICATION);

        //insert new parts to DB
        for (Row row : sheet) {
            //skip header
            if (row.getRowNum() == 0) continue;

            //check empty row
            boolean isRowEmpty = true;
            Iterator<Cell> cellIterator = row.cellIterator();
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                if (!cell.getStringCellValue().isEmpty()) isRowEmpty = false;
            }
            if (isRowEmpty) continue;

            String partNumber = row.getCell(0).getStringCellValue();
            String partName = row.getCell(1).getStringCellValue();
            Float inPrice = Float.parseFloat(row.getCell(2).getStringCellValue());
            Float optPrice = Float.parseFloat(row.getCell(3).getStringCellValue());
            Float outPrice = Float.parseFloat(row.getCell(4).getStringCellValue());
            String comment = row.getCell(5).getStringCellValue();

            //check exist parts
            PartSearchCriteria criteria = new PartSearchCriteria();
            criteria.setPartNumber(partNumber);
            List<Part> existParts = partService.findPartsByCriteria(criteria, null).getContent();
            if (!existParts.isEmpty()) {
                //Notification.show(ViewUtils
                //                .getMessage("notifications.partExist") + " " + existParts.get(0).getPartNumber(),
                //        Notification.Type.ERROR_MESSAGE);
                continue;
            }

            Part part = new Part();
            part.setPartNumber(partNumber);
            part.setName(partName);
            part.setLastInPrice(inPrice);
            part.setLastOptPrice(optPrice);
            part.setLastOutPrice(outPrice);
            part.setComment(comment);

            try {
                partService.savePart(part);
            } catch (Exception e) {
                StringBuilder builder = new StringBuilder(ViewUtils.getMessage("notifications.notSaved"));
                if (e.getLocalizedMessage() != null) {
                    builder.append("\n");
                    builder.append(e.getLocalizedMessage());
                } else {
                    for (StackTraceElement element : e.getStackTrace()) {
                        builder.append("\n");
                        builder.append(element);
                    }
                }
                Notification.show(builder.toString(), Notification.Type.ERROR_MESSAGE);
            }
        }
    }

    private static HSSFWorkbook readExcelFile(File readFile) {
        FileInputStream fis = null;
        HSSFWorkbook workbook = null;
        try {
            fis = new FileInputStream(readFile);
            try {
                workbook = new HSSFWorkbook(fis);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return workbook;
    }

    private static void writeExcelFile(File writeFile, HSSFWorkbook workbook) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(writeFile);
            workbook.write(fos);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.flush();
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
