package ru.service99.web.model;

import java.util.Set;

public class SaleOrderShop {

    private Long id;
    private String customerName;
    private String customerPhone;
    private String customerAddress;
    private Set<PartShop> parts;
    private String comment;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public Set<PartShop> getParts() {
        return parts;
    }

    public void setParts(Set<PartShop> parts) {
        this.parts = parts;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
