package ru.service99.web.model;

import ru.service99.domain.RepairOrder;

public class RepairStatus {

    private Long id;
    private String status;
    private String comment;

    public static RepairStatus repairOrderToRepairStatus(RepairOrder repairOrder) {
        RepairStatus result = new RepairStatus();
        result.setId(repairOrder.getId());
        result.setStatus(repairOrder.getStatus().toString());
        result.setComment(repairOrder.getCommentForCustomer() != null ? repairOrder.getCommentForCustomer() : "");
        return result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
