package ru.service99.web.model;

import ru.service99.domain.PartPending;
import ru.service99.domain.RepairOrder;

public class RepairPending implements Comparable<RepairPending> {

    private RepairOrder order;
    private PartPending partPending;
    private Long freeCount;

    public RepairOrder getOrder() {
        return order;
    }

    public void setOrder(RepairOrder order) {
        this.order = order;
    }

    public PartPending getPartPending() {
        return partPending;
    }

    public void setPartPending(PartPending partPending) {
        this.partPending = partPending;
    }

    public Long getFreeCount() {
        return freeCount;
    }

    public void setFreeCount(Long freeCount) {
        this.freeCount = freeCount;
    }

    @Override
    public int compareTo(RepairPending o) {
        return getOrder().getId().compareTo(o.getOrder().getId());
    }
}
