package ru.service99.web.model;

import ru.service99.domain.Model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ModelShop {

    private Long id;
    private String name;

    public static List<ModelShop> modelsToModelsShop(Collection<Model> models) {
        List<ModelShop> result = new ArrayList<>(models.size());
        for (Model m : models) {
            ModelShop ms = new ModelShop();
            ms.setId(m.getId());
            ms.setName(m.getName());
            result.add(ms);
        }
        return result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
