package ru.service99.web.model;

import ru.service99.SecurityUtils;
import ru.service99.domain.Authority;
import ru.service99.domain.Part;
import ru.service99.domain.PartInstance;
import ru.service99.domain.Stock;

import java.util.*;

public class FreePart implements Comparable<FreePart> {
    private Stock stock;
    private Part part;
    private int count;
    private float inPrice;
    private float optPrice;
    private float outPrice;
    private int reserve;

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public Part getPart() {
        return part;
    }

    public void setPart(Part part) {
        this.part = part;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public float getInPrice() {
        return inPrice;
    }

    public void setInPrice(float inPrice) {
        this.inPrice = inPrice;
    }

    public float getOptPrice() {
        return optPrice;
    }

    public void setOptPrice(float optPrice) {
        this.optPrice = optPrice;
    }

    public float getOutPrice() {
        return outPrice;
    }

    public void setOutPrice(float outPrice) {
        this.outPrice = outPrice;
    }

    public int getReserve() {
        return reserve;
    }

    public void setReserve(int reserve) {
        this.reserve = reserve;
    }

    public static List<FreePart> instancesToFreeParts(Collection<PartInstance> instances) {

        long start = System.currentTimeMillis();

        TreeSet<FreePart> result = new TreeSet<>();
        for (PartInstance instance : instances) {
            boolean isContain = false;
            for (FreePart fp : result) {
                if (Objects.equals(instance.getPart().getId(), fp.getPart().getId())
                        && Objects.equals(instance.getStock(), fp.getStock())
                        && (!SecurityUtils.hasAuthority(Authority.видеть_входную_цену) || Objects.equals(instance.getInPrice(), fp.getInPrice()))
                        && Objects.equals(instance.getOptPrice(), fp.getOptPrice())
                        && Objects.equals(instance.getOutPrice(), fp.getOutPrice())) {
                    isContain = true;
                    break;
                }
            }
            if (isContain) continue;

            int count = 0;
            int reserve = 0;
            for (PartInstance inst : instances) {
                if (Objects.equals(instance.getPart().getId(), inst.getPart().getId())
                        && Objects.equals(instance.getStock(), inst.getStock())
                        && Objects.equals(instance.getInPrice(), inst.getInPrice())
                        && Objects.equals(instance.getOptPrice(), inst.getOptPrice())
                        && Objects.equals(instance.getOutPrice(), inst.getOutPrice())) {
                    count++;
                    if (inst.isReserve()) reserve++;
                }
            }
            FreePart freePart = new FreePart();
            freePart.part = instance.getPart();
            freePart.stock = instance.getStock();
            freePart.count = count;
            freePart.inPrice = instance.getInPrice();
            freePart.optPrice = instance.getOptPrice();
            freePart.outPrice = instance.getOutPrice();
            freePart.reserve = reserve;
            result.add(freePart);
        }

        long end = System.currentTimeMillis();
        long res = end - start;
        //Notification.show("res = " + res, Notification.Type.TRAY_NOTIFICATION);

        return new ArrayList<>(result);
    }

    @Override
    public int compareTo(FreePart o) {

        String partName1 = part.getName();
        String partName2 = o.getPart().getName();
        if (!partName1.equals(partName2)) return partName1.compareTo(partName2);

        if (stock == null && o.getStock() != null) return 1;
        if (stock != null && o.getStock() == null) return -1;
        if (stock != null && !Objects.equals(stock.getId(), o.getStock().getId()))
            return getStock().getId().compareTo(o.getStock().getId());

        if (inPrice != o.getInPrice()) return new Float(inPrice).compareTo(o.getInPrice());
        if (optPrice != o.getOptPrice()) return new Float(optPrice).compareTo(o.getOptPrice());
        return new Float(outPrice).compareTo(o.getOutPrice());
    }
}
