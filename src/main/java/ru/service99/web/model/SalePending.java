package ru.service99.web.model;

import ru.service99.domain.PartPending;
import ru.service99.domain.SalesOrder;

public class SalePending implements Comparable<SalePending> {

    private SalesOrder order;
    private PartPending partPending;
    private Long freeCount;

    public SalesOrder getOrder() {
        return order;
    }

    public void setOrder(SalesOrder order) {
        this.order = order;
    }

    public PartPending getPartPending() {
        return partPending;
    }

    public void setPartPending(PartPending partPending) {
        this.partPending = partPending;
    }

    public Long getFreeCount() {
        return freeCount;
    }

    public void setFreeCount(Long freeCount) {
        this.freeCount = freeCount;
    }

    @Override
    public int compareTo(SalePending o) {
        return getOrder().getId().compareTo(o.getOrder().getId());
    }
}
