package ru.service99.web.model;

import ru.service99.domain.Producer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ProducerShop {

    private Long id;
    private String name;

    public static List<ProducerShop> producersToProducersShop(Collection<Producer> producers) {
        List<ProducerShop> result = new ArrayList<>(producers.size());
        for (Producer p : producers) {
            ProducerShop ps = new ProducerShop();
            ps.setId(p.getId());
            ps.setName(p.getName());
            result.add(ps);
        }
        return result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
