package ru.service99.web.model;

import ru.service99.domain.TypeDevice;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TypeDeviceShop {

    private Long id;
    private String name;

    public static List<TypeDeviceShop> typeDeviceToTypeDeviceShop(Collection<TypeDevice> typeDevices) {
        List<TypeDeviceShop> result = new ArrayList<>(typeDevices.size());
        for (TypeDevice td : typeDevices) {
            TypeDeviceShop tds = new TypeDeviceShop();
            tds.setId(td.getId());
            tds.setName(td.getName());
            result.add(tds);
        }
        return result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
