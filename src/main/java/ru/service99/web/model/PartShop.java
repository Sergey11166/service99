package ru.service99.web.model;

import ru.service99.domain.PartInstance;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class PartShop {

    private Long partId;
    private String partNumber;
    private String partName;
    private Integer count;

    public static List<PartShop> instancesToPartsShop(Collection<PartInstance> instances) {
        List<PartShop> result = new ArrayList<>();

        for (PartInstance pi : instances) {
            boolean isContain = false;
            for (PartShop ps : result) {
                if (Objects.equals(pi.getPart().getId(), ps.getPartId())) isContain = true;
            }
            if (isContain) continue;
            int count = 0;
            for (PartInstance i : instances) {
                if (Objects.equals(pi.getPart().getId(), i.getPart().getId())) count++;
            }
            PartShop partShop = new PartShop();
            partShop.setPartId(pi.getPart().getId());
            partShop.setPartNumber(pi.getPart().getPartNumber());
            partShop.setPartName(pi.getPart().getName());
            partShop.setCount(count);
            result.add(partShop);
        }
        return result;
    }

    public Long getPartId() {
        return partId;
    }

    public void setPartId(Long partId) {
        this.partId = partId;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
