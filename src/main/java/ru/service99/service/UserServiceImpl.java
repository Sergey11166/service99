package ru.service99.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.service99.domain.StatusRepairOrder;
import ru.service99.domain.StatusSalesOrder;
import ru.service99.domain.User;
import ru.service99.service.criteria.RepairOrderSearchCriteria;
import ru.service99.service.criteria.SalesOrderSearchCriteria;
import ru.service99.service.repository.UserRepository;
import ru.service99.web.ui.ViewUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RepairOrderService repairOrderService;
    @Autowired
    private SalesOrderService salesOrderService;

    @Override
    public List<User> findAll() {
        return userRepository.findByIsRemovedFalseOrderByNameAsc();
    }

    @Override
    public User findById(long id) {
        return userRepository.findByIdAndIsRemovedFalse(id);
    }

    @Override
    public User findByLogin(String login) {
        return userRepository.findByLoginAndIsRemovedFalse(login);
    }

    @Override
    public User save(User user) {
        if (user.getId() == null) user.setIsRemoved(false);
        if (user.getIsRemoved()) {
            RepairOrderSearchCriteria criteria = new RepairOrderSearchCriteria();
            criteria.setReceiver(user);
            Set<StatusRepairOrder> statuses = new HashSet<>(Arrays.asList(StatusRepairOrder.values()));
            statuses.remove(StatusRepairOrder.Отменён);
            statuses.remove(StatusRepairOrder.Выдан);
            criteria.setStatuses(statuses);
            if (!repairOrderService.findByCriteria(criteria, null).getContent().isEmpty()) {
                throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.errorRemove"));
            }
            criteria.setReceiver(null);
            criteria.setMaster(user);
            if (!repairOrderService.findByCriteria(criteria, null).getContent().isEmpty()) {
                throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.errorRemove"));
            }
            SalesOrderSearchCriteria salesCriteria = new SalesOrderSearchCriteria();
            salesCriteria.setReceiver(user);
            Set<StatusSalesOrder> statusSalesOrders = new HashSet<>(Arrays.asList(StatusSalesOrder.values()));
            statusSalesOrders.remove(StatusSalesOrder.Отменён);
            statusSalesOrders.remove(StatusSalesOrder.Выдан);
            salesCriteria.setStatuses(statusSalesOrders);
            if (!salesOrderService.findByCriteria(salesCriteria, null).getContent().isEmpty()) {
                throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.errorRemove"));
            }
            salesCriteria.setReceiver(null);
            salesCriteria.setManager(user);
            if (!salesOrderService.findByCriteria(salesCriteria, null).getContent().isEmpty()) {
                throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.errorRemove"));
            }
            if (!user.getAllowedStocks().isEmpty()) {
                throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.allowedStocksIsNotEmpty"));
            }
        }
        if (user.getName().isEmpty()) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.userNameIsEmpty"));
        }
        if (user.getLogin().isEmpty()) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.loginIsEmpty"));
        }
        if (user.getPassword().isEmpty()) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.passwordIsEmpty"));
        }
        if (user.getFirm() == null) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.firmIsNull"));
        }
        if (user.getPlace() == null) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.placeIsNull"));
        }
        if (user.getAuthorities().isEmpty()) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.authoritiesIsEmpty"));
        }
        return userRepository.save(user);
    }
}
