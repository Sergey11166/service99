package ru.service99.service;

import ru.service99.domain.Firm;

import java.util.List;

public interface FirmService {

    List<Firm> findAll();

    Firm findById(long id);

    Firm save(Firm firm);
}