package ru.service99.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.service99.domain.Model;
import ru.service99.domain.Producer;
import ru.service99.domain.StatusRepairOrder;
import ru.service99.domain.TypeDevice;
import ru.service99.service.criteria.RepairOrderSearchCriteria;
import ru.service99.service.repository.ModelRepository;
import ru.service99.service.repository.ProducerRepository;
import ru.service99.service.repository.TypeDeviceRepository;
import ru.service99.web.ui.ViewUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class ModelServiceImpl implements ModelService {

    @Autowired
    private ModelRepository modelRepository;
    @Autowired
    private TypeDeviceRepository typeDeviceRepository;
    @Autowired
    private ProducerRepository producerRepository;
    @Autowired
    private RepairOrderService repairOrderService;

    @Override
    public List<Model> findAllModels() {
        return modelRepository.findByIsRemovedFalseOrderByNameAsc();
    }

    @Override
    public List<Producer> findAllProducers() {
        return producerRepository.findByIsRemovedFalseOrderByNameAsc();
    }

    @Override
    public List<TypeDevice> findAllTypeDevices() {
        return typeDeviceRepository.findByIsRemovedFalseOrderByNameAsc();
    }

    @Override
    public Model findModelById(long id) {
        return modelRepository.findByIdAndIsRemovedFalse(id);
    }

    @Override
    public TypeDevice findTypeDeviceById(long id) {
        return typeDeviceRepository.findByIdAndIsRemovedFalse(id);
    }

    @Override
    public Producer findProducerById(long id) {
        return producerRepository.findByIdAndIsRemovedFalse(id);
    }

    @Override
    public List<Model> findModelByProducerId(long prodId) {
        return modelRepository.findByProducerIdAndIsRemovedFalseOrderByNameAsc(prodId);
    }

    @Override
    public List<Model> findModelByTypeDeviceId(long typeId) {
        return modelRepository.findByTypeDeviceIdAndIsRemovedFalseOrderByNameAsc(typeId);
    }

    @Override
    public List<Model> findModelByProducerIdAndTypeDeviceId(long prodId, long typeId) {
        return modelRepository.findByProducerIdAndTypeDeviceIdAndIsRemovedFalseOrderByNameAsc(prodId, typeId);
    }

    @Override
    public Model saveModel(Model model) {
        if (model.getId() == null) model.setIsRemoved(false);
        if (model.getIsRemoved()) {
            RepairOrderSearchCriteria criteria = new RepairOrderSearchCriteria();
            criteria.setModel(model);
            Set<StatusRepairOrder> statuses = new HashSet<>(Arrays.asList(StatusRepairOrder.values()));
            statuses.remove(StatusRepairOrder.Отменён);
            statuses.remove(StatusRepairOrder.Выдан);
            criteria.setStatuses(statuses);
            if (!repairOrderService.findByCriteria(criteria, null).getContent().isEmpty()) {
                throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.errorRemove"));
            }
        }
        if (model.getName().isEmpty()) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.nameIsEmpty"));
        }
        if (model.getTypeDevice() == null) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.typeDeviceIsNull"));
        }
        if (model.getProducer() == null) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.producerIsNull"));
        }
        return modelRepository.save(model);
    }

    @Override
    public TypeDevice saveTypeDevice(TypeDevice typeDevice) {
        if (typeDevice.getId() == null) typeDevice.setIsRemoved(false);
        if (typeDevice.getIsRemoved()) {
            RepairOrderSearchCriteria criteria = new RepairOrderSearchCriteria();
            criteria.setTypeDevice(typeDevice);
            Set<StatusRepairOrder> statuses = new HashSet<>(Arrays.asList(StatusRepairOrder.values()));
            statuses.remove(StatusRepairOrder.Отменён);
            statuses.remove(StatusRepairOrder.Выдан);
            criteria.setStatuses(statuses);
            if (!repairOrderService.findByCriteria(criteria, null).getContent().isEmpty()) {
                throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.errorRemove"));
            }
        }
        if (typeDevice.getName().isEmpty()) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.nameIsEmpty"));
        }
        return typeDeviceRepository.save(typeDevice);
    }

    @Override
    public Producer saveProducer(Producer producer) {
        if (producer.getId() == null) producer.setIsRemoved(false);
        if (producer.getIsRemoved()) {
            RepairOrderSearchCriteria criteria = new RepairOrderSearchCriteria();
            criteria.setProducer(producer);
            Set<StatusRepairOrder> statuses = new HashSet<>(Arrays.asList(StatusRepairOrder.values()));
            statuses.remove(StatusRepairOrder.Отменён);
            statuses.remove(StatusRepairOrder.Выдан);
            criteria.setStatuses(statuses);
            if (!repairOrderService.findByCriteria(criteria, null).getContent().isEmpty()) {
                throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.errorRemove"));
            }
        }
        if (producer.getName().isEmpty()) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.nameIsEmpty"));
        }
        return producerRepository.save(producer);
    }
}
