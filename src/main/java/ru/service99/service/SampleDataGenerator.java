package ru.service99.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.service99.SecurityUtils;
import ru.service99.domain.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class SampleDataGenerator {

    @Autowired
    private PartService partService;
    @Autowired
    private UserService userService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private DocumentService documentService;
    @Autowired
    private FirmService firmService;
    @Autowired
    private PlaceService placeService;
    @Autowired
    private ModelService modelService;
    @Autowired
    private RepairOrderService repairOrderService;
    @Autowired
    private SalesOrderService salesOrderService;

    public void generateSampleData() {
        if (placeService.findAllPlaces().size() < 2) genSamplePlace();
        if (firmService.findAll().size() < 2) genSampleFirms();
        if (partService.findAllStocks().size() < 2) genSampleStock();
        if (userService.findAll().size() < 2) genSampleUsers();
        SecurityUtils.user = userService.findById(2);
        if (customerService.findAll(null).getTotalElements() == 0) genSampleCustomers();
        if (modelService.findAllProducers().size() == 0) genSampleProducers();
        if (modelService.findAllTypeDevices().size() == 0) genSampleTypeDevices();
        if (modelService.findAllModels().size() == 0) genSampleModels();
        if (partService.findPartsByCriteria(null, null).getTotalElements() == 0) genSampleParts();
        if (partService.findFreeInstancesByCriteria(null, null).getTotalElements() == 0) genSamplePartInstances();
        if (repairOrderService.findByCriteria(null, null).getTotalElements() == 0) genSampleRepairsOrders();
        if (salesOrderService.findByCriteria(null, null).getTotalElements() == 0) genSampleSalesOrders();
    }

    private RepairOrder newSampleRepairOrder() {
        RepairOrder result = new RepairOrder();
        result.setModel(modelService.findModelById(1));//iq285
        result.setCustomer(customerService.findCustomerById(1));
        result.setStartCost(1200f);
        result.setVisibleDamage("потерт, царапины");
        result.setImei("01234567889");
        result.setStatedDefect("Разбит дисплей");
        result.setReceiverComment("При приеме не включился, Test");
        return result;
    }

    private SalesOrder newSalesOrder() {
        SalesOrder result = new SalesOrder();
        result.setCustomer(customerService.findCustomerById(1));
        result.setReceiverComment("Test");
        result.setType(TypeSale.Розница);
        return result;
    }

    private void genSampleSalesOrders() {
        SalesOrder order = newSalesOrder();
        salesOrderService.save(order);

        SecurityUtils.user = userService.findById(2);
        order = newSalesOrder();
        order.setManager(userService.findById(2));
        order.setFirm(firmService.findById(3));
        salesOrderService.save(order);
        order = salesOrderService.findById(order.getId());
        order.setStatus(StatusSalesOrder.Готов_к_выдаче);
        salesOrderService.save(order);

        SecurityUtils.user = userService.findById(1);
        order = newSalesOrder();
        order.setManager(userService.findById(3));
        salesOrderService.save(order);
    }

    private void genSampleRepairsOrders() {
        RepairOrder order = newSampleRepairOrder();
        repairOrderService.save(order);

        SecurityUtils.user = userService.findById(2L);
        order = newSampleRepairOrder();
        order.setModel(modelService.findModelById(2));//galaxyTab10
        order.setImei("11111122222");
        repairOrderService.save(order);

        order = newSampleRepairOrder();
        order.setModel(modelService.findModelById(2));//galaxyTab10
        order.setImei("222222222222");
        order.setMaster(userService.findById(2));
        repairOrderService.save(order);

        order = newSampleRepairOrder();
        order.setImei("222222222222");
        order.setModel(modelService.findModelById(3));//valio
        order.setMaster(userService.findById(2));
        repairOrderService.save(order);

        SecurityUtils.user = userService.findById(1L);
        order = newSampleRepairOrder();
        order.setSn("AAAAA12342222");
        order.setModel(modelService.findModelById(3));//valio
        order.setMaster(userService.findById(2));
        repairOrderService.save(order);

        order = newSampleRepairOrder();
        order.setModel(modelService.findModelById(4));//iphone
        order.setImei("333333333333");
        repairOrderService.save(order);

        order = newSampleRepairOrder();
        order.setModel(modelService.findModelById(4));//iphone
        order.setImei("11111111111");
        order.setSn("BBBBBB123456");
        repairOrderService.save(order);

        for (int i = 0; i < 10; i++) {
            order = newSampleRepairOrder();
            order.setModel(modelService.findModelById(4));//iphone
            order.setImei(i + "11111111111");
            order.setSn("BBBBBB1234" + i);
            repairOrderService.save(order);
        }

        order = repairOrderService.findById(order.getId());
        order.setMaster(userService.findById(1));
        order.setStatus(StatusRepairOrder.В_ремонте);
        repairOrderService.save(order);
    }

    private void genSampleFirms() {
        Firm firm1 = new Firm();
        firm1.setName("ИП Иванов А.И.");
        firm1.setINN("111111111");
        firmService.save(firm1);

        Firm firm2 = new Firm();
        firm2.setName("ИП Сидоров М.Ф.");
        firm2.setINN("22222222");
        firmService.save(firm2);

        Firm firm3 = new Firm();
        firm3.setName("ИП Петров Ф.П.");
        firm3.setINN("33333333");
        Firm saved = firmService.save(firm3);
        saved.setIsRemoved(true);
        firmService.save(saved);
    }

    private void genSampleStock() {
        Stock stock2 = new Stock();
        stock2.setName("Иванов");
        partService.saveStock(stock2);

        Stock stock3 = new Stock();
        stock3.setName("Петров");
        partService.saveStock(stock3);

        Stock stock4 = new Stock();
        stock4.setName("Сидоров");
        partService.saveStock(stock4);

        Stock stock5 = new Stock();
        stock5.setName("Ботаника");
        partService.saveStock(stock5);

        Stock stock6 = new Stock();
        stock6.setName("ЖБИ");
        partService.saveStock(stock6);
    }

    private void genSamplePlace() {
        Place place1 = new Place();
        place1.setName("Ботаника");
        place1.setAddress("Екатеринбург, Родонитовая 23");
        place1.setContact("Contact1");
        placeService.savePlace(place1);

        Place place2 = new Place();
        place2.setName("ЖБИ");
        place2.setAddress("Екатеринбург, Сулимова 32");
        place2.setContact("Contact2");
        placeService.savePlace(place2);
    }

    private void genSampleProducers() {
        Producer samsung = new Producer();
        samsung.setName("Samsung");
        modelService.saveProducer(samsung);

        Producer fly = new Producer();
        fly.setName("Fly");
        modelService.saveProducer(fly);

        Producer htc = new Producer();
        htc.setName("Htc");
        modelService.saveProducer(htc);

        Producer sony = new Producer();
        sony.setName("Sony");
        modelService.saveProducer(sony);

        Producer apple = new Producer();
        apple.setName("Apple");
        modelService.saveProducer(apple);
    }

    private void genSampleTypeDevices() {
        TypeDevice smartphone = new TypeDevice();
        smartphone.setName("Смартфон");
        modelService.saveTypeDevice(smartphone);

        TypeDevice notebook = new TypeDevice();
        notebook.setName("Ноутбук");
        modelService.saveTypeDevice(notebook);

        TypeDevice tablet = new TypeDevice();
        tablet.setName("Планшет");
        modelService.saveTypeDevice(tablet);
    }

    private void genSampleModels() {
        Model iq285 = new Model();
        iq285.setName("iq285");
        iq285.setProducer(modelService.findProducerById(2));
        iq285.setTypeDevice(modelService.findTypeDeviceById(1));
        modelService.saveModel(iq285);

        Model galaxyTab10 = new Model();
        galaxyTab10.setName("Galaxy Tab 10");
        galaxyTab10.setProducer(modelService.findProducerById(1));
        galaxyTab10.setTypeDevice(modelService.findTypeDeviceById(3));
        modelService.saveModel(galaxyTab10);

        Model valio = new Model();
        valio.setName("Valio");
        valio.setProducer(modelService.findProducerById(4));
        valio.setTypeDevice(modelService.findTypeDeviceById(2));
        modelService.saveModel(valio);

        Model iphone = new Model();
        iphone.setName("Iphone 3G");
        iphone.setProducer(modelService.findProducerById(5));
        iphone.setTypeDevice(modelService.findTypeDeviceById(1));
        modelService.saveModel(iphone);
    }

    private void genSampleParts() {
        Part part1 = new Part();
        part1.setName("Дисплей Iphone 3G");
        part1.setPartNumber("000001");
        //part1.setInPrice(1000f);
        //part1.setOptPrice(1300f);
        //part1.setOutPrice(1500f);
        Set<Model> models = new HashSet<>();
        models.add(modelService.findModelById(4));
        part1.setModels(models);
        partService.savePart(part1);

        Part part2 = new Part();
        part2.setName("Дисплей Iphone 3GS");
        part2.setPartNumber("000002");
        //part2.setInPrice(1000f);
        //part2.setOptPrice(1300f);
        //part2.setOutPrice(1500f);
        partService.savePart(part2);

        Part part3 = new Part();
        part3.setName("Дисплей Iphone 4");
        part3.setPartNumber("000003");
        //part3.setInPrice(1000f);
        //part3.setOptPrice(1300f);
        //part3.setOutPrice(1500f);
        partService.savePart(part3);

        Part part4 = new Part();
        part4.setName("Дисплей Iphone 4S");
        part4.setPartNumber("000004");
        //part4.setInPrice(1000f);
        //part4.setOptPrice(1300f);
        //part4.setOutPrice(1500f);
        partService.savePart(part4);

        Part part5 = new Part();
        part5.setName("Дисплей Iphone 5");
        part5.setPartNumber("000005");
        //part5.setInPrice(1000f);
        //part5.setOptPrice(1300f);
        //part5.setOutPrice(1500f);
        partService.savePart(part5);

        Part part6 = new Part();
        part6.setName("Дисплей Iphone 5S");
        part6.setPartNumber("000006");
        //part6.setInPrice(1000f);
        //part6.setOptPrice(1300f);
        //part6.setOutPrice(1500f);
        partService.savePart(part6);

        Part part7 = new Part();
        part7.setName("Дисплей Iphone 5C");
        part7.setPartNumber("000007");
        //part7.setInPrice(1000f);
        //part7.setOptPrice(1300f);
        //part7.setOutPrice(1500f);
        partService.savePart(part7);

        Part part8 = new Part();
        part8.setName("Дисплей Iphone 6");
        part8.setPartNumber("000008");
        //part8.setInPrice(1000f);
        //part8.setOptPrice(1300f);
        //part8.setOutPrice(1500f);
        partService.savePart(part8);

        Part part9 = new Part();
        part9.setName("Дисплей Iphone 6+");
        part9.setPartNumber("000009");
        //part9.setInPrice(1000f);
        //part9.setOptPrice(1300f);
        //part9.setOutPrice(1500f);
        partService.savePart(part9);

        Part part10 = new Part();
        part10.setName("Дисплей Iphone 6S");
        part10.setPartNumber("000010");
        //part10.setInPrice(1000f);
        //part10.setOptPrice(1300f);
        //part10.setOutPrice(1500f);
        partService.savePart(part10);
    }

    private void genSampleUsers() {
        Set<Stock> allowedStocks = new HashSet<>();

        User user1 = new User();
        user1.setName("Иванов Павел");
        user1.setLogin("ivanovP");
        user1.setPassword("111");
        user1.setAuthorities(new HashSet<>(Arrays.asList(Authority.values())));
        user1.setPlace(placeService.findPlaceById(2));
        user1.setFirm(firmService.findById(2));
        allowedStocks.clear();
        allowedStocks.add(partService.findStockById(3));
        allowedStocks.add(partService.findStockById(5));
        user1.setAllowedStocks(allowedStocks);
        userService.save(user1);

        User user2 = new User();
        user2.setName("Иванова Марина");
        user2.setLogin("ivanovaM");
        user2.setPassword("222");
        user2.setAuthorities(new HashSet<>(Arrays.asList(Authority.values())));
        user2.setPlace(placeService.findPlaceById(3));
        user2.setFirm(firmService.findById(3));
        allowedStocks.clear();
        allowedStocks.add(partService.findStockById(3));
        user2.setAllowedStocks(allowedStocks);
        userService.save(user2);

        User user3 = new User();
        user3.setName("Сидоров Александр");
        user3.setLogin("sidorovA");
        user3.setPassword("333");
        user3.setAuthorities(new HashSet<>(Arrays.asList(Authority.values())));
        user3.setPlace(placeService.findPlaceById(2));
        user3.setFirm(firmService.findById(2));
        allowedStocks.clear();
        allowedStocks.add(partService.findStockById(4));
        user3.setAllowedStocks(allowedStocks);
        userService.save(user3);
    }

    private void genSampleCustomers() {
        Customer customer1 = new Customer();
        customer1.setName("Иванов Андрей Петрович");
        customer1.setPhoneNumber("+79022456789");
        customer1.setComment("Test");
        customerService.save(customer1);

        Customer customer2 = new Customer();
        customer2.setName("Сидоров Илья Иваныч");
        customer2.setPhoneNumber("+79022456789");
        customer2.setComment("Test");
        customerService.save(customer2);

        for (int i = 0; i < 90; i++) {
            Customer customer = new Customer();
            customer.setName("Воробьёв" + i);
            customer.setPhoneNumber("+79022456789");
            customer.setComment("Test");
            customerService.save(customer);
        }

        for (int i = 0; i < 100; i++) {
            Customer customer = new Customer();
            customer.setName("Воробьёв Сергей" + i);
            customer.setPhoneNumber("+79022456789");
            customer.setComment("Test");
            customerService.save(customer);
        }
    }

    private void genSamplePartInstances() {
        List<Part> parts = partService.findPartsByCriteria(null, null).getContent();
        DocumentIn document = new DocumentIn();
        document.setStockTo(partService.findStockById(5));
        document.setComment("test");
        Set<PartInstance> instances = new HashSet<>();
        for (int i = 1; i < 11; i++) {
            PartInstance instance = new PartInstance();
            instance.setPart(parts.get(i - 1));
            instance.setDocumentIn(document);
            instances.add(instance);
        }
        for (int i = 1; i < 5; i++) {
            PartInstance instance = new PartInstance();
            instance.setPart(parts.get(i - 1));
            instance.setDocumentIn(document);
            instances.add(instance);
        }
        document.setPartInstances(instances);
        long newDocumentId = documentService.saveDocumentIn(document).getId();
        DocumentIn savedDoc = documentService.findDocumentInById(newDocumentId);
        savedDoc.setStatus(StatusDocument.Завершено);
        documentService.saveDocumentIn(savedDoc);
    }
}
