package ru.service99.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.history.Revision;
import org.springframework.data.history.Revisions;
import ru.service99.domain.MyRevisionEntity;
import ru.service99.domain.SalesOrder;
import ru.service99.service.criteria.SalesOrderSearchCriteria;

import java.util.List;

public interface SalesOrderService {

    Page<SalesOrder> findByCriteria(SalesOrderSearchCriteria criteria, Pageable pageable);

    SalesOrder findById(long id);

    Revisions<Integer, SalesOrder> findRevisionsById(long id);

    Revision<Integer, SalesOrder> findLastRevisionById(long id);

    MyRevisionEntity findRevisionEntityByRevision(Revision<Integer, SalesOrder> revision);

    List<MyRevisionEntity> findMyRevisionEntitiesById(long id);

    SalesOrder save(SalesOrder order);
}