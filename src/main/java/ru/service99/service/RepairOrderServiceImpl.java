package ru.service99.service;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.history.Revision;
import org.springframework.data.history.Revisions;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;
import ru.service99.SecurityUtils;
import ru.service99.domain.*;
import ru.service99.service.criteria.RepairOrderSearchCriteria;
import ru.service99.service.repository.MyRevisionEntityRepository;
import ru.service99.service.repository.PartInstanceRepository;
import ru.service99.service.repository.PartPendingRepository;
import ru.service99.service.repository.RepairOrderRepository;
import ru.service99.web.ui.ViewUtils;

import java.util.*;

import static org.springframework.data.jpa.domain.Specifications.where;
import static ru.service99.service.criteria.RepairOrderSpecifications.*;

@Component
public class RepairOrderServiceImpl implements RepairOrderService {

    @Autowired
    private DocumentService documentService;
    @Autowired
    private RepairOrderRepository repairOrderRepository;
    @Autowired
    private MyRevisionEntityRepository myRevisionEntityRepository;
    @Autowired
    private PartInstanceRepository instanceRepository;
    @Autowired
    private PartService partService;
    @Autowired
    private PlatformTransactionManager transactionManager;
    @Autowired
    private PartPendingRepository partPendingRepository;
    private TransactionTemplate transactionTemplate;

    @Override
    public Page<RepairOrder> findByCriteria(RepairOrderSearchCriteria criteria, Pageable pageable) {
        if (criteria == null) return repairOrderRepository.findAll(pageable);
        return repairOrderRepository.findAll(where(idEqual(criteria.getId()))
                .and(firmEqual(criteria.getFirm()))
                .and(masterEqual(criteria.getMaster()))
                .and(receiverEqual(criteria.getReceiver()))
                .and(customerEqual(criteria.getCustomer()))
                .and(modelEqual(criteria.getModel()))
                .and(typeDeviceEqual(criteria.getTypeDevice()))
                .and(producerEqual(criteria.getProducer()))
                .and(startDateBetween(criteria.getStartDate1(), criteria.getStartDate2()))
                .and(endDateBetween(criteria.getEndDate1(), criteria.getEndDate2()))
                .and(statusEqual(criteria.getStatus()))
                .and(statusContainEqual(criteria.getStatuses()))
                .and(placeReceiveEqual(criteria.getPlaceReceive()))
                .and(currentPlaceEqual(criteria.getCurrentPlace()))
                .and(imeiSnIsLike(criteria.getImeiSn())), pageable);
    }

    @Override
    public RepairOrder findById(long id) {
        return repairOrderRepository.findOne(id);
    }

    @Override
    public Revisions<Integer, RepairOrder> findRevisionsById(long id) {
        if (transactionTemplate == null) transactionTemplate = new TransactionTemplate(transactionManager);
        return transactionTemplate.execute(transactionStatus -> {
            Revisions<Integer, RepairOrder> result = repairOrderRepository.findRevisions(id);
            result.getContent().forEach(revision -> {
                Hibernate.initialize(revision.getEntity().getCustomer());
                Hibernate.initialize(revision.getEntity().getReceiver());
                Hibernate.initialize(revision.getEntity().getMaster());
                Hibernate.initialize(revision.getEntity().getModel());
                Hibernate.initialize(revision.getEntity().getFirm());
                Hibernate.initialize(revision.getEntity().getPlaceReceive());
                Hibernate.initialize(revision.getEntity().getCurrentPlace());
                /*Hibernate.initialize(revision.getEntity().getDocument());
                if (revision.getEntity().getDocument() != null){
                    for (PartInstance instance : revision.getEntity().getDocument().getPartInstances()){
                        Hibernate.initialize(instance.getPart());
                    }
                    log.info("revisionSizeInRevisionRepairOrder", revision.getEntity().getDocument().getPartInstances().size());
                }*/
            });
            return result;
        });
    }

    @Override
    public Revision<Integer, RepairOrder> findLastRevisionById(long id) {
        return repairOrderRepository.findLastChangeRevision(id);
    }

    @Override
    public MyRevisionEntity findRevisionEntityByRevision(Revision<Integer, RepairOrder> revision) {
        return myRevisionEntityRepository.findOne(revision.getRevisionNumber());
    }

    @Override
    public List<MyRevisionEntity> findMyRevisionEntitiesById(long id) {
        Revisions<Integer, RepairOrder> revisions = findRevisionsById(id);
        List<MyRevisionEntity> result = new ArrayList<>(revisions.getContent().size());
        for (Revision<Integer, RepairOrder> revision : revisions) {
            result.add(myRevisionEntityRepository.findOne(revision.getRevisionNumber()));
        }
        return result;
    }

    @Override
    public List<RepairOrder> findPreviousRepairs(String imei, String sn) {
        List<RepairOrder> result = new ArrayList<>();
        List<RepairOrder> repairsByNewImeiSn = new ArrayList<>();
        if (imei != null && !imei.isEmpty()) result.addAll(repairOrderRepository.findByImei(imei));
        if (result.isEmpty() && sn != null && !sn.isEmpty()) result.addAll(repairOrderRepository.findBySn(sn));
        if (imei != null && !imei.isEmpty()) repairsByNewImeiSn.addAll(repairOrderRepository.findByNewImei(imei));
        if (repairsByNewImeiSn.isEmpty() && sn != null && !sn.isEmpty())
            repairsByNewImeiSn.addAll(repairOrderRepository.findByNewSn(sn));
        result.addAll(repairsByNewImeiSn);
        return result;
    }

    @Override
    public RepairOrder save(RepairOrder order) {
        if (transactionTemplate == null) transactionTemplate = new TransactionTemplate(transactionManager);
        return transactionTemplate.execute(transactionStatus -> {
            boolean isNewOrder = order.getId() == null;
            RepairOrder existOrder = null;
            if (isNewOrder) {
                order.setStartDate(new Date());
                order.setStatus(StatusRepairOrder.Новый);
                User currentUser = SecurityUtils.getCurrentUser();
                if (order.getFirm() == null) order.setFirm(currentUser.getFirm());
                order.setReceiver(currentUser);
                order.setPlaceReceive(currentUser.getPlace());
                order.setCurrentPlace(currentUser.getPlace());
            } else {
                existOrder = repairOrderRepository.findOne(order.getId());
                if (existOrder.getEndDate() == null &&
                        order.getStatus() == StatusRepairOrder.Выдан) order.setEndDate(new Date());
            }
            order.validate(existOrder);
            Float endCost = order.getEndCost();
            if (endCost != null) {
                if (endCost < 0) {
                    removeInstancesFromPrevLastOrder(order);
                }
            }
            Set<PartPending> partsPending = new HashSet<>(order.getPartsPending());
            for (PartPending pp : partsPending) {
                PartPending existPP;
                if (pp.getId() == null) {
                    pp.setStartDate(new Date());
                    pp.setReceiver(SecurityUtils.getCurrentUser());
                    pp.setType(TypePartPending.ремонт);
                    if (pp.getOrderedCount() != 0) {
                        pp.setOrderedDate(new Date());
                        pp.setManager(SecurityUtils.getCurrentUser());
                    }
                } else {
                    existPP = partPendingRepository.findOne(pp.getId());
                    if (existPP.getOrderedCount() == 0 && pp.getOrderedCount() != 0) {
                        pp.setOrderedDate(new Date());
                        pp.setManager(SecurityUtils.getCurrentUser());
                    }
                }
            }
            if (!isNewOrder) {
                for (PartPending exPP : existOrder.getPartsPending()) {
                    boolean found = false;
                    for (PartPending pp : partsPending) {
                        if (Objects.equals(exPP.getId(), pp.getId())) found = true;
                    }
                    if (!found) {
                        if (exPP.getOrderedCount() != 0) {
                            throw new IllegalStateException(ViewUtils
                                    .getMessage("exception.IllegalState.notAllowedRemoveOrderedPart"));
                        }
                        if (!Objects.equals(exPP.getReceiver().getId(), SecurityUtils.getCurrentUser().getId())) {
                            throw new IllegalStateException(ViewUtils
                                    .getMessage("exception.IllegalState.notAllowedRemoveAlien"));
                        }
                        partPendingRepository.delete(exPP.getId());
                    }
                }
            }
            order.getPartsPending().clear();
            DocumentOut document = order.getDocument();
            if (document != null) {
                Long orderId;
                if (order.getId() != null) orderId = order.getId();
                else orderId = repairOrderRepository.findAll(new PageRequest(0, 1, new Sort(Sort.Direction.DESC, "id")))
                        .getContent().get(0).getId() + 1;
                document.setComment(ViewUtils.getMessage("string.repair") + " " + orderId);
                order.setDocument(documentService.saveDocumentOut(document));
            }
            RepairOrder savedOrder = repairOrderRepository.save(order);
            for (PartPending pp : partsPending) {
                pp.setRepairOrder(savedOrder);
            }
            savedOrder.setPartsPending(new HashSet<>(partPendingRepository.save(partsPending)));
            return savedOrder;
        });
    }

    private void removeInstancesFromPrevLastOrder(RepairOrder order) {
        List<RepairOrder> prevOrders = findPreviousRepairs(order.getImei(), order.getSn());
        if (!prevOrders.isEmpty()) {
            RepairOrder prevLastOrder = prevOrders.get(prevOrders.size() - 1);
            DocumentOut documentOut = prevLastOrder.getDocument();
            if (documentOut != null) {
                Set<PartInstance> instances = new HashSet<>(documentOut.getPartInstances());
                instances.forEach(instance -> {
                    instance.setDocumentOut(null);
                    instance.setStock(partService.findStockById(5));
                    instanceRepository.save(instance);
                });
            }
        }
    }
}
