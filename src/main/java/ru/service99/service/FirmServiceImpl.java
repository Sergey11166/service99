package ru.service99.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.service99.domain.Firm;
import ru.service99.domain.StatusRepairOrder;
import ru.service99.domain.StatusSalesOrder;
import ru.service99.service.criteria.RepairOrderSearchCriteria;
import ru.service99.service.criteria.SalesOrderSearchCriteria;
import ru.service99.service.repository.FirmRepository;
import ru.service99.web.ui.ViewUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class FirmServiceImpl implements FirmService {

    @Autowired
    private FirmRepository firmRepository;
    @Autowired
    private RepairOrderService repairOrderService;
    @Autowired
    private SalesOrderService salesOrderService;

    @Override
    public List<Firm> findAll() {
        return firmRepository.findByIsRemovedFalseOrderByNameAsc();
    }

    @Override
    public Firm findById(long id) {
        return firmRepository.findByIdAndIsRemovedFalse(id);
    }

    @Override
    public Firm save(Firm firm) {
        if (firm.getId() == null) firm.setIsRemoved(false);
        if (firm.getIsRemoved()) {
            RepairOrderSearchCriteria repairsCriteria = new RepairOrderSearchCriteria();
            repairsCriteria.setFirm(firm);
            Set<StatusRepairOrder> statusRepairOrders = new HashSet<>(Arrays.asList(StatusRepairOrder.values()));
            statusRepairOrders.remove(StatusRepairOrder.Отменён);
            statusRepairOrders.remove(StatusRepairOrder.Выдан);
            repairsCriteria.setStatuses(statusRepairOrders);
            if (!repairOrderService.findByCriteria(repairsCriteria, null).getContent().isEmpty()) {
                throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.errorRemove"));
            }
            SalesOrderSearchCriteria salesCriteria = new SalesOrderSearchCriteria();
            salesCriteria.setFirm(firm);
            Set<StatusSalesOrder> statusSalesOrders = new HashSet<>(Arrays.asList(StatusSalesOrder.values()));
            statusSalesOrders.remove(StatusSalesOrder.Отменён);
            statusSalesOrders.remove(StatusSalesOrder.Выдан);
            salesCriteria.setStatuses(statusSalesOrders);
            if (!salesOrderService.findByCriteria(salesCriteria, null).getContent().isEmpty()) {
                throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.errorRemove"));
            }
        }
        if (firm.getName().length() < 1) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.nameIsEmpty"));
        }
        if (firm.getINN().length() < 1) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.INNIsEmpty"));
        }
        return firmRepository.save(firm);
    }
}
