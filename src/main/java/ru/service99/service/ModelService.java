package ru.service99.service;

import ru.service99.domain.Model;
import ru.service99.domain.Producer;
import ru.service99.domain.TypeDevice;

import java.util.List;

public interface ModelService {

    List<Model> findAllModels();

    List<Producer> findAllProducers();

    List<TypeDevice> findAllTypeDevices();

    Model findModelById(long id);

    TypeDevice findTypeDeviceById(long id);

    Producer findProducerById(long id);

    List<Model> findModelByProducerId(long prodId);

    List<Model> findModelByTypeDeviceId(long typeId);

    List<Model> findModelByProducerIdAndTypeDeviceId(long prodId, long typeId);

    Model saveModel(Model model);

    TypeDevice saveTypeDevice(TypeDevice typeDevice);

    Producer saveProducer(Producer producer);
}
