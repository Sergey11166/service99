package ru.service99.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.service99.domain.Place;
import ru.service99.domain.StatusRepairOrder;
import ru.service99.service.criteria.RepairOrderSearchCriteria;
import ru.service99.service.repository.PlaceRepository;
import ru.service99.web.ui.ViewUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class PlaceServiceImpl implements PlaceService {

    @Autowired
    private PlaceRepository placeRepository;
    @Autowired
    private RepairOrderService repairOrderService;

    @Override
    public List<Place> findAllPlaces() {
        return placeRepository.findByIsRemovedFalseOrderByNameAsc();
    }

    @Override
    public Place findPlaceById(long id) {
        return placeRepository.findByIdAndIsRemovedFalse(id);
    }

    @Override
    public Place savePlace(Place place) {
        if (place.getId() == null) place.setIsRemoved(false);
        if (place.getIsRemoved()) {
            RepairOrderSearchCriteria criteria = new RepairOrderSearchCriteria();
            criteria.setCurrentPlace(place);
            Set<StatusRepairOrder> statuses = new HashSet<>(Arrays.asList(StatusRepairOrder.values()));
            statuses.remove(StatusRepairOrder.Отменён);
            statuses.remove(StatusRepairOrder.Выдан);
            criteria.setStatuses(statuses);
            if (!repairOrderService.findByCriteria(criteria, null).getContent().isEmpty()) {
                throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.errorRemove"));
            }
        }
        if (place.getName().isEmpty()) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.nameIsEmpty"));
        }
        if (place.getAddress().isEmpty()) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.addressIsEmpty"));
        }
        return placeRepository.save(place);
    }
}
