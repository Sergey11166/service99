package ru.service99.service;

import ru.service99.domain.Place;

import java.util.List;

public interface PlaceService {

    List<Place> findAllPlaces();

    Place findPlaceById(long id);

    Place savePlace(Place place);
}
