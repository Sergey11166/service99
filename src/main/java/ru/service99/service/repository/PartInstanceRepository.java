package ru.service99.service.repository;

import org.springframework.data.envers.repository.support.EnversRevisionRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import ru.service99.domain.PartInstance;

import java.util.List;

public interface PartInstanceRepository
        extends
        EnversRevisionRepository<PartInstance, Long, Integer>,
        JpaSpecificationExecutor<PartInstance>,
        PagingAndSortingRepository<PartInstance, Long> {

    List<PartInstance> findByDocumentOutId(long id);

    long countByPartId(long id);
}