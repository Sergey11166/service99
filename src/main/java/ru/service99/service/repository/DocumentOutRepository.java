package ru.service99.service.repository;

import org.springframework.data.envers.repository.support.EnversRevisionRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import ru.service99.domain.DocumentOut;

public interface DocumentOutRepository
        extends
        EnversRevisionRepository<DocumentOut, Long, Integer>,
        JpaSpecificationExecutor<DocumentOut>,
        PagingAndSortingRepository<DocumentOut, Long> {
}
