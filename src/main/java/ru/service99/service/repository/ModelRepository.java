package ru.service99.service.repository;

import org.springframework.data.envers.repository.support.EnversRevisionRepository;
import org.springframework.data.repository.CrudRepository;
import ru.service99.domain.Model;

import java.util.List;

public interface ModelRepository
        extends
        EnversRevisionRepository<Model, Long, Integer>,
        CrudRepository<Model, Long> {

    List<Model> findByIsRemovedFalseOrderByNameAsc();

    Model findByIdAndIsRemovedFalse(long id);

    List<Model> findByProducerIdAndIsRemovedFalseOrderByNameAsc(long prodId);

    List<Model> findByTypeDeviceIdAndIsRemovedFalseOrderByNameAsc(long typeId);

    List<Model> findByProducerIdAndTypeDeviceIdAndIsRemovedFalseOrderByNameAsc(long prodId, long typeId);
}
