package ru.service99.service.repository;

import org.springframework.data.envers.repository.support.EnversRevisionRepository;
import org.springframework.data.repository.CrudRepository;
import ru.service99.domain.Firm;

import java.util.List;

public interface FirmRepository
        extends
        EnversRevisionRepository<Firm, Long, Integer>,
        CrudRepository<Firm, Long> {

    List<Firm> findByIsRemovedFalseOrderByNameAsc();

    Firm findByIdAndIsRemovedFalse(long id);
}