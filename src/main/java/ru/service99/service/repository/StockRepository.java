package ru.service99.service.repository;

import org.springframework.data.envers.repository.support.EnversRevisionRepository;
import org.springframework.data.repository.CrudRepository;
import ru.service99.domain.Stock;

import java.util.List;

public interface StockRepository extends
        EnversRevisionRepository<Stock, Long, Integer>,
        CrudRepository<Stock, Long> {

    List<Stock> findByIsRemovedFalseOrderByNameAsc();

    Stock findByIdAndIsRemovedFalse(long id);
}