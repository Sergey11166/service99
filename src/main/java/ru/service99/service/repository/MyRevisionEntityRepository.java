package ru.service99.service.repository;

import org.springframework.data.repository.CrudRepository;
import ru.service99.domain.MyRevisionEntity;

public interface MyRevisionEntityRepository extends CrudRepository<MyRevisionEntity, Integer> {
}
