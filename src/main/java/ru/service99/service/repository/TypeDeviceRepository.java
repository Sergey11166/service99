package ru.service99.service.repository;

import org.springframework.data.envers.repository.support.EnversRevisionRepository;
import org.springframework.data.repository.CrudRepository;
import ru.service99.domain.TypeDevice;

import java.util.List;

public interface TypeDeviceRepository
        extends
        EnversRevisionRepository<TypeDevice, Long, Integer>,
        CrudRepository<TypeDevice, Long> {

    List<TypeDevice> findByIsRemovedFalseOrderByNameAsc();

    TypeDevice findByIdAndIsRemovedFalse(long id);
}