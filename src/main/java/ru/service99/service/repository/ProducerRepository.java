package ru.service99.service.repository;

import org.springframework.data.envers.repository.support.EnversRevisionRepository;
import org.springframework.data.repository.CrudRepository;
import ru.service99.domain.Producer;

import java.util.List;

public interface ProducerRepository
        extends
        EnversRevisionRepository<Producer, Long, Integer>,
        CrudRepository<Producer, Long> {

    List<Producer> findByIsRemovedFalseOrderByNameAsc();

    Producer findByIdAndIsRemovedFalse(long id);
}
