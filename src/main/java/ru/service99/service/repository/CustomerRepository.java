package ru.service99.service.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.envers.repository.support.EnversRevisionRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import ru.service99.domain.Customer;

public interface CustomerRepository
        extends
        EnversRevisionRepository<Customer, Long, Integer>,
        PagingAndSortingRepository<Customer, Long> {

    Page<Customer> findByNameLikeOrderByNameAsc(String name, Pageable pageable);
}