package ru.service99.service.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.envers.repository.support.EnversRevisionRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import ru.service99.domain.Part;

public interface PartRepository
        extends
        EnversRevisionRepository<Part, Long, Integer>,
        JpaSpecificationExecutor<Part>,
        PagingAndSortingRepository<Part, Long> {

    Page<Part> findByIsRemovedFalse(Pageable pageable);
}
