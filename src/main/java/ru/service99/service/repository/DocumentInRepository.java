package ru.service99.service.repository;

import org.springframework.data.envers.repository.support.EnversRevisionRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import ru.service99.domain.DocumentIn;

public interface DocumentInRepository
        extends
        EnversRevisionRepository<DocumentIn, Long, Integer>,
        JpaSpecificationExecutor<DocumentIn>,
        PagingAndSortingRepository<DocumentIn, Long> {
}
