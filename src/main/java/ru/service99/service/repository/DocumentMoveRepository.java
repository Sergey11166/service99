package ru.service99.service.repository;

import org.springframework.data.envers.repository.support.EnversRevisionRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import ru.service99.domain.DocumentMove;

public interface DocumentMoveRepository
        extends
        EnversRevisionRepository<DocumentMove, Long, Integer>,
        JpaSpecificationExecutor<DocumentMove>,
        PagingAndSortingRepository<DocumentMove, Long> {
}