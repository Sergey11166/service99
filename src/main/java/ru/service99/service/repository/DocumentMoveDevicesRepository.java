package ru.service99.service.repository;

import org.springframework.data.envers.repository.support.EnversRevisionRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import ru.service99.domain.DocumentMoveDevices;

public interface DocumentMoveDevicesRepository
        extends
        EnversRevisionRepository<DocumentMoveDevices, Long, Integer>,
        JpaSpecificationExecutor<DocumentMoveDevices>,
        PagingAndSortingRepository<DocumentMoveDevices, Long> {
}