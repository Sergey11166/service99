package ru.service99.service.repository;

import org.springframework.data.envers.repository.support.EnversRevisionRepository;
import org.springframework.data.repository.CrudRepository;
import ru.service99.domain.Place;

import java.util.List;

public interface PlaceRepository extends
        EnversRevisionRepository<Place, Long, Integer>,
        CrudRepository<Place, Long> {

    List<Place> findByIsRemovedFalseOrderByNameAsc();

    Place findByIdAndIsRemovedFalse(long id);
}
