package ru.service99.service.repository;

import org.springframework.data.envers.repository.support.EnversRevisionRepository;
import org.springframework.data.repository.CrudRepository;
import ru.service99.domain.User;

import java.util.List;

public interface UserRepository
        extends
        EnversRevisionRepository<User, Long, Integer>,
        CrudRepository<User, Long> {

    List<User> findByIsRemovedFalseOrderByNameAsc();

    User findByIdAndIsRemovedFalse(long id);

    User findByLoginAndIsRemovedFalse(String login);
}
