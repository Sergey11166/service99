package ru.service99.service.repository;

import org.springframework.data.envers.repository.support.EnversRevisionRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import ru.service99.domain.RepairOrder;

import java.util.List;

public interface RepairOrderRepository
        extends
        EnversRevisionRepository<RepairOrder, Long, Integer>,
        JpaSpecificationExecutor<RepairOrder>,
        PagingAndSortingRepository<RepairOrder, Long> {

    List<RepairOrder> findByImei(String imei);

    List<RepairOrder> findBySn(String sn);

    List<RepairOrder> findByNewImei(String newImei);

    List<RepairOrder> findByNewSn(String newSn);

    List<RepairOrder> findByDocumentId(long id);
}
