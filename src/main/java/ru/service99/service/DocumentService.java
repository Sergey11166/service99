package ru.service99.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.history.Revision;
import ru.service99.domain.DocumentIn;
import ru.service99.domain.DocumentMove;
import ru.service99.domain.DocumentMoveDevices;
import ru.service99.domain.DocumentOut;
import ru.service99.service.criteria.DocumentSearchCriteria;

public interface DocumentService {

    DocumentIn findDocumentInById(long id);

    DocumentOut findDocumentOutById(long id);

    DocumentMove findDocumentMoveById(long id);

    DocumentMoveDevices findDocumentMoveDevicesById(long id);

    Page<DocumentIn> findDocumentInByCriteria(DocumentSearchCriteria criteria, Pageable pageable);

    Page<DocumentOut> findDocumentOutByCriteria(DocumentSearchCriteria criteria, Pageable pageable);

    Page<DocumentMove> findDocumentMoveByCriteria(DocumentSearchCriteria criteria, Pageable pageable);

    Page<DocumentMoveDevices> findDocumentMoveDevicesByCriteria(DocumentSearchCriteria criteria, Pageable pageable);

    DocumentIn saveDocumentIn(DocumentIn document);

    DocumentOut saveDocumentOut(DocumentOut document);

    DocumentMove saveDocumentMove(DocumentMove document);

    DocumentMoveDevices saveDocumentMoveDevices(DocumentMoveDevices document);

    Revision<Integer, DocumentIn> findLastRevisionDocumentInById(long id);

    Revision<Integer, DocumentOut> findLastRevisionDocumentOutById(long id);

    Revision<Integer, DocumentMove> findLastRevisionDocumentMoveById(long id);

    void deleteDocumentMove(DocumentMove document);
}