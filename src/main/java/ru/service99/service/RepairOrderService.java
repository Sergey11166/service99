package ru.service99.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.history.Revision;
import org.springframework.data.history.Revisions;
import ru.service99.domain.MyRevisionEntity;
import ru.service99.domain.RepairOrder;
import ru.service99.service.criteria.RepairOrderSearchCriteria;

import java.util.List;

public interface RepairOrderService {

    Page<RepairOrder> findByCriteria(RepairOrderSearchCriteria criteria, Pageable pageable);

    RepairOrder findById(long id);

    Revisions<Integer, RepairOrder> findRevisionsById(long id);

    Revision<Integer, RepairOrder> findLastRevisionById(long id);

    MyRevisionEntity findRevisionEntityByRevision(Revision<Integer, RepairOrder> revision);

    List<MyRevisionEntity> findMyRevisionEntitiesById(long id);

    List<RepairOrder> findPreviousRepairs(String imei, String sn);

    RepairOrder save(RepairOrder order);
}
