package ru.service99.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.service99.domain.Part;
import ru.service99.domain.PartInstance;
import ru.service99.domain.PartPending;
import ru.service99.domain.Stock;
import ru.service99.service.criteria.PartPendingSearchCriteria;
import ru.service99.service.criteria.PartSearchCriteria;

import java.util.List;

public interface PartService {

    Page<Part> findPartsByCriteria(PartSearchCriteria criteria, Pageable pageable);

    Page<PartInstance> findFreeInstancesByCriteria(PartSearchCriteria criteria, Pageable pageable);

    long getCountFreeInstancesByCriteria(PartSearchCriteria criteria);

    Part savePart(Part part);

    List<Stock> findAllStocks();

    Stock findStockById(long id);

    Stock saveStock(Stock stock);

    PartPending findPartPendingById(long id);

    Page<PartPending> findPartsPendingByCriteria(PartPendingSearchCriteria criteria, Pageable pageable);

    PartPending savePartPending(PartPending partPending);

    void removePartPending(PartPending partPending);
}
