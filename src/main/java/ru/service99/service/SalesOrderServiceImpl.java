package ru.service99.service;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.history.Revision;
import org.springframework.data.history.Revisions;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;
import ru.service99.SecurityUtils;
import ru.service99.domain.*;
import ru.service99.service.criteria.SalesOrderSearchCriteria;
import ru.service99.service.repository.MyRevisionEntityRepository;
import ru.service99.service.repository.PartPendingRepository;
import ru.service99.service.repository.SalesOrderRepository;
import ru.service99.web.ui.ViewUtils;

import java.util.*;

import static org.springframework.data.jpa.domain.Specifications.where;
import static ru.service99.service.criteria.SalesOrderSpecifications.*;

@Component()
public class SalesOrderServiceImpl implements SalesOrderService {

    @Autowired
    private SalesOrderRepository salesOrderRepository;
    @Autowired
    private MyRevisionEntityRepository myRevisionEntityRepository;
    @Autowired
    private DocumentService documentService;
    @Autowired
    private PlatformTransactionManager transactionManager;
    @Autowired
    private PartPendingRepository partPendingRepository;

    private TransactionTemplate transactionTemplate;

    @Override
    public Page<SalesOrder> findByCriteria(SalesOrderSearchCriteria criteria, Pageable pageable) {
        if (criteria == null) return salesOrderRepository.findAll(pageable);
        return salesOrderRepository.findAll(where(idEqual(criteria.getId()))
                        .and(startDateBetween(criteria.getStartDate1(), criteria.getStartDate2()))
                        .and(endDateBetween(criteria.getEndDate1(), criteria.getEndDate2()))
                        .and(firmEqual(criteria.getFirm()))
                        .and(customerEqual(criteria.getCustomer()))
                        .and(receiverEqual(criteria.getReceiver()))
                        .and(managerEqual(criteria.getManager()))
                        .and(placeReceiveEqual(criteria.getPlaceReceive()))
                        .and(statusEqual(criteria.getStatus()))
                        .and(typeEqual(criteria.getType()))
                        .and(statusContainEqual(criteria.getStatuses()))
                , pageable);
    }

    @Override
    public SalesOrder findById(long id) {
        return salesOrderRepository.findOne(id);
    }

    @Override
    public Revisions<Integer, SalesOrder> findRevisionsById(long id) {
        if (transactionTemplate == null) transactionTemplate = new TransactionTemplate(transactionManager);
        return transactionTemplate.execute(transactionStatus -> {
            Revisions<Integer, SalesOrder> result = salesOrderRepository.findRevisions(id);
            result.getContent().forEach(revision -> {
                Hibernate.initialize(revision.getEntity().getCustomer());
                Hibernate.initialize(revision.getEntity().getManager());
                Hibernate.initialize(revision.getEntity().getReceiver());
                Hibernate.initialize(revision.getEntity().getFirm());
            });
            return result;
        });
    }

    @Override
    public Revision<Integer, SalesOrder> findLastRevisionById(long id) {
        return salesOrderRepository.findLastChangeRevision(id);
    }

    @Override
    public MyRevisionEntity findRevisionEntityByRevision(Revision<Integer, SalesOrder> revision) {
        return myRevisionEntityRepository.findOne(revision.getRevisionNumber());
    }

    @Override
    public List<MyRevisionEntity> findMyRevisionEntitiesById(long id) {
        Revisions<Integer, SalesOrder> revisions = findRevisionsById(id);
        List<MyRevisionEntity> result = new ArrayList<>(revisions.getContent().size());
        for (Revision<Integer, SalesOrder> revision : revisions) {
            result.add(myRevisionEntityRepository.findOne(revision.getRevisionNumber()));
        }
        return result;
    }

    @Override
    public SalesOrder save(SalesOrder order) {
        if (transactionTemplate == null) transactionTemplate = new TransactionTemplate(transactionManager);
        return transactionTemplate.execute(transactionStatus -> {
            boolean isNewOrder = order.getId() == null;
            SalesOrder existOrder = null;
            if (isNewOrder) {
                order.setStartDate(new Date());
                order.setStatus(StatusSalesOrder.Новый);
                User currentUser = SecurityUtils.getCurrentUser();
                if (order.getFirm() == null) order.setFirm(currentUser.getFirm());
                order.setReceiver(currentUser);
                order.setPlaceReceive(currentUser.getPlace());
            } else {
                existOrder = salesOrderRepository.findOne(order.getId());
                if (existOrder.getEndDate() == null &&
                        order.getStatus() == StatusSalesOrder.Выдан) order.setEndDate(new Date());
            }
            order.validate(existOrder);
            Set<PartPending> partsPending = new HashSet<>(order.getPartsPending());
            for (PartPending pp : partsPending) {
                PartPending existPP;
                if (pp.getId() == null) {
                    pp.setStartDate(new Date());
                    pp.setReceiver(SecurityUtils.getCurrentUser());
                    pp.setType(TypePartPending.продажа);
                    if (pp.getOrderedCount() != 0) {
                        pp.setOrderedDate(new Date());
                        pp.setManager(SecurityUtils.getCurrentUser());
                    }
                } else {
                    existPP = partPendingRepository.findOne(pp.getId());
                    if (existPP.getOrderedCount() == 0 && pp.getOrderedCount() != 0) {
                        pp.setOrderedDate(new Date());
                        pp.setManager(SecurityUtils.getCurrentUser());
                    }
                }
            }
            if (!isNewOrder) {
                for (PartPending exPP : existOrder.getPartsPending()) {
                    boolean found = false;
                    for (PartPending pp : partsPending) {
                        if (Objects.equals(exPP.getId(), pp.getId())) found = true;
                    }
                    if (!found) {
                        if (exPP.getOrderedCount() != 0) {
                            throw new IllegalStateException(ViewUtils
                                    .getMessage("exception.IllegalState.notAllowedRemoveOrderedPart"));
                        }
                        if (!Objects.equals(exPP.getReceiver().getId(), SecurityUtils.getCurrentUser().getId())) {
                            throw new IllegalStateException(ViewUtils
                                    .getMessage("exception.IllegalState.notAllowedRemoveAlien"));
                        }
                        partPendingRepository.delete(exPP.getId());
                    }
                }
            }
            order.getPartsPending().clear();
            DocumentOut document = order.getDocument();
            if (document != null) {
                Long orderId = 0L;
                if (order.getId() != null) orderId = order.getId();
                else {
                    List<SalesOrder> content = salesOrderRepository.findAll(new PageRequest(0, 1,
                            new Sort(Sort.Direction.DESC, "id"))).getContent();
                    if (content.isEmpty()) orderId = 1L;
                    else orderId = content.get(0).getId() + 1;
                }
                document.setComment(ViewUtils.getMessage("string.sale") + " " + orderId);
                order.setDocument(documentService.saveDocumentOut(document));
            }
            SalesOrder savedOrder = salesOrderRepository.save(order);
            for (PartPending pp : partsPending) {
                pp.setSaleOrder(savedOrder);
            }
            savedOrder.setPartsPending(new HashSet<>(partPendingRepository.save(partsPending)));
            return savedOrder;
        });
    }
}
