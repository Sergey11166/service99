package ru.service99.service.criteria;

import org.springframework.data.jpa.domain.Specification;
import ru.service99.domain.*;

import java.util.Date;

public class DocumentMoveSpecifications {

    public static Specification<DocumentMove> authorEqual(final User author) {
        return (root, query, cb) -> {
            if (author != null) return cb.equal(root.get(AbstractDocument_.author), author);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<DocumentMove> statusEqual(final StatusDocument status) {
        return (root, query, cb) -> {
            if (status != null) return cb.equal(root.get(AbstractDocument_.status), status);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<DocumentMove> dateBetween(Date date1, final Date date2) {
        return (root, query, cb) -> {
            if (date1 == null && date2 == null) return cb.isNotNull(root.get(AbstractEntity_.id));
            if (date1 == null) return cb.lessThan(root.get(AbstractDocument_.date), date2);
            if (date2 == null) return cb.greaterThan(root.get(AbstractDocument_.date), date1);
            return cb.between(root.get(AbstractDocument_.date), date1, date2);
        };
    }

    public static Specification<DocumentMove> stockFromEqual(final Stock stock) {
        return (root, query, cb) -> {
            if (stock != null) return cb.equal(root.get(DocumentMove_.stockFrom), stock);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<DocumentMove> stockToEqual(final Stock stock) {
        return (root, query, cb) -> {
            if (stock != null) return cb.equal(root.get(DocumentMove_.stockTo), stock);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }
}
