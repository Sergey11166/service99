package ru.service99.service.criteria;


import org.springframework.data.jpa.domain.Specification;
import ru.service99.domain.*;

import java.util.Date;
import java.util.Set;

public class SalesOrderSpecifications {

    public static Specification<SalesOrder> idEqual(final Long id) {
        return (root, query, cb) -> {
            if (id != null) return cb.equal(root.get(AbstractEntity_.id), id);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<SalesOrder> customerEqual(final Customer customer) {
        return (root, query, cb) -> {
            if (customer != null) return cb.equal(root.get(AbstractOrder_.customer), customer);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<SalesOrder> receiverEqual(final User receiver) {
        return (root, query, cb) -> {
            if (receiver != null) return cb.equal(root.get(AbstractOrder_.receiver), receiver);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<SalesOrder> managerEqual(final User manager) {
        return (root, query, cb) -> {
            if (manager != null) return cb.equal(root.get(SalesOrder_.manager), manager);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<SalesOrder> firmEqual(final Firm firm) {
        return (root, query, cb) -> {
            if (firm != null) return cb.equal(root.get(AbstractOrder_.firm), firm);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<SalesOrder> startDateBetween(Date startDate1, final Date startDate2) {
        return (root, query, cb) -> {
            if (startDate1 == null && startDate2 == null) return cb.isNotNull(root.get(AbstractEntity_.id));
            if (startDate1 == null) return cb.lessThan(root.get(AbstractOrder_.startDate), startDate2);
            if (startDate2 == null) return cb.greaterThan(root.get(AbstractOrder_.startDate), startDate1);
            return cb.between(root.get(AbstractOrder_.startDate), startDate1, startDate2);
        };
    }

    public static Specification<SalesOrder> endDateBetween(Date endDate1, final Date endDate2) {
        return (root, query, cb) -> {
            if (endDate1 == null && endDate2 == null) return cb.isNotNull(root.get(AbstractEntity_.id));
            if (endDate1 == null) return cb.lessThan(root.get(AbstractOrder_.endDate), endDate2);
            if (endDate2 == null) return cb.greaterThan(root.get(AbstractOrder_.endDate), endDate1);
            return cb.between(root.get(AbstractOrder_.endDate), endDate1, endDate2);
        };
    }

    public static Specification<SalesOrder> statusEqual(final StatusSalesOrder status) {
        return (root, query, cb) -> {
            if (status != null) return cb.equal(root.get(SalesOrder_.status), status);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<SalesOrder> statusContainEqual(final Set<StatusSalesOrder> statuses) {
        return (root, query, cb) -> {
            if (statuses != null) {
                if (!statuses.isEmpty()) return cb.isTrue(root.get(SalesOrder_.status).in(statuses));
            }
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<SalesOrder> typeEqual(final TypeSale type) {
        return (root, query, cb) -> {
            if (type != null) return cb.equal(root.get(SalesOrder_.type), type);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<SalesOrder> placeReceiveEqual(final Place place) {
        return (root, query, cb) -> {
            if (place != null) return cb.equal(root.get(AbstractOrder_.placeReceive), place);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }
}
