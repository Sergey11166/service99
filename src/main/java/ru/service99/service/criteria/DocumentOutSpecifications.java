package ru.service99.service.criteria;

import org.springframework.data.jpa.domain.Specification;
import ru.service99.domain.*;

import java.util.Date;

public class DocumentOutSpecifications {

    public static Specification<DocumentOut> authorEqual(final User author) {
        return (root, query, cb) -> {
            if (author != null) return cb.equal(root.get(AbstractDocument_.author), author);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<DocumentOut> statusEqual(final StatusDocument status) {
        return (root, query, cb) -> {
            if (status != null) return cb.equal(root.get(AbstractDocument_.status), status);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<DocumentOut> dateBetween(Date date1, final Date date2) {
        return (root, query, cb) -> {
            if (date1 == null && date2 == null) return cb.isNotNull(root.get(AbstractEntity_.id));
            if (date1 == null) return cb.lessThan(root.get(AbstractDocument_.date), date2);
            if (date2 == null) return cb.greaterThan(root.get(AbstractDocument_.date), date1);
            return cb.between(root.get(AbstractDocument_.date), date1, date2);
        };
    }
}
