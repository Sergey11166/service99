package ru.service99.service.criteria;

import ru.service99.domain.Model;
import ru.service99.domain.Producer;
import ru.service99.domain.Stock;
import ru.service99.domain.TypeDevice;

public class PartSearchCriteria {

    private Long partId;
    private Long partInstanceId;
    private String name;
    private String partNumber;
    private Float inPrice;
    private Float optPrice;
    private Float outPrice;
    private Stock stock;
    private Model model;
    private Producer producer;
    private TypeDevice typeDevice;

    public Long getPartId() {
        return partId;
    }

    public void setPartId(Long partId) {
        this.partId = partId;
    }

    public Long getPartInstanceId() {
        return partInstanceId;
    }

    public void setPartInstanceId(Long partInstanceId) {
        this.partInstanceId = partInstanceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public Float getInPrice() {
        return inPrice;
    }

    public void setInPrice(Float inPrice) {
        this.inPrice = inPrice;
    }

    public Float getOptPrice() {
        return optPrice;
    }

    public void setOptPrice(Float optPrice) {
        this.optPrice = optPrice;
    }

    public Float getOutPrice() {
        return outPrice;
    }

    public void setOutPrice(Float outPrice) {
        this.outPrice = outPrice;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }

    public TypeDevice getTypeDevice() {
        return typeDevice;
    }

    public void setTypeDevice(TypeDevice typeDevice) {
        this.typeDevice = typeDevice;
    }
}
