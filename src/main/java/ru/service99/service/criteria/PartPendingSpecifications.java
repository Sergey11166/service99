package ru.service99.service.criteria;

import org.springframework.data.jpa.domain.Specification;
import ru.service99.domain.*;

import javax.persistence.criteria.Join;
import java.util.Date;

public class PartPendingSpecifications {

    public static Specification<PartPending> idEqual(final Long id) {
        return (root, query, cb) -> {
            if (id != null) return cb.equal(root.get(AbstractEntity_.id), id);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<PartPending> partIdEqual(final Long id) {
        return (root, query, cb) -> {
            if (id != null) {
                Join<PartPending, Part> joinPart = root.join(PartPending_.part);
                return cb.equal(joinPart.get(AbstractEntity_.id), id);
            }
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<PartPending> partNumberEqual(final String partNumber) {
        return (root, query, cb) -> {
            if (partNumber != null && partNumber.length() > 0) {
                Join<PartPending, Part> joinPart = root.join(PartPending_.part);
                return cb.equal(joinPart.get(Part_.partNumber), partNumber);
            } else {
                return cb.isNotNull(root.get(AbstractEntity_.id));
            }
        };
    }

    public static Specification<PartPending> producerEqual(final Producer producer) {
        return (root, query, cb) -> {
            if (producer != null) {
                Join<PartPending, Part> joinPart = root.join(PartPending_.part);
                Join<Part, Model> joinModel = joinPart.join(Part_.models);
                return cb.equal(joinModel.get(Model_.producer), producer);
            }
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<PartPending> typeDeviceEqual(final TypeDevice typeDevice) {
        return (root, query, cb) -> {
            if (typeDevice != null) {
                Join<PartPending, Part> joinPart = root.join(PartPending_.part);
                Join<Part, Model> joinModel = joinPart.join(Part_.models);
                return cb.equal(joinModel.get(Model_.typeDevice), typeDevice);
            }
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<PartPending> modelsContain(final Model model) {
        return (root, query, cb) -> {
            if (model != null) {
                Join<PartPending, Part> joinPart = root.join(PartPending_.part);
                return cb.isMember(model, joinPart.get(Part_.models));
            } else {
                return cb.isNotNull(root.get(AbstractEntity_.id));
            }
        };
    }

    public static Specification<PartPending> typeEqual(final TypePartPending type) {
        return (root, query, cb) -> {
            if (type != null) {
                return cb.equal(root.get(PartPending_.type), type);
            } else {
                return cb.isNotNull(root.get(AbstractEntity_.id));
            }
        };
    }

    public static Specification<PartPending> receiverEqual(final User receiver) {
        return (root, query, cb) -> {
            if (receiver != null) {
                return cb.equal(root.get(PartPending_.receiver), receiver);
            } else {
                return cb.isNotNull(root.get(AbstractEntity_.id));
            }
        };
    }

    public static Specification<PartPending> managerEqual(final User manager) {
        return (root, query, cb) -> {
            if (manager != null) {
                return cb.equal(root.get(PartPending_.manager), manager);
            } else {
                return cb.isNotNull(root.get(AbstractEntity_.id));
            }
        };
    }

    public static Specification<PartPending> startDateBetween(Date startDate1, final Date startDate2) {
        return (root, query, cb) -> {
            if (startDate1 == null && startDate2 == null) return cb.isNotNull(root.get(AbstractEntity_.id));
            if (startDate1 == null) return cb.lessThan(root.get(PartPending_.startDate), startDate2);
            if (startDate2 == null) return cb.greaterThan(root.get(PartPending_.startDate), startDate1);
            return cb.between(root.get(PartPending_.startDate), startDate1, startDate2);
        };
    }

    public static Specification<PartPending> orderedDateBetween(Date orderedDate1, final Date orderedDate2) {
        return (root, query, cb) -> {
            if (orderedDate1 == null && orderedDate2 == null) return cb.isNotNull(root.get(AbstractEntity_.id));
            if (orderedDate1 == null) return cb.lessThan(root.get(PartPending_.orderedDate), orderedDate2);
            if (orderedDate2 == null) return cb.greaterThan(root.get(PartPending_.orderedDate), orderedDate1);
            return cb.between(root.get(PartPending_.orderedDate), orderedDate1, orderedDate2);
        };
    }
}
