package ru.service99.service.criteria;

import ru.service99.domain.*;

import java.util.Date;
import java.util.Set;

public class RepairOrderSearchCriteria {

    private Long id;
    private Firm firm;
    private User master;
    private User receiver;
    private Customer customer;
    private Model model;
    private Producer producer;
    private TypeDevice typeDevice;
    private Date startDate1;
    private Date startDate2;
    private Date endDate1;
    private Date endDate2;
    private StatusRepairOrder status;
    private Set<StatusRepairOrder> statuses;
    private Place placeReceive;
    private Place currentPlace;
    private String imeiSn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Firm getFirm() {
        return firm;
    }

    public void setFirm(Firm firm) {
        this.firm = firm;
    }

    public User getMaster() {
        return master;
    }

    public void setMaster(User master) {
        this.master = master;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }

    public TypeDevice getTypeDevice() {
        return typeDevice;
    }

    public void setTypeDevice(TypeDevice typeDevice) {
        this.typeDevice = typeDevice;
    }

    public Date getStartDate1() {
        return startDate1;
    }

    public void setStartDate1(Date startDate1) {
        this.startDate1 = startDate1;
    }

    public Date getStartDate2() {
        return startDate2;
    }

    public void setStartDate2(Date startDate2) {
        this.startDate2 = startDate2;
    }

    public Date getEndDate1() {
        return endDate1;
    }

    public void setEndDate1(Date endDate1) {
        this.endDate1 = endDate1;
    }

    public Date getEndDate2() {
        return endDate2;
    }

    public void setEndDate2(Date endDate2) {
        this.endDate2 = endDate2;
    }

    public StatusRepairOrder getStatus() {
        return status;
    }

    public void setStatus(StatusRepairOrder status) {
        this.status = status;
    }

    public Set<StatusRepairOrder> getStatuses() {
        return statuses;
    }

    public void setStatuses(Set<StatusRepairOrder> statuses) {
        this.statuses = statuses;
    }

    public Place getPlaceReceive() {
        return placeReceive;
    }

    public void setPlaceReceive(Place placeReceive) {
        this.placeReceive = placeReceive;
    }

    public Place getCurrentPlace() {
        return currentPlace;
    }

    public void setCurrentPlace(Place currentPlace) {
        this.currentPlace = currentPlace;
    }

    public String getImeiSn() {
        return imeiSn;
    }

    public void setImeiSn(String imeiSn) {
        this.imeiSn = imeiSn;
    }
}
