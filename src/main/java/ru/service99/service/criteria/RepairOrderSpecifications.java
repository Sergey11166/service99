package ru.service99.service.criteria;

import org.springframework.data.jpa.domain.Specification;
import ru.service99.domain.*;

import javax.persistence.criteria.*;
import java.util.Date;
import java.util.Set;

public class RepairOrderSpecifications {

    public static Specification<RepairOrder> idEqual(final Long id) {
        return (root, query, cb) -> {
            if (id != null) return cb.equal(root.get(AbstractEntity_.id), id);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<RepairOrder> firmEqual(final Firm firm) {
        return (root, query, cb) -> {
            if (firm != null) return cb.equal(root.get(AbstractOrder_.firm), firm);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<RepairOrder> masterEqual(final User master) {
        return (root, query, cb) -> {
            if (master != null) return cb.equal(root.get(RepairOrder_.master), master);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<RepairOrder> receiverEqual(final User receiver) {
        return (root, query, cb) -> {
            if (receiver != null) return cb.equal(root.get(AbstractOrder_.receiver), receiver);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<RepairOrder> customerEqual(final Customer customer) {
        return (root, query, cb) -> {
            if (customer != null) return cb.equal(root.get(AbstractOrder_.customer), customer);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<RepairOrder> modelEqual(final Model model) {
        return (root, query, cb) -> {
            if (model != null) return cb.equal(root.get(RepairOrder_.model), model);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<RepairOrder> producerEqual(final Producer producer) {
        return (root, query, cb) -> {
            if (producer != null) {
                Join<RepairOrder, Model> joinModel = root.join(RepairOrder_.model);
                return cb.equal(joinModel.get(Model_.producer), producer);
            }
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<RepairOrder> typeDeviceEqual(final TypeDevice typeDevice) {
        return (root, query, cb) -> {
            if (typeDevice != null) {
                Join<RepairOrder, Model> joinModel = root.join(RepairOrder_.model);
                return cb.equal(joinModel.get(Model_.typeDevice), typeDevice);
            }
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<RepairOrder> startDateBetween(Date startDate1, final Date startDate2) {
        return (root, query, cb) -> {
            if (startDate1 == null && startDate2 == null) return cb.isNotNull(root.get(AbstractEntity_.id));
            if (startDate1 == null) return cb.lessThan(root.get(AbstractOrder_.startDate), startDate2);
            if (startDate2 == null) return cb.greaterThan(root.get(AbstractOrder_.startDate), startDate1);
            return cb.between(root.get(AbstractOrder_.startDate), startDate1, startDate2);
        };
    }

    public static Specification<RepairOrder> endDateBetween(Date endDate1, final Date endDate2) {
        return (root, query, cb) -> {
            if (endDate1 == null && endDate2 == null) return cb.isNotNull(root.get(AbstractEntity_.id));
            if (endDate1 == null) return cb.lessThan(root.get(AbstractOrder_.endDate), endDate2);
            if (endDate2 == null) return cb.greaterThan(root.get(AbstractOrder_.endDate), endDate1);
            return cb.between(root.get(AbstractOrder_.endDate), endDate1, endDate2);
        };
    }

    public static Specification<RepairOrder> statusEqual(final StatusRepairOrder status) {
        return (root, query, cb) -> {
            if (status != null) return cb.equal(root.get(RepairOrder_.status), status);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<RepairOrder> statusContainEqual(final Set<StatusRepairOrder> statuses) {
        return (root, query, cb) -> {
            if (statuses != null) {
                if (!statuses.isEmpty()) return cb.isTrue(root.get(RepairOrder_.status).in(statuses));
            }
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<RepairOrder> placeReceiveEqual(final Place place) {
        return (root, query, cb) -> {
            if (place != null) return cb.equal(root.get(AbstractOrder_.placeReceive), place);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<RepairOrder> currentPlaceEqual(final Place place) {
        return (root, query, cb) -> {
            if (place != null) return cb.equal(root.get(RepairOrder_.currentPlace), place);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<RepairOrder> imeiSnIsLike(final String number) {
        return new Specification<RepairOrder>() {
            @Override
            public Predicate toPredicate(Root<RepairOrder> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                if (number != null) {
                    String searchTerm = getLikePattern(number);
                    return cb.or(
                            cb.like(root.get(RepairOrder_.imei), searchTerm),
                            cb.like(root.get(RepairOrder_.newImei), searchTerm),
                            cb.like(root.get(RepairOrder_.sn), searchTerm),
                            cb.like(root.get(RepairOrder_.newSn), searchTerm)
                    );
                }
                return cb.isNotNull(root.get(AbstractEntity_.id));
            }

            private String getLikePattern(final String searchTerm) {
                return "%" + searchTerm + "%";
            }
        };
    }
}
