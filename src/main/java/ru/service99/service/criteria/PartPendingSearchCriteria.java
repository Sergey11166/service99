package ru.service99.service.criteria;

import ru.service99.domain.*;

import java.util.Date;

public class PartPendingSearchCriteria {

    private Long id;
    private Long partId;
    private String partNumber;
    private Model model;
    private Producer producer;
    private TypeDevice typeDevice;
    private TypePartPending type;
    private User receiver;
    private User manager;
    private Date startDate1;
    private Date orderedDate1;
    private Date startDate2;
    private Date orderedDate2;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPartId() {
        return partId;
    }

    public void setPartId(Long partId) {
        this.partId = partId;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }

    public TypeDevice getTypeDevice() {
        return typeDevice;
    }

    public void setTypeDevice(TypeDevice typeDevice) {
        this.typeDevice = typeDevice;
    }

    public TypePartPending getType() {
        return type;
    }

    public void setType(TypePartPending type) {
        this.type = type;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public User getManager() {
        return manager;
    }

    public void setManager(User manager) {
        this.manager = manager;
    }

    public Date getStartDate1() {
        return startDate1;
    }

    public void setStartDate1(Date startDate1) {
        this.startDate1 = startDate1;
    }

    public Date getOrderedDate1() {
        return orderedDate1;
    }

    public void setOrderedDate1(Date orderedDate1) {
        this.orderedDate1 = orderedDate1;
    }

    public Date getStartDate2() {
        return startDate2;
    }

    public void setStartDate2(Date startDate2) {
        this.startDate2 = startDate2;
    }

    public Date getOrderedDate2() {
        return orderedDate2;
    }

    public void setOrderedDate2(Date orderedDate2) {
        this.orderedDate2 = orderedDate2;
    }
}
