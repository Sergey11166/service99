package ru.service99.service.criteria;

import ru.service99.domain.*;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

public class SalesOrderSearchCriteria implements Serializable {

    private Long id;
    private Date startDate1;
    private Date startDate2;
    private Date endDate1;
    private Date endDate2;
    private Customer customer;
    private User receiver;
    private User manager;
    private Firm firm;
    private Place placeReceive;
    private StatusSalesOrder status;
    private TypeSale type;
    private Set<StatusSalesOrder> statuses;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartDate1() {
        return startDate1;
    }

    public void setStartDate1(Date startDate1) {
        this.startDate1 = startDate1;
    }

    public Date getStartDate2() {
        return startDate2;
    }

    public void setStartDate2(Date startDate2) {
        this.startDate2 = startDate2;
    }

    public Date getEndDate1() {
        return endDate1;
    }

    public void setEndDate1(Date endDate1) {
        this.endDate1 = endDate1;
    }

    public Date getEndDate2() {
        return endDate2;
    }

    public void setEndDate2(Date endDate2) {
        this.endDate2 = endDate2;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public User getManager() {
        return manager;
    }

    public void setManager(User manager) {
        this.manager = manager;
    }

    public Firm getFirm() {
        return firm;
    }

    public void setFirm(Firm firm) {
        this.firm = firm;
    }

    public Place getPlaceReceive() {
        return placeReceive;
    }

    public void setPlaceReceive(Place placeReceive) {
        this.placeReceive = placeReceive;
    }

    public StatusSalesOrder getStatus() {
        return status;
    }

    public void setStatus(StatusSalesOrder status) {
        this.status = status;
    }

    public TypeSale getType() {
        return type;
    }

    public void setType(TypeSale type) {
        this.type = type;
    }

    public Set<StatusSalesOrder> getStatuses() {
        return statuses;
    }

    public void setStatuses(Set<StatusSalesOrder> statuses) {
        this.statuses = statuses;
    }
}
