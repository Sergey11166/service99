package ru.service99.service.criteria;

import ru.service99.domain.Place;
import ru.service99.domain.StatusDocument;
import ru.service99.domain.Stock;
import ru.service99.domain.User;

import java.util.Date;

public class DocumentSearchCriteria {

    private User author;
    private StatusDocument status;
    private Date date1;
    private Date date2;
    private Stock stockFrom;
    private Stock stockTo;
    private Place placeFrom;
    private Place placeTo;

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public StatusDocument getStatus() {
        return status;
    }

    public void setStatus(StatusDocument status) {
        this.status = status;
    }

    public Date getDate1() {
        return date1;
    }

    public void setDate1(Date date1) {
        this.date1 = date1;
    }

    public Date getDate2() {
        return date2;
    }

    public void setDate2(Date date2) {
        this.date2 = date2;
    }

    public Stock getStockFrom() {
        return stockFrom;
    }

    public void setStockFrom(Stock stockFrom) {
        this.stockFrom = stockFrom;
    }

    public Stock getStockTo() {
        return stockTo;
    }

    public void setStockTo(Stock stockTo) {
        this.stockTo = stockTo;
    }

    public Place getPlaceFrom() {
        return placeFrom;
    }

    public void setPlaceFrom(Place placeFrom) {
        this.placeFrom = placeFrom;
    }

    public Place getPlaceTo() {
        return placeTo;
    }

    public void setPlaceTo(Place placeTo) {
        this.placeTo = placeTo;
    }
}
