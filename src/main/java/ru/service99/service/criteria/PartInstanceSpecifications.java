package ru.service99.service.criteria;

import org.springframework.data.jpa.domain.Specification;
import ru.service99.domain.*;

import javax.persistence.criteria.*;

public class PartInstanceSpecifications {

    public static Specification<PartInstance> idEqual(final Long id) {
        return (root, query, cb) -> {
            if (id != null) return cb.equal(root.get(AbstractEntity_.id), id);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<PartInstance> partIdEqual(final Long id) {
        return (root, query, cb) -> {
            if (id != null) {
                Join<PartInstance, Part> joinPart = root.join(PartInstance_.part);
                return cb.equal(joinPart.get(AbstractEntity_.id), id);
            }
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<PartInstance> nameIsLike(final String name) {
        return new Specification<PartInstance>() {
            @Override
            public Predicate toPredicate(Root<PartInstance> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                if (name != null) {
                    Join<PartInstance, Part> joinPart = root.join(PartInstance_.part);
                    return cb.like(joinPart.get(Part_.name), getLikePattern(name));
                } else {
                    return cb.isNotNull(root.get(AbstractEntity_.id));
                }
            }

            private String getLikePattern(final String searchTerm) {
                return "%" + searchTerm + "%";
            }
        };
    }

    public static Specification<PartInstance> partNumberEqual(final String partNumber) {
        return (root, query, cb) -> {
            if (partNumber != null && partNumber.length() > 0) {
                Join<PartInstance, Part> joinPart = root.join(PartInstance_.part);
                return cb.equal(joinPart.get(Part_.partNumber), partNumber);
            } else {
                return cb.isNotNull(root.get(AbstractEntity_.id));
            }
        };
    }

    public static Specification<PartInstance> inPriceEqual(final Float inPrice) {
        return (root, query, cb) -> {
            if (inPrice != null) {
                return cb.equal(root.get(PartInstance_.inPrice), inPrice);
            } else {
                return cb.isNotNull(root.get(AbstractEntity_.id));
            }
        };
    }

    public static Specification<PartInstance> optPriceEqual(final Float optPrice) {
        return (root, query, cb) -> {
            if (optPrice != null) {
                return cb.equal(root.get(PartInstance_.optPrice), optPrice);
            } else {
                return cb.isNotNull(root.get(AbstractEntity_.id));
            }
        };
    }

    public static Specification<PartInstance> outPriceEqual(final Float outPrice) {
        return (root, query, cb) -> {
            if (outPrice != null) {
                return cb.equal(root.get(PartInstance_.outPrice), outPrice);
            } else {
                return cb.isNotNull(root.get(AbstractEntity_.id));
            }
        };
    }

    public static Specification<PartInstance> stockEqual(final Stock stock) {
        return (root, query, cb) -> {
            if (stock != null) {
                return cb.equal(root.get(PartInstance_.stock), stock);
            } else {
                return cb.isNotNull(root.get(AbstractEntity_.id));
            }
        };
    }

    public static Specification<PartInstance> producerEqual(final Producer producer) {
        return (root, query, cb) -> {
            if (producer != null) {
                Join<PartInstance, Part> joinPart = root.join(PartInstance_.part);
                Join<Part, Model> joinModel = joinPart.join(Part_.models);
                return cb.equal(joinModel.get(Model_.producer), producer);
            }
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<PartInstance> typeDeviceEqual(final TypeDevice typeDevice) {
        return (root, query, cb) -> {
            if (typeDevice != null) {
                Join<PartInstance, Part> joinPart = root.join(PartInstance_.part);
                Join<Part, Model> joinModel = joinPart.join(Part_.models);
                return cb.equal(joinModel.get(Model_.typeDevice), typeDevice);
            }
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<PartInstance> modelsContain(final Model model) {
        return (root, query, cb) -> {
            if (model != null) {
                Join<PartInstance, Part> joinPart = root.join(PartInstance_.part);
                return cb.isMember(model, joinPart.get(Part_.models));
            } else {
                return cb.isNotNull(root.get(AbstractEntity_.id));
            }
        };
    }

    public static Specification<PartInstance> documentOutIsNull() {
        return (root, query, cb) -> cb.isNull(root.get(PartInstance_.documentOut));
    }

    public static Specification<PartInstance> statusDocumentInIs2() {
        return (root, query, cb) -> {
            Join<PartInstance, DocumentIn> joinDocumentIn = root.join(PartInstance_.documentIn);
            return cb.equal(joinDocumentIn.get(AbstractDocument_.status), StatusDocument.Завершено);
        };
    }
}
