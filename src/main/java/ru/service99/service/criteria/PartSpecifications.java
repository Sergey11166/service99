package ru.service99.service.criteria;

import org.springframework.data.jpa.domain.Specification;
import ru.service99.domain.*;

import javax.persistence.criteria.*;

public class PartSpecifications {

    public static Specification<Part> idEqual(final Long id) {
        return (root, query, cb) -> {
            if (id != null) return cb.equal(root.get(AbstractEntity_.id), id);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<Part> nameIsLike(final String name) {
        return new Specification<Part>() {
            @Override
            public Predicate toPredicate(Root<Part> partRoot, CriteriaQuery<?> query, CriteriaBuilder cb) {
                if (name != null) {
                    return cb.like(partRoot.get(Part_.name), getLikePattern(name));
                } else {
                    return cb.isNotNull(partRoot.get(AbstractEntity_.id));
                }
            }

            private String getLikePattern(final String searchTerm) {
                return "%" + searchTerm + "%";
            }
        };
    }

    public static Specification<Part> partNumberEqual(final String partNumber) {
        return (root, query, cb) -> {
            if (partNumber != null && partNumber.length() > 0) {
                return cb.equal(root.get(Part_.partNumber), partNumber);
            } else {
                return cb.isNotNull(root.get(AbstractEntity_.id));
            }
        };
    }

    public static Specification<Part> producerEqual(final Producer producer) {
        return (root, query, cb) -> {
            if (producer != null) {
                Join<Part, Model> joinModel = root.join(Part_.models);
                return cb.equal(joinModel.get(Model_.producer), producer);
            }
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<Part> typeDeviceEqual(final TypeDevice typeDevice) {
        return (root, query, cb) -> {
            if (typeDevice != null) {
                Join<Part, Model> joinModel = root.join(Part_.models);
                return cb.equal(joinModel.get(Model_.typeDevice), typeDevice);
            }
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<Part> modelsContain(final Model model) {
        return (root, query, cb) -> {
            if (model != null) {
                return cb.isMember(model, root.get(Part_.models));
            } else {
                return cb.isNotNull(root.get(AbstractEntity_.id));
            }
        };
    }

    public static Specification<Part> isRemovedEqual(final Boolean isRemoved) {
        return (root, query, cb) -> cb.equal(root.get(Part_.isRemoved), isRemoved);
    }
}
