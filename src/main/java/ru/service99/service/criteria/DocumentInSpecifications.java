package ru.service99.service.criteria;

import org.springframework.data.jpa.domain.Specification;
import ru.service99.domain.*;

import java.util.Date;

public class DocumentInSpecifications {

    public static Specification<DocumentIn> authorEqual(final User author) {
        return (root, query, cb) -> {
            if (author != null) return cb.equal(root.get(AbstractDocument_.author), author);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<DocumentIn> statusEqual(final StatusDocument status) {
        return (root, query, cb) -> {
            if (status != null) return cb.equal(root.get(AbstractDocument_.status), status);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    public static Specification<DocumentIn> stockToEqual(final Stock stock) {
        return (root, query, cb) -> {
            if (stock != null) return cb.equal(root.get(DocumentIn_.stockTo), stock);
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }

    /*public static Specification<DocumentIn> instancesContainStock(final Stock stock){
        return (root, query, cb) -> {
            if (stock != null) {
                SetJoin<DocumentIn, PartInstance> joinInstances = root.join(DocumentIn_.partInstances);
                return cb.equal(joinInstances.get(PartInstance_.stock), stock);
            }
            return cb.isNotNull(root.get(AbstractEntity_.id));
        };
    }*/

    public static Specification<DocumentIn> dateBetween(Date date1, final Date date2) {
        return (root, query, cb) -> {
            if (date1 == null && date2 == null) return cb.isNotNull(root.get(AbstractEntity_.id));
            if (date1 == null) return cb.lessThan(root.get(AbstractDocument_.date), date2);
            if (date2 == null) return cb.greaterThan(root.get(AbstractDocument_.date), date1);
            return cb.between(root.get(AbstractDocument_.date), date1, date2);
        };
    }
}
