package ru.service99.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import ru.service99.domain.Customer;
import ru.service99.service.repository.CustomerRepository;
import ru.service99.web.ui.ViewUtils;

@Component()
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public Page<Customer> findAll(Pageable pageable) {
        return customerRepository.findAll(pageable);
    }

    @Override
    public Page<Customer> findByName(String name, Pageable pageable) {
        return customerRepository.findByNameLikeOrderByNameAsc(name, pageable);
    }

    @Override
    public Customer findCustomerById(long id) {
        return customerRepository.findOne(id);
    }

    @Override
    public Customer save(Customer customer) {
        if (customer.getName().length() < 1) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.userNameIsEmpty"));
        }
        if (customer.getPhoneNumber().length() < 1 && customer.getAddress().length() < 1) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.phoneAndAddressIsEmpty"));
        }
        return customerRepository.save(customer);
    }
}
