package ru.service99.service;


import ru.service99.domain.User;

import java.util.List;

public interface UserService {

    List<User> findAll();

    User findById(long id);

    User findByLogin(String email);

    User save(User user);
}
