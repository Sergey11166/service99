package ru.service99.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.service99.domain.Customer;

public interface CustomerService {

    Page<Customer> findAll(Pageable pageable);

    Page<Customer> findByName(String name, Pageable pageable);

    Customer findCustomerById(long id);

    Customer save(Customer customer);
}
