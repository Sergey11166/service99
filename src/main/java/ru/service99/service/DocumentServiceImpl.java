package ru.service99.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.history.Revision;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;
import ru.service99.SecurityUtils;
import ru.service99.domain.*;
import ru.service99.service.criteria.*;
import ru.service99.service.repository.*;
import ru.service99.web.ui.ViewUtils;

import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static org.springframework.data.jpa.domain.Specifications.where;

@Component
public class DocumentServiceImpl implements DocumentService {

    @Autowired
    private DocumentInRepository documentInRepository;
    @Autowired
    private DocumentOutRepository documentOutRepository;
    @Autowired
    private DocumentMoveRepository documentMoveRepository;
    @Autowired
    private DocumentMoveDevicesRepository documentMoveDevicesRepository;
    @Autowired
    private PartInstanceRepository partInstanceRepository;
    @Autowired
    private RepairOrderRepository repairOrderRepository;
    @Autowired
    private PlatformTransactionManager transactionManager;
    @Autowired
    private PartService partService;

    private TransactionTemplate transactionTemplate;

    @Override
    public DocumentIn findDocumentInById(long id) {
        return documentInRepository.findOne(id);
    }

    @Override
    public DocumentOut findDocumentOutById(long id) {
        return documentOutRepository.findOne(id);
    }

    @Override
    public DocumentMove findDocumentMoveById(long id) {
        return documentMoveRepository.findOne(id);
    }

    @Override
    public DocumentMoveDevices findDocumentMoveDevicesById(long id) {
        return documentMoveDevicesRepository.findOne(id);
    }

    @Override
    public Page<DocumentIn> findDocumentInByCriteria(DocumentSearchCriteria criteria, Pageable pageable) {
        if (criteria == null) return documentInRepository.findAll(pageable);
        return documentInRepository.findAll(where(DocumentInSpecifications.authorEqual(criteria.getAuthor()))
                .and(DocumentInSpecifications.statusEqual(criteria.getStatus()))
                .and(DocumentInSpecifications.stockToEqual(criteria.getStockTo()))
                .and(DocumentInSpecifications.dateBetween(criteria.getDate1(), criteria.getDate2())), pageable);
    }

    @Override
    public Page<DocumentOut> findDocumentOutByCriteria(DocumentSearchCriteria criteria, Pageable pageable) {
        if (criteria == null) return documentOutRepository.findAll(pageable);
        return documentOutRepository.findAll(where(DocumentOutSpecifications.authorEqual(criteria.getAuthor()))
                .and(DocumentOutSpecifications.statusEqual(criteria.getStatus()))
                .and(DocumentOutSpecifications.dateBetween(criteria.getDate1(), criteria.getDate2())), pageable);
    }

    @Override
    public Page<DocumentMove> findDocumentMoveByCriteria(DocumentSearchCriteria criteria, Pageable pageable) {
        if (criteria == null) return documentMoveRepository.findAll(pageable);
        return documentMoveRepository.findAll(where(DocumentMoveSpecifications.authorEqual(criteria.getAuthor()))
                .and(DocumentMoveSpecifications.statusEqual(criteria.getStatus()))
                .and(DocumentMoveSpecifications.dateBetween(criteria.getDate1(), criteria.getDate2()))
                .and(DocumentMoveSpecifications.stockFromEqual(criteria.getStockFrom()))
                .and(DocumentMoveSpecifications.stockToEqual(criteria.getStockTo())), pageable);
    }

    @Override
    public Page<DocumentMoveDevices> findDocumentMoveDevicesByCriteria(DocumentSearchCriteria criteria, Pageable pageable) {
        if (criteria == null) return documentMoveDevicesRepository.findAll(pageable);
        return documentMoveDevicesRepository.findAll(where(DocumentMoveDevicesSpecifications.authorEqual(criteria.getAuthor()))
                .and(DocumentMoveDevicesSpecifications.statusEqual(criteria.getStatus()))
                .and(DocumentMoveDevicesSpecifications.dateBetween(criteria.getDate1(), criteria.getDate2()))
                .and(DocumentMoveDevicesSpecifications.placeFromEqual(criteria.getPlaceFrom()))
                .and(DocumentMoveDevicesSpecifications.placeToEqual(criteria.getPlaceTo())), pageable);
    }

    @Override
    public DocumentIn saveDocumentIn(DocumentIn document) {
        if (transactionTemplate == null) transactionTemplate = new TransactionTemplate(transactionManager);
        return transactionTemplate.execute(transactionStatus -> {
            boolean isNew = document.getId() == null;
            Set<PartInstance> newInstances = new HashSet<>(document.getPartInstances());
            if (document.getStockTo() == null) {
                throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.stockIsNull"));
            }
            if (newInstances.isEmpty()) {
                throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.partsIsEmpty"));
            }
            if (isNew) {
                document.setAuthor(SecurityUtils.getCurrentUser());
                document.setDate(new Date());
                document.setStatus(StatusDocument.Новый);
            } else {
                DocumentIn existDocument = documentInRepository.findOne(document.getId());
                if (existDocument.getStatus() == StatusDocument.Завершено) {
                    throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.notAllowedAfterFinished"));
                }
                if (!Objects.equals(document.getStockTo().getId(), existDocument.getStockTo().getId())) {
                    throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.notAllowedChangeStock"));
                }
                if (existDocument.getStatus().ordinal() > document.getStatus().ordinal()) {
                    throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.notAllowedChangeStatus"));
                }
                Set<PartInstance> existInstances = new HashSet<>(existDocument.getPartInstances());
                existInstances.forEach(ei -> {
                    boolean found = false;
                    for (PartInstance i : newInstances) {
                        if (Objects.equals(ei.getId(), i.getId())) found = true;
                    }
                    if (!found) {
                        partInstanceRepository.delete(ei.getId());
                    }
                });
            }
            document.getPartInstances().clear();
            DocumentIn result = documentInRepository.save(document);
            newInstances.forEach(instance -> {
                instance.setDocumentIn(document);
                instance.setStock(document.getStockTo());
            });
            Set<PartInstance> savedInstances = new HashSet<>(partInstanceRepository.save(newInstances));
            result.setPartInstances(savedInstances);

            for (PartInstance pi : savedInstances) {
                Part part = pi.getPart();
                if (Objects.equals(part.getLastInPrice(), pi.getInPrice())
                        && Objects.equals(part.getLastOptPrice(), pi.getOptPrice())
                        && Objects.equals(part.getLastOutPrice(), pi.getOutPrice())) continue;
                part.setLastInPrice(pi.getInPrice());
                part.setLastOptPrice(pi.getOptPrice());
                part.setLastOutPrice(pi.getOutPrice());
                partService.savePart(part);
            }

            return result;
        });
    }

    @Override
    public DocumentMove saveDocumentMove(DocumentMove document) {
        if (transactionTemplate == null) transactionTemplate = new TransactionTemplate(transactionManager);
        return transactionTemplate.execute(transactionStatus -> {
            DocumentMove existDocument = null;
            boolean isNew = document.getId() == null;
            if (!isNew) existDocument = documentMoveRepository.findOne(document.getId());
            Set<PartInstance> movingExistInstances = new HashSet<>(document.getPartInstances().size());
            movingExistInstances.addAll(document.getPartInstances().stream().map(newIns ->
                    partInstanceRepository.findOne(newIns.getId())).collect(Collectors.toList()));
            document.validate(existDocument, movingExistInstances);
            Set<PartInstance> newInstances = new HashSet<>(document.getPartInstances());
            document.getPartInstances().clear();
            if (isNew) {
                document.setAuthor(SecurityUtils.getCurrentUser());
                document.setDate(new Date());
                document.setStatus(StatusDocument.Новый);
            } else {
                Set<PartInstance> existInstances = new HashSet<>(existDocument.getPartInstances());
                final DocumentMove finalExistDocument = existDocument;
                existInstances.forEach(ei -> {
                    boolean found = false;
                    for (PartInstance i : newInstances) {
                        if (Objects.equals(ei.getId(), i.getId())) found = true;
                    }
                    if (!found) {
                        ei.getDocumentMoves().remove(finalExistDocument);
                        partInstanceRepository.save(ei);
                    }
                });
            }
            DocumentMove result = documentMoveRepository.save(document);
            newInstances.forEach(instance -> {
                instance.getDocumentMoves().add(document);
                if (document.getStatus() == StatusDocument.Завершено) {
                    instance.setStock(document.getStockTo());
                }
            });
            Set<PartInstance> savedInstances = new HashSet<>(partInstanceRepository.save(newInstances));
            result.setPartInstances(savedInstances);
            return result;
        });
    }

    @Override
    public DocumentOut saveDocumentOut(DocumentOut document) {
        if (transactionTemplate == null) transactionTemplate = new TransactionTemplate(transactionManager);
        return transactionTemplate.execute(transactionStatus -> {
            boolean isNewDocument = document.getId() == null;
            Set<PartInstance> newInstances = new HashSet<>(document.getPartInstances());
            document.getPartInstances().clear();
            if (isNewDocument) {
                if (newInstances.isEmpty()) {
                    throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.partsIsEmpty"));
                }
            }
            for (PartInstance instance : newInstances) {
                PartInstance exIns = partInstanceRepository.findOne(instance.getId());
                Set<DocumentMove> documentMoves = new HashSet<>(exIns.getDocumentMoves());
                for (DocumentMove dm : documentMoves) {
                    if (dm.getStatus() != StatusDocument.Завершено) {
                        throw new IllegalStateException(ViewUtils
                                .getMessage("exception.IllegalState.partInReserve") + dm.getId());
                    }
                }
                if (!Objects.equals(exIns.getStock().getId(), instance.getStock().getId())) {
                    throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.partMove") +
                            exIns.getStock().getName() + "!");
                }
                if (exIns.getDocumentOut() == null) continue;
                if (instance.getDocumentOut() == null) {
                    //List<RepairOrder> orders = repairOrderRepository.findByDocumentId(exIns.getDocumentOut().getId());
                    //String whereSpending = orders.isEmpty() ? ViewUtils.getMessage("string.spendingDocument") + " - " +
                    //        exIns.getDocumentOut().getId() : ViewUtils.getMessage("string.repair") + " - " + orders.get(0).getId();
                    String docComment = exIns.getDocumentOut().getComment();
                    String whereSpending;
                    if (docComment != null) {
                        if (!docComment.isEmpty()) {
                            whereSpending = docComment;
                        } else {
                            whereSpending = ViewUtils.getMessage("string.spending") + " - " +
                                    exIns.getDocumentOut().getId();
                        }
                    } else {
                        whereSpending = ViewUtils.getMessage("string.spending") + " - " + exIns.getDocumentOut().getId();
                    }

                    throw new IllegalStateException(ViewUtils
                            .getMessage("exception.IllegalState.partSpendingAnotherUser") + "\n" +
                            whereSpending + "\n" +
                            ViewUtils.getMessage("string.part") + " - " + instance.getPart().getPartNumber() + "\n" +
                            ViewUtils.getMessage("string.author") + " - " + exIns.getDocumentOut().getAuthor().getName());
                }
                if (!Objects.equals(exIns.getDocumentOut().getId(), instance.getDocumentOut().getId())) {
                    String docComment = exIns.getDocumentOut().getComment();
                    String whereSpending;
                    if (docComment != null) {
                        if (!docComment.isEmpty()) {
                            whereSpending = docComment;
                        } else {
                            whereSpending = ViewUtils.getMessage("string.spending") + " - " +
                                    exIns.getDocumentOut().getId();
                        }
                    } else {
                        whereSpending = ViewUtils.getMessage("string.spending") + " - " + exIns.getDocumentOut().getId();
                    }

                    throw new IllegalStateException(ViewUtils
                            .getMessage("exception.IllegalState.partSpendingAnotherUser") + "\n" +
                            whereSpending + "\n" +
                            ViewUtils.getMessage("string.part") + " - " + instance.getPart().getPartNumber() + "\n" +
                            ViewUtils.getMessage("string.author") + " - " + exIns.getDocumentOut().getAuthor().getName());
                }
            }
            if (isNewDocument) {
                document.setDate(new Date());
                document.setStatus(StatusDocument.Завершено);
                document.setAuthor(SecurityUtils.getCurrentUser());
            } else {
                Set<PartInstance> existInstances = new HashSet<>(
                        partInstanceRepository.findByDocumentOutId(document.getId()));
                existInstances.forEach(ei -> {
                    boolean found = false;
                    for (PartInstance i : newInstances) {
                        if (Objects.equals(ei.getId(), i.getId())) found = true;
                    }
                    if (!found) {
                        ei.setDocumentOut(null);
                        partInstanceRepository.save(ei);
                    }
                });
            }
            DocumentOut savedDocument = documentOutRepository.save(document);
            newInstances.forEach(instance -> instance.setDocumentOut(savedDocument));
            Set<PartInstance> savedInstances = new HashSet<>(partInstanceRepository.save(newInstances));
            savedDocument.setPartInstances(savedInstances);
            return savedDocument;
        });
    }

    @Override
    public DocumentMoveDevices saveDocumentMoveDevices(DocumentMoveDevices document) {
        if (transactionTemplate == null) transactionTemplate = new TransactionTemplate(transactionManager);
        return transactionTemplate.execute(transactionStatus -> {
            boolean isNew = document.getId() == null;
            Set<RepairOrder> newRepairOrders = new HashSet<>(document.getRepairOrders());
            if (document.getPlaceFrom() == null || document.getPlaceTo() == null) {
                throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.placeIsNull"));
            }
            if (Objects.equals(document.getPlaceTo().getId(), document.getPlaceFrom().getId())) {
                throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.notAllowedSamePlaces"));
            }
            if (newRepairOrders.isEmpty()) {
                throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.repairsIsEmpty"));
            }
            if (isNew) {
                document.setAuthor(SecurityUtils.getCurrentUser());
                document.setDate(new Date());
                document.setStatus(StatusDocument.Новый);
            } else {
                DocumentMoveDevices existDocument = documentMoveDevicesRepository.findOne(document.getId());
                if (existDocument.getStatus() == StatusDocument.Завершено) {
                    throw new IllegalStateException(ViewUtils
                            .getMessage("exception.IllegalState.notAllowedAfterFinished"));
                }
                Set<RepairOrder> existOrders = new HashSet<>(existDocument.getRepairOrders());
                boolean isChangedRepairs = false;
                for (RepairOrder order : existOrders) {
                    boolean found = false;
                    for (RepairOrder o : newRepairOrders) {
                        if (Objects.equals(order.getId(), o.getId())) found = true;
                    }
                    if (!found) {
                        isChangedRepairs = true;
                        break;
                    }
                }
                for (RepairOrder order : newRepairOrders) {
                    boolean found = false;
                    for (RepairOrder o : existOrders) {
                        if (Objects.equals(order.getId(), o.getId())) found = true;
                    }
                    if (!found) {
                        isChangedRepairs = true;
                        break;
                    }
                }
                if (existDocument.getStatus().ordinal() > document.getStatus().ordinal()) {
                    throw new IllegalStateException(ViewUtils
                            .getMessage("exception.IllegalState.notAllowedBackChangeStatus"));
                }
                if (existDocument.getStatus() == StatusDocument.Передан && isChangedRepairs) {
                    throw new IllegalStateException(ViewUtils
                            .getMessage("exception.IllegalState.notAllowedChangeRepairsPassed"));
                }
                if (!Objects.equals(document.getPlaceFrom().getId(), existDocument.getPlaceFrom().getId()) ||
                        !Objects.equals(document.getPlaceTo().getId(), existDocument.getPlaceTo().getId())) {
                    throw new IllegalStateException(ViewUtils
                            .getMessage("exception.IllegalState.notAllowedChangePlace"));
                }
                existOrders.forEach(eo -> {
                    boolean found = false;
                    for (RepairOrder o : newRepairOrders) {
                        if (Objects.equals(eo.getId(), o.getId())) found = true;
                    }
                    if (!found) {
                        eo.getDocumentMoveDevices().remove(document);
                        repairOrderRepository.save(eo);
                    }
                });
            }
            document.getRepairOrders().clear();
            DocumentMoveDevices result = documentMoveDevicesRepository.save(document);
            newRepairOrders.forEach(order -> {
                order.getDocumentMoveDevices().add(document);
                if (document.getStatus() == StatusDocument.Завершено) {
                    order.setCurrentPlace(document.getPlaceTo());
                }
            });
            Set<RepairOrder> savedRepairs = new HashSet<>(repairOrderRepository.save(newRepairOrders));
            result.setRepairOrders(savedRepairs);
            return result;
        });
    }

    @Override
    public Revision<Integer, DocumentIn> findLastRevisionDocumentInById(long id) {
        return documentInRepository.findLastChangeRevision(id);
    }

    @Override
    public Revision<Integer, DocumentOut> findLastRevisionDocumentOutById(long id) {
        return documentOutRepository.findLastChangeRevision(id);
    }

    @Override
    public Revision<Integer, DocumentMove> findLastRevisionDocumentMoveById(long id) {
        return documentMoveRepository.findLastChangeRevision(id);
    }

    @Override
    public void deleteDocumentMove(DocumentMove document) {
        if (document.getStatus() == StatusDocument.Завершено) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.errorRemoveSuccessPartMoveDocument"));
        }
        User currentUser = SecurityUtils.getCurrentUser();
        if (!Objects.equals(currentUser.getId(), document.getAuthor().getId())) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.errorRemoveAlienPartMoveDocument"));
        }
        document.getPartInstances().clear();
        saveDocumentMove(document);
        documentMoveRepository.delete(document);
    }
}
