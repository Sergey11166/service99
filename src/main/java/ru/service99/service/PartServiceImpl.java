package ru.service99.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import ru.service99.SecurityUtils;
import ru.service99.domain.*;
import ru.service99.service.criteria.PartInstanceSpecifications;
import ru.service99.service.criteria.PartPendingSearchCriteria;
import ru.service99.service.criteria.PartPendingSpecifications;
import ru.service99.service.criteria.PartSearchCriteria;
import ru.service99.service.repository.PartInstanceRepository;
import ru.service99.service.repository.PartPendingRepository;
import ru.service99.service.repository.PartRepository;
import ru.service99.service.repository.StockRepository;
import ru.service99.web.ui.ViewUtils;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import static org.springframework.data.jpa.domain.Specifications.where;
import static ru.service99.service.criteria.PartSpecifications.*;


@Component
public class PartServiceImpl implements PartService {

    @Autowired
    private PartRepository partRepository;
    @Autowired
    private StockRepository stockRepository;
    @Autowired
    private PartPendingRepository partPendingRepository;
    @Autowired
    private PartInstanceRepository partInstanceRepository;

    @Override
    public Page<Part> findPartsByCriteria(PartSearchCriteria criteria, Pageable pageable) {
        if (criteria == null) return partRepository.findAll(pageable);
        return partRepository.findAll(where(
                idEqual(criteria.getPartId()))
                        .and(nameIsLike(criteria.getName()))
                        .and(partNumberEqual(criteria.getPartNumber()))
                        .and(producerEqual(criteria.getProducer()))
                        .and(typeDeviceEqual(criteria.getTypeDevice()))
                        .and(modelsContain(criteria.getModel()))
                        .and(isRemovedEqual(false))
                , pageable);
    }

    @Override
    public Page<PartInstance> findFreeInstancesByCriteria(PartSearchCriteria criteria, Pageable pageable) {
        if (criteria == null) return partInstanceRepository.findAll(pageable);
        return partInstanceRepository.findAll(where(PartInstanceSpecifications.idEqual(criteria.getPartInstanceId()))
                        .and(PartInstanceSpecifications.partIdEqual(criteria.getPartId()))
                        .and(PartInstanceSpecifications.nameIsLike(criteria.getName()))
                        .and(PartInstanceSpecifications.partNumberEqual(criteria.getPartNumber()))
                        .and(PartInstanceSpecifications.inPriceEqual(criteria.getInPrice()))
                        .and(PartInstanceSpecifications.optPriceEqual(criteria.getOptPrice()))
                        .and(PartInstanceSpecifications.outPriceEqual(criteria.getOutPrice()))
                        .and(PartInstanceSpecifications.stockEqual(criteria.getStock()))
                        .and(PartInstanceSpecifications.producerEqual(criteria.getProducer()))
                        .and(PartInstanceSpecifications.typeDeviceEqual(criteria.getTypeDevice()))
                        .and(PartInstanceSpecifications.modelsContain(criteria.getModel()))
                        .and(PartInstanceSpecifications.documentOutIsNull())
                        .and(PartInstanceSpecifications.statusDocumentInIs2())
                , pageable);
    }

    @Override
    public long getCountFreeInstancesByCriteria(PartSearchCriteria criteria) {
        if (criteria == null) return partInstanceRepository.count();
        return partInstanceRepository.count(where(PartInstanceSpecifications.idEqual(criteria.getPartInstanceId()))
                .and(PartInstanceSpecifications.partIdEqual(criteria.getPartId()))
                .and(PartInstanceSpecifications.nameIsLike(criteria.getName()))
                .and(PartInstanceSpecifications.partNumberEqual(criteria.getPartNumber()))
                .and(PartInstanceSpecifications.stockEqual(criteria.getStock()))
                .and(PartInstanceSpecifications.producerEqual(criteria.getProducer()))
                .and(PartInstanceSpecifications.typeDeviceEqual(criteria.getTypeDevice()))
                .and(PartInstanceSpecifications.modelsContain(criteria.getModel()))
                .and(PartInstanceSpecifications.documentOutIsNull())
                .and(PartInstanceSpecifications.statusDocumentInIs2()));
    }

    @Override
    public Part savePart(Part part) {
        if (part.getId() == null) part.setIsRemoved(false);
        if (part.getIsRemoved()) {
            if (partInstanceRepository.countByPartId(part.getId()) > 0) {
                throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.errorRemove"));
            }
            PartPendingSearchCriteria ppsCriteria = new PartPendingSearchCriteria();
            ppsCriteria.setPartId(part.getId());
            List<PartPending> partsPending = findPartsPendingByCriteria(ppsCriteria, new PageRequest(0, 1)).getContent();
            for (PartPending pp : partsPending) {
                if (!Objects.equals(pp.getNeededCount(), pp.getReceivedCount())) {
                    throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.errorRemove"));
                }
            }
        }
        if (part.getName().isEmpty()) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.nameIsEmpty"));
        }
        if (part.getPartNumber().isEmpty()) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.numberIsEmpty"));
        }
        return partRepository.save(part);
    }

    @Override
    public List<Stock> findAllStocks() {
        return stockRepository.findByIsRemovedFalseOrderByNameAsc();
    }

    @Override
    public Stock findStockById(long id) {
        return stockRepository.findByIdAndIsRemovedFalse(id);
    }

    @Override
    public Stock saveStock(Stock stock) {
        if (stock.getId() == null) stock.setIsRemoved(false);
        if (stock.getIsRemoved()) {
            PartSearchCriteria criteria = new PartSearchCriteria();
            criteria.setStock(stock);
            if (getCountFreeInstancesByCriteria(criteria) > 0 || !stock.getUsers().isEmpty()) {
                throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.errorRemove"));
            }
        }
        if (stock.getName().isEmpty()) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.nameIsEmpty"));
        }
        return stockRepository.save(stock);
    }

    @Override
    public PartPending findPartPendingById(long id) {
        return partPendingRepository.findOne(id);
    }

    @Override
    public Page<PartPending> findPartsPendingByCriteria(PartPendingSearchCriteria criteria, Pageable pageable) {
        if (criteria == null) return partPendingRepository.findAll(pageable);
        return partPendingRepository.findAll(where(PartPendingSpecifications.partIdEqual(criteria.getPartId()))
                        .and(PartPendingSpecifications.idEqual(criteria.getId()))
                        .and(PartPendingSpecifications.modelsContain(criteria.getModel()))
                        .and(PartPendingSpecifications.startDateBetween(criteria.getStartDate1(), criteria.getStartDate2()))
                        .and(PartPendingSpecifications.orderedDateBetween(criteria.getOrderedDate1(), criteria.getOrderedDate2()))
                        .and(PartPendingSpecifications.producerEqual(criteria.getProducer()))
                        .and(PartPendingSpecifications.typeDeviceEqual(criteria.getTypeDevice()))
                        .and(PartPendingSpecifications.receiverEqual(criteria.getReceiver()))
                        .and(PartPendingSpecifications.managerEqual(criteria.getManager()))
                        .and(PartPendingSpecifications.partNumberEqual(criteria.getPartNumber()))
                        .and(PartPendingSpecifications.typeEqual(criteria.getType()))
                , pageable);
    }

    @Override
    public PartPending savePartPending(PartPending partPending) {
        PartPending existPartPending;
        if (partPending.getId() == null) {
            partPending.setStartDate(new Date());
            if (partPending.getOrderedCount() != 0) {
                partPending.setOrderedDate(new Date());
                partPending.setManager(SecurityUtils.getCurrentUser());
            }
            partPending.setReceiver(SecurityUtils.getCurrentUser());
            partPending.setType(TypePartPending.запас);
        } else {
            existPartPending = partPendingRepository.findOne(partPending.getId());
            if (partPending.getOrderedCount() != 0 && existPartPending.getOrderedCount() == 0) {
                partPending.setOrderedDate(new Date());
                partPending.setManager(SecurityUtils.getCurrentUser());
            }
            if (!Objects.equals(existPartPending.getNeededCount(), partPending.getNeededCount()) &&
                    !Objects.equals(existPartPending.getReceiver().getId(), SecurityUtils.getCurrentUser().getId())) {
                throw new IllegalStateException(ViewUtils
                        .getMessage("exception.IllegalState.notAllowedChangeNeededCountInAlien"));
            }
            if (existPartPending.getOrderedCount() != 0) {
                if (!Objects.equals(partPending.getNeededCount(), existPartPending.getNeededCount())) {
                    throw new IllegalStateException(ViewUtils
                            .getMessage("exception.IllegalState.notAllowedChangeNeededCountIfOrdered"));
                }
                if (!Objects.equals(partPending.getOrderedCount(), existPartPending.getOrderedCount())) {
                    throw new IllegalStateException(ViewUtils
                            .getMessage("exception.IllegalState.notAllowedChangeOrderedCountIfOrdered"));
                }
                if (!Objects.equals(partPending.getPart().getId(), existPartPending.getPart().getId())) {
                    throw new IllegalStateException(ViewUtils
                            .getMessage("exception.IllegalState.notAllowedChangePartIfOrdered"));
                }
            }
        }
        if (partPending.getNeededCount() < 1) {
            throw new IllegalStateException(ViewUtils
                    .getMessage("exception.IllegalState.neededCountIsZero"));
        }
        return partPendingRepository.save(partPending);
    }

    @Override
    public void removePartPending(PartPending partPending) {
        if (partPending.getType() != TypePartPending.запас) {
            throw new IllegalStateException(ViewUtils
                    .getMessage("exception.IllegalState.allowedRemoveOnlyTypeReserve"));
        }
        if (partPending.getOrderedCount() != 0) {
            throw new IllegalStateException(ViewUtils
                    .getMessage("exception.IllegalState.notAllowedRemoveOrderedPart"));
        }
        if (!Objects.equals(partPending.getReceiver().getId(), SecurityUtils.getCurrentUser().getId())) {
            throw new IllegalStateException(ViewUtils
                    .getMessage("exception.IllegalState.notAllowedRemoveAlien"));
        }
        partPendingRepository.delete(partPending);
    }
}
