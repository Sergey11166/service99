package ru.service99;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.service99.domain.Authority;
import ru.service99.domain.CurrentUser;
import ru.service99.domain.Stock;
import ru.service99.domain.User;

import java.util.*;
import java.util.stream.Collectors;

public class SecurityUtils {
    public static User user;

    public static User getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || !authentication.isAuthenticated()) return user;
        User userContext = ((CurrentUser) authentication.getPrincipal()).getUser();
        if (userContext != null) return userContext;
        else return user;
    }

    public static List<GrantedAuthority> createAuthorityList(Collection<? extends Authority> authorities) {
        List<GrantedAuthority> result = new ArrayList<>(authorities.size());
        result.addAll(authorities.stream().map(authority ->
                new SimpleGrantedAuthority(authority.toString())).collect(Collectors.toList()));
        return result;
    }

    public static boolean hasAuthority(Authority authority) {
        User user = getCurrentUser();
        Set<Authority> authoritySet = user.getAuthorities();
        return authoritySet.contains(authority);
    }

    public static boolean hasAllowedStock(Stock stock) {
        User user = getCurrentUser();
        Set<Stock> stocks = new HashSet<>(user.getAllowedStocks());
        for (Stock st : stocks) {
            if (Objects.equals(st.getId(), stock.getId())) return true;
        }
        return false;
    }
}
