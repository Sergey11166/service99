package ru.service99.domain;

import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Set;

@Entity
@Audited
public class Model extends AbstractEntity {

    @Column(nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(nullable = false)
    private TypeDevice typeDevice;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Producer producer;

    @ManyToMany(mappedBy = "models", fetch = FetchType.EAGER)
    private Set<Part> parts;

    @Column(nullable = false)
    private Boolean isRemoved;

    public Long getId() {
        return super.getId();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TypeDevice getTypeDevice() {
        return typeDevice;
    }

    public void setTypeDevice(TypeDevice typeDevice) {
        this.typeDevice = typeDevice;
    }

    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }

    public Set<Part> getParts() {
        return parts;
    }

    public void setParts(Set<Part> parts) {
        this.parts = parts;
    }

    public Boolean getIsRemoved() {
        return isRemoved;
    }

    public void setIsRemoved(Boolean isRemoved) {
        this.isRemoved = isRemoved;
    }

    @Override
    public String toString() {
        return "Model{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                ", typeDevice=" + typeDevice +
                ", producer=" + producer +
                //", parts=" + (parts != null ? Arrays.toString(parts.toArray()) : "null") +
                ", isRemoved-'" + isRemoved +
                '}';
    }
}
