package ru.service99.domain;

import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;

@Audited
@MappedSuperclass
public class AbstractDocument extends AbstractEntity {

    @ManyToOne()
    @JoinColumn(nullable = false)
    private User author;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    @Column(nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private StatusDocument status;

    private String comment;

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public StatusDocument getStatus() {
        return status;
    }

    public void setStatus(StatusDocument status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
