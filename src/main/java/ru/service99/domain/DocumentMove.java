package ru.service99.domain;

import org.hibernate.envers.Audited;
import ru.service99.SecurityUtils;
import ru.service99.web.ui.ViewUtils;

import javax.persistence.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Audited
public class DocumentMove extends AbstractDocument {

    @ManyToMany(mappedBy = "documentMoves", fetch = FetchType.EAGER)
    private Set<PartInstance> partInstances;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Stock stockFrom;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Stock stockTo;

    public void validate(DocumentMove existDocument, Set<PartInstance> movingExistInstances) {
        Set<PartInstance> newInstances = new HashSet<>(getPartInstances());
        for (PartInstance exIns : movingExistInstances) {
            Set<DocumentMove> exDocs = new HashSet<>(exIns.getDocumentMoves());
            for (DocumentMove doc : exDocs) {
                if (doc.getStatus() != StatusDocument.Завершено) {
                    if (Objects.equals(doc.getId(), getId())) continue;
                    throw new IllegalStateException(ViewUtils
                            .getMessage("exception.IllegalState.partInReserve") + doc.getId());
                }
            }
            if (exIns.getDocumentOut() != null) {
                throw new IllegalStateException(ViewUtils
                        .getMessage("exception.IllegalState.partSpendingAnotherUser") + "\n" +
                        ViewUtils.getMessage("string.spendingDocument") + exIns.getDocumentOut().getId() + "\n" +
                        ViewUtils.getMessage("string.part") + " - " + exIns.getPart().getPartNumber() + "\n" +
                        ViewUtils.getMessage("string.author") + " - " + exIns.getDocumentOut().getAuthor().getName());
            }
        }

        if (getStockFrom() == null || getStockTo() == null) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.stockIsNull"));
        }
        if (Objects.equals(getStockTo().getId(), getStockFrom().getId())) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.notAllowedSameStocks"));
        }
        /*if (newInstances.isEmpty()) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.partsIsEmpty"));
        }*/
        if (getStatus() == StatusDocument.Передан && !SecurityUtils.hasAllowedStock(getStockFrom())) {
            throw new IllegalStateException(ViewUtils
                    .getMessage("exception.IllegalState.notAllowedChangeStatusPassedFromAlien"));
        }
        if (getStatus() == StatusDocument.Завершено && !SecurityUtils.hasAllowedStock(getStockTo())) {
            throw new IllegalStateException(ViewUtils
                    .getMessage("exception.IllegalState.notAllowedChangeStatusFinishedToAlien"));
        }
        if ((getStatus() == StatusDocument.Новый || getStatus() == null)
                && !SecurityUtils.hasAllowedStock(getStockTo())
                && !SecurityUtils.hasAllowedStock(getStockFrom())) {
            throw new IllegalStateException(ViewUtils
                    .getMessage("exception.IllegalState.notAllowedMovePartsFromAlienToAlienStocks"));
        }
        if (existDocument != null) {
            Set<PartInstance> existInstances = new HashSet<>(existDocument.getPartInstances());
            boolean isChangedInstances = false;
            for (PartInstance existInstance : existInstances) {
                boolean found = false;
                for (PartInstance i : newInstances) {
                    if (Objects.equals(existInstance.getId(), i.getId())) found = true;
                }
                if (!found) {
                    isChangedInstances = true;
                    break;
                }
            }
            for (PartInstance instance : newInstances) {
                boolean found = false;
                for (PartInstance i : existInstances) {
                    if (Objects.equals(instance.getId(), i.getId())) found = true;
                }
                if (!found) {
                    isChangedInstances = true;
                    break;
                }
            }
            /*if (existDocument.getStatus() == StatusDocument.Передан && isChangedInstances) {
                throw new IllegalStateException(ViewUtils
                        .getMessage("exception.IllegalState.notAllowedChangePartsPassed"));
            }*/
            if (existDocument.getStatus() == StatusDocument.Завершено) {
                throw new IllegalStateException(ViewUtils
                        .getMessage("exception.IllegalState.notAllowedAfterFinished"));
            }
            if (!Objects.equals(getStockFrom().getId(), existDocument.getStockFrom().getId()) ||
                    !Objects.equals(getStockTo().getId(), existDocument.getStockTo().getId())) {
                throw new IllegalStateException(ViewUtils
                        .getMessage("exception.IllegalState.notAllowedChangeStock"));
            }
            if (existDocument.getStatus().ordinal() > getStatus().ordinal()) {
                throw new IllegalStateException(ViewUtils
                        .getMessage("exception.IllegalState.notAllowedBackChangeStatus"));
            }
            if (existDocument.getStatus() == StatusDocument.Новый && getStatus() == StatusDocument.Завершено) {
                throw new IllegalStateException(ViewUtils
                        .getMessage("exception.IllegalState.notAllowedChangeStatusNewToFinish"));
            }
        }
    }

    public Set<PartInstance> getPartInstances() {
        return partInstances;
    }

    public void setPartInstances(Set<PartInstance> partInstances) {
        this.partInstances = partInstances;
    }

    public Stock getStockFrom() {
        return stockFrom;
    }

    public void setStockFrom(Stock stockFrom) {
        this.stockFrom = stockFrom;
    }

    public Stock getStockTo() {
        return stockTo;
    }

    public void setStockTo(Stock stockTo) {
        this.stockTo = stockTo;
    }

    @Override
    public String toString() {
        return "DocumentMove{" +
                "id=" + getId() +
                ", date=" + getDate() +
                ", status=" + getStatus() +
                ", comment='" + getComment() + '\'' +
                ", partInstances=" + Arrays.toString(partInstances.toArray()) +
                ", countPartInstances=" + partInstances.size() +
                ", stockFrom=" + stockFrom +
                ", stockTo=" + stockTo +
                '}';
    }
}
