package ru.service99.domain;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import ru.service99.SecurityUtils;
import ru.service99.web.ui.ViewUtils;

import javax.persistence.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Audited
public class RepairOrder extends AbstractOrder {

    @ManyToOne
    @JoinColumn(nullable = false)
    private Model model;

    @ManyToOne
    private User master;

    @Column(nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private StatusRepairOrder status;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Place currentPlace;

    @ManyToMany(mappedBy = "repairOrders", fetch = FetchType.EAGER)
    private Set<DocumentMoveDevices> documentMoveDevices;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "repairOrder", fetch = FetchType.EAGER)
    @NotAudited
    private Set<PartPending> partsPending = new HashSet<>();

    private Float startCost;

    private Float endCost;

    private String visibleDamage;

    private String equipment;

    private String imei;

    private String newImei;

    private String sn;

    private String newSn;

    @Column(nullable = false)
    private String statedDefect;

    private String discoveredDefect;

    private String worksDone;

    private String commentForCustomer;

    private String masterComment;

    private String partsPendingString;

    public void validate(RepairOrder existOrder) {
        if (getModel() == null) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.modelIsNull"));
        }
        if (getCustomer() == null) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.customerIsNull"));
        }
        if (getStatedDefect().isEmpty()) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.defectIsEmpty"));
        }
        if (getImei() == null && getSn() == null) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.imeiAndSnIsEmpty"));
        }
        if (getStatus() != StatusRepairOrder.Новый && getStatus() != StatusRepairOrder.Отменён && getMaster() == null) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.masterIsNull"));
        }
        if (getDocument() != null && getDocument().getId() == null) {
            Set<PartInstance> instances = new HashSet<>(getDocument().getPartInstances());
            for (PartInstance instance : instances) {
                Stock s = instance.getStock();
                if (!SecurityUtils.hasAllowedStock(s)) {
                    throw new IllegalStateException(ViewUtils
                            .getMessage("exception.IllegalState.notAllowedSpendingFromThisStock") + " " + s.getName());
                }
            }
        }

        if (existOrder != null) {
            if (existOrder.getStatus() == StatusRepairOrder.Выдан
                    && getStatus() != StatusRepairOrder.Выдан) {
                throw new IllegalStateException(ViewUtils
                        .getMessage("exception.IllegalState.notAllowedChangeStatusAfterDelivered"));
            }
            DocumentOut existDocument = existOrder.getDocument();
            DocumentOut document = getDocument();
            Set<PartInstance> existInstances = new HashSet<>();
            Set<PartInstance> newInstances = new HashSet<>();
            if (existDocument != null) existInstances.addAll(existDocument.getPartInstances());
            if (document != null) newInstances.addAll(document.getPartInstances());
            boolean isChangedInstances = false;
            for (PartInstance existInstance : existInstances) {
                boolean found = false;
                for (PartInstance i : newInstances) {
                    if (Objects.equals(existInstance.getId(), i.getId())) found = true;
                }
                if (!found) {
                    isChangedInstances = true;
                    break;
                }
            }
            for (PartInstance instance : newInstances) {
                boolean found = false;
                for (PartInstance i : existInstances) {
                    if (Objects.equals(instance.getId(), i.getId())) found = true;
                }
                if (!found) {
                    isChangedInstances = true;
                    break;
                }
            }
            if (existOrder.getStatus() == StatusRepairOrder.Выдан && isChangedInstances)
                throw new IllegalStateException(ViewUtils
                        .getMessage("exception.IllegalState.notAllowedChangePartsDelivered"));
            if (!Objects.equals(existOrder.getMaster(), getMaster())
                    && existOrder.getStatus() == StatusRepairOrder.Выдан)
                throw new IllegalStateException(ViewUtils
                        .getMessage("exception.IllegalState.notAllowedChangeMasterDelivered"));
            if (!Objects.equals(existOrder.getEndCost(), getEndCost())
                    && existOrder.getStatus() == StatusRepairOrder.Выдан)
                throw new IllegalStateException(ViewUtils
                        .getMessage("exception.IllegalState.notAllowedChangeCostDelivered"));
            if (existOrder.getStatus() != StatusRepairOrder.Выдан
                    && getStatus() == StatusRepairOrder.Выдан) {
                Set<DocumentMoveDevices> documentMoveDevices = new HashSet<>(existOrder.getDocumentMoveDevices());
                if (!documentMoveDevices.isEmpty()) {
                    for (DocumentMoveDevices doc : documentMoveDevices) {
                        if (doc.getStatus() != StatusDocument.Завершено) {
                            throw new IllegalStateException(ViewUtils
                                    .getMessage("exception.IllegalState.notAllowedDeliverIfMovingRepair"));
                        }
                    }
                }
            }
        }
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public User getMaster() {
        return master;
    }

    public void setMaster(User master) {
        this.master = master;
    }

    public StatusRepairOrder getStatus() {
        return status;
    }

    public void setStatus(StatusRepairOrder status) {
        this.status = status;
    }

    public Place getCurrentPlace() {
        return currentPlace;
    }

    public void setCurrentPlace(Place currentPlace) {
        this.currentPlace = currentPlace;
    }

    public Set<DocumentMoveDevices> getDocumentMoveDevices() {
        return documentMoveDevices;
    }

    public void setDocumentMoveDevices(Set<DocumentMoveDevices> documentMoveDevices) {
        this.documentMoveDevices = documentMoveDevices;
    }

    public Float getStartCost() {
        return startCost;
    }

    public void setStartCost(Float startCost) {
        this.startCost = startCost;
    }

    public Float getEndCost() {
        return endCost;
    }

    public void setEndCost(Float endCost) {
        this.endCost = endCost;
    }

    public String getVisibleDamage() {
        return visibleDamage;
    }

    public void setVisibleDamage(String visibleDamage) {
        this.visibleDamage = visibleDamage;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String IMEI) {
        this.imei = IMEI;
    }

    public String getNewImei() {
        return newImei;
    }

    public void setNewImei(String newImei) {
        this.newImei = newImei;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getNewSn() {
        return newSn;
    }

    public void setNewSn(String newSn) {
        this.newSn = newSn;
    }

    public String getStatedDefect() {
        return statedDefect;
    }

    public void setStatedDefect(String statedDefect) {
        this.statedDefect = statedDefect;
    }

    public String getDiscoveredDefect() {
        return discoveredDefect;
    }

    public void setDiscoveredDefect(String discoveredDefect) {
        this.discoveredDefect = discoveredDefect;
    }

    public String getWorksDone() {
        return worksDone;
    }

    public void setWorksDone(String worksDone) {
        this.worksDone = worksDone;
    }

    public String getCommentForCustomer() {
        return commentForCustomer;
    }

    public void setCommentForCustomer(String commentForCustomer) {
        this.commentForCustomer = commentForCustomer;
    }

    public String getMasterComment() {
        return masterComment;
    }

    public void setMasterComment(String masterComment) {
        this.masterComment = masterComment;
    }

    public Set<PartPending> getPartsPending() {
        return partsPending;
    }

    public void setPartsPending(Set<PartPending> partsPending) {
        this.partsPending = partsPending;
        this.partsPendingString = PartPending.partsPendingToString(partsPending);
    }

    public String getPartsPendingString() {
        return partsPendingString;
    }

    @Override
    public String toString() {
        return "RepairOrder{" +
                "id=" + getId() +
                ", startDate=" + getStartDate() +
                ", endDate=" + getEndDate() +
                ", receiverId=" + (getReceiver() != null ? getReceiver().getId() : "null") +
                ", customerId=" + (getCustomer() != null ? getCustomer().getId() : "null") +
                ", receiverComment='" + getReceiverComment() + '\'' +
                ", firmId=" + (getFirm() != null ? getFirm().getId() : "null") +
                ", modelId=" + (model != null ? model.getId() : "null") +
                ", masterId=" + (master != null ? master.getId() : "null") +
                ", status=" + status +
                ", placeReceiveId=" + (getPlaceReceive() != null ? getPlaceReceive().getId() : "null") +
                ", currentPlaceId=" + (getCurrentPlace() != null ? getCurrentPlace().getId() : "null") +
                ", startCost=" + startCost +
                ", endCost=" + endCost +
                ", visibleDamage='" + visibleDamage + '\'' +
                ", equipment='" + equipment + '\'' +
                ", imei='" + imei + '\'' +
                ", newImei='" + newImei + '\'' +
                ", sn='" + sn + '\'' +
                ", newSn='" + newSn + '\'' +
                ", statedDefect='" + statedDefect + '\'' +
                ", discoveredDefect='" + discoveredDefect + '\'' +
                ", worksDone='" + worksDone + '\'' +
                ", commentForCustomer='" + commentForCustomer + '\'' +
                ", masterComment='" + masterComment + '\'' +
                ", partsPending=" + Arrays.toString(partsPending.toArray()) +
                '}';
    }
}
