package ru.service99.domain;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.util.Date;

@Audited
@MappedSuperclass
public class AbstractOrder extends AbstractEntity {

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    @ManyToOne
    @JoinColumn(nullable = false)
    private User receiver;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Customer customer;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Place placeReceive;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Firm firm;

    @ManyToOne
    @NotAudited
    private DocumentOut document;

    private String receiverComment;

    private String partsString;

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Place getPlaceReceive() {
        return placeReceive;
    }

    public void setPlaceReceive(Place placeReceive) {
        this.placeReceive = placeReceive;
    }

    public Firm getFirm() {
        return firm;
    }

    public void setFirm(Firm firm) {
        this.firm = firm;
    }

    public DocumentOut getDocument() {
        return document;
    }

    public void setDocument(DocumentOut document) {
        this.document = document;
        this.partsString = document == null ? null : PartInstance.instancesToString(document.getPartInstances());
    }

    public String getReceiverComment() {
        return receiverComment;
    }

    public void setReceiverComment(String receiverComment) {
        this.receiverComment = receiverComment;
    }

    public String getPartsString() {
        return partsString;
    }
}
