package ru.service99.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(DocumentMoveDevices.class)
public class DocumentMoveDevices_ {
    public static volatile SingularAttribute<DocumentMoveDevices, Place> placeFrom;
    public static volatile SingularAttribute<DocumentMoveDevices, Place> placeTo;
}
