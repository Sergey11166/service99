package ru.service99.domain;

public enum StatusRepairOrder {
    Новый,
    Согласование,
    В_ремонте,
    Ож_детали,
    Отремонтирован,
    Готов_к_выдаче,
    Выдан,
    Отменён
}
