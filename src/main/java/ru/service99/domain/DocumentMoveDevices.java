package ru.service99.domain;

import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Set;

@Entity
@Audited
public class DocumentMoveDevices extends AbstractDocument {

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<RepairOrder> repairOrders;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Place placeFrom;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Place placeTo;

    public Set<RepairOrder> getRepairOrders() {
        return repairOrders;
    }

    public void setRepairOrders(Set<RepairOrder> repairOrders) {
        this.repairOrders = repairOrders;
    }

    public Place getPlaceFrom() {
        return placeFrom;
    }

    public void setPlaceFrom(Place placeFrom) {
        this.placeFrom = placeFrom;
    }

    public Place getPlaceTo() {
        return placeTo;
    }

    public void setPlaceTo(Place placeTo) {
        this.placeTo = placeTo;
    }

    @Override
    public String toString() {
        return "DocumentMoveDevices{" +
                "id=" + getId() +
                ", date=" + getDate() +
                ", status=" + getStatus() +
                ", comment='" + getComment() + '\'' +
                ", repairOrders=" + Arrays.toString(repairOrders.toArray()) +
                ", placeFrom=" + placeFrom +
                ", placeTo=" + placeTo +
                '}';
    }
}
