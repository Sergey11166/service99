package ru.service99.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(DocumentIn.class)
public class DocumentIn_ {
    public static volatile SingularAttribute<DocumentIn, Stock> stockTo;
}
