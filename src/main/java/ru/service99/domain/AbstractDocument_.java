package ru.service99.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

@StaticMetamodel(AbstractDocument.class)
public class AbstractDocument_ {
    public static volatile SingularAttribute<AbstractDocument, User> author;
    public static volatile SingularAttribute<AbstractDocument, Date> date;
    public static volatile SingularAttribute<AbstractDocument, StatusDocument> status;
}
