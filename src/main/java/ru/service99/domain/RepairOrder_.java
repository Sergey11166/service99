package ru.service99.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(RepairOrder.class)
public class RepairOrder_ {
    public static volatile SingularAttribute<RepairOrder, User> master;
    public static volatile SingularAttribute<RepairOrder, Model> model;
    public static volatile SingularAttribute<RepairOrder, StatusRepairOrder> status;
    public static volatile SingularAttribute<RepairOrder, Place> currentPlace;
    public static volatile SingularAttribute<RepairOrder, String> imei;
    public static volatile SingularAttribute<RepairOrder, String> newImei;
    public static volatile SingularAttribute<RepairOrder, String> sn;
    public static volatile SingularAttribute<RepairOrder, String> newSn;
}
