package ru.service99.domain;

import org.springframework.security.core.GrantedAuthority;
import ru.service99.SecurityUtils;

import java.util.Collection;

public class CurrentUser extends org.springframework.security.core.userdetails.User {

    private User user;

    public CurrentUser(User user) {
        super(user.getLogin(), user.getPassword(), SecurityUtils.createAuthorityList(user.getAuthorities()));
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public Long getId() {
        return user.getId();
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return SecurityUtils.createAuthorityList(user.getAuthorities());
    }

    @Override
    public String toString() {
        return "CurrentUser{" +
                "user=" + user +
                "} " + super.toString();
    }
}
