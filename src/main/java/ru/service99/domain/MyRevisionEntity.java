package ru.service99.domain;

import org.hibernate.envers.DefaultTrackingModifiedEntitiesRevisionEntity;
import org.hibernate.envers.RevisionEntity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Arrays;

@Entity
@RevisionEntity(MyRevisionListener.class)
public class MyRevisionEntity
        extends
        //DefaultRevisionEntity {
        DefaultTrackingModifiedEntitiesRevisionEntity {

    @ManyToOne
    @JoinColumn()
    private User auditor;

    public User getAuditor() {
        return auditor;
    }

    public void setAuditor(User auditor) {
        this.auditor = auditor;
    }

    @Override
    public String toString() {
        return "MyRevisionEntity{" +
                "id=" + getId() +
                ", auditor=" + auditor +
                ", date=" + getRevisionDate() +
                ", modifiedEntityNames=" + Arrays.toString(getModifiedEntityNames().toArray()) +
                '}';
    }
}
