package ru.service99.domain;

import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Part.class)
public class Part_ {
    public static volatile SingularAttribute<Part, String> name;
    public static volatile SingularAttribute<Part, String> partNumber;
    public static volatile SetAttribute<Part, Model> models;
    public static volatile SingularAttribute<Part, Boolean> isRemoved;
}
