package ru.service99.domain;

import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Set;

@Entity
@Audited
public class DocumentIn extends AbstractDocument {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "documentIn", fetch = FetchType.EAGER)
    private Set<PartInstance> partInstances;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Stock stockTo;

    public Set<PartInstance> getPartInstances() {
        return partInstances;
    }

    public void setPartInstances(Set<PartInstance> partInstances) {
        this.partInstances = partInstances;
    }

    public Stock getStockTo() {
        return stockTo;
    }

    public void setStockTo(Stock stockTo) {
        this.stockTo = stockTo;
    }

    @Override
    public String toString() {
        return "DocumentIn{" +
                "id=" + getId() +
                ", author=" + getAuthor() +
                ", date=" + getDate() +
                ", status=" + getStatus() +
                ", stockTo=" + getStockTo() +
                ", comment='" + getComment() + '\'' +
                //", partInstances=" + Arrays.toString(partInstances.toArray()) +
                ", countPartInstances=" + partInstances.size() +
                '}';
    }
}
