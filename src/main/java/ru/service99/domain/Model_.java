package ru.service99.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Model.class)
public class Model_ {
    public static volatile SingularAttribute<Model, String> name;
    public static volatile SingularAttribute<Model, Producer> producer;
    public static volatile SingularAttribute<Model, TypeDevice> typeDevice;
}
