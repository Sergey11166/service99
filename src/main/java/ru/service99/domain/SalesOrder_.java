package ru.service99.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(SalesOrder.class)
public class SalesOrder_ {
    public static volatile SingularAttribute<SalesOrder, User> manager;
    public static volatile SingularAttribute<SalesOrder, StatusSalesOrder> status;
    public static volatile SingularAttribute<SalesOrder, TypeSale> type;
}