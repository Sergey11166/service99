package ru.service99.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

@StaticMetamodel(PartPending.class)
public class PartPending_ {
    public static volatile SingularAttribute<PartPending, Part> part;
    public static volatile SingularAttribute<PartPending, Date> startDate;
    public static volatile SingularAttribute<PartPending, Date> orderedDate;
    public static volatile SingularAttribute<PartPending, TypePartPending> type;
    public static volatile SingularAttribute<PartPending, User> receiver;
    public static volatile SingularAttribute<PartPending, User> manager;
}
