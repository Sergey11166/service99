package ru.service99.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(PartInstance.class)
public class PartInstance_ {
    public static volatile SingularAttribute<PartInstance, Part> part;
    public static volatile SingularAttribute<PartInstance, Stock> stock;
    public static volatile SingularAttribute<PartInstance, Float> inPrice;
    public static volatile SingularAttribute<PartInstance, Float> optPrice;
    public static volatile SingularAttribute<PartInstance, Float> outPrice;
    public static volatile SingularAttribute<PartInstance, DocumentIn> documentIn;
    public static volatile SingularAttribute<PartInstance, DocumentOut> documentOut;
}
