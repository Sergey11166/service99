package ru.service99.domain;

import org.hibernate.envers.Audited;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity
@Audited
public class DocumentOut extends AbstractDocument {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "documentOut", fetch = FetchType.EAGER)
    private Set<PartInstance> partInstances;

    public Set<PartInstance> getPartInstances() {
        return partInstances;
    }

    public void setPartInstances(Set<PartInstance> partInstances) {
        this.partInstances = partInstances;
    }

    public Float getPartsCostOut() {
        if (partInstances == null) return 0F;
        Float result = 0F;
        for (PartInstance instance : partInstances) {
            result += instance.getOutPrice();
        }
        return result;
    }

    public Float getPartsCostOpt() {
        if (partInstances == null) return 0F;
        Float result = 0F;
        for (PartInstance instance : partInstances) {
            result += instance.getOptPrice();
        }
        return result;
    }

    public Float getPartsCostIn() {
        if (partInstances == null) return 0F;
        Float result = 0F;
        for (PartInstance instance : partInstances) {
            result += instance.getInPrice();
        }
        return result;
    }

    @Override
    public String toString() {
        return "DocumentOut{" +
                "id=" + getId() +
                ", date=" + getDate() +
                ", status=" + getStatus() +
                ", comment='" + getComment() + '\'' +
                //", partInstances=" + Arrays.toString(partInstances.toArray()) +
                ", countPartInstances=" + partInstances.size() +
                '}';
    }
}
