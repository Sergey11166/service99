package ru.service99.domain;

public enum StatusSalesOrder {
    Новый,
    Ож_детали,
    Готов_к_выдаче,
    Выдан,
    Отменён
}
