package ru.service99.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(DocumentMove.class)
public class DocumentMove_ {
    public static volatile SingularAttribute<DocumentMove, Stock> stockFrom;
    public static volatile SingularAttribute<DocumentMove, Stock> stockTo;
}
