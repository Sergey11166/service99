package ru.service99.domain;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import ru.service99.SecurityUtils;
import ru.service99.web.ui.ViewUtils;

import javax.persistence.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Audited
public class SalesOrder extends AbstractOrder {

    @ManyToOne()
    private User manager;

    @Column(nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private StatusSalesOrder status;

    @Column(nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private TypeSale type;

    private String managerComment;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "saleOrder", fetch = FetchType.EAGER)
    @NotAudited
    private Set<PartPending> partsPending = new HashSet<>();

    private String partsPendingString;

    public Float getPartsPendingCostOut() {
        if (partsPending == null) return 0F;
        Float result = 0F;
        for (PartPending partPending : partsPending) {
            result += partPending.getPart().getLastOutPrice();
        }
        return result;
    }

    public Float getPartsPendingCostOpt() {
        if (partsPending == null) return 0F;
        Float result = 0F;
        for (PartPending partPending : partsPending) {
            result += partPending.getPart().getLastOptPrice();
        }
        return result;
    }

    public Float getPartsPendingCostIn() {
        if (partsPending == null) return 0F;
        Float result = 0F;
        for (PartPending partPending : partsPending) {
            result += partPending.getPart().getLastInPrice();
        }
        return result;
    }

    public void validate(SalesOrder existOrder) {
        if (getCustomer() == null) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.customerIsNull"));
        }
        if (getStatus() != StatusSalesOrder.Новый && getStatus() != StatusSalesOrder.Отменён && getManager() == null) {
            throw new IllegalStateException(ViewUtils.getMessage("exception.IllegalState.managerIsNull"));
        }
        if (getDocument() != null && getDocument().getId() == null) {
            Set<PartInstance> instances = new HashSet<>(getDocument().getPartInstances());
            for (PartInstance instance : instances) {
                Stock s = instance.getStock();
                if (!SecurityUtils.hasAllowedStock(s)) {
                    throw new IllegalStateException(ViewUtils
                            .getMessage("exception.IllegalState.notAllowedSpendingFromThisStock") + " " + s.getName());
                }
            }
        }

        if (existOrder != null) {
            if (existOrder.getStatus() == StatusSalesOrder.Выдан
                    && getStatus() != StatusSalesOrder.Выдан) {
                throw new IllegalStateException(ViewUtils
                        .getMessage("exception.IllegalState.notAllowedChangeStatusAfterDelivered"));
            }
            DocumentOut existDocument = existOrder.getDocument();
            DocumentOut document = getDocument();
            Set<PartInstance> existInstances = new HashSet<>();
            Set<PartInstance> newInstances = new HashSet<>();
            if (existDocument != null) existInstances.addAll(existDocument.getPartInstances());
            if (document != null) newInstances.addAll(document.getPartInstances());
            boolean isChangedInstances = false;
            for (PartInstance existInstance : existInstances) {
                boolean found = false;
                for (PartInstance i : newInstances) {
                    if (Objects.equals(existInstance.getId(), i.getId())) found = true;
                }
                if (!found) {
                    isChangedInstances = true;
                    break;
                }
            }
            for (PartInstance instance : newInstances) {
                boolean found = false;
                for (PartInstance i : existInstances) {
                    if (Objects.equals(instance.getId(), i.getId())) found = true;
                }
                if (!found) {
                    isChangedInstances = true;
                    break;
                }
            }
            if (existOrder.getStatus() == StatusSalesOrder.Выдан && isChangedInstances)
                throw new IllegalStateException(ViewUtils
                        .getMessage("exception.IllegalState.notAllowedChangePartsDelivered"));
            if (!Objects.equals(existOrder.getManager(), getManager())
                    && existOrder.getStatus() == StatusSalesOrder.Выдан)
                throw new IllegalStateException(ViewUtils
                        .getMessage("exception.IllegalState.notAllowedChangeManagerDelivered"));
        }
    }

    public User getManager() {
        return manager;
    }

    public void setManager(User manager) {
        this.manager = manager;
    }

    public StatusSalesOrder getStatus() {
        return status;
    }

    public void setStatus(StatusSalesOrder status) {
        this.status = status;
    }

    public TypeSale getType() {
        return type;
    }

    public void setType(TypeSale type) {
        this.type = type;
    }

    public Set<PartPending> getPartsPending() {
        return partsPending;
    }

    public void setPartsPending(Set<PartPending> partsPending) {
        this.partsPending = partsPending;
        this.partsPendingString = PartPending.partsPendingToString(partsPending);
    }

    public String getManagerComment() {
        return managerComment;
    }

    public void setManagerComment(String managerComment) {
        this.managerComment = managerComment;
    }

    public String getPartsPendingString() {
        return partsPendingString;
    }

    @Override
    public String toString() {
        return "SalesOrder{" +
                "id=" + getId() +
                ", startDate=" + getStartDate() +
                ", endDate=" + getEndDate() +
                ", receiverId=" + (getReceiver() != null ? getReceiver().getId() : "null") +
                ", firmId=" + (getFirm() != null ? getFirm().getId() : "null") +
                ", receiverComment='" + getReceiverComment() + '\'' +
                ", managerId=" + (manager != null ? manager.getId() : "null") +
                ", customerId=" + getCustomer().getId() +
                ", placeReceiveId=" + getPlaceReceive().getId() +
                ", status=" + status +
                ", typeSale=" + type +
                ", partsPending=" + Arrays.toString(partsPending.toArray()) +
                ", documentId=" + (getDocument() != null ? getDocument().getId() : "null") +
                '}';
    }
}
