package ru.service99.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import java.util.Set;

@Entity
@Audited
public class Part extends AbstractEntity {

    @Column(nullable = false)
    private String name;

    @Column(nullable = false, updatable = false)
    private String partNumber;

    /*@Column(nullable = false)
    private Float outPrice = 0f;

    @Column(nullable = false)
    private Float optPrice = 0f;

    @Column(nullable = false)
    private Float inPrice = 0f;*/

    @Column(nullable = false)
    private Float lastOutPrice = 0f;

    @Column(nullable = false)
    private Float lastOptPrice = 0f;

    @Column(nullable = false)
    private Float lastInPrice = 0f;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Model> models;

    @Column(nullable = false)
    private String comment = "";

    @Column(nullable = false)
    private Boolean isRemoved;

    public Long getId() {
        return super.getId();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    /*public Float getOutPrice() {
        return outPrice;
    }
    public void setOutPrice(Float outPrice) {
        this.outPrice = outPrice;
    }

    public Float getOptPrice() {
        return optPrice;
    }
    public void setOptPrice(Float optPrice) {
        this.optPrice = optPrice;
    }

    public Float getInPrice() {
        return inPrice;
    }
    public void setInPrice(Float inPrice) {
        this.inPrice = inPrice;
    }*/

    public Float getLastOutPrice() {
        return lastOutPrice;
    }

    public void setLastOutPrice(Float lastOutPrice) {
        this.lastOutPrice = lastOutPrice;
    }

    public Float getLastOptPrice() {
        return lastOptPrice;
    }

    public void setLastOptPrice(Float lastOptPrice) {
        this.lastOptPrice = lastOptPrice;
    }

    public Float getLastInPrice() {
        return lastInPrice;
    }

    public void setLastInPrice(Float lastInPrice) {
        this.lastInPrice = lastInPrice;
    }

    public Set<Model> getModels() {
        return models;
    }

    public void setModels(Set<Model> models) {
        this.models = models;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Boolean getIsRemoved() {
        return isRemoved;
    }

    public void setIsRemoved(Boolean isRemoved) {
        this.isRemoved = isRemoved;
    }

    @Override
    public String toString() {
        return "Part{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                ", partNumber='" + partNumber + '\'' +
                ", lastOutPrice=" + lastOutPrice +
                ", lastOptPrice=" + lastOptPrice +
                ", lastInPrice=" + lastInPrice +
                //", models=" + (models != null ? Arrays.toString(models.toArray()) : "null") +
                ", comment='" + comment + '\'' +
                ", isRemoved-'" + isRemoved +
                '}';
    }
}
