package ru.service99.domain;

import org.hibernate.envers.Audited;
import ru.service99.web.model.FreePart;

import javax.persistence.*;
import java.util.*;

@Entity
@Audited
public class PartPending extends AbstractEntity implements Comparable<PartPending> {

    @ManyToOne
    @JoinColumn(nullable = false)
    private Part part;

    private Integer neededCount = 0;

    private Integer orderedCount = 0;

    private Integer receivedCount = 0;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date orderedDate;

    @Column(nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private TypePartPending type;

    @ManyToOne
    @JoinColumn(nullable = false)
    private User receiver;

    @ManyToOne
    private User manager;

    @ManyToOne
    private SalesOrder saleOrder;

    @ManyToOne
    private RepairOrder repairOrder;

    private String comment;

    public static List<PartPending> freePartToPartsPending(Collection<FreePart> freeParts) {
        TreeSet<PartPending> result = new TreeSet<>();
        for (FreePart fp : freeParts) {
            PartPending pp = new PartPending();
            pp.setPart(fp.getPart());
            pp.setNeededCount(fp.getCount());
            result.add(pp);
        }
        return new ArrayList<>(result);
    }

    public static String partsPendingToString(Set<PartPending> partsPending) {
        List<PartPending> partsPendingList = new ArrayList<>(partsPending);
        String[] partNumbers = new String[partsPendingList.size()];
        for (int i = 0; i < partNumbers.length; i++) {
            partNumbers[i] = partsPendingList.get(i).getPart().getPartNumber();
        }
        Arrays.sort(partNumbers);
        return Arrays.toString(partNumbers);
    }

    public Part getPart() {
        return part;
    }

    public void setPart(Part part) {
        this.part = part;
    }

    public Integer getNeededCount() {
        return neededCount;
    }

    public void setNeededCount(Integer needCount) {
        this.neededCount = needCount;
    }

    public Integer getOrderedCount() {
        return orderedCount;
    }

    public void setOrderedCount(Integer orderedCount) {
        this.orderedCount = orderedCount;
    }

    public Integer getReceivedCount() {
        return receivedCount;
    }

    public void setReceivedCount(Integer receivedCount) {
        this.receivedCount = receivedCount;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getOrderedDate() {
        return orderedDate;
    }

    public void setOrderedDate(Date orderedDate) {
        this.orderedDate = orderedDate;
    }

    public TypePartPending getType() {
        return type;
    }

    public void setType(TypePartPending type) {
        this.type = type;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public User getManager() {
        return manager;
    }

    public void setManager(User manager) {
        this.manager = manager;
    }

    public SalesOrder getSaleOrder() {
        return saleOrder;
    }

    public void setSaleOrder(SalesOrder salesOrder) {
        this.saleOrder = salesOrder;
    }

    public RepairOrder getRepairOrder() {
        return repairOrder;
    }

    public void setRepairOrder(RepairOrder repairOrder) {
        this.repairOrder = repairOrder;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "PartPending{" +
                "partId=" + (part != null ? part.getId() : "null") +
                ", neededCount=" + neededCount +
                ", orderedCount=" + orderedCount +
                ", receivedCount=" + receivedCount +
                ", startDate=" + startDate +
                ", orderedDate=" + orderedDate +
                ", type=" + type +
                ", receiverId=" + (receiver != null ? receiver.getId() : "null") +
                ", managerId=" + (manager != null ? manager.getId() : "null") +
                ", saleOrderId=" + (saleOrder != null ? saleOrder.getId() : "null") +
                ", repairOrderId=" + (repairOrder != null ? repairOrder.getId() : "null") +
                ", comment='" + comment + '\'' +
                '}';
    }

    @Override
    public int compareTo(PartPending o) {
        String partName1 = getPart().getName();
        String partName2 = o.getPart().getName();
        if (!partName1.equals(partName2)) return partName1.compareTo(partName2);
        if (getId() == null && o.getId() == null) return 0;
        if (getId() == null) return 1;
        if (o.getId() == null) return -1;
        return getId().compareTo(o.getId());
    }
}
