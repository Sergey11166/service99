package ru.service99.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import java.util.Set;

@Entity
@Audited
public class Stock extends AbstractEntity {

    @Column(nullable = false, unique = true)
    private String name;

    @ManyToMany(mappedBy = "allowedStocks", fetch = FetchType.EAGER)
    private Set<User> users;

    @Column(nullable = false)
    private Boolean isRemoved;

    public Long getId() {
        return super.getId();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Boolean getIsRemoved() {
        return isRemoved;
    }

    public void setIsRemoved(Boolean isRemoved) {
        this.isRemoved = isRemoved;
    }

    @Override
    public String toString() {
        return "Stock{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                //", users=" + Arrays.toString(users.toArray()) +
                ", isRemoved-'" + isRemoved +
                '}';
    }
}
