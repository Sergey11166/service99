package ru.service99.domain;

import org.hibernate.envers.RevisionListener;
import ru.service99.SecurityUtils;

public class MyRevisionListener implements RevisionListener {

    @Override
    public void newRevision(Object revisionEntity) {
        MyRevisionEntity entity = (MyRevisionEntity) revisionEntity;
        entity.setAuditor(SecurityUtils.getCurrentUser());
    }
}
