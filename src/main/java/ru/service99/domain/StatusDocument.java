package ru.service99.domain;

public enum StatusDocument {
    Новый,
    Передан,
    Завершено
}
