package ru.service99.domain;

import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Set;

@Entity
@Audited
public class User extends AbstractEntity {

    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false, unique = true)
    private String login;

    @Column(nullable = false)
    private String password;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Place place;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Firm firm;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Stock> allowedStocks;

    @Column(nullable = false)
    @ElementCollection(targetClass = Authority.class, fetch = FetchType.EAGER)
    private Set<Authority> authorities;

    @Column(nullable = false)
    private Boolean isRemoved;

    public Long getId() {
        return super.getId();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public Firm getFirm() {
        return firm;
    }

    public void setFirm(Firm firm) {
        this.firm = firm;
    }

    public Set<Stock> getAllowedStocks() {
        return allowedStocks;
    }

    public void setAllowedStocks(Set<Stock> allowedStocks) {
        this.allowedStocks = allowedStocks;
    }

    public Set<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }

    public Boolean getIsRemoved() {
        return isRemoved;
    }

    public void setIsRemoved(Boolean isRemoved) {
        this.isRemoved = isRemoved;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", place=" + place +
                ", firm=" + firm +
                ", allowedStocksCount=" + allowedStocks.size() +
                ", authorities=" + Arrays.toString(authorities.toArray()) +
                ", isRemoved=" + isRemoved +
                '}';
    }


    /*@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User other = (User) obj;
        if (getId() == null) {
            if (other.getId() != null)
                return false;
        } else if (!getId().equals(other.getId()))
            return false;
        if (getName() == null) {
            if (other.getName() != null)
                return false;
        } else if (!getName().equals(other.getName()))
            return false;
        return true;
    }*/
}
