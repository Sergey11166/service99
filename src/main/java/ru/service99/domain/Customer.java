package ru.service99.domain;

import org.hibernate.envers.Audited;
import ru.service99.web.ui.ViewUtils;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Audited
public class Customer extends AbstractEntity {

    @Column(nullable = false)
    private String name;

    private String phoneNumber;

    private String address;

    private String comment;

    public String description() {
        return "<ul><li>" +
                ViewUtils.getMessage("field.userName") + ": " + name + "</li><li>" +
                ViewUtils.getMessage("field.phone") + ": " + (phoneNumber != null ? phoneNumber : "") + "</li><li>" +
                ViewUtils.getMessage("field.address") + ": " + (address != null ? address : "") + "</li><li>" +
                ViewUtils.getMessage("field.comment") + ": " + (comment != null ? comment : "") + "</li></ul>";
    }

    public Long getId() {
        return super.getId();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address='" + address + '\'' +
                ", comment='" + comment + '\'' +
                '}';
    }
}
