package ru.service99.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

@StaticMetamodel(AbstractOrder.class)
public class AbstractOrder_ {
    public static volatile SingularAttribute<AbstractOrder, Firm> firm;
    public static volatile SingularAttribute<AbstractOrder, User> receiver;
    public static volatile SingularAttribute<AbstractOrder, Customer> customer;
    public static volatile SingularAttribute<AbstractOrder, Place> placeReceive;
    public static volatile SingularAttribute<AbstractOrder, Date> startDate;
    public static volatile SingularAttribute<AbstractOrder, Date> endDate;
}
