package ru.service99.domain;

import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Entity
@Audited
public class PartInstance extends AbstractEntity {

    @ManyToOne
    private Part part;

    @ManyToOne
    @JoinColumn(nullable = false)
    private DocumentIn documentIn;

    @ManyToOne
    private DocumentOut documentOut;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<DocumentMove> documentMoves;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Stock stock;

    @Column(nullable = false)
    private Float outPrice = 0f;

    @Column(nullable = false)
    private Float optPrice = 0f;

    @Column(nullable = false)
    private Float inPrice = 0f;

    public boolean isReserve() {
        if (documentMoves != null) {
            for (DocumentMove d : documentMoves) {
                if (d.getStatus() != StatusDocument.Завершено && d.getStockFrom() == stock) return true;
            }
        }
        return false;
    }

    public static String instancesToString(Set<PartInstance> instanceSet) {
        List<PartInstance> instanceList = new ArrayList<>(instanceSet);
        String[] partNumbers = new String[instanceList.size()];
        for (int i = 0; i < partNumbers.length; i++) {
            partNumbers[i] = instanceList.get(i).getPart().getPartNumber();
        }
        Arrays.sort(partNumbers);
        return Arrays.toString(partNumbers);
    }

    public Long getId() {
        return super.getId();
    }

    public Part getPart() {
        return part;
    }

    public void setPart(Part part) {
        this.part = part;
    }

    public DocumentIn getDocumentIn() {
        return documentIn;
    }

    public void setDocumentIn(DocumentIn documentIn) {
        this.documentIn = documentIn;
    }

    public DocumentOut getDocumentOut() {
        return documentOut;
    }

    public void setDocumentOut(DocumentOut documentOut) {
        this.documentOut = documentOut;
    }

    public Set<DocumentMove> getDocumentMoves() {
        return documentMoves;
    }

    public void setDocumentMoves(Set<DocumentMove> documentMoves) {
        this.documentMoves = documentMoves;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public Float getOutPrice() {
        return outPrice;
    }

    public void setOutPrice(Float outPrice) {
        this.outPrice = outPrice;
    }

    public Float getOptPrice() {
        return optPrice;
    }

    public void setOptPrice(Float optPrice) {
        this.optPrice = optPrice;
    }

    public Float getInPrice() {
        return inPrice;
    }

    public void setInPrice(Float inPrice) {
        this.inPrice = inPrice;
    }

    @Override
    public String toString() {
        return "PartInstance{" +
                "id=" + getId() +
                ", part=" + part +
                ", outPrice=" + outPrice +
                ", optPrice=" + optPrice +
                ", inPrice=" + inPrice +
                ", documentIn=" + (documentIn != null ? documentIn : "null") +
                ", documentOut=" + (documentOut != null ? documentOut : "null") +
                ", countDocumentMoves=" + (documentMoves != null ? documentMoves.size() : "null") +
                '}';
    }
}
