package ru.service99.service;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.history.Revisions;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.service99.Application;
import ru.service99.SecurityUtils;
import ru.service99.domain.*;
import ru.service99.service.criteria.SalesOrderSearchCriteria;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class SalesOrderServiceIntegrationTest {

    private static final Logger log = LoggerFactory
            .getLogger(SalesOrderServiceIntegrationTest.class);

    @Autowired
    private SalesOrderService salesOrderService;
    @Autowired
    private PartService partService;
    @Autowired
    private UserService userService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private FirmService firmService;
    @Autowired
    private SampleDataGenerator sampleDataGenerator;

    @BeforeClass
    public static void init() {
        log.debug("==================before class=========================");
    }

    @Before
    public void beforeTestCase() {
        log.debug("==================before test case=========================");

        sampleDataGenerator.generateSampleData();
    }

    @After
    public void afterTestCase() {
        log.debug("==================after test case=========================");
        //conferenceRepository.deleteAll();
        // userRepository.deleteAll();
        //em.flush();
    }

    private SalesOrder newSalesOrder() {
        SalesOrder result = new SalesOrder();
        result.setCustomer(customerService.findCustomerById(1));
        result.setReceiverComment("Test");
        return result;
    }

    @Test
    public void saveAndRetrieveSalesOrder() {
        log.debug("Test saveAndRetrieveSalesOrder");

        SalesOrder newOrder = newSalesOrder();
        DocumentOut newDocumentOut = new DocumentOut();
        newDocumentOut.setComment("Test");
        List<PartInstance> allInstances = partService.findFreeInstancesByCriteria(null, null).getContent();
        Set<PartInstance> newInstances = new HashSet<>();
        for (int i = 1; i < 5; i++) {
            newInstances.add(allInstances.get(i - 1));
        }
        newDocumentOut.setPartInstances(newInstances);
        newOrder.setDocument(newDocumentOut);

        assertThat(allInstances.size(), is(14L));
        assertThat(newOrder, is(notNullValue()));
        assertThat(newOrder.getId(), is(nullValue()));
        assertThat(newOrder.getDocument(), is(notNullValue()));
        assertThat(newOrder.getDocument().getPartInstances().size(), is(4));

        log.debug("");
        log.debug("preSave " + newOrder);
        log.debug("preSave " + Arrays.toString(newOrder.getDocument().getPartInstances().toArray()));
        SalesOrder savedOrder = salesOrderService.save(newOrder);
        log.debug("");
        log.debug("postSave " + savedOrder);
        log.debug("postSave " + Arrays.toString(savedOrder.getDocument().getPartInstances().toArray()));

        SalesOrder retrievedOrder = salesOrderService.findById(savedOrder.getId());
        log.debug("");
        log.debug("retrieved " + retrievedOrder);

        allInstances = partService.findFreeInstancesByCriteria(null, null).getContent();

        assertThat(allInstances.size(), is(10L));
        assertThat(retrievedOrder.getId(), is(savedOrder.getId()));
        assertThat(retrievedOrder.getDocument().getPartInstances().size(), is(4));
        assertThat(retrievedOrder.getStatus(), is(StatusSalesOrder.Новый));
        assertThat(retrievedOrder.getEndDate(), is(nullValue()));
    }

    @Test
    public void retrieveRevisionsSalesOrder() {
        log.debug("Test retrieveRevisionsSalesOrder");

        SalesOrder newOrder = newSalesOrder();
        newOrder.setManager(SecurityUtils.getCurrentUser());
        DocumentOut newDocumentOut = new DocumentOut();
        newDocumentOut.setComment("Test");
        List<PartInstance> allInstances = partService.findFreeInstancesByCriteria(null, null).getContent();
        Set<PartInstance> newInstances = new HashSet<>(5);
        for (int i = 1; i < 5; i++) {
            newInstances.add(allInstances.get(i - 1));
        }
        newDocumentOut.setPartInstances(newInstances);
        newOrder.setDocument(newDocumentOut);
        SalesOrder savedOrder = salesOrderService.save(newOrder);

        SalesOrder retrievedOrder = salesOrderService.findById(savedOrder.getId());
        log.debug("");
        log.debug("retrieved " + retrievedOrder);
        log.debug("retrieved.getPartInstances().size()=" + retrievedOrder.getDocument().getPartInstances().size());

        Revisions<Integer, SalesOrder> revisions = salesOrderService.findRevisionsById(retrievedOrder.getId());
        log.debug("");
        log.debug("revisions.getContent().size() = " + revisions.getContent().size());

        assertThat(revisions.getContent().size(), is(1));
        assertThat(revisions.getContent().get(0).getEntity(), is(notNullValue()));
        assertThat(revisions.getContent().get(0).getEntity().getCustomer(), is(notNullValue()));
        assertThat(revisions.getContent().get(0).getEntity().getManager(), is(notNullValue()));
        log.debug("revisions.getContent().get(0).getEntity()=" + revisions.getContent().get(0).getEntity());

        //#################

        retrievedOrder.setStatus(StatusSalesOrder.Готов_к_выдаче);
        salesOrderService.save(retrievedOrder);
        SalesOrder updatedOrder1 = salesOrderService.findById(retrievedOrder.getId());
        log.debug("");
        log.debug("updatedOrder1 = " + updatedOrder1);

        Revisions<Integer, SalesOrder> revisions1 = salesOrderService.findRevisionsById(updatedOrder1.getId());
        log.debug("");
        log.debug("revisions1.getContent().size() = " + revisions1.getContent().size());

        assertThat(updatedOrder1.getStatus(), is(StatusSalesOrder.Готов_к_выдаче));
        assertThat(revisions1.getContent().size(), is(2));
        assertThat(revisions1.getContent().get(1).getEntity(), is(notNullValue()));
        assertThat(revisions1.getContent().get(1).getEntity().getStatus(), is(StatusSalesOrder.Готов_к_выдаче));
        log.debug("");
        log.debug("revisions1.getContent().get(1).getEntity()= " + revisions1.getContent().get(1).getEntity());

        //##############

        SecurityUtils.user = userService.findById(2L);
        updatedOrder1.setManager(SecurityUtils.getCurrentUser());
        salesOrderService.save(updatedOrder1);
        SalesOrder updatedOrder2 = salesOrderService.findById(updatedOrder1.getId());
        log.debug("");
        log.debug("updatedOrder2 = " + updatedOrder2);
        log.debug("retrieved.getPartInstances().size()=" + updatedOrder2.getDocument().getPartInstances().size());

        Revisions<Integer, SalesOrder> revisions2 = salesOrderService.findRevisionsById(updatedOrder2.getId());
        log.debug("");
        log.debug("revisions2.getContent().size() = " + revisions2.getContent().size());

        assertThat(updatedOrder2.getManager().getId(), is(2L));
        assertThat(revisions2.getContent().size(), is(2));
        assertThat(revisions2.getContent().get(1).getEntity(), is(notNullValue()));
        assertThat(revisions2.getContent().get(1).getEntity().getId(),
                is(updatedOrder2.getId()));
        assertThat(revisions2.getContent().get(1).getEntity().getManager().getId(),
                is(updatedOrder2.getManager().getId()));
        assertThat(revisions2.getContent().get(1).getEntity().getReceiver().getId(),
                is(updatedOrder2.getReceiver().getId()));
        log.debug("");
        log.debug("revisions2.getContent().get(2).getEntity()= " + revisions2.getContent().get(1).getEntity());

        MyRevisionEntity revisionEntitySpring = salesOrderService.findRevisionEntityByRevision(
                revisions2.getContent().get(1));
        assertThat(revisionEntitySpring, is(notNullValue()));
        log.debug("");
        log.debug("revisionEntitySpring[revisions2.get(2)]= " + revisionEntitySpring);
    }

    @Test
    public void findSalesOrdersByCriteria() {
        log.debug("Test findSalesOrdersByCriteria");

        SalesOrderSearchCriteria criteria = new SalesOrderSearchCriteria();
        List<SalesOrder> orders = salesOrderService.findByCriteria(criteria, null).getContent();
        assertThat(orders.size(), is(3));
        log.debug("orders1=" + Arrays.toString(orders.toArray()));

        criteria.setFirm(firmService.findById(1));
        orders = salesOrderService.findByCriteria(criteria, null).getContent();
        assertThat(orders.size(), is(2));
        log.debug("orders2=" + Arrays.toString(orders.toArray()));

        criteria.setFirm(null);
        criteria.setStatus(StatusSalesOrder.Готов_к_выдаче);
        orders = salesOrderService.findByCriteria(criteria, null).getContent();
        assertThat(orders.size(), is(1));
        log.debug("orders3=" + Arrays.toString(orders.toArray()));

        criteria.setStatus(null);
        criteria.setReceiver(userService.findById(2));
        orders = salesOrderService.findByCriteria(criteria, null).getContent();
        assertThat(orders.size(), is(1));
        log.debug("orders4=" + Arrays.toString(orders.toArray()));
    }
}