package ru.service99.service;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.history.Revisions;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.service99.Application;
import ru.service99.SecurityUtils;
import ru.service99.domain.*;
import ru.service99.service.criteria.RepairOrderSearchCriteria;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class RepairOrderServiceIntegrationTest {

    private static final Logger log = LoggerFactory
            .getLogger(RepairOrderServiceIntegrationTest.class);

    @Autowired
    private PartService partService;
    @Autowired
    private UserService userService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private FirmService firmService;
    @Autowired
    private ModelService modelService;
    @Autowired
    private RepairOrderService repairOrderService;
    @Autowired
    private SampleDataGenerator sampleDataGenerator;

    @BeforeClass
    public static void init() {
        log.debug("==================before class=========================");

    }

    @Before
    public void beforeTestCase() {
        log.debug("==================before test case=========================");

        sampleDataGenerator.generateSampleData();
    }

    @After
    public void afterTestCase() {
        log.debug("==================after test case=========================");
        //conferenceRepository.deleteAll();
        // userRepository.deleteAll();
        //em.flush();
    }

    private RepairOrder newSampleRepairOrder() {
        RepairOrder result = new RepairOrder();
        result.setModel(modelService.findModelById(1));//iq285
        result.setCustomer(customerService.findCustomerById(1));
        result.setStartCost(1200f);
        result.setVisibleDamage("потерт, царапины");
        result.setImei("01234567889");
        result.setStatedDefect("Разбит дисплей");
        result.setReceiverComment("При приеме не включился, Test");
        return result;
    }

    @Test
    public void saveAndRetrieveRepairOrder() {
        log.debug("Test saveAndRetrieveRepairOrder");

        RepairOrder newOrder = newSampleRepairOrder();
        DocumentOut newDocumentOut = new DocumentOut();
        newDocumentOut.setComment("Test");
        List<PartInstance> allInstances = partService.findFreeInstancesByCriteria(null, null).getContent();
        Set<PartInstance> newInstances = new HashSet<>();
        for (int i = 1; i < 6; i++) {
            newInstances.add(allInstances.get(i - 1));
        }
        newDocumentOut.setPartInstances(newInstances);
        newOrder.setDocument(newDocumentOut);

        assertThat(allInstances.size(), is(14L));
        assertThat(newOrder, is(notNullValue()));
        assertThat(newOrder.getId(), is(nullValue()));
        assertThat(newOrder.getDocument(), is(notNullValue()));
        assertThat(newOrder.getDocument().getPartInstances().size(), is(5));

        log.debug("");
        log.debug("preSave " + newOrder);
        log.debug("preSave " + Arrays.toString(newOrder.getDocument().getPartInstances().toArray()));
        RepairOrder repairOrder = repairOrderService.save(newOrder);
        log.debug("");
        log.debug("postSave " + repairOrder);
        log.debug("postSave " + Arrays.toString(repairOrder.getDocument().getPartInstances().toArray()));

        RepairOrder retrievedOrder = repairOrderService.findById(repairOrder.getId());
        log.debug("");
        log.debug("retrieved " + retrievedOrder);

        allInstances = partService.findFreeInstancesByCriteria(null, null).getContent();

        assertThat(allInstances.size(), is(9L));
        assertThat(retrievedOrder.getId(), is(repairOrder.getId()));
        assertThat(retrievedOrder.getDocument().getPartInstances().size(), is(5));
        assertThat(retrievedOrder.getStatus(), is(StatusRepairOrder.Новый));
        assertThat(retrievedOrder.getEndDate(), is(nullValue()));
    }

    @Test
    public void retrieveRevisionsRepairOrder() {
        log.debug("Test retrieveRevisionsRepairOrder");

        RepairOrder newOrder = newSampleRepairOrder();
        DocumentOut newDocumentOut = new DocumentOut();
        newDocumentOut.setComment("Test");
        List<PartInstance> allInstances = partService.findFreeInstancesByCriteria(null, null).getContent();
        Set<PartInstance> newInstances = new HashSet<>(5);
        for (int i = 1; i < 6; i++) {
            newInstances.add(allInstances.get(i - 1));
        }
        newDocumentOut.setPartInstances(newInstances);
        newOrder.setDocument(newDocumentOut);
        RepairOrder savedOrder = repairOrderService.save(newOrder);

        //##############

        RepairOrder retrievedOrder = repairOrderService.findById(savedOrder.getId());
        log.debug("");
        log.debug("retrieved " + retrievedOrder);
        log.debug("retrieved.getPartInstances().size()=" + retrievedOrder.getDocument().getPartInstances().size());

        Revisions<Integer, RepairOrder> revisions = repairOrderService.findRevisionsById(retrievedOrder.getId());
        log.debug("");
        log.debug("revisions.getContent().size() = " + revisions.getContent().size());

        assertThat(revisions.getContent().size(), is(1));
        assertThat(revisions.getContent().get(0).getEntity(), is(notNullValue()));
        assertThat(revisions.getContent().get(0).getEntity().getCustomer(), is(notNullValue()));
        log.debug("revisions.getContent().get(0).getEntity()=" + revisions.getContent().get(0).getEntity());

        retrievedOrder.setStatus(StatusRepairOrder.В_ремонте);
        retrievedOrder.setMaster(userService.findById(2));
        repairOrderService.save(retrievedOrder);
        RepairOrder updatedOrder1 = repairOrderService.findById(retrievedOrder.getId());
        log.debug("");
        log.debug("updatedOrder1 = " + updatedOrder1);

        Revisions<Integer, RepairOrder> revisions1 = repairOrderService.findRevisionsById(updatedOrder1.getId());
        log.debug("");
        log.debug("revisions1.getContent().size() = " + revisions1.getContent().size());

        assertThat(updatedOrder1.getStatus(), is(StatusRepairOrder.В_ремонте));
        assertThat(revisions1.getContent().size(), is(2));
        assertThat(revisions1.getContent().get(1).getEntity(), is(notNullValue()));
        assertThat(revisions1.getContent().get(1).getEntity().getStatus(), is(StatusRepairOrder.В_ремонте));
        log.debug("");
        log.debug("revisions1.getContent().get(1).getEntity()= " + revisions1.getContent().get(1).getEntity());

        //##############

        SecurityUtils.user = userService.findById(2L);
        updatedOrder1.setMaster(SecurityUtils.getCurrentUser());
        repairOrderService.save(updatedOrder1);
        RepairOrder updatedOrder2 = repairOrderService.findById(updatedOrder1.getId());
        log.debug("");
        log.debug("updatedOrder2 = " + updatedOrder2);
        log.debug("retrieved.getPartInstances().size()=" + updatedOrder2.getDocument().getPartInstances().size());

        Revisions<Integer, RepairOrder> revisions2 = repairOrderService.findRevisionsById(updatedOrder2.getId());
        log.debug("");
        log.debug("revisions2.getContent().size() = " + revisions2.getContent().size());

        assertThat(updatedOrder2.getMaster().getId(), is(2L));
        assertThat(revisions2.getContent().size(), is(2));
        assertThat(revisions2.getContent().get(1).getEntity(), is(notNullValue()));
        assertThat(revisions2.getContent().get(1).getEntity().getId(),
                is(updatedOrder2.getId()));
        assertThat(revisions2.getContent().get(1).getEntity().getMaster().getId(),
                is(updatedOrder2.getMaster().getId()));
        assertThat(revisions2.getContent().get(1).getEntity().getReceiver().getId(),
                is(updatedOrder2.getReceiver().getId()));
        log.debug("");
        log.debug("revisions2.getContent().get(1).getEntity()= " + revisions2.getContent().get(1).getEntity());

        MyRevisionEntity revisionEntitySpring = repairOrderService.findRevisionEntityByRevision(
                revisions2.getContent().get(1));
        assertThat(revisionEntitySpring, is(notNullValue()));
        log.debug("");
        log.debug("revisionEntitySpring[revisions2.get(1)]= " + revisionEntitySpring);
    }

    @Test
    public void findRepairOrdersByCriteria() {
        log.debug("Test findRepairOrdersByCriteria");

        RepairOrderSearchCriteria criteria = new RepairOrderSearchCriteria();
        List<RepairOrder> orders = repairOrderService.findByCriteria(criteria, null).getContent();
        assertThat(orders.size(), is(18));
        log.debug("orders1=" + Arrays.toString(orders.toArray()));

        criteria.setImeiSn("222");
        orders = repairOrderService.findByCriteria(criteria, null).getContent();
        assertThat(orders.size(), is(4));
        log.debug("orders2=" + Arrays.toString(orders.toArray()));

        criteria.setImeiSn(null);
        criteria.setModel(modelService.findModelById(4));
        orders = repairOrderService.findByCriteria(criteria, null).getContent();
        assertThat(orders.size(), is(12));
        log.debug("orders3=" + Arrays.toString(orders.toArray()));

        criteria.setModel(null);
        criteria.setStatus(StatusRepairOrder.В_ремонте);
        orders = repairOrderService.findByCriteria(criteria, null).getContent();
        assertThat(orders.size(), is(1));
        log.debug("orders4=" + Arrays.toString(orders.toArray()));

        criteria.setId(1L);
        criteria.setStatus(null);
        orders = repairOrderService.findByCriteria(criteria, null).getContent();
        assertThat(orders.size(), is(1));
        log.debug("orders5=" + Arrays.toString(orders.toArray()));

        criteria.setId(null);
        criteria.setFirm(firmService.findById(2));
        orders = repairOrderService.findByCriteria(criteria, null).getContent();
        assertThat(orders.size(), is(5));
        log.debug("orders6=" + Arrays.toString(orders.toArray()));

        criteria.setFirm(null);
        criteria.setProducer(modelService.findAllProducers().get(1));
        orders = repairOrderService.findByCriteria(criteria, null).getContent();
        assertThat(orders.size(), is(2));
        log.debug("orders7=" + Arrays.toString(orders.toArray()));
    }
}
