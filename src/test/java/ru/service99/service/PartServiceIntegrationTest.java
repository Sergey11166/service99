package ru.service99.service;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.service99.Application;
import ru.service99.domain.Part;
import ru.service99.domain.PartInstance;
import ru.service99.service.criteria.PartSearchCriteria;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class PartServiceIntegrationTest {

    private static final Logger log = LoggerFactory
            .getLogger(PartServiceIntegrationTest.class);

    @Autowired
    private PartService partService;
    @Autowired
    private ModelService modelService;
    @Autowired
    private SampleDataGenerator sampleDataGenerator;

    @BeforeClass
    public static void init() {
        log.debug("==================before class=========================");

    }

    @Before
    public void beforeTestCase() {
        log.debug("==================before test case=========================");

        sampleDataGenerator.generateSampleData();
    }

    @After
    public void afterTestCase() {
        log.debug("==================after test case=========================");
        //conferenceRepository.deleteAll();
        // userRepository.deleteAll();
        //em.flush();
    }

    @Test
    public void findPartsByCriteria() {
        log.debug("Test findPartsByCriteria");

        List<Part> parts = partService.findPartsByCriteria(null, null).getContent();
        assertThat(parts.size(), is(10));

        PartSearchCriteria criteria = new PartSearchCriteria();
        criteria.setModel(modelService.findModelById(4));
        parts = partService.findPartsByCriteria(criteria, null).getContent();
        assertThat(parts.size(), is(1));

        criteria.setModel(null);
        criteria.setName("6");
        parts = partService.findPartsByCriteria(criteria, null).getContent();
        assertThat(parts.size(), is(3));

        criteria.setPartNumber("000010");
        parts = partService.findPartsByCriteria(criteria, null).getContent();
        assertThat(parts.size(), is(1));

        criteria = new PartSearchCriteria();
        criteria.setTypeDevice(modelService.findAllTypeDevices().get(0));
        parts = partService.findPartsByCriteria(criteria, null).getContent();
        assertThat(parts.size(), is(1));
        log.debug("parts " + Arrays.toString(parts.toArray()));
    }

    @Test
    public void findFreePartInstancesByPartSearchCriteria() {
        log.debug("Test findFreePartInstancesByPartSearchCriteria");

        PartSearchCriteria criteria = new PartSearchCriteria();
        List<PartInstance> instances = partService.findFreeInstancesByCriteria(criteria, null).getContent();
        assertThat(instances.size(), is(14));

        criteria.setName("6");
        instances = partService.findFreeInstancesByCriteria(criteria, null).getContent();
        assertThat(instances.size(), is(3));

        criteria.setName(null);
        criteria.setProducer(modelService.findAllProducers().get(4));
        instances = partService.findFreeInstancesByCriteria(criteria, null).getContent();
        assertThat(instances.size(), is(2));
        log.debug("instances " + Arrays.toString(instances.toArray()));
    }
}
